package com.smart.logistic.annotation;

import java.lang.annotation.Annotation;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.exception.UserNotAuthorizedException;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.IUserService;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.AuthUtils;

public class AuthenticatedArgumentResolver implements HandlerMethodArgumentResolver {

	private static final Logger LOGGER = Logger.getLogger("AuthenticatedArgumentResolver");

	@Autowired
	private IUserService userService;

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.hasParameterAnnotation(Authenticated.class);
	}

	@Override
	public User resolveArgument(MethodParameter param, ModelAndViewContainer mavContainer, NativeWebRequest request,
			WebDataBinderFactory binderFactory) throws Exception {
		Annotation[] paramAnns = param.getParameterAnnotations();
		for (Annotation annotation : paramAnns) {
			if (Authenticated.class.isInstance(annotation)) {
				Authenticated authAnnotation = (Authenticated) annotation;
				HttpServletRequest httprequest = (HttpServletRequest) request.getNativeRequest();
				String token = httprequest.getHeader(AppConstants.AUTH_TOKEN);
				return checkUserAuthetication(token, authAnnotation.forRole());
			}
		}
		return null;
	}

	private User checkUserAuthetication(String token, Role[] forRole) throws WmsException {
		User user = checkUser(token);
		if (!Arrays.asList(forRole).contains(user.getRole())) {
			LOGGER.error("Role does not match");
			throw new UserNotAuthorizedException();
		}
		if (Role.USER.equals(user.getRole()) || Role.ADMIN.equals(user.getRole())) {
			return checkUser(token);
		}
		return user;
	}

	private User checkUser(String token) throws NumberFormatException, WmsException {
		if (StringUtils.isEmpty(token)) {
			LOGGER.error("Token Empty !");
			throw new UserNotAuthorizedException();
		}
		String userId = AuthUtils.decodeToken(token);
		User user = userService.findOne(Long.valueOf(userId));
		if (!user.getActive()) {
			LOGGER.error("User Inactive");
			throw new UserNotAuthorizedException();
		}
		return user;
	}

}
