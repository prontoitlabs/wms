package com.smart.logistic.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.smart.logistic.domain.enums.Role;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Authenticated {
	/**
	 * Matches the role and authenticates against it.
	 */
	Role[] forRole() default { Role.USER, Role.ADMIN };

	boolean required() default true;

}
