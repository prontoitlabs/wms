package com.smart.logistic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smart.logistic.annotation.Authenticated;
import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.dto.ConfirmStatusDto;
import com.smart.logistic.dto.PageDto;
import com.smart.logistic.dto.SalesOrderDto;
import com.smart.logistic.dto.SalesOrderPackSlipDispatchDetailsDto;
import com.smart.logistic.dto.SalesOrderPackslipDto;
import com.smart.logistic.dto.SalesOrderPackslipItemDto;
import com.smart.logistic.dto.VehicleAllocationDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.IOrderPackslipItemService;
import com.smart.logistic.service.IOrderPackslipService;
import com.smart.logistic.service.IPackslipDispatchDetailsService;
import com.smart.logistic.service.ISalesOrderService;
import com.smart.logistic.transformer.SalesOrderPackslipDispatchDetailsTransformer;
import com.smart.logistic.transformer.SalesOrderPackslipItemTransformer;
import com.smart.logistic.transformer.SalesOrderPackslipTransformer;
import com.smart.logistic.transformer.SalesOrderTransformer;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.RestResponse;
import com.smart.logistic.util.RestUtils;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping(value = AppConstants.API_PREFIX + "/packing-slip")
public class SalesOrderPackSlipController {

	@Autowired
	private IOrderPackslipService orderPackslipService;

	@Autowired
	private ISalesOrderService salesOrderService;

	@Autowired
	private IOrderPackslipItemService orderPackslipItemService;

	@Autowired
	private SalesOrderPackslipTransformer orderPackslipTransformer;

	@Autowired
	private SalesOrderPackslipItemTransformer salesOrderPackslipItemTransformer;

	@Autowired
	private SalesOrderTransformer salesOrderTransformer;

	@Autowired
	private IPackslipDispatchDetailsService packslipDispatchDetailsService;

	@Autowired
	private SalesOrderPackslipDispatchDetailsTransformer salesOrderPackslipDispatchDetailsTransformer;

	@RequestMapping(value = "/all/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<SalesOrderPackslipDto>>> getAllPackingSlips(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Integer pageNumber,
			@PathVariable Integer pageSize) throws WmsException {
		log.info("finding all product pageNumber  pageSize: " + pageNumber + pageSize);
		return RestUtils.successResponse(
				orderPackslipTransformer.toPageDto(orderPackslipService.getAllPackingSlips(pageNumber, pageSize)));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<SalesOrderPackslipDto>> getOrderPackslipDetailById(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Long id) throws WmsException {
		return RestUtils.successResponse(orderPackslipItemService.getOrderPackslipDetailById(id));
	}

	@RequestMapping(value = "/{id}/allocate", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<SalesOrderPackslipDto>> allocateInventoryForOrderPackslip(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Long id) throws WmsException {
		return RestUtils.successResponse(orderPackslipTransformer.toDto(orderPackslipService.allocateInventoryForOrderPackslip(id)));
	}

	@RequestMapping(value = "/dispatch-details", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<SalesOrderPackSlipDispatchDetailsDto>>> getPackslipDispatchDetailByPackSlipNumber(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user,
			@RequestParam(required = true) String packSlipNumber, @RequestParam(required = true) String sku)
			throws WmsException {
		log.info("inside getPackslipDispatchDetailByPackSlipNumber : " + packSlipNumber, sku);
		return RestUtils.successResponse(
				packslipDispatchDetailsService.getPackslipDispatchDetailByPackSlipNumberAndSku(packSlipNumber, sku));
	}

	@RequestMapping(value = "/confirm-status", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<RestResponse<List<SalesOrderPackSlipDispatchDetailsDto>>> confirmDispatchDetails(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user,
			@RequestBody @Valid ConfirmStatusDto confirmStatusDto) throws WmsException {
		return RestUtils.successResponse(packslipDispatchDetailsService
				.confirmDispatchDetails(confirmStatusDto.getIds(), confirmStatusDto.getStatus()));
	}


	@RequestMapping(value = "/validate/{packslipId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<SalesOrderPackslipDto>> validateSalesOrderPackSlip(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Long packslipId)
			throws WmsException {
		return RestUtils.successResponse(
				orderPackslipTransformer.toDto(orderPackslipService.validateSalesOrderPackSlip(packslipId)));
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<SalesOrderPackslipDto>> createPackSlip(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user) throws WmsException {
		log.info("inside createPackSlip");
		return RestUtils.successResponse(orderPackslipService.createPackSlip());
	}

	@RequestMapping(value = "/delivery-orders/customer/{customerCode}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<SalesOrderDto>>> getAllDeliveryOrdersByCustomerCode(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable String customerCode)
			throws WmsException {
		return RestUtils.successResponse(
				salesOrderTransformer.toDto(salesOrderService.getAllDeliveryOrdersByCustomerCode(customerCode)));
	}

	@RequestMapping(value = "/pending-items", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RestResponse<List<SalesOrderPackslipItemDto>>> getAllPendingItemsFromSalesOrders(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @RequestBody List<Long> salesOrderIds)
			throws WmsException {
		return RestUtils.successResponse(salesOrderPackslipItemTransformer
				.toDto(orderPackslipService.getAllPendingItemsFromSalesOrders(salesOrderIds)));
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RestResponse<SalesOrderPackslipDto>> savePackSlip(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user,
			@RequestBody @Valid SalesOrderPackslipDto orderPackslipDto) throws WmsException {
		return RestUtils.successResponse(orderPackslipService.savePackSlip(orderPackslipDto));
	}

	@RequestMapping(value = "/edit", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<RestResponse<SalesOrderPackslipDto>> editPackSlip(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user,
			@RequestBody @Valid SalesOrderPackslipDto orderPackslipDto) throws WmsException {
		return RestUtils.successResponse(orderPackslipService.editPackSlip(orderPackslipDto));
	}

	@RequestMapping(value = "/allocate-vehicle", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RestResponse<List<SalesOrderPackslipDto>>> allocateVehicle(
			@RequestBody @Valid List<VehicleAllocationDto> vehicleAllocationDto) throws WmsException {
		return RestUtils.successResponse(
				orderPackslipTransformer.toDto(orderPackslipService.allocateVehicle(vehicleAllocationDto)));
	}

	@RequestMapping(value = "/dispatch-details/{packSlipId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<SalesOrderPackSlipDispatchDetailsDto>>> findAllPackslipDispatchDetailsByPackslipId(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Long packSlipId)
			throws WmsException {
		log.info("inside getPackslipDispatchDetailByPackSlipId : " + packSlipId);
		return RestUtils.successResponse(salesOrderPackslipDispatchDetailsTransformer
				.toDto(packslipDispatchDetailsService.findAllPackslipDispatchDetailsByPackslipId(packSlipId)));
	}
	
	@RequestMapping(value = "/import-hst-data/{packSlipId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<SalesOrderPackslipDto>> importHSTDataForPackslip(
			@Authenticated(forRole = { Role.ADMIN , Role.USER}) User user, @PathVariable Long packSlipId) throws WmsException {
		log.info("Inside Import HST Data For Packslip");
		return RestUtils.successResponse(orderPackslipTransformer.toDto(orderPackslipService.importHSTDataForPackslip(packSlipId)));
	}
	
	@RequestMapping(value = "/delete/{packslipId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<RestResponse<Boolean>> deletePackSlip(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user,
			@PathVariable Long packslipId) throws WmsException {

		return RestUtils.successResponse(orderPackslipService.deletePackSlip(packslipId));
	}

}
