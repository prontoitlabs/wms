package com.smart.logistic.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smart.logistic.annotation.Authenticated;
import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.dto.PageDto;
import com.smart.logistic.dto.ProductDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.IProductService;
import com.smart.logistic.transformer.ProductTransformer;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.RestResponse;
import com.smart.logistic.util.RestUtils;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping(value = AppConstants.API_PREFIX + "/product")
public class ProductController {

	@Autowired
	private IProductService productService;

	@Autowired
	private ProductTransformer productTransformer;

	/**
	 * 
	 * @param user
	 * @param productDto
	 * @return ProductDto
	 * @throws WmsException
	 */

	@RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RestResponse<ProductDto>> saveProduct(@Authenticated User user,
			@RequestBody @Valid ProductDto productDto) throws WmsException {
		log.info("dto for save product: " + productDto);
		return RestUtils.successResponse(productService.saveProduct(productDto));
	}

	/**
	 * 
	 * @param user
	 * @param pageNumber
	 * @param pageSize
	 * @return PageDto<ProductDto>
	 * @throws WmsException
	 */

	@RequestMapping(value = "/all/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<ProductDto>>> getAllProduct(
			@Authenticated(forRole = { Role.ADMIN }) User user, @PathVariable Integer pageNumber,
			@PathVariable Integer pageSize) throws WmsException {
		log.info("finding all product pageNumber  pageSize: " + pageNumber + pageSize);
		return RestUtils
				.successResponse(productTransformer.toPageDto(productService.getAllProduct(pageNumber, pageSize)));
	}

	/**
	 * 
	 * @param user
	 * @param sku
	 * @return ProductDto based on sku
	 * @throws WmsException
	 */
	@RequestMapping(value = "by/sku", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<ProductDto>> findBySku(@Authenticated User user,
			@RequestParam(required = true) String sku) throws WmsException {
		log.info("inside findBySku : " + sku);
		return RestUtils.successResponse(productTransformer.toDto(productService.findBySkuIgnoreCase(sku)));
	}

	@RequestMapping(value = "/import", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<ProductDto>>> importItemMaster(
			@Authenticated(forRole = { Role.ADMIN }) User user) throws WmsException {
		log.info("inside Import Item Master");
		// productService.importItemMaster();
		productService.importAllItemsFromRamco();
		return RestUtils.successResponse(productTransformer.toPageDto(productService.findTopTenProduct()));
	}

	@RequestMapping(value = "/reconcile", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<Boolean>> reconcileProductQuantityInHand(
			@Authenticated(forRole = { Role.ADMIN }) User user) throws WmsException {
		productService.reconcileProductQuantityInHand();
		return RestUtils.successResponse(Boolean.TRUE);
	}

}
