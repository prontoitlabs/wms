package com.smart.logistic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smart.logistic.annotation.Authenticated;
import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.dto.TransportMasterDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.ITransportMasterService;
import com.smart.logistic.transformer.TransportMasterTransformer;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.RestResponse;
import com.smart.logistic.util.RestUtils;



@Controller
@RequestMapping(value = AppConstants.API_PREFIX + "/transport-master")
public class TransportMasterController {

	@Autowired
	private ITransportMasterService  transportMasterService;
	
	@Autowired
	private TransportMasterTransformer  transportMasterTransformer;
	
	
	@RequestMapping(value = { "/all" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<TransportMasterDto>>> dataBuilder(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user) throws WmsException {
		return RestUtils.successResponse(transportMasterTransformer.toDto(transportMasterService.findAll()));
	}
	
}
