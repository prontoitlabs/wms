package com.smart.logistic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smart.logistic.annotation.Authenticated;
import com.smart.logistic.domain.User;
import com.smart.logistic.dto.LocationDto;
import com.smart.logistic.dto.PageDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.ILocationService;
import com.smart.logistic.transformer.LocationTransformer;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.RestResponse;
import com.smart.logistic.util.RestUtils;

@Controller
@RequestMapping(value = AppConstants.API_PREFIX + "/location")
public class LocationController {

	@Autowired
	ILocationService locationService;

	@Autowired
	private LocationTransformer locationTransformer;

	@RequestMapping(value = "/all/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<LocationDto>>> findingAllCustomer(@Authenticated User user,
			@PathVariable Integer pageNumber, @PathVariable Integer pageSize) throws WmsException {
		return RestUtils.successResponse(locationTransformer.toPageDto(locationService.findingAllLocation(pageNumber, pageSize)));
	}

}
