package com.smart.logistic.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smart.logistic.annotation.Authenticated;
import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.dto.CustomerDto;
import com.smart.logistic.dto.PageDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.ICustomerService;
import com.smart.logistic.transformer.CustomerTransformer;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.RestResponse;
import com.smart.logistic.util.RestUtils;

@Controller
@RequestMapping(value = AppConstants.API_PREFIX + "/customer")
public class CustomerController {

	@Autowired
	ICustomerService customerService;
	
	@Autowired
	private CustomerTransformer customerTransformer;

	@RequestMapping(value = "/import", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<CustomerDto>>> importCustomerMaster(
			@Authenticated(forRole = { Role.ADMIN }) User user) throws WmsException {
//		customerService.importCustomerMaster();
		customerService.importAllCustomersFromRamco();
		return RestUtils.successResponse(customerTransformer.toPageDto(customerService.findTopTenCustomer()));
	}
	
	@RequestMapping(value = "/all/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<CustomerDto>>> findingAllCustomer(@Authenticated User user,
			@PathVariable Integer pageNumber, @PathVariable Integer pageSize) throws WmsException {
		return RestUtils.successResponse(
				customerTransformer.toPageDto(customerService.findingAllCustomer(pageNumber, pageSize)));
	}
	
	@RequestMapping(value = "/all/delivery-order", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<Map<String,CustomerDto>>> getAllCustomersForPendingDeliveryOrders(
			@Authenticated(forRole = { Role.ADMIN }) User user) throws WmsException {
		return RestUtils.successResponse(customerService.getAllCustomersForPendingDeliveryOrders());
	}
	
}
