package com.smart.logistic.controller;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smart.logistic.annotation.Authenticated;
import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.dto.PageDto;
import com.smart.logistic.dto.TransferOrderDto;
import com.smart.logistic.dto.TransferOrderPackSlipDispatchDetailsDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.ITransferOrderPackSlipDispatchDetailsService;
import com.smart.logistic.service.ITransferOrderService;
import com.smart.logistic.transformer.TransferOrderPackSlipDispatchDetailsTransformer;
import com.smart.logistic.transformer.TransferOrdersTransformer;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.RestResponse;
import com.smart.logistic.util.RestUtils;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping(value = AppConstants.API_PREFIX + "/transfer-order")
public class TransferOrderController {

	@Autowired
	private ITransferOrderService transferOrderService;

	@Autowired
	private TransferOrdersTransformer transferOrderTransformer;

	@Autowired
	private ITransferOrderPackSlipDispatchDetailsService transferOrderPackSlipDispatchDetailsService;

	@Autowired
	private TransferOrderPackSlipDispatchDetailsTransformer transferOrderPackSlipDispatchDetailsTransformer;

	@Value(value = "${transferorder.csv.path}")
	private String transferOrderCsvPath;

	/**
	 * 
	 * @param user
	 * @return List<TransferOrderDto> with pageable
	 * @throws WmsException
	 */
	@RequestMapping(value = "/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<TransferOrderDto>>> getListOfTransferOrder(@Authenticated User user,
			@PathVariable Integer pageNumber, @PathVariable Integer pageSize) throws WmsException {
		log.info("getting paginated list of Transfer orders:");
		return RestUtils.successResponse(
				transferOrderTransformer.toPageDto(transferOrderService.getListOfTransferOrder(pageNumber, pageSize)));
	}

	/**
	 * 
	 * @param user
	 * @param id
	 * @return TransferOrderDto based on order id
	 * @throws WmsException
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<TransferOrderDto>> getTransferOrderById(@Authenticated User user,
			@PathVariable Long id) throws WmsException {
		log.info("getting transfer order by id:");
		return RestUtils.successResponse(transferOrderService.getTransferOrderById(id));
	}

	@RequestMapping(value = "/dispatch-details/{transferOrderId}/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<TransferOrderPackSlipDispatchDetailsDto>>> getTransferOrderPackslipDispatchDetailsById(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Long transferOrderId,
			@PathVariable Integer pageNumber, @PathVariable Integer pageSize) throws WmsException {
		log.info("finding all Packslips pageNumber  pageSize: " + pageNumber + pageSize);
		log.info("inside getTransferOrderPackslipDispatchDetailsById : " + transferOrderId);
		return RestUtils.successResponse(
				transferOrderPackSlipDispatchDetailsTransformer.toPageDto(transferOrderPackSlipDispatchDetailsService
						.getTransferOrderPackslipDispatchDetailsById(transferOrderId, pageNumber, pageSize)));
	}

	@RequestMapping(value = "/dispatch-details/{transferOrderId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<TransferOrderPackSlipDispatchDetailsDto>>> getTransferOrderPackslipDispatchDetails(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Long transferOrderId)
			throws WmsException {
		log.info("inside getTransferOrderPackslipDispatchDetails : " + transferOrderId);
		return RestUtils.successResponse(
				transferOrderPackSlipDispatchDetailsTransformer.toDto(transferOrderPackSlipDispatchDetailsService
						.getTransferOrderPackslipDispatchDetailsById(transferOrderId)));
	}

	@RequestMapping(value = {
			"/csv/export/{transferOrderId}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public FileSystemResource exportAllOpenSalesOrders(HttpServletResponse response,
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Long transferOrderId)
			throws Exception {
		String csvFileName = "/tmp/packslip-" + transferOrderId + ".csv";
		transferOrderPackSlipDispatchDetailsService.createCsvFileByTransferOrderId(response, transferOrderId,
				csvFileName);
		return new FileSystemResource(new File(csvFileName));
	}

	@RequestMapping(value = "/inward-detail", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RestResponse<String>> planDispatchforPendingTransferOrders(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user) throws WmsException {
		log.info("inside planDispatchforPendingTransferOrders controller");

		String response = transferOrderService.sendDispatchPlanforPendingTransferOrders();

		return RestUtils.successResponse(response);
	}

}
