package com.smart.logistic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smart.logistic.annotation.Authenticated;
import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.dto.CustomerDto;
import com.smart.logistic.dto.ProductDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.ICustomerService;
import com.smart.logistic.service.IDataBuilderService;
import com.smart.logistic.service.IProductService;
import com.smart.logistic.transformer.CustomerTransformer;
import com.smart.logistic.transformer.ProductTransformer;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.RestResponse;
import com.smart.logistic.util.RestUtils;

@Controller
@RequestMapping(value = AppConstants.API_PREFIX + "/data-builder")
public class DataBuilderController {

	@Autowired
	private IDataBuilderService dataBuilderService;

	@Autowired
	private IProductService productService;

	@Autowired
	private ICustomerService customerService;

	@Autowired
	private ProductTransformer productTransformer;

	@Autowired
	private CustomerTransformer customerTransformer;

	@RequestMapping(value = { "/product-master/create" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<ProductDto>>> createProductMaster(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user) throws WmsException {
		return RestUtils.successResponse(productTransformer.toDto(productService.importItemMaster()));
	}

	@RequestMapping(value = { "/customer-master/create" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<CustomerDto>>> createCustomerMaster(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user) throws WmsException {
		return RestUtils.successResponse(customerTransformer.toDto(customerService.importCustomerMaster()));
	}

	@RequestMapping(value = { "/dummy-data" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<Object>> dataBuilder(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user) throws WmsException {
		return RestUtils.successResponse(dataBuilderService.createDummyData());
	}
}
