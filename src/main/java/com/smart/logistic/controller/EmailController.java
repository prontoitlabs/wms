package com.smart.logistic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smart.logistic.annotation.Authenticated;
import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.dto.EmailDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.EmailService;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.RestResponse;
import com.smart.logistic.util.RestUtils;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping(value = AppConstants.API_PREFIX + "/email")
public class EmailController {

	@Autowired
	private EmailService emailService;
	
	@Value(value = "${email.from}")
	private String emailFrom;

	@RequestMapping(value = "/send", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RestResponse<String>> sendEmail(@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user,
			@RequestBody EmailDto emailDto) throws WmsException {
		log.info("inside EmailController send method");
		emailDto.setFrom(emailFrom);
		return RestUtils.successResponse(emailService.send(emailDto));
	}

}
