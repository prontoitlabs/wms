package com.smart.logistic.controller;

import javax.validation.Valid;

import com.google.gson.JsonObject;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smart.logistic.annotation.Authenticated;
import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.dto.InventoryDto;
import com.smart.logistic.dto.PageDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.IInventoryService;
import com.smart.logistic.transformer.InventoryTransformer;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.RestResponse;
import com.smart.logistic.util.RestUtils;

@Controller
@RequestMapping(value = AppConstants.API_PREFIX + "/inventory")
public class InventoryController {

	@Autowired
	IInventoryService  inventoryService;
	
	
	@Autowired
	InventoryTransformer  inventoryTransformer;
	
	
	@RequestMapping(value = { "/all/{pageNumber}/{pageSize}" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<InventoryDto>>> findAllInventory(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user,@PathVariable Integer pageNumber,
			@PathVariable Integer pageSize) throws WmsException {
		return RestUtils.successResponse(inventoryTransformer.toPageDto(inventoryService.fidAllPaginatedProdct(pageNumber, pageSize)));
	}
	
	@RequestMapping(value = { "/product/{productId}" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<InventoryDto>> findByProductId(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Long productId) throws WmsException {
		return RestUtils.successResponse(inventoryTransformer.toDto(inventoryService.findByProductId(productId)));
	}
	
	
	@RequestMapping(value = { "/product/{productSku}" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<InventoryDto>> findByProductSku(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable String productSku) throws WmsException {
		return RestUtils.successResponse(inventoryTransformer.toDto(inventoryService.findByProductSku(productSku)));
	}
	
	@RequestMapping(value = { "/" }, method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RestResponse<InventoryDto>>createInventory(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @RequestBody @Valid InventoryDto inventoryDto) throws WmsException {
		return RestUtils.successResponse(inventoryService.saveInventory(inventoryDto));
	}
	
	
	@RequestMapping(value = { "/update/{inventoryId}" }, method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<RestResponse<InventoryDto>>updateInventory(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @RequestBody @Valid InventoryDto inventoryDto,@PathVariable Long inventoryId) throws WmsException {
		return RestUtils.successResponse(inventoryService.updateInventory(inventoryId, inventoryDto));
	}

	@RequestMapping(value = { "/barcode/{oldBarCode}" }, method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<RestResponse<InventoryDto>>updateBarCode(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @RequestBody String barcodeInfo, @PathVariable String oldBarCode) throws WmsException {

		JSONParser parser = new JSONParser(JSONParser.MODE_JSON_SIMPLE);
        JSONObject obj = null;
        try {
            obj = (JSONObject)parser.parse(barcodeInfo);
            String newBarCode =  obj.getAsString ("newBarCode");
            inventoryService.updateBarCode(oldBarCode, newBarCode);
            return RestUtils.successResponse();
        } catch (ParseException e) {
            throw new WmsException (e.getMessage());
        }



	}
	
	
	
	}
