package com.smart.logistic.controller;

import java.util.ArrayList;
import java.util.List;

import com.smart.logistic.service.ISalesOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smart.logistic.annotation.Authenticated;
import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.dto.InventoryStatusDto;
import com.smart.logistic.dto.OrderStatusDto;
import com.smart.logistic.dto.ProductStatusOnDashBoardDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.IInventoryService;
import com.smart.logistic.service.IProductService;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.RestResponse;
import com.smart.logistic.util.RestUtils;

@Controller
@RequestMapping(value = AppConstants.API_PREFIX + "/dashboard")
public class DashboardController {

	@Autowired
	IInventoryService inventoryService;

	@Autowired
	IProductService productService;

	@Autowired
	ISalesOrderService salesOrderService;

	@RequestMapping(value = { "/inventory" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<InventoryStatusDto>>> dataBuilder(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user) throws WmsException {
		return RestUtils.successResponse(inventoryService.findIvetoryByOrderStatus());
	}

	@RequestMapping(value = { "/orders" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<OrderStatusDto>>> orderStatus(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user) throws WmsException {
		return RestUtils.successResponse(salesOrderService.getOrderStatusToday());
	}

	@RequestMapping(value = "/products", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<ProductStatusOnDashBoardDto>>> findItemAndQuantityInHand(
			@Authenticated User user) throws WmsException {
		return RestUtils.successResponse(productService.findItemAndQuantityInHand());
	}

	@RequestMapping(value = { "/stockin" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<InventoryStatusDto>>> countStockIn(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user) throws WmsException {

		List <InventoryStatusDto> isdList = new ArrayList<InventoryStatusDto>();
		InventoryStatusDto isd = new InventoryStatusDto();
		isd.setStatus ("StockIn");
		isd.setCount (inventoryService.countInventoryCreatedToday());
		isdList.add(isd);

		return RestUtils.successResponse(isdList);
	}

}
