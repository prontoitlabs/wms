package com.smart.logistic.controller;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smart.logistic.annotation.Authenticated;
import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.dto.AuthDto;
import com.smart.logistic.dto.LoginDto;
import com.smart.logistic.dto.UserDto;
import com.smart.logistic.exception.UserNotAuthenticatedException;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.IAuthService;
import com.smart.logistic.service.IUserService;
import com.smart.logistic.transformer.UserTransformer;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.EncryptionUtil;
import com.smart.logistic.util.RestResponse;
import com.smart.logistic.util.RestUtils;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping(value = AppConstants.API_PREFIX + "/auth")
public class AuthController {

	@Autowired
	private IAuthService authService;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private UserTransformer userTransformer;
	

	/**
	 * 
	 * @param loginDto
	 * @param user
	 * @return AuthDto
	 * @throws UserNotAuthenticatedException
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RestResponse<AuthDto>> login(@RequestBody @Valid LoginDto loginDto)
			throws UserNotAuthenticatedException {
		log.info("Request dto for login user : " + loginDto);
		return RestUtils.successResponse(authService.login(loginDto));
	}

	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> logout(@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user,
			@RequestHeader(value = AppConstants.AUTH_TOKEN) String token) throws WmsException {
		log.info("Logout " + user);
		authService.logout(user, token);
		return RestUtils.successResponse(null);
	}

	@RequestMapping(value = "/create-user", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RestResponse<UserDto>> createUser(@RequestBody @Valid UserDto userDto)
			throws UserNotAuthenticatedException {
		log.info("Request dto for create user : " + userDto);
		User user = new User();
		BeanUtils.copyProperties(userDto, user);
		user.setPassword(EncryptionUtil.encrypt(userDto.getPassword()));
		return RestUtils.successResponse(userTransformer.toDto(userService.save(user)));
	}
	
}
