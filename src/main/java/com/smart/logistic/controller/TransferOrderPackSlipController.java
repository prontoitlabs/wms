package com.smart.logistic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smart.logistic.annotation.Authenticated;
import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.dto.ConfirmStatusDto;
import com.smart.logistic.dto.PageDto;
import com.smart.logistic.dto.TransferOrderPackSlipDispatchDetailsDto;
import com.smart.logistic.dto.TransferOrderPackslipDto;
import com.smart.logistic.dto.TransferOrderReceiptDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.ITransferOrderPackSlipDispatchDetailsService;
import com.smart.logistic.service.ITransferOrderPackslipItemService;
import com.smart.logistic.service.ITransferOrderPackslipService;
import com.smart.logistic.transformer.TransferOrderPackslipTransformer;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.RestResponse;
import com.smart.logistic.util.RestUtils;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping(value = AppConstants.API_PREFIX + "/transfer-packing-slip")
public class TransferOrderPackSlipController {

	@Autowired
	private ITransferOrderPackslipService transferOrderPackslipService;

	@Autowired
	private ITransferOrderPackslipItemService transferOrderPackslipItemService;

	@Autowired
	private ITransferOrderPackSlipDispatchDetailsService transferOrderPackSlipDispatchDetailsService;

	@Autowired
	private TransferOrderPackslipTransformer transferOrderPackslipTransformer;

	@RequestMapping(value = "/all/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<TransferOrderPackslipDto>>> getAllTransferOrderPackingSlips(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Integer pageNumber,
			@PathVariable Integer pageSize) throws WmsException {
		log.info("finding all Packslips pageNumber  pageSize: " + pageNumber + pageSize);
		return RestUtils.successResponse(transferOrderPackslipTransformer
				.toPageDto(transferOrderPackslipService.getAllTransferOrderPackingSlips(pageNumber, pageSize)));
	}

	@RequestMapping(value = "/transfer-order/{transferOrderId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<TransferOrderPackslipDto>>> getTransferOrderPackslipsByTransferOrderId(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Long transferOrderId)
			throws WmsException {
		return RestUtils.successResponse(
				transferOrderPackslipService.getTransferOrderPackslipsByTransferOrderId(transferOrderId));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<TransferOrderPackslipDto>> getTransferOrderPackslipDetailById(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Long id) throws WmsException {
		return RestUtils.successResponse(transferOrderPackslipItemService.getTransferOrderPackslipDetailById(id));
	}


	@RequestMapping(value = "/confirm-status", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<RestResponse<List<TransferOrderPackSlipDispatchDetailsDto>>> confirmListOfPackSlipDispatchDetailAsDone(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user,
			@RequestBody @Valid ConfirmStatusDto ConfirmStatusDto) throws WmsException {
		return RestUtils.successResponse(transferOrderPackSlipDispatchDetailsService
				.confirmListOfPackSlipDispatchDetailAsDone(ConfirmStatusDto.getIds(), ConfirmStatusDto.getStatus()));
	}

	@RequestMapping(value = "/validate/{packslipId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<TransferOrderPackslipDto>> validateTransferOrderPackSlip(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Long packslipId)
			throws WmsException {
		return RestUtils.successResponse(
				transferOrderPackslipTransformer.toDto(transferOrderPackslipService.validatePackslip(packslipId)));
	}

	@RequestMapping(value = "all/validate/{transferOrderId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<TransferOrderPackslipDto>>> validateTransferOrderPackSlips(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Long transferOrderId)
			throws WmsException {
		return RestUtils.successResponse(transferOrderPackslipTransformer
				.toDto(transferOrderPackslipService.validateTransferOrderPackSlips(transferOrderId)));
	}

	@RequestMapping(value = "/transfer-order/all/{transferOrderId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<TransferOrderReceiptDto>> getPackslipsOfTransferOrderByTransferOrderId(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Long transferOrderId)
			throws WmsException {
		return RestUtils.successResponse(
				(transferOrderPackslipService.getPackslipsOfTransferOrderByTransferOrderId(transferOrderId)));
	}
}
