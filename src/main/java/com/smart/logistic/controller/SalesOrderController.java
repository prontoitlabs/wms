package com.smart.logistic.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.smart.logistic.service.IProductService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.smart.logistic.annotation.Authenticated;
import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.OrderFulfillmentType;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.dto.AddToDeliveryDto;
import com.smart.logistic.dto.DeliveryDetailsRequestDto;
import com.smart.logistic.dto.DistinctProductDto;
import com.smart.logistic.dto.PageDto;
import com.smart.logistic.dto.SalesOrderDto;
import com.smart.logistic.dto.UpdateStatusDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.ISalesOrderService;
import com.smart.logistic.transformer.SalesOrderTransformer;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.RestResponse;
import com.smart.logistic.util.RestUtils;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping(value = AppConstants.API_PREFIX + "/sales-order")
public class SalesOrderController {

	@Autowired
	private ISalesOrderService salesOrderService;

	@Autowired
	private SalesOrderTransformer salesOrderTransformer;

	@Autowired
	private IProductService productService;

	/**
	 * 
	 * @param user
	 * @return List<SalesOrderDto> with pageable
	 * @throws WmsException
	 */
	@RequestMapping(value = "/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<SalesOrderDto>>> getListOfSalesOrder(@Authenticated User user,
			@PathVariable Integer pageNumber, @PathVariable Integer pageSize) throws WmsException {
		log.info("getting paginated list of sales order:");
		return RestUtils.successResponse(
				salesOrderTransformer.toPageDto(salesOrderService.getListOfSalesOrder(pageNumber, pageSize)));
	}

	/**
	 * 
	 * @param user
	 * @return List<SalesOrderDto> with pageable
	 * @throws WmsException
	 */
	@RequestMapping(value = "/dispatch/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<SalesOrderDto>>> getListOfDeliveryOrders(@Authenticated User user,
			@PathVariable Integer pageNumber, @PathVariable Integer pageSize) throws WmsException {
		log.info("getting paginated list of Delivery Orders:");
		return RestUtils.successResponse(
				salesOrderTransformer.toPageDto(salesOrderService.getListOfDeliveryOrders(pageNumber, pageSize)));
	}

	/**
	 * 
	 * @param user
	 * @param id
	 * @return SalesOrderDto based on order id
	 * @throws WmsException
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<SalesOrderDto>> getSalesOrderById(@Authenticated User user,
			@PathVariable Long id) throws WmsException {
		log.info("getting sales order by id:");
        return RestUtils.successResponse(salesOrderTransformer.toDtoEntity(salesOrderService.findOne(id)));
	}

	/**
	 * 
	 * @param user
	 * @param id
	 * @return SalesOrderDto based on order id
	 * @throws WmsException
	 */
	@RequestMapping(value = "/add-to-dispatch/{id}/{orderFulfillmentType}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<SalesOrderDto>> addSalesOrderToDispatch(@Authenticated User user,
			@PathVariable Long id, @PathVariable OrderFulfillmentType orderFulfillmentType) throws WmsException {
		log.info("Adding sales order to Dispatch:");
		return RestUtils.successResponse(salesOrderService.addSalesOrderToDispatch(id, orderFulfillmentType));
	}

	@RequestMapping(value = "/remove-from-dispatch/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<SalesOrderDto>> removeSalesOrderFromDispatch(@Authenticated User user,
			@PathVariable Long id) throws WmsException {
		log.info("Removing sales order From Dispatch:");
		return RestUtils.successResponse(salesOrderService.removeSalesOrderFromDispatch(id));
	}

	/**
	 * 
	 * @param user
	 * @return List<DistinctProductDto> contains distict product name and sku list
	 * @throws WmsException
	 */
	@RequestMapping(value = "/distinct/{salesOrderId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<DistinctProductDto>>> findDistinctSkuAndProductBySalesOrderId(
			@Authenticated User user, @PathVariable Long salesOrderId) throws WmsException {
		log.info("inside findDistinctSkuAndProductBySalesOrderId controller" + salesOrderId);
		return RestUtils.successResponse(salesOrderService.findDistinctSkuAndProductBySalesOrderId(salesOrderId));
	}

	@RequestMapping(value = "/delivery-detail", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RestResponse<Object>> planDispatchforSelectedSalesOrder(
			@RequestBody DeliveryDetailsRequestDto deliveryDetailsRequestDto,
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user) throws WmsException {
		log.info("inside findSalesOrderDeliveryDetail controller" + deliveryDetailsRequestDto.getIds());

		String response = salesOrderService.sendDeliveryOrdersForAllocation(deliveryDetailsRequestDto);

		return RestUtils.successResponse(response);
	}

	@RequestMapping(value = "/import", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<SalesOrderDto>>> importAllOpenSalesOrders(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user)
			throws WmsException, JsonParseException, JsonMappingException, IOException {
		log.info("inside importAllOpenSalesOrdersFromRamco controller Method");
		salesOrderService.importAllOpenSalesOrdersFromRamco();
		return RestUtils.successResponse(salesOrderTransformer.toPageDto(salesOrderService.findTopTenSalesOrder()));
	}

	@RequestMapping(value = {
			"/csv/export/{salesOrderPackslipId}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public FileSystemResource exportAllOpenSalesOrders(HttpServletResponse response,
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Long salesOrderPackslipId)
			throws Exception {
		String csvFileName = "/tmp/salesorder-packslip-" + salesOrderPackslipId + ".csv";
		salesOrderService.createCsvFileBySalesOrderPackSlipId(response, salesOrderPackslipId, csvFileName);
		return new FileSystemResource(new File(csvFileName));
	}

	@RequestMapping(value = {
			"/csv/export" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public FileSystemResource exportSalesOrderDeviationReport(HttpServletResponse response,
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMYYYY");
		String format = sdf.format(DateTime.now().toDate());
		String csvFileName = "/tmp/Salesorder_Deviation_Report-" + format + ".csv";
		salesOrderService.exportSalesOrderDeviationReport(response, csvFileName);
		return new FileSystemResource(new File(csvFileName));
	}

	@RequestMapping(value = {
			"{salesOrderId}/csv/export" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public FileSystemResource exportSalesOrderDetails(HttpServletResponse response,
															  @Authenticated(forRole = { Role.ADMIN, Role.USER }) User user,
													  @PathVariable Long salesOrderId) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMYYYY");
		String format = sdf.format(DateTime.now().toDate());
		String csvFileName = "/tmp/Salesorder_Detail_Report-" + format + ".csv";
		salesOrderService.exportSalesOrderIdDetails(response, csvFileName, salesOrderId);
		return new FileSystemResource(new File(csvFileName));
	}

	@RequestMapping(value = "/add-to-dispatch/{id}/{orderFulfillmentType}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<RestResponse<SalesOrderDto>> addSalesOrderForDispatch(@Authenticated User user,
			@RequestBody @Valid AddToDeliveryDto addToDeliveryDto, @PathVariable Long id,
			@PathVariable OrderFulfillmentType orderFulfillmentType) throws WmsException {
		log.info("Adding sales order for Dispatch:");
		return RestUtils.successResponse(salesOrderService
				.addSalesOrderForDispatch(addToDeliveryDto.getSalesOrderDetailsDto(), id, orderFulfillmentType));
	}

	@RequestMapping(value = "/release-product/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<SalesOrderDto>> releaseReservedProductFromSalesOrderDetails(
			@Authenticated User user, @PathVariable Long id) throws WmsException {
		log.info("Releasing product from sales order details:");
		return RestUtils.successResponse(
				salesOrderTransformer.toDtoEntity(salesOrderService.releaseReservedProductFromSalesOrderDetails(id)));
	}

	@RequestMapping(value = "/update-status", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<RestResponse<SalesOrderDto>> updateSalesOrderStatus(@Authenticated User user,
			@RequestBody @Valid UpdateStatusDto updateStatusDto) throws WmsException {
		log.info("inside updateSalesOrderStatus method in controller :" + updateStatusDto);
		return RestUtils.successResponse(
				salesOrderTransformer.toDtoEntity(salesOrderService.updateSalesOrderStatus(updateStatusDto)));
	}

	/**
	 * 
	 * @param user
	 * @param pageNumber
	 *            // can't be negative
	 * @param pageSize
	 *            // item per page (can't be negative)
	 * @return PageDto<SalesOrderDto> // All sales orders whose status is FULFILLED
	 * @throws WmsException
	 */
	@RequestMapping(value = "/fulfilled/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<SalesOrderDto>>> getAllFulfilledSalesOrders(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Integer pageNumber,
			@PathVariable Integer pageSize) throws WmsException {
		log.info("inside getAllFulfilledSalesOrders controller Method");
		return RestUtils.successResponse(
				salesOrderTransformer.toPageDto(salesOrderService.getAllFulfilledSalesOrders(pageNumber, pageSize)));
	}

	/**
	 * 
	 * @param user
	 * @param pageNumber
	 *            // can't be negative
	 * @param pageSize
	 *            // item per page (can't be negative)
	 * @return PageDto<SalesOrderDto> // All pending sales orders
	 * @throws WmsException
	 */
	@RequestMapping(value = "/pending/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<SalesOrderDto>>> getAllPendingSalesOrders(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Integer pageNumber,
			@PathVariable Integer pageSize) throws WmsException {
		log.info("inside getAllPendingSalesOrders controller Method");
		return RestUtils.successResponse(
				salesOrderTransformer.toPageDto(salesOrderService.getAllPendingSalesOrders(pageNumber, pageSize)));
	}

	/**
	 *
	 * @param user
	 * @param pageNumber
	 *            // can't be negative
	 * @param pageSize
	 *            // item per page (can't be negative)
	 * @return PageDto<SalesOrderDto> // All pending sales orders
	 * @throws WmsException
	 */
	@RequestMapping(value = "/partiallyFulfilled/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<PageDto<SalesOrderDto>>> getAllPartiallyFulfilled(
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user, @PathVariable Integer pageNumber,
			@PathVariable Integer pageSize) throws WmsException {
		log.info("inside getAllPartiallyFulfilled controller Method");
		return RestUtils.successResponse(
				salesOrderTransformer.toPageDto(salesOrderService.getAllPartiallyFulfilledSalesOrders(pageNumber, pageSize)));
	}
}
