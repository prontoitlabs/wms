package com.smart.logistic.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.smart.logistic.annotation.Authenticated;
import com.smart.logistic.domain.DeviationReport;
import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.dto.IndentReportDto;
import com.smart.logistic.dto.IndividualSalesOrderDeviationReportDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.IReportService;
import com.smart.logistic.transformer.IndentReportTransformer;
import com.smart.logistic.transformer.IndividualSalesOrderDeviationReportTransformer;
import com.smart.logistic.util.AppConstants;
import com.smart.logistic.util.RestResponse;
import com.smart.logistic.util.RestUtils;

@Controller
@RequestMapping(value = AppConstants.API_PREFIX + "/report")
public class ReportController {

	@Autowired
	private IReportService reportService;

	@Autowired
	private IndentReportTransformer indentReportTransformer;

	@Autowired
	private IndividualSalesOrderDeviationReportTransformer individualSalesOrderDeviationReportTransformer;

	@RequestMapping(value = "/loss-leader", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<DeviationReport>>> getLossLeaderReport(
			@Authenticated(forRole = { Role.ADMIN }) User user) throws WmsException {
		return RestUtils.successResponse(reportService.getdeviationReportForLossLeader());
	}

	@RequestMapping(value = "/individual-sales-order", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<IndividualSalesOrderDeviationReportDto>>> getDeviationReportForIndividualSalesOrder(
			@Authenticated(forRole = { Role.ADMIN }) User user) throws WmsException {
		return RestUtils.successResponse(individualSalesOrderDeviationReportTransformer
				.toDto(reportService.getDeviationReportForIndividualSalesOrder()));
	}

	@RequestMapping(value = "/indent", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<IndentReportDto>>> findByStatusAndDateCreatedBetween(
			@Authenticated(forRole = { Role.ADMIN }) User user) throws WmsException {
		return RestUtils.successResponse(indentReportTransformer.toDto(reportService.getIndentReport()));
	}

	@RequestMapping(value = "/generate/deviation", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<IndividualSalesOrderDeviationReportDto>>> generateDeviationReport(
			@Authenticated(forRole = { Role.ADMIN }) User user) throws WmsException {
		return RestUtils.successResponse(individualSalesOrderDeviationReportTransformer
				.toDto(reportService.saveDeviationReportForIndividualSalesOrder()));
	}

	@RequestMapping(value = {
			"/indent/export" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public FileSystemResource exportIndentReport(HttpServletResponse response,
			@Authenticated(forRole = { Role.ADMIN, Role.USER }) User user) throws WmsException, Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMYYYY  hh:mm:ss");
		String format = sdf.format(DateTime.now().toDate());
		String csvFileName = "/tmp/indent-Report-" + format + ".csv";
		reportService.exportIndentReport(response, csvFileName);
		return new FileSystemResource(new File(csvFileName));
	}

	@RequestMapping(value = "/generate/indent", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<IndentReportDto>>> generateIndentReport(
			@Authenticated(forRole = { Role.ADMIN }) User user) throws WmsException {
		return RestUtils.successResponse(indentReportTransformer.toDto(reportService.saveIndentReport()));
	}

	@RequestMapping(value = "/generate/stock-in-day", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<IndentReportDto>>> generateStockInDayReport(
			@Authenticated(forRole = { Role.ADMIN }) User user) throws WmsException {
		return RestUtils.successResponse(indentReportTransformer.toDto(reportService.saveIndentReport()));
	}

	@RequestMapping(value = "/generate/stock-in-month", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<RestResponse<List<IndentReportDto>>> generateStockInMonthReport(
			@Authenticated(forRole = { Role.ADMIN }) User user) throws WmsException {
		return RestUtils.successResponse(indentReportTransformer.toDto(reportService.saveIndentReport()));
	}

}
