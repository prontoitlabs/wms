package com.smart.logistic.util;

import com.smart.logistic.domain.Product;
import com.smart.logistic.dto.EmailDto;
import com.smart.logistic.dto.ProductStatusOnDashBoardDto;
import com.smart.logistic.service.EmailService;

import com.smart.logistic.service.IInventoryService;
import com.smart.logistic.service.IProductService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.IReportService;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class ScheduleTask {

    @Autowired
    private IReportService reportService;

    @Autowired
    private EmailService emailService;

    @Autowired
    IProductService productService;

    @Autowired
    IInventoryService inventoryService;


    @Value(value = "${email.to}")
    private String emailTo;

    @Value(value = "${email.from}")
    private String emailFrom;



    // this has been scheduled for every 9 pm night
    @Scheduled(cron = "0 0 21 * * ?")
    public void runTask() throws WmsException {
        sendInventoryStatusReport();
        sendStockInReport();
    }

    private void sendStockInReport() {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        log.info("Creating stock in reports for " + formatter.format(date).toString());

        List<Object[]> productList = inventoryService.findInventoryCreatedToday();
        try {
            List<Long> productIds = new ArrayList<Long>();

            for (Object[] o : productList) {

                productIds.add(Long.valueOf(o[0].toString()));
//			String count = o[1].toString();

//			System.out.println("Product = " + productId + ", count = "+ count);
            }
            List<Product> products = productService.findByIdIn(productIds);



            Workbook workbook = new XSSFWorkbook();

            // create excel xls sheet
            Sheet sheet = workbook.createSheet("Inventory Status");

            // Create a Font for styling header cells
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setColor(IndexedColors.RED.getIndex());

            // Create a CellStyle with the font
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Create a Row
            Row headerRow = sheet.createRow(0);

            // Create cells
            String[] columns = {"Item Name", "Item SKU", "Description", "Quantity imported"};
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
            }

            int rowNum = 1;

            int count = 0;

            for (Product product : products) {

                System.out.print("Name -> " + product.getName()
                        + " SKU -> " + product.getSku()
                        + " Description -> " + product.getDescription()
                        + " Count -> " + productList.get(count)[1].toString());

                Row row = sheet.createRow(rowNum++);

                row.createCell(0)
                        .setCellValue(product.getName());

                row.createCell(1)
                        .setCellValue(product.getSku());

                row.createCell(2)
                        .setCellValue(product.getDescription());

                row.createCell(3)
                        .setCellValue(Double.valueOf(productList.get(count)[1].toString()));

                count++;

            }


            // Resize all columns to fit the content size
            for (int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }


            try {
                File file = File.createTempFile("Stock In " + formatter.format(date).toString(), ".xlsx");
                // Write the output to a file
                FileOutputStream fileOut = new FileOutputStream(file);
                workbook.write(fileOut);
                fileOut.close();

                // Closing the workbook
                workbook.close();

                EmailDto emailDto = EmailDto.builder().to(emailTo).from(emailFrom)
                        .subject("Stock In - " + formatter.format(date).toString()).file(file).build();

                emailService.send(emailDto);

            } catch (Exception ex) {
                log.error(ex.getMessage());
            }

        } catch (Throwable thr) {
            thr.printStackTrace();
        }


    }


    private void sendInventoryStatusReport() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        log.info("Creating inventory reports for " + formatter.format(date).toString());

        // saving indent report
        //reportService.saveIndentReport();

        // saving deviation report for individual sales order
        // reportService.saveDeviationReportForIndividualSalesOrder();


        List<ProductStatusOnDashBoardDto> productList = productService.findItemAndQuantityInHand();

        Workbook workbook = new XSSFWorkbook();

        // create excel xls sheet
        Sheet sheet = workbook.createSheet("Inventory Status");

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Create cells
        String[] columns = {"Item Name", "Item SKU", "Quantity in Hand"};
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1;
        for (ProductStatusOnDashBoardDto productItem : productList) {
            Row row = sheet.createRow(rowNum++);

            row.createCell(0)
                    .setCellValue(productItem.getItemName());

            row.createCell(1)
                    .setCellValue(productItem.getSku());

            row.createCell(2)
                    .setCellValue(productItem.getQuantityInHand());

        }

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }


        try {
            File file = File.createTempFile("Inventory " + formatter.format(date).toString(), ".xlsx");
            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream(file);
            workbook.write(fileOut);
            fileOut.close();

            // Closing the workbook
            workbook.close();

            EmailDto emailDto = EmailDto.builder().to(emailTo).from(emailFrom)
                    .subject("Inventory - " + formatter.format(date).toString()).file(file).build();

            emailService.send(emailDto);

        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

}
