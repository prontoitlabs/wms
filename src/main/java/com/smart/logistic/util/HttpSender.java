package com.smart.logistic.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

@SuppressWarnings("deprecation")
public class HttpSender {

	private static HttpClient httpClient = new DefaultHttpClient();

	/**
	 * @param url
	 * @param params
	 * @param headers
	 * @return
	 * @throws URISyntaxException
	 * @throws UnsupportedEncodingException
	 * @throws HttpTransportException
	 */
	public String executeGet(String url, Map<String, String> params, Map<String, String> headers)
			throws URISyntaxException, UnsupportedEncodingException {

		HttpGet httpGet = new HttpGet(createURL(url, params));

		httpGet.addHeader("Content-Type", "application/x-www-form-urlencoded");

		if (headers != null) {
			for (Map.Entry<String, String> hEntry : headers.entrySet()) {
				httpGet.addHeader(hEntry.getKey(), hEntry.getValue());
			}
		}
		try {
			HttpResponse response = httpClient.execute(httpGet);
			return getContent(response.getEntity());
		} catch (Exception e) {
			throw new RuntimeException("Unable to execute http get", e);
		}
	}

	/**
	 * @param url
	 * @param params
	 * @param headers
	 * @return
	 */
	public String executePost(String url, Map<String, String> params, Map<String, String> headers) {
		HttpPost httpPost = new HttpPost(url);

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		if (params != null) {
			for (Entry<String, String> m : params.entrySet()) {
				nameValuePairs.add(new BasicNameValuePair(m.getKey(), m.getValue()));
			}
		}
		if (headers != null) {
			for (Map.Entry<String, String> hEntry : headers.entrySet()) {
				httpPost.addHeader(hEntry.getKey(), hEntry.getValue());
			}
		}
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpClient.execute(httpPost);
			return getContent(response.getEntity());
		} catch (Exception e) {

			e.printStackTrace();
			System.out.println(e);
		}
		return null;
	}

	/**
	 * @param entity
	 * @return
	 * @throws IOException
	 */
	private static String getContent(HttpEntity entity) throws IOException {
		String charset = EntityUtils.getContentCharSet(entity);
		InputStreamReader inputStreamReader = null;
		BufferedReader br = null;
		StringBuilder response = new StringBuilder();
		try {
			if (charset != null && !charset.isEmpty()) {
				inputStreamReader = new InputStreamReader(entity.getContent(), charset);
			} else {
				inputStreamReader = new InputStreamReader(entity.getContent());
			}
			br = new BufferedReader(inputStreamReader);
			String line = null;
			while ((line = br.readLine()) != null) {
				response.append(line).append("\n");
			}
			// This is to ensure the resources are released properly
			EntityUtils.consume(entity);
		} finally {
			if (inputStreamReader != null) {
				inputStreamReader.close();
			}
			if (br != null) {
				br.close();
			}
		}
		return response.toString();
	}

	private static String createURL(String url, Map<String, String> params) {
		StringBuilder uri = new StringBuilder();
		uri.append(url);
		if (!params.isEmpty())
			uri.append("?");

		for (Entry<String, String> m : params.entrySet()) {
			uri.append(m.getKey());
			uri.append("=");
			uri.append(URLEncoder.encode(m.getValue()));
			uri.append("&");
		}
		return uri.substring(0, uri.length() - 1).toString();
	}
}
