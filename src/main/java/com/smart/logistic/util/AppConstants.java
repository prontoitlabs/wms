package com.smart.logistic.util;

public interface AppConstants {

	String API_PREFIX = "/api/v1";

	String AUTH_TOKEN = "X-AUTH-TOKEN";

}
