package com.smart.logistic.util;

import java.io.File;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.smart.logistic.dto.EmailDto;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EmailUtils {
	@Value(value = "${mailgun.apikey}")
	private String mailgunApiKey;

	@Value(value = "${mailgun.url}")
	private String mailgunURL;
	
	@Value(value = "${email.from}")
	private String emailFrom;

	@Async
	public String sendMailWithoutAttachment(EmailDto emailDto) {
		log.info("inside sendMailWithoutAttachment method"+emailDto);
		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter("api", mailgunApiKey));
		WebResource webResource = client.resource(mailgunURL + "/messages");
		FormDataMultiPart formData = new FormDataMultiPart();
		formData.field("from", emailDto.getFrom());
		formData.field("to", emailDto.getTo());
		formData.field("subject", emailDto.getSubject());
		formData.field("html", emailDto.getMessage());
		webResource.type(MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class, formData);
		return "Email sent Successfully";
	}

	@Async
	public String sendMailWithAttachment(EmailDto emailDto) {
		log.info("inside sendMailWithAttachment method "+emailDto);
		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter("api", mailgunApiKey));
		WebResource webResource = client.resource(mailgunURL + "/messages");
		FormDataMultiPart formData = new FormDataMultiPart();
		formData.field("from", emailFrom);
		formData.field("to", emailDto.getTo());
		formData.field("subject", emailDto.getSubject());
		formData.field("html", emailDto.getMessage());
		File attachment = new File(emailDto.getFile().getAbsolutePath());
		log.info("attachment "+attachment);
		formData.bodyPart(new FileDataBodyPart("attachment", attachment, MediaType.APPLICATION_OCTET_STREAM_TYPE));
		webResource.type(MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class, formData);
		return "Email sent Successfully";
	}

}
