package com.smart.logistic.util;

import java.util.Comparator;

import com.smart.logistic.domain.Product;

public class QuantityInHandForProductComparator implements Comparator<Product> {

	@Override
	public int compare(Product p1, Product p2) {
		// will return in descending order
		return -p1.getQuantityInHand().compareTo(p2.getQuantityInHand());
	}

}
