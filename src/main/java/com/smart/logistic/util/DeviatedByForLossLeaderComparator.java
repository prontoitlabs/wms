package com.smart.logistic.util;

import java.util.Comparator;

import com.smart.logistic.domain.DeviationReport;

public class DeviatedByForLossLeaderComparator implements Comparator<DeviationReport>{

	@Override
	public int compare(DeviationReport d1, DeviationReport d2) {
		// will return in descending order
		return -d1.getDeviatedBy().compareTo(d2.getDeviatedBy());
	}

}
