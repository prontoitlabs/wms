package com.smart.logistic.config;


import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Slf4j
@Configuration
@EnableScheduling
public class S3Config {


    @Value(value = "${aws.region.name}")
    private String awsRegionName;

    @Value(value = "${aws.access.key}")
    private String awsAccessKey;

    @Value(value = "${aws.secret.key}")
    private String awsSecretKey;


    @Autowired
    private AmazonS3 amazonS3;

;


    @Bean
    public AmazonS3 amazonS3() {

        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
        AWSStaticCredentialsProvider awsStaticCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);
        amazonS3 = AmazonS3ClientBuilder.standard().withRegion(Regions.fromName(awsRegionName))
                .withCredentials(awsStaticCredentialsProvider).build();

        return amazonS3;
    }



}
