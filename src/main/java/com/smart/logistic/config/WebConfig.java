package com.smart.logistic.config;

import com.smart.logistic.annotation.AuthenticatedArgumentResolver;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;
import java.util.Properties;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

	@Value("${email.template.path}")
	private String templatePath;

	@Bean
	public AuthenticatedArgumentResolver authenticatedArgumentResolver() {
		return new AuthenticatedArgumentResolver();
	}

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(authenticatedArgumentResolver());
	}

	@Bean
	public VelocityEngine velocityEngine() {

		VelocityEngine ve = new VelocityEngine();
		ve.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, templatePath);

		Properties p = new Properties();
		p.setProperty("resource.loader", "class");
		p.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		p.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogChute");
		ve.init( p );  
		return ve;
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplateBuilder().setConnectTimeout(60 * 1000).setReadTimeout(60 * 1000).build();
	}

	@Bean
	public AsyncRestTemplate asyncRestTemplate() {
		AsyncRestTemplate template = new AsyncRestTemplate();
		return template;
	}

}
