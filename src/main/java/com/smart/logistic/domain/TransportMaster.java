package com.smart.logistic.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "transport_master")
public class TransportMaster extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Column(unique = true)
	private String name;

	@NotNull
	@Column(unique = true)
	private String vehicleNo;

	private String vehicleType;

	@NotNull
	@Column(unique = true)
	private String email;

	private String phoneNumber;

	private String mobileNumber;

	private String gstn;

	@NotNull
	private String destination;

	@NotNull
	private String source;

	@NotBlank
	private String addressline1;

	private String addressline2;

	private String addressline3;

	@NotBlank
	private String city;

	@NotBlank
	private String state;

	@NotBlank
	private String pinCode;

	@NotBlank
	private String country;

}
