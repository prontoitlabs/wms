package com.smart.logistic.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.smart.logistic.domain.enums.Role;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = { "password", "otpToken" })
@Entity(name = "wms_user")
@NoArgsConstructor
@Data
@AllArgsConstructor
public class User extends AbstractEntity {

	private static final long serialVersionUID = 8130937494411040875L;

	@Column(unique = true)
	private String email;

	private String password;

	private String phoneNumber;

	private String mobileNumber;

	private String name;

	@Enumerated(EnumType.STRING)
	private Role role;

	@Column
	@ElementCollection(targetClass = String.class)
	@Builder.Default
	private List<String> invalidTokens = new ArrayList<String>();

	private String otpToken;

}
