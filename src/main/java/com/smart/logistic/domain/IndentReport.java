package com.smart.logistic.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "indent_report")
@Table(indexes = { @Index(name = "INDENT_INDX_0", columnList = "fromDate") })
public class IndentReport extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	private String sku;

	private String name;

	private Integer totalDispatchedItem;
	
	private Date fromDate;

	private Date tillDate;
}
