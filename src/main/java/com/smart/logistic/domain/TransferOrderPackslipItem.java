package com.smart.logistic.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "transfer_order_packslip_item")
public class TransferOrderPackslipItem extends AbstractEntity {

	private static final long serialVersionUID = -8178118573714965792L;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "transferOrderPackslipId", nullable = false)
	private TransferOrderPackslip transferOrderPackslip;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "productId", nullable = false)
	private Product product;

	private Integer pendingCases;

	private Integer reservedCases;

	private Integer doneCases;

	private Double packslipQuantity;

}
