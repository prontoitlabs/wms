package com.smart.logistic.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.smart.logistic.domain.enums.OrderFulfillmentType;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.RamcoOrderStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.BatchSize;

@Builder
@EqualsAndHashCode(callSuper = true, exclude = "packslips")
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@Data
@AllArgsConstructor
@Entity(name = "sales_order")
@Table(indexes = { @Index(name = "SALES_ORDER_NUMBER_INDX_0", columnList = "orderNumber") })
public class SalesOrder extends AbstractEntity {

	private static final long serialVersionUID = 8257385299956220239L;

	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "customerId")
	private Customer customer;

	@NotNull
	@Column(unique = true)
	private String orderNumber;

	@OneToMany(mappedBy = "salesOrder", cascade = CascadeType.ALL)
	@BatchSize(size = 50)
	private List<SalesOrderDetails> salesOrderDetails;

	@Enumerated(EnumType.STRING)
	private OrderStatus status;

	private Date orderDate;

	private BigDecimal untaxedAmount;

	private BigDecimal taxes;

	private BigDecimal grandTotal;
	
	private OrderFulfillmentType orderFulfillmentType;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@BatchSize(size = 50)
	@JoinTable(name = "salesorder_packslips", joinColumns = {
			@JoinColumn(name = "salesorder_id") }, inverseJoinColumns = { @JoinColumn(name = "packslip_id") })
	@Builder.Default
	private Set<SalesOrderPackslip> packslips = new HashSet<SalesOrderPackslip>();
	
	@Enumerated(EnumType.STRING)
	private RamcoOrderStatus ramcoOrderStatus;
	
}
