package com.smart.logistic.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.BatchSize;

@Builder
@Data
@EqualsAndHashCode(callSuper = true, exclude = {"salesOrder", "orderPackslip"})
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "order_packslip_item")
public class SalesOrderPackslipItem extends AbstractEntity {

	private static final long serialVersionUID = -8178118573714965792L;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "orderPackslipId", nullable = false)
	private SalesOrderPackslip orderPackslip;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "orderId", nullable = false)
	private SalesOrder salesOrder;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "productId", nullable = false)
	private Product product;

	private Integer packslipQuantity;

	private Integer verifiedCases;

	@Enumerated(EnumType.STRING)
	private UnitsOfMeasurement unitOfMeasure;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "orderPackslipItem")
	@BatchSize(size = 50)
	@Builder.Default
	private List<SalesOrderPackSlipDispatchDetails> packslipDispatchDetails = new ArrayList<SalesOrderPackSlipDispatchDetails>();

}
