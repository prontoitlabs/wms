package com.smart.logistic.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@Data
@AllArgsConstructor
@Entity(name = "inventory")
@Table(indexes = { @Index(name = "INVENTORY_INDX_0", columnList = "scanBarcode") })
public class Inventory extends AbstractEntity {
	
	private static final long serialVersionUID = -9026027577074281943L;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "productId", nullable = false)
	private Product product;
	
	private Double quantity;

	@Enumerated(EnumType.STRING)
	private UnitsOfMeasurement unitOfMeasure;
	
	@Column(unique = true)
	private String scanBarcode;

	private String batchNumber;

	private String palletBarcode;

	private Date inventoryDate;
	
	private Double netWeight;
	
	private Double grossWeight;
	
	@Enumerated(EnumType.STRING)
	private OrderStatus orderStatus;
}
