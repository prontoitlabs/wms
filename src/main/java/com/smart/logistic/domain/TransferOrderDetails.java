package com.smart.logistic.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "transfer_order_details")
public class TransferOrderDetails extends AbstractEntity {

	private static final long serialVersionUID = 8768210390857234351L;

	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "transferOrderId")
	private TransferOrder transferOrder;

	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "productId")
	private Product product;

	private Double transferQuantity;

	@Enumerated(EnumType.STRING)
	private UnitsOfMeasurement unitsOfMeasure;
	

}
