package com.smart.logistic.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "deviation_report")
public class DeviationReport extends AbstractEntity{

	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "individualSalesOrderDeviationReportId")
	private IndividualSalesOrderDeviationReport individualSalesOrderDeviationReport;
	
	private String productSku;
	
	private String productName;
	
	private String productDescription;
	
	private Integer pendingQuantity;

	private Integer availableQuantity;
	
	@Builder.Default
	private Integer deviatedBy  = 0;
	
}
