package com.smart.logistic.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.BatchSize;

@Builder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@Data
@AllArgsConstructor
@Entity(name = "individual_sales_order_deviation_report")
public class IndividualSalesOrderDeviationReport extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "salesOrderId")
	private SalesOrder salesOrder;
	
	@OneToMany(mappedBy = "individualSalesOrderDeviationReport", cascade = CascadeType.ALL)
	@BatchSize(size = 50)
	private List<DeviationReport> listOfDeviationReport;
	
	private Date generatedDateTime;

}
