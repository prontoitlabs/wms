package com.smart.logistic.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.smart.logistic.domain.enums.OrderStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@EqualsAndHashCode(callSuper = true, exclude = {"product", "orderPackslipItem"})
@Data
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "packslip_dispatch_detail")
public class SalesOrderPackSlipDispatchDetails extends AbstractEntity {

	private static final long serialVersionUID = -8665386088302623338L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "orderPackslipItemId", nullable = false)
	private SalesOrderPackslipItem orderPackslipItem;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "productId", nullable = false)
	private Product product;

	@Column(unique = true)
	private String scanBarcode;
	
	private Date scanDate;

	private String batchNumber;

	private String palletBarcode;

	@Enumerated(EnumType.STRING)
	private OrderStatus status;
	
	private Double netWeight;

	private Double grossWeight;


}
