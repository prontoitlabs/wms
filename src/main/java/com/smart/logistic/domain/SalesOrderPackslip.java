package com.smart.logistic.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.BatchSize;

@Builder
@Data
@EqualsAndHashCode(callSuper = true, exclude = "salesOrders")
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "order_packslip")
@Table(indexes = { @Index(name = "SALES_ORDER_PACKSLIP_NUMBER_INDX_0", columnList = "packslipNumber") })
public class SalesOrderPackslip extends AbstractEntity {

	private static final long serialVersionUID = -3709029944329546520L;

	@Column(unique = true)
	private String packslipNumber;
	
	@NotNull
	private Date packslipDate;

	private String vehicleNumber;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "packslips")
	@BatchSize(size = 50)
	@Builder.Default
	private Set<SalesOrder> salesOrders = new HashSet<SalesOrder>();

	private Integer verifiedCases;

	private Integer packslipQuantity;

	@Enumerated(EnumType.STRING)
	private UnitsOfMeasurement unitOfMeasure;

	@Enumerated(EnumType.STRING)
	private OrderStatus orderStatus;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval=true, fetch = FetchType.LAZY, mappedBy = "orderPackslip")
	@BatchSize(size = 50)
	@Builder.Default
	private List<SalesOrderPackslipItem> orderPackslipItems = new ArrayList<SalesOrderPackslipItem>();

}
