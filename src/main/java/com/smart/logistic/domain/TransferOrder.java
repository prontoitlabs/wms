package com.smart.logistic.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.smart.logistic.domain.enums.OrderStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.BatchSize;

@Builder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@Data
@AllArgsConstructor
@Entity(name = "transfer_order")
@Table(indexes = { @Index(name = "TRANSFER_ORDER_NUMBER_INDX_0", columnList = "trasferOrderNumber") })
public class TransferOrder extends AbstractEntity {

	private static final long serialVersionUID = 968228170687821246L;

	@NotNull
	@Column(unique = true)
	private String trasferOrderNumber;

	@OneToMany(mappedBy = "transferOrder", cascade=CascadeType.ALL)
	@BatchSize(size = 50)
	private List<TransferOrderDetails> transferOrderDetails;

	@Enumerated(EnumType.STRING)
	private OrderStatus status;

	private Date transferOrderDate;
	
	@NotNull
	private String ramcoOrderNumber;
	
}
