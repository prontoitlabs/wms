package com.smart.logistic.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.hibernate.validator.constraints.NotBlank;

import com.smart.logistic.domain.enums.LocationType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "location")
public class Location extends AbstractEntity{

	private static final long serialVersionUID = -2206903881346413857L;
	
	@NotBlank
	private String locationName;

	private String email;
	
	private String websiteUrl;

	private String gstn;

	private String language;
	
	@NotBlank
	private String addressline1;

	private String addressline2;
	
	private String addressline3;

	private String city;

	private String state;

	private String pinCode;

	@NotBlank
	private String country;

	private String mobileNumber;

	private String phoneNumber1;
	
	private String phoneNumber2;
	
	private String faxNumber;
	
	@Builder.Default
	private Boolean isDefault = Boolean.FALSE;
	
	@Enumerated(EnumType.STRING)
	private LocationType locationType;

}
