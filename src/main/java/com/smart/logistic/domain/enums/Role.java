package com.smart.logistic.domain.enums;

public enum Role {
	USER, ADMIN
}
