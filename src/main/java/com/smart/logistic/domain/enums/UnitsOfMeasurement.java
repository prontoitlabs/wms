package com.smart.logistic.domain.enums;

public enum UnitsOfMeasurement {

	CARTONS, PALETTE, KG, POR, GLS, CUP, PLA, PCS, NO, PKT, BOT, CAN, BOX, KGS, NOS;

}
