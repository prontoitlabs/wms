package com.smart.logistic.domain.enums;

public enum AddressType {
	SHIPPING_ADDRESS, BILLING_ADRESS
}
