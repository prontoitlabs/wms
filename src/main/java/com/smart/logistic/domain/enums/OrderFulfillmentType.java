package com.smart.logistic.domain.enums;

public enum OrderFulfillmentType {
	
	CARTON, PALLET;

}
