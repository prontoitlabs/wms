package com.smart.logistic.domain.enums;

import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
public enum ProductType {

	CONSUMABLE_PRODUCT("CONSUMABLE PRODUCTS"), STOCKABLE_PRODUCT("STOCKABLE PRODUCTS"), SERVICES("SERVICES");

	private String productType;

	private ProductType(String productType) {
		this.productType = productType;
	}

}
