package com.smart.logistic.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.smart.logistic.domain.enums.ProductType;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@EqualsAndHashCode(callSuper = true,of = {"sku"})
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@Data
@AllArgsConstructor
@Entity(name = "product")
@Table(indexes = { @Index(name = "PRODUCT_INDX_0", columnList = "sku") })
public class Product extends AbstractEntity {

	private static final long serialVersionUID = -7965015800785438911L;

	@Column(unique = true)
	private String sku;

	private String name;

	@Size(max = 250)
	private String description;
	
	@Size(max = 200)
	private String shortDescription;

	@Enumerated(EnumType.STRING)
	private ProductType productType;

	private String category;

	@Column(unique = true)
	private String barCode;

	@Enumerated(EnumType.STRING)
	private UnitsOfMeasurement salesUnitsOfMeasure;

	private BigDecimal salesPrice;

	private String imageUrl;

	private String hsnsacCode;

	@Enumerated(EnumType.STRING)
	private UnitsOfMeasurement fromUnitsOfMeasurement;

	@Enumerated(EnumType.STRING)
	private UnitsOfMeasurement toUnitsOfMeasurement;

	private Double conversionFactor;
	
	@Builder.Default
	private Integer quantityInHand = 0;
	
	@Enumerated(EnumType.STRING)
	@Builder.Default
	private UnitsOfMeasurement quanitityInHandUnitsOfMeasurement = UnitsOfMeasurement.CARTONS;
	
	private Integer cartonToPacketConversionFactor;
	
	private Double grammage;
	
}
