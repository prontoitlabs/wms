package com.smart.logistic.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.smart.logistic.domain.enums.OrderStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "transfer_order_packslip_dispatch_details")
public class TransferOrderPackSlipDispatchDetails extends AbstractEntity {

	private static final long serialVersionUID = -8665386088302623338L;

	private Date scanDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "transferOrderId", nullable = false)
	private TransferOrder transferOrder;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "transferOrderPackslipId", nullable = false)
	private TransferOrderPackslip transferOrderPackslip;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "productId", nullable = false)
	private Product product;

	@Column(unique = true)
	private String scanBarcode;

	private String batchNumber;

	private String palletBarcode;

	private Double netWeight;

	private Double grossWeight;

	@Enumerated(EnumType.STRING)
	private OrderStatus status;

}
