package com.smart.logistic.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "sales_order_details")
public class SalesOrderDetails extends AbstractEntity {

	private static final long serialVersionUID = 3494212002299796206L;

	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "salesOrderId")
	private SalesOrder salesOrder;

	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "productId")
	private Product product;

	private Double totalOrderQuanityInKG;

	private Integer totalOrderQuanityInCB;

	private Double shippedQuantityInKG;

	private Integer shippedQuantityInCB;

	private Double pendingQuantityInKG;

	private Integer pendingQuantityInCB;

	private Double conversionFactor;

	@Enumerated(EnumType.STRING)
	private UnitsOfMeasurement unitsOfMeasure;

	private BigDecimal unitPrice;

	private BigDecimal subTotal;

	private BigDecimal taxes;

	@Builder.Default
	private Integer actualReservedQuantityInCB = new Integer(0);

	@Builder.Default
	private Double actualReservedQuantityInKG = new Double(0d);

	@Builder.Default
	private Integer actualShippedQuantityInCB = new Integer(0);

	@Builder.Default
	private Double actualShippedQuantityInKG = new Double(0d);

}
