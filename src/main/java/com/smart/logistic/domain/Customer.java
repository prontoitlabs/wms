package com.smart.logistic.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "customer")
@Table(indexes = { @Index(name = "CUSTOMER_INDX_0", columnList = "customerCode") })
public class Customer extends AbstractEntity {

	private static final long serialVersionUID = 5445653742699316731L;
	
	@Column(unique = true)
	private String customerCode;

	@NotBlank
	private String name;
	
	private String customerNameShd;
	
	private String customerLongDescription;

	private String email;

	private String websiteUrl;

	private String gstn;

	private String language;
	
	@NotBlank
	private String addressline1;

	private String addressline2;
	
	private String addressline3;

	private String city;

	private String state;

	private String pinCode;

	@NotBlank
	private String country;

	private String mobileNumber;

	private String phoneNumber1;
	
	private String phoneNumber2;
	
	private String faxNumber;
	
	@Builder.Default
	private Boolean isDefault = Boolean.FALSE;

}
