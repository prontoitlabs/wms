package com.smart.logistic.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.smart.logistic.domain.enums.CategoryType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "category")
public class Category extends AbstractEntity {

	private static final long serialVersionUID = 1941404980909663683L;

	private String name;

	private String parentCategoryId;

	@Enumerated(EnumType.STRING)
	private CategoryType categoryType;

}
