package com.smart.logistic.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.BatchSize;

@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "transfer_order_packslip")
@Table(indexes = { @Index(name = "TRANSFER_ORDER_PACKSLIP_NUMBER_INDX_0", columnList = "packslipNumber") })
public class TransferOrderPackslip extends AbstractEntity {

	private static final long serialVersionUID = -3709029944329546520L;

	@NotNull
	@Column(unique = true)
	private String packslipNumber;

	@NotNull
	private Date packslipDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "transferOrderId", nullable = false)
	private TransferOrder transferOrder;

	private Integer pendingCases;

	private Integer reservedCases;

	private Integer doneCases;

	private Double packslipQuantity;
	
	private String vehicleNumber;

	@Enumerated(EnumType.STRING)
	private UnitsOfMeasurement unitOfMeasure;

	@Enumerated(EnumType.STRING)
	private OrderStatus orderStatus;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "transferOrderPackslip")
	@BatchSize(size = 50)
	@Builder.Default
	private List<TransferOrderPackslipItem> transferOrderPackslipItem = new ArrayList<TransferOrderPackslipItem>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "transferOrderPackslip")
	@Builder.Default
	private List<TransferOrderPackSlipDispatchDetails> transferOrderPackSlipDispatchDetails = new ArrayList<TransferOrderPackSlipDispatchDetails>();


}
