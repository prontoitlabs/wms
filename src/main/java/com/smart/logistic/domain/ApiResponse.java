package com.smart.logistic.domain;

import java.util.Map;

public class ApiResponse {

	private Map<String, String> items;

	private String message;

	private String status;

	public Map<String, String> getItems() {
		return items;
	}

	public void setItems(Map<String, String> items) {
		this.items = items;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ApiResponse [items=" + items + ", message=" + message + ", status=" + status + "]";
	}

}
