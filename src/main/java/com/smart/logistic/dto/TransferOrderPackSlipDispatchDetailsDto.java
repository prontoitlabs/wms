package com.smart.logistic.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.smart.logistic.domain.enums.OrderStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class TransferOrderPackSlipDispatchDetailsDto implements Serializable{

	private static final long serialVersionUID = 6219875784895303340L;
	
	private Long id;
	
	private Date scanDate;

	private Long transferOrderId;
	
	private String packslipNumber;

	private Long productId;
	
	private String sku;
	
	private String productName;

	private String barcodePrefix;

	private String scanBarcode;

	private String batchNumber;

	private String palletBarcode;

	private OrderStatus status;
	
	private Double netWeight;

	private Double grossWeight;
	
	private String transferOrderNumber;
	
	
	
	

}
