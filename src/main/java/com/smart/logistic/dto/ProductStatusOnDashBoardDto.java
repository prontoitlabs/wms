package com.smart.logistic.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class ProductStatusOnDashBoardDto  implements Serializable{

	private static final long serialVersionUID = 1L;

	String itemName;
	
	Integer quantityInHand ;

	String sku;
	
	
}
