package com.smart.logistic.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class IndentReportDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String sku;

	private String name;

	private int totalDispatchedItem;

	private Date fromDate;

	private Date tillDate;
}
