package com.smart.logistic.dto;

import java.io.Serializable;

import com.smart.logistic.domain.enums.OrderStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Data
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
public class UpdateStatusDto implements Serializable {

	private static final long serialVersionUID = -8114681062172344306L;

	private Long id;

	private OrderStatus orderStatus;

}
