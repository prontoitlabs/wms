package com.smart.logistic.dto;

import java.io.Serializable;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.smart.logistic.domain.enums.Role;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"password"})
public class UserDto implements Serializable {

	private static final long serialVersionUID = -3208329032331190295L;

	private Long id;

	@NotBlank
	@Email
	private String email;

	@NotBlank
	private Role role;

	private String phoneNumber;

	private String name;
	
	@NotBlank
	private String password;

}
