package com.smart.logistic.dto;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public class EmailDto implements Serializable {
	private static final long serialVersionUID = 5515550314102035640L;

	private String from;

	private String to;

	private List<String> cc;

	private List<String> bcc;

	private String subject;

	private String message;

	private File file;

}
