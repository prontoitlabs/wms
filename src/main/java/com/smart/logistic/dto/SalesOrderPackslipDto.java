package com.smart.logistic.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class SalesOrderPackslipDto implements Serializable{
	
	private static final long serialVersionUID = 6481688824897807534L;
	
	private Long id;

	private String packslipNumber;
	
	private Date packslipDate;

	private String vehicleNumber;
	
	private String customerName;
	
	private Integer verifiedCases;

	private Integer packslipQuantity;
	
	private OrderStatus orderStatus;
	
	private UnitsOfMeasurement unitOfMeasure;
	
	@Builder.Default
	private Set<SalesOrderDto> salesOrdersDto = new HashSet<SalesOrderDto>();

	@Builder.Default
	private List<SalesOrderPackslipItemDto> orderPackSlipItemDto = new ArrayList<SalesOrderPackslipItemDto>();
	
}
