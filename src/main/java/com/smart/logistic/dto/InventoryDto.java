package com.smart.logistic.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class InventoryDto implements Serializable {

	private static final long serialVersionUID = -9026027577074281943L;

	private Long id;

	private Double quantity;

	private UnitsOfMeasurement unitOfMeasure;

	private String scanBarcode;

	private String batchNumber;

	private String palletBarcode;

	private Date inventoryDate;

	private OrderStatus orderStatus;

	private String skuCode;

	private String productName;

	private String productDescription;

	private String transferOrderNumber;

	private Double netWeight;

	private Double grossWeight;
}
