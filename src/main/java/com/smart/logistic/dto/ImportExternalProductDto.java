package com.smart.logistic.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class ImportExternalProductDto implements Serializable {


	private static final long serialVersionUID = 8612921191133741224L;

	private String iou_itemcode;
	
	private Integer iou_ou;
	
	private String ibu_itemtype;
	
	private String iou_effectivefrom;
	
	private String iou_stdwhcode;
	
	private String iou_effectiveto;
	
	private String ibu_category;
	
	private String iou_stdwhcode1;
	
	private String loi_itemdesc;
	
	private String TaxGroup;
	
	private String HSNCode;
	
	private String ItemUom;
	
	private Double KG_CB_Con_Fact;
	
	private Double NO_CB_Con_Fact;
	
	private Double PKT_CB_Con_Fact;
	
	private Double PCS_CB_Con_Fact;
	
	private Double NOS_KG_Con_Fact;
	
	private Double Grammage;

}
