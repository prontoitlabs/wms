package com.smart.logistic.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.smart.logistic.domain.enums.OrderStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class TransferOrderPackslipDto implements Serializable{
	
	private static final long serialVersionUID = 6481688824897807534L;
	
	private Long id;

	private Date packslipDate;
	
	private Long transferOrderId;

	private OrderStatus orderStatus;
	
	private String transferOrderNumber;
	
	private String packslipNumber;
	
	private Integer pendingCases;

	private Integer reservedCases;

	private Integer doneCases;

	private Double packslipQuantity;
	
	private String vehicleNumber;
	
	private Date transferOrderDate;
	
	private List<TransferOrderPackslipItemDto> transferOrderPackSlipItemDto;
	
}
