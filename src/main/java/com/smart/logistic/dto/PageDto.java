package com.smart.logistic.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class PageDto<T> {
	private Integer currentPage;

	private Integer currentPageSize;

	private Integer totalPages;

	private Long totalElements;

	private List<T> content = new ArrayList<T>();

	public PageDto() {
	    super();
	  }

	public PageDto(Integer currentPage, Integer currentPageSize, Integer totalPages, Long totalElements, List<T> content) {
	    super();
	    this.currentPage = currentPage;
	    this.currentPageSize = currentPageSize;
	    this.totalPages = totalPages;
	    this.totalElements = totalElements;
	    this.content = content;
	  }
}
