package com.smart.logistic.dto;

import java.io.Serializable;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = { "password" })
public class LoginDto implements Serializable {

	private static final long serialVersionUID = 5174233776838220095L;

	@NotBlank
	@Email
	private String email;

	@NotBlank
	private String password;

}
