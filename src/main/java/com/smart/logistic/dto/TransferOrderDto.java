package com.smart.logistic.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.smart.logistic.domain.enums.OrderStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class TransferOrderDto implements Serializable {

	private static final long serialVersionUID = -8214362420727550626L;

	private Long id;

	private String transferOrderNumber;

	private List<TransferOrderDetailsDto> transferOrderDetailsDto;

	private OrderStatus orderStatus;

	private Date transferOrderDate;

	private Double transferQuantity;
	
	private String ramcoOrderNumber;
	
}
