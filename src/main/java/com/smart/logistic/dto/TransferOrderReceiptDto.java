package com.smart.logistic.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class TransferOrderReceiptDto implements Serializable {

	private static final long serialVersionUID = 6481688824897807534L;

	private TransferOrderDto transferOrderDto;

	private List<TransferOrderPackslipDto> transferOrderPackslipDto;

	private Date transferOrderDate;

	private Double transferQuantity;
	
	private Integer doneCases;

	private String fromLocation;

	private String toLocation;

}
