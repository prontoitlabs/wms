package com.smart.logistic.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@ToString(callSuper = true)
public class IndividualSalesOrderDeviationReportDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private SalesOrderDto salesOrder;
	
	private List<DeviationReportDto> listOfDeviationReport;
	
}
