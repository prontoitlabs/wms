package com.smart.logistic.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.smart.logistic.domain.enums.ProductType;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@Data
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ProductDto implements Serializable {

	private static final long serialVersionUID = 4206239383419521082L;

	private String sku;

	private String name;

	private String description;
	
	private String shortDescription;
	
	private String category;

	private ProductType productType;

	private String barCode;

	private UnitsOfMeasurement salesUnitsOfMeasure;

	private BigDecimal salesPrice;

	private String imageUrl;

	private String hsnsacCode;

	private UnitsOfMeasurement fromUnitsOfMeasurement;

	private UnitsOfMeasurement toUnitsOfMeasurement;

	private Double conversionFactor;
	
	@Builder.Default
	private Integer quantityInHand = 0;
	
	@Builder.Default
	private UnitsOfMeasurement quanitityInHandUnitsOfMeasurement = UnitsOfMeasurement.CARTONS;
	
	private Integer cartonToPacketConversionFactor;

}
