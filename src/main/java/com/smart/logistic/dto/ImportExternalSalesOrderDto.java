package com.smart.logistic.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.smart.logistic.domain.enums.RamcoOrderStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class ImportExternalSalesOrderDto implements Serializable {


	private static final long serialVersionUID = 8612921191133741224L;

	private String OU_ID;

	private String Order_No;

	private String Order_Date;

	private String Cust_Code;

	private String clo_cust_name;

	private String Sales_Type;

	private String itemcode;

	private String ItemDescription;

	private String ItemShortDesc;

	private String ItemUOM;

	private double Pending_Qty;

	private double Rem_Qty_in_CB;

	private double Shipped_Qty;

	private double Shp_Qty_in_CB;

	private double Order_Qty;

	private double Req_Qty_in_CB;

	private double KG_CB_Con_Fact;
	
	private double Unit_Price;
	
	private double Total_Price;
	
	private double Tax_Amount;
	
	private RamcoOrderStatus Order_Status;

}
