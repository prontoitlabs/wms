package com.smart.logistic.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class SalesOrderDetailsDto implements Serializable {
	
	private static final long serialVersionUID = -834219122798362975L;
	
	private Long id;

	private Double totalOrderQuanityInKG;

	private Integer totalOrderQuanityInCB;

	private Double shippedQuantityInKG;
	
	private Integer shippedQuantityInCB;
	
	private Double pendingQuantityInKG;
	
	private Integer pendingQuantityInCB;
	
	private Double conversionFactor;

	private UnitsOfMeasurement unitsOfMeasure;

	private BigDecimal unitPrice;

	private BigDecimal subTotal;

	private BigDecimal taxes;
	
	private ProductDto productDto;
	
	private Integer availableQuantity;
	
	private Integer reservedQuantity;
	
	private Integer actualReservedQuantityInCB;
	
	private Double actualReservedQuantityInKG;

	private Integer actualShippedQuantityInCB;
	
	private Double actualShippedQuantityInKG;
	
	
}
