package com.smart.logistic.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public class VehicleAllocationDto implements Serializable {

	private static final long serialVersionUID = -3023983366822303532L;
	
	private String vehicleNumber;
	
	private List<String> salesOrderNumbers;
	
	private Integer totalPackslipQuantityInCartons;

}
