package com.smart.logistic.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class SalesOrderPackslipItemDto implements Serializable{

	private static final long serialVersionUID = -3151132651397972026L;
	
	private Long id;
	
	private String sku;
	
	private String productName;
	
	private Integer initialDemand;
	
	private Integer packslipQuantity;
	
	private Integer verifiedCases;
	
	private UnitsOfMeasurement unitOfMeasure;
	
	private Integer availableQuantity;
	
	private String salesOrderNumber;

}
