package com.smart.logistic.dto;

import java.io.Serializable;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.smart.logistic.domain.enums.LocationType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class LocationDto implements Serializable{

	
	private static final long serialVersionUID = 2659679245465422048L;

	private String locationName;

	private String email;
	
	private String websiteUrl;

	private String gstn;

	private String language;
	
	private String addressline1;

	private String addressline2;
	
	private String addressline3;

	private String city;

	private String state;

	private String pinCode;

	private String country;

	private String mobileNumber;

	private String phoneNumber1;
	
	private String phoneNumber2;
	
	private String faxNumber;
	
	@Builder.Default
	private Boolean isDefault = Boolean.FALSE;
	
	@Enumerated(EnumType.STRING)
	private LocationType locationType;

	
}
