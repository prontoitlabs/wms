package com.smart.logistic.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public class AuthDto implements Serializable {

	private static final long serialVersionUID = -2821719913606337268L;

	private String token;

	private UserDto user;

}
