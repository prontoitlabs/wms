package com.smart.logistic.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class TransferOrderDetailsDto implements Serializable {
	
	private static final long serialVersionUID = -834219122798362975L;

	private Double transferQuantity;

	private UnitsOfMeasurement unitsOfMeasure;

	private ProductDto productDto;
}
