package com.smart.logistic.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@Builder
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public class DeviationReportDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String productSku;
	
	private String productName;
	
	private String productDescription;
	
	private Integer pendingQuantity;

	private Integer availableQuantity;
	
	private Integer deviatedBy;
	
}
