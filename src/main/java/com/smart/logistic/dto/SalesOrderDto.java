package com.smart.logistic.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.smart.logistic.domain.Customer;
import com.smart.logistic.domain.enums.OrderFulfillmentType;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.RamcoOrderStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
@JsonInclude(Include.NON_NULL)
public class SalesOrderDto implements Serializable {

	private static final long serialVersionUID = 6323731285691366130L;
	
	private Long id;
	
	private String orderNumber;
	
	private Customer customer;
	
	private List<SalesOrderDetailsDto> salesOrderDetails;
	
	private OrderStatus status;

	private Date orderDate;

	private BigDecimal untaxedAmount;

	private BigDecimal taxes;

	private BigDecimal grandTotal;
	
	private Double totalOrderQuanityInKG;

	private Integer totalOrderQuanityInCB;

	private Double shippedQuantityInKG;
	
	private Integer shippedQuantityInCB;
	
	private Double pendingQuantityInKG;
	
	private Integer pendingQuantityInCB;
	
	private Integer totalReservedQuantity;
	
	private Integer totalPendingQuantity;
	
	private OrderFulfillmentType orderFulfillmentType;
	
	private RamcoOrderStatus ramcoOrderStatus;
	
	private Integer actualReservedQuantityInCB;
	
	private Double actualReservedQuantityInKG ;
	
	private Integer actualShippedQuantityInCB;
	
	private Double actualShippedQuantityInKG;

}
