package com.smart.logistic.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.smart.logistic.domain.enums.OrderStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class SalesOrderPackSlipDispatchDetailsDto implements Serializable{

	private static final long serialVersionUID = 2327872480601176507L;
	
	private Long id;

	private SalesOrderPackslipItemDto orderPackslipItemDto;

	private String barcodePrefix;

	private String scanBarcode;

	private Date scanDate;
	
	private String batchNumber;

	private String palletBarcode;

	private String pouchCode;
	
	private OrderStatus status;
	
	private Double netWeight;

	private Double grossWeight;
}
