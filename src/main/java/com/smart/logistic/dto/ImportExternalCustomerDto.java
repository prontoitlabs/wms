package com.smart.logistic.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class ImportExternalCustomerDto implements Serializable {


	private static final long serialVersionUID = 8612921191133741224L;

	private String clo_cust_code;
	
	private String clo_cust_name;
	
	private String clo_cust_name_shd;
	
	private String clo_addrline1;
	
	private String clo_addrline2;
	
	private String clo_addrline3;
	
	private String clo_city;
	
	private String clo_state;
	
	private String clo_country;
	
	private String clo_zip;
	
	private String clo_phone1;
	
	private String clo_phone2;
	
	private String clo_mobile;
	
	private String clo_fax;
	
	private String clo_email;
	
	private String clo_account_group_code;

}
