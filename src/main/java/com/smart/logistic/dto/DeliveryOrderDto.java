package com.smart.logistic.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class DeliveryOrderDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7712031752252285661L;

	private Long id;

	private String orderNumber;

	private String customerCode;

	private String toLocation;

	private String fromLocation;
	
	private Double totalOrderQuanityInKG;

	private Integer totalOrderQuanityInCB;

	private Double shippedQuantityInKG;

	private Integer shippedQuantityInCB;

	private Double pendingQuantityInKG;

	private Integer pendingQuantityInCB;
	

}
