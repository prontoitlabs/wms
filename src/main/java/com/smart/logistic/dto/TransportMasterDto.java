package com.smart.logistic.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;



@Builder
@Data
@ToString(callSuper = true, exclude = {})
@NoArgsConstructor
@AllArgsConstructor
public class TransportMasterDto implements Serializable {

	private static final long serialVersionUID = 1L;


	private String name;
	
	private String vehicleNo;
	
	private String vehicleType;
	
	private String email;
	
	private String phoneNumber;

	private String mobileNumber;
	
	private String gstn;
	
	private String destination;
	
	private String source;
	
	private String addressline1;

	private String addressline2;
	
	private String addressline3;

	private String city;

	private String state;

	private String pinCode;

	private String country;

}
