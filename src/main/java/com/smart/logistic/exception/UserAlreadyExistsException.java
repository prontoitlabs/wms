package com.smart.logistic.exception;

import org.springframework.http.HttpStatus;

public class UserAlreadyExistsException extends WmsException {

	private static final long serialVersionUID = 3642770990168988049L;

	private static final String DEFAULT_MESSAGE = "User already exists !";

	private static final HttpStatus DEFAULT_HTTP_STATUS = HttpStatus.CONFLICT;

	public UserAlreadyExistsException() {
		this(DEFAULT_HTTP_STATUS, DEFAULT_MESSAGE);
	}

	public UserAlreadyExistsException(HttpStatus httpStatus, String message) {
		super(httpStatus, DEFAULT_HTTP_STATUS, message, DEFAULT_MESSAGE);
	}

	public UserAlreadyExistsException(String message) {
		this(DEFAULT_HTTP_STATUS, message);
	}

}
