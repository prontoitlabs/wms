package com.smart.logistic.exception;

import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

public class WmsException extends Exception {

	private static final long serialVersionUID = 3763450069018457566L;

	public static final HttpStatus DEFAULT_HTTP_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

	public static final String DEFAULT_MESSAGE = "Something Bad Happened !";

	private HttpStatus httpStatus;

	private String message;

	public WmsException(HttpStatus httpStatus, String message) {
		this(httpStatus, DEFAULT_HTTP_STATUS, message, DEFAULT_MESSAGE);
	}

	public WmsException(HttpStatus httpStatus, HttpStatus defaultStatus, String message, String defaultMessage) {
		super();
		this.httpStatus = httpStatus == null ? defaultStatus : httpStatus;
		this.message = StringUtils.isEmpty(message) ? defaultMessage : message;
	}

	public WmsException(String message) {
		this(DEFAULT_HTTP_STATUS, message);
	}

	public WmsException() {
		this(DEFAULT_HTTP_STATUS, DEFAULT_MESSAGE);
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	protected void setData(HttpStatus status, String message) {
		if (httpStatus == null) {
			setHttpStatus(status);
		}
		if (StringUtils.isEmpty(message)) {
			setMessage(message);
		}
	}

}
