package com.smart.logistic.exception;

import org.springframework.http.HttpStatus;

public class UserNotFoundException extends WmsException {

	private static final long serialVersionUID = 1455288609270613866L;

	private static final String DEFAULT_MESSAGE = "User not found !";

	private static final HttpStatus DEFAULT_HTTP_STATUS = HttpStatus.NOT_FOUND;

	public UserNotFoundException() {
		this(DEFAULT_HTTP_STATUS, DEFAULT_MESSAGE);
	}

	public UserNotFoundException(HttpStatus httpStatus, String message) {
		super(httpStatus, DEFAULT_HTTP_STATUS, message, DEFAULT_MESSAGE);
	}

	public UserNotFoundException(String message) {
		this(DEFAULT_HTTP_STATUS, message);
	}

}
