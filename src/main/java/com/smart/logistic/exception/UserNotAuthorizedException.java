package com.smart.logistic.exception;

import org.springframework.http.HttpStatus;

public class UserNotAuthorizedException extends WmsException {

	private static final long serialVersionUID = -3561580853594023742L;

	private static final String DEFAULT_MESSAGE = "User is not authorized !";

	private static final HttpStatus DEFAULT_HTTP_STATUS = HttpStatus.UNAUTHORIZED;

	public UserNotAuthorizedException() {
		this(DEFAULT_HTTP_STATUS, DEFAULT_MESSAGE);
	}

	public UserNotAuthorizedException(HttpStatus httpStatus, String message) {
		super(httpStatus, DEFAULT_HTTP_STATUS, message, DEFAULT_MESSAGE);
	}

	public UserNotAuthorizedException(String message) {
		this(DEFAULT_HTTP_STATUS, message);
	}

}
