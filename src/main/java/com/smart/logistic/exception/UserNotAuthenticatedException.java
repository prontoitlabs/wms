package com.smart.logistic.exception;

import org.springframework.http.HttpStatus;

public class UserNotAuthenticatedException extends WmsException {

	private static final long serialVersionUID = 763801764959412956L;

	private static final String DEFAULT_MESSAGE = "User not authenticated !";

	private static final HttpStatus DEFAULT_HTTP_STATUS = HttpStatus.FORBIDDEN;

	public UserNotAuthenticatedException() {
		this(DEFAULT_HTTP_STATUS, DEFAULT_MESSAGE);
	}

	public UserNotAuthenticatedException(HttpStatus httpStatus, String message) {
		super(httpStatus, DEFAULT_HTTP_STATUS, message, DEFAULT_MESSAGE);
	}

	public UserNotAuthenticatedException(String message) {
		this(DEFAULT_HTTP_STATUS, message);
	}

}
