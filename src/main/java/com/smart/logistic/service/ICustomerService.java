package com.smart.logistic.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;

import com.smart.logistic.domain.Customer;
import com.smart.logistic.dto.CustomerDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.generic.IGenericService;

public interface ICustomerService extends IGenericService<Customer>{
	
	List<Customer> importCustomerMaster() throws WmsException;
	
	Page<Customer> findingAllCustomer(Integer pageNumber, Integer pageSize) throws WmsException;

	Map<String, CustomerDto> getAllCustomersForPendingDeliveryOrders() throws WmsException;

	Customer findCustomerByCode(String cust_Code) throws WmsException;
	
	Page<Customer> findTopTenCustomer() throws WmsException;

	List<Customer> importAllCustomersFromRamco() throws WmsException;

}
