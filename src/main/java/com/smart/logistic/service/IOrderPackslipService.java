package com.smart.logistic.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import com.smart.logistic.domain.SalesOrderPackslip;
import com.smart.logistic.domain.SalesOrderPackslipItem;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.dto.SalesOrderPackslipDto;
import com.smart.logistic.dto.VehicleAllocationDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.generic.IGenericService;

public interface IOrderPackslipService extends IGenericService<SalesOrderPackslip> {

	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 * @throws WmsException
	 */

	Page<SalesOrderPackslip> getAllPackingSlips(Integer pageNumber, Integer pageSize) throws WmsException;

	SalesOrderPackslip validateSalesOrderPackSlip(Long packslipId) throws WmsException;

	SalesOrderPackslipDto createPackSlip() throws WmsException;


	SalesOrderPackslipDto savePackSlip(SalesOrderPackslipDto orderPackslipDto) throws WmsException;

	SalesOrderPackslip allocateInventoryForOrderPackslip(Long id) throws WmsException;


	SalesOrderPackslipDto editPackSlip(SalesOrderPackslipDto orderPackslipDto) throws WmsException;


	List<SalesOrderPackslip> allocateVehicle(List<VehicleAllocationDto> vehiclesAlloted) throws WmsException;


	List<SalesOrderPackslipItem> getAllPendingItemsFromSalesOrders(List<Long> ids) throws WmsException;

	List<SalesOrderPackslip> findByLastModifiedBetweenAndOrderStatus(Date startDate,Date endDate,OrderStatus orderStatus);


	SalesOrderPackslip importHSTDataForPackslip(Long packSlipId) throws WmsException;

	Boolean deletePackSlip(Long packslipId) throws WmsException;
	
}
