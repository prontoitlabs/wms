package com.smart.logistic.service;

import com.smart.logistic.exception.WmsException;

public interface IDataBuilderService{
	/**
	 * 
	 * @param id
	 * @param user
	 * @return
	 * @throws WmsException 
	 */
	Object createDummyData() throws WmsException;
	
}
