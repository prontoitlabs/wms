package com.smart.logistic.service.impl;

import java.io.StringWriter;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.logistic.dto.EmailDto;
import com.smart.logistic.service.EmailService;
import com.smart.logistic.util.EmailUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EmailServiceImpl implements EmailService {

	@Autowired
	private VelocityEngine velocityEngine;
	
	@Autowired
	private EmailUtils emailUtils;

	@Override
	public String send(EmailDto emailDto) {
		String getTemplate = getTemplate();
		emailDto.setMessage(getTemplate);
		return emailUtils.sendMailWithAttachment(emailDto);
	}

	private String getTemplate() {
		log.info("inside getTemplate  method");
		velocityEngine.init();
		Template template = null;
		try {
			template = velocityEngine.getTemplate("templates/email.vm");
		} catch (Exception e) {
			log.error("inside getTemplate exceptions" + e);
		}
		if (template == null) {
			return null;
		}
		VelocityContext context = new VelocityContext();
		StringWriter writer = new StringWriter();
		template.merge(context, writer);
		return writer.toString();
	}
}
