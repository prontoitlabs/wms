package com.smart.logistic.service.impl;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.ITransferOrderService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FileProcessorService {

	@Autowired
	private ITransferOrderService transferOrderService;
	
	@Value(value = "${destination.csv.path}")
	private String destinationStockInFilePath;

	public void process(Message<File> msg) throws IOException, WmsException {
		File destinationDirectory = new File(destinationStockInFilePath);
		if (! destinationDirectory.exists()){
			destinationDirectory.mkdir();
	    }
		File file = msg.getPayload();
		try {
			transferOrderService.importTransferOrdersFromCsv(file);
		} catch (WmsException wmsException) {
			if(wmsException.getHttpStatus().equals(HttpStatus.ALREADY_REPORTED)) {
				if(file.exists()) {
					boolean isRemoved = file.renameTo(new File(destinationDirectory + "/" + file.getName()));
					log.info("Removing The File in case of Duplicate  :" + isRemoved);
				}
			}
			throw wmsException;
		}
		
		if(file.exists()) {
			log.info("Moving The File in case of Successful Import");
			file.renameTo(new File(destinationDirectory + "/" + file.getName()));
		}
		log.info("getting file from directory :" + file);
	}
}
