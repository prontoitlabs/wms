package com.smart.logistic.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.TransferOrder;
import com.smart.logistic.domain.TransferOrderDetails;
import com.smart.logistic.domain.TransferOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.TransferOrderPackslip;
import com.smart.logistic.domain.TransferOrderPackslipItem;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.ITransferOrderPackslipRepository;
import com.smart.logistic.repository.ITransferOrderRepository;
import com.smart.logistic.service.IDataBuilderService;
import com.smart.logistic.service.ILocationService;
import com.smart.logistic.service.IProductService;

@Service
public class DataBuilderServiceImpl implements IDataBuilderService {

	@Autowired
	private ITransferOrderRepository iTransferOrderRepository;

	@Autowired
	private ITransferOrderPackslipRepository iTransferOrderPackslipRepository;

	@Autowired
	private IProductService productService;

	@Autowired
	ILocationService locationService;

	@Override
	public Object createDummyData() throws WmsException {

		List<Product> products = productService.findAll();

		List<TransferOrder> transferOrders = createTransferOrder(products);

		createTransferOrderPackslip(transferOrders, products);

		return null;

	}
	
	// Create Sample TransferOrder
	private List<TransferOrder> createTransferOrder(List<Product> products) {
		List<TransferOrder> listOfTransferOrder = new ArrayList<TransferOrder>();
		for (int i = 0; i < 100; i++) {
			TransferOrder transferOrder = new TransferOrder();
			transferOrder.setTrasferOrderNumber("TRA/" + UUID.randomUUID() + "/17-18");
			transferOrder.setTransferOrderDetails(createTransferOrderDetails(transferOrder, products.get(i % 24)));
			transferOrder.setStatus(OrderStatus.RESERVED);
			transferOrder.setTransferOrderDate(DateTime.now().toDate());
			transferOrder.setRamcoOrderNumber("SOTC/" + UUID.randomUUID() + "/17-18");
			listOfTransferOrder.add(transferOrder);
		}
		return iTransferOrderRepository.save(listOfTransferOrder);
	}

	// creating Sample Transfer Order Detail
	private List<TransferOrderDetails> createTransferOrderDetails(TransferOrder transferOrder, Product product) {
		List<TransferOrderDetails> listOfTransferOrderDetails = new ArrayList<TransferOrderDetails>();

		TransferOrderDetails transferOrderDetails = new TransferOrderDetails();
		transferOrderDetails.setTransferOrder(transferOrder);
		transferOrderDetails.setProduct(product);
		transferOrderDetails.setTransferQuantity(10.0);
		transferOrderDetails.setUnitsOfMeasure(UnitsOfMeasurement.CARTONS);
		listOfTransferOrderDetails.add(transferOrderDetails);

		return listOfTransferOrderDetails;

	}

	// create TransferOrder Packslip
	private List<TransferOrderPackslip> createTransferOrderPackslip(List<TransferOrder> transferOrders,
			List<Product> products) {
		List<TransferOrderPackslip> transferOrderPackslips = new ArrayList<TransferOrderPackslip>();
		int i = 0;
		for (TransferOrder transferOrder : transferOrders) {
			TransferOrderPackslip transferOrderPackslip = new TransferOrderPackslip();
			transferOrderPackslip.setPackslipNumber("PKSC/" + UUID.randomUUID() + "/1718");
			transferOrderPackslip.setPackslipDate(DateTime.now().toDate());
			transferOrderPackslip.setTransferOrder(transferOrder);
			transferOrderPackslip.setPendingCases(10);
			transferOrderPackslip.setReservedCases(10);
			transferOrderPackslip.setDoneCases(0);
			transferOrderPackslip.setPackslipQuantity(10.0);
			transferOrderPackslip.setUnitOfMeasure(UnitsOfMeasurement.CARTONS);
			transferOrderPackslip.setOrderStatus(OrderStatus.WAITING);
			transferOrderPackslip.setVehicleNumber("DL4p879");
			Product product = products.get(i % 24);
			transferOrderPackslip.setTransferOrderPackslipItem(
					createTransferOrderPackslipItem(transferOrderPackslip, product, transferOrder));
			transferOrderPackslip.setTransferOrderPackSlipDispatchDetails(createTransferOrderPackSlipDispatchDetails(transferOrderPackslip, product, transferOrder));
			transferOrderPackslips.add(transferOrderPackslip);
			i++;
		}

		return iTransferOrderPackslipRepository.save(transferOrderPackslips);
	}

	// create Transfer Order Pack slip Item
	private List<TransferOrderPackslipItem> createTransferOrderPackslipItem(TransferOrderPackslip transferOrderPackslip,
			Product product, TransferOrder transferOrder) {
		List<TransferOrderPackslipItem> transferOrderPackslipItems = new ArrayList<TransferOrderPackslipItem>();
		TransferOrderPackslipItem transferOrderPackslipItem = new TransferOrderPackslipItem();
		transferOrderPackslipItem.setTransferOrderPackslip(transferOrderPackslip);
		transferOrderPackslipItem.setPendingCases(10);
		transferOrderPackslipItem.setReservedCases(10);
		transferOrderPackslipItem.setDoneCases(0);
		transferOrderPackslipItem.setPackslipQuantity(10.0);
		transferOrderPackslipItem.setProduct(product);
		transferOrderPackslipItems.add(transferOrderPackslipItem);
		return transferOrderPackslipItems;
	}

	// create TransferOrder PackSlipDispatch Details
	private List<TransferOrderPackSlipDispatchDetails> createTransferOrderPackSlipDispatchDetails(
			TransferOrderPackslip transferOrderPackslip, Product product, TransferOrder transferOrder) {
		List<TransferOrderPackSlipDispatchDetails> listOfTransferOrderPackSlipDispatchDetails = new ArrayList<TransferOrderPackSlipDispatchDetails>();
		for (int i = 0; i < 10; i++) {
			TransferOrderPackSlipDispatchDetails transferOrderPackSlipDispatchDetails = new TransferOrderPackSlipDispatchDetails();
			transferOrderPackSlipDispatchDetails.setTransferOrder(transferOrder);
			transferOrderPackSlipDispatchDetails.setProduct(product);
			transferOrderPackSlipDispatchDetails.setScanBarcode("sc354789" + i + UUID.randomUUID());
			transferOrderPackSlipDispatchDetails.setBatchNumber("BATCH5678");
			transferOrderPackSlipDispatchDetails.setPalletBarcode("plt3566");
			transferOrderPackSlipDispatchDetails.setScanDate(DateTime.now().toDate());
			transferOrderPackSlipDispatchDetails.setStatus(OrderStatus.RESERVED);
			transferOrderPackSlipDispatchDetails.setScanDate(DateTime.now().toDate());
			transferOrderPackSlipDispatchDetails.setTransferOrderPackslip(transferOrderPackslip);
			listOfTransferOrderPackSlipDispatchDetails.add(transferOrderPackSlipDispatchDetails);

		}
		return listOfTransferOrderPackSlipDispatchDetails;
	}
}
