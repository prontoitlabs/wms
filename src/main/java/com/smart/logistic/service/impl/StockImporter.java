package com.smart.logistic.service.impl;


import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.google.gson.*;
import com.opencsv.CSVReader;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.IProductService;
import com.smart.logistic.service.ITransferOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class StockImporter {
    @Value(value = "${aws.region.name}")
    private String awsRegionName;

    @Value(value = "${aws.access.key}")
    private String awsAccessKey;

    @Value(value = "${aws.secret.key}")
    private String awsSecretKey;

    @Value(value = "${aws.bucket.name}")
    private String awsBucketName;

    @Value(value = "${aws.stockin.remote.inward.directory}")
    private String awsStockInRemoteDirectory;

    @Value(value = "${transferorder.csv.path}")
    private String transferOrderCsvPath;

    @Value(value = "${aws.dispatch.summary.remote.inward.directory}")
    private String awsDispatchSummaryRemoteDirectory;


    @Value(value = "${aws.stockin.remote.processed.directory}")
    private String awsStockInProcessedDirectory;

    @Autowired
    private AmazonS3 amazonS3;

    @Autowired
    private ITransferOrderService transferOrderService;

    private static final ExpressionParser PARSER = new SpelExpressionParser();

    @Autowired
    private IProductService productService;
//
//    @Value(value = "${aws.queue.name}")
//    private String awsQueueName;

    // Received
//    {
//        "Records": [{
//        "eventVersion": "2.0",
//                "eventSource": "aws:s3",
//                "awsRegion": "ap-south-1",
//                "eventTime": "2018-08-06T19:48:22.705Z",
//                "eventName": "ObjectCreated:Put",
//                "userIdentity": {
//            "principalId": "A17NL3F6916Q05"
//        },
//        "requestParameters": {
//            "sourceIPAddress": "192.150.10.214"
//        },
//        "responseElements": {
//            "x-amz-request-id": "37CAD0F4FBF07F37",
//                    "x-amz-id-2": "iqMsyYCEqsp61jHZj2DsKCRkInK9Iot0EIElt19em89XurYFxLuCJM6eLt8kBHsrvxvGvfPVerY="
//        },
//        "s3": {
//                    "s3SchemaVersion": "1.0",
//                    "configurationId": "warehouse-listener-dev",
//                    "bucket": {
//                      "name": "smart-logistics-in-dev",
//                        "ownerIdentity": {
//                           "principalId": "A17NL3F6916Q05"
//                       },
//                         "arn": "arn:aws:s3:::smart-logistics-in-dev"
//                      },
//                  "object": {
//                        "key": "stock-in-summary/stnd-001223-1819-StockIN.csv",
//                        "size": 82964,
//                        "eTag": "169002df586f2c9ae364752b85ac8cdc",
//                        "sequencer": "005B68A606A5D3F8D8"
//            }
//        }
//    }]
//    }
    @JmsListener(destination = "${aws.queue.name}")
    public void parseMessage(String requestJSON) throws JMSException {

        JsonObject object = new JsonParser ().parse(requestJSON).getAsJsonObject();
        JsonArray records = object.getAsJsonArray("Records");
        for (JsonElement record : records) {
            JsonObject recordObj = record.getAsJsonObject();
            JsonObject s3FileObj = recordObj.getAsJsonObject("s3").getAsJsonObject("object");

            String srcKey = s3FileObj.get("key").getAsString();

            // ignore copy notifications
            if(srcKey.contains(awsStockInProcessedDirectory) || srcKey.contains(awsDispatchSummaryRemoteDirectory))
                continue;

            String tgtKey = awsStockInProcessedDirectory + srcKey.substring(srcKey.indexOf('/'));


            JsonObject s3BucketObj = recordObj.getAsJsonObject("s3").getAsJsonObject("bucket");
            String bucketName = s3BucketObj.get("name").getAsString();

            S3Object fullObject = amazonS3.getObject(new GetObjectRequest(bucketName, srcKey));
            try {
                log.info ("Processing " + srcKey);
//                if(srcKey.contains("stnd-"))
                    handleFile(fullObject.getObjectContent());
//                else
//                    handleManualInput(fullObject.getObjectContent());

                // copy the file to processed folder
                CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, srcKey, bucketName, tgtKey);
                amazonS3.copyObject(copyObjRequest);
                // delete the file from original location
                amazonS3.deleteObject(new DeleteObjectRequest(bucketName, srcKey));
                log.info ("Processed " + srcKey);
            }catch (Exception ex){
                log.error (ex.toString());
            }
        }

    }

    private void handleFile(InputStream input) throws IOException, WmsException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        String line = null;
        List <String [] > stockInList = new ArrayList <String[]> ();
        while ((line = reader.readLine()) != null) {
            CSVReader csvReader = new CSVReader(new StringReader(line));
            stockInList.add(csvReader.readNext());
        }
        transferOrderService.importTransferOrdersFromLines(stockInList);
        // reconcile after every import
        productService.reconcileProductQuantityInHand();

    }

    private void handleManualInput(InputStream input) throws IOException, WmsException {

        // Read the text input stream one line at a time and display each line.
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        String line = null;
        List <String [] > stockInList = new ArrayList <String[]> ();
        while ((line = reader.readLine()) != null) {
            CSVReader csvReader = new CSVReader(new StringReader(line));
            stockInList.add(csvReader.readNext());
        }

        transferOrderService.importManualTransferOrder(stockInList);
        // reconcile after every import
        productService.reconcileProductQuantityInHand();
    }




}
