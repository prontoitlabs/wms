package com.smart.logistic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.SalesOrderPackslip;
import com.smart.logistic.domain.SalesOrderPackslipItem;
import com.smart.logistic.dto.SalesOrderPackslipDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.IOrderPackslipItemService;
import com.smart.logistic.service.IOrderPackslipService;
import com.smart.logistic.service.generic.AbstractService;
import com.smart.logistic.transformer.SalesOrderPackslipTransformer;

@Service
public class SalesOrderPackslipItemServiceImpl extends AbstractService<SalesOrderPackslipItem>
		implements IOrderPackslipItemService {

	@Autowired
	private IOrderPackslipService orderPackslipService;

	@Autowired
	private SalesOrderPackslipTransformer orderPackslipTransformer;

	@Override
	protected WmsException notFoundException() {
		return new WmsException("order pack slip item not found");
	}

	@Override
	public SalesOrderPackslipDto getOrderPackslipDetailById(Long id) throws WmsException {
		SalesOrderPackslip salesOrderPackslip = orderPackslipService.findOne(id);
		return orderPackslipTransformer.toDtoEntity(salesOrderPackslip);
	}

}
