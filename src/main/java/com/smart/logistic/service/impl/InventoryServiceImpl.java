package com.smart.logistic.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.smart.logistic.domain.Inventory;
import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.dto.InventoryDto;
import com.smart.logistic.dto.InventoryStatusDto;
import com.smart.logistic.dto.SalesOrderDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.IInventoryRepository;
import com.smart.logistic.service.IInventoryService;
import com.smart.logistic.service.ISalesOrderService;
import com.smart.logistic.service.generic.AbstractService;
import com.smart.logistic.transformer.InventoryTransformer;
import com.smart.logistic.transformer.SalesOrderTransformer;

@Service
public class InventoryServiceImpl extends AbstractService<Inventory> implements IInventoryService {

	@Autowired
	private IInventoryRepository inventoryRepository;

	@Autowired
	private InventoryTransformer inventoryTransformer;

	@Autowired
	private SalesOrderTransformer salesOrderTransformer;

	@Autowired
	private ISalesOrderService salesOrderService;

	@Override
	protected WmsException notFoundException() {
		return new WmsException("Inventory not found");
	}

	@Override
	public Inventory findByProductId(Long productId) {
		return inventoryRepository.findByProduct_Id(productId);
	}

	@Override
	public Inventory findByProductSku(String productSku) {
		return inventoryRepository.findByProduct_Sku(productSku);
	}

	@Override
	public InventoryDto saveInventory(InventoryDto inventoryDto) throws WmsException {
		Inventory inventory = new Inventory();
		BeanUtils.copyProperties(inventoryDto, inventory);
		return inventoryTransformer.toDto(save(inventory));
	}

	@Override
	public InventoryDto updateInventory(Long inventoryId, InventoryDto inventoryDto) throws WmsException {
		Inventory inventory = inventoryRepository.findOne(inventoryId);
		BeanUtils.copyProperties(inventoryDto, inventory);
		return inventoryTransformer.toDto(save(inventory));
	}

	@Override
	@Transactional
	public void updateBarCode(String oldBarCode, String newBarCode) throws WmsException {
		inventoryRepository.updateBarCodeForInventory(oldBarCode, newBarCode);
	}

	@Override
	public Page<Inventory> fidAllPaginatedProdct(Integer pageNumber, Integer pageSize) throws WmsException {
		Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "dateCreated"));
		validatePageNumber(pageNumber, pageSize);
		return inventoryRepository.findAll(pageable);
	}

	private void validatePageNumber(Integer pageNumber, Integer pageSize) throws WmsException {
		if ((pageNumber == null) || (pageSize == null) || (pageNumber < 0) || (pageSize < 0)) {
			throw new WmsException(HttpStatus.BAD_REQUEST, "PageNumber or PageSize not valid");
		}
	}

	@Override
	public List<InventoryStatusDto> findIvetoryByOrderStatus() throws WmsException {

		List<InventoryStatusDto> inventoryStatusDtos = new ArrayList<InventoryStatusDto>();
		for (Object[] object : inventoryRepository.findIvetoryByOrderStatus()) {
			InventoryStatusDto inventoryStatusDto = new InventoryStatusDto();
			inventoryStatusDto.setStatus(object[0]);
			inventoryStatusDto.setCount(object[1]);
			if (!inventoryStatusDto.getStatus().equals(OrderStatus.DONE)) {
				inventoryStatusDtos.add(inventoryStatusDto);
			}
		}
		List<SalesOrderDto> salesOrderDtoList = salesOrderTransformer
				.toDto(salesOrderService.getListOfDeliveryOrders());
		Integer actualReservedQuantityInCB = 0;
		for (SalesOrderDto salesOrderDto : salesOrderDtoList) {
			actualReservedQuantityInCB = actualReservedQuantityInCB + salesOrderDto.getActualReservedQuantityInCB();
		}
//		List<SalesOrderDto> shippedSalesOrders = salesOrderTransformer
//				.toDto(salesOrderService.getListOfShippedOrders());

//		Integer actualShippedQuantityInCB = 0;
//		for (SalesOrderDto shippedOrderDto : shippedSalesOrders) {
//			actualShippedQuantityInCB = actualShippedQuantityInCB + shippedOrderDto.getActualShippedQuantityInCB();
//		}

		InventoryStatusDto inventoryStatusDto = new InventoryStatusDto();
		inventoryStatusDto.setStatus(OrderStatus.TO_BE_DELIVERED);
		inventoryStatusDto.setCount(actualReservedQuantityInCB);
		inventoryStatusDtos.add(inventoryStatusDto);

		InventoryStatusDto shippedInventoryStatusDto = new InventoryStatusDto();
//		shippedInventoryStatusDto.setStatus(OrderStatus.DONE);
//		shippedInventoryStatusDto.setCount(actualShippedQuantityInCB);
//		inventoryStatusDtos.add(shippedInventoryStatusDto);

//        InventoryStatusDto pendingInventoryStatusDto = new InventoryStatusDto();
        shippedInventoryStatusDto.setStatus(OrderStatus.WAITING);
        shippedInventoryStatusDto.setCount(salesOrderService.getCountOfPendingOrders());
        inventoryStatusDtos.add(shippedInventoryStatusDto);

		return inventoryStatusDtos;
	}

	@Override
	public Page<Inventory> findInventoryByOrderStatusAndProduct(OrderStatus orderStatus, Product product,
			Pageable page) {
		return inventoryRepository.findAllByOrderStatusAndProduct(orderStatus, product, page);
	}

	@Override
	@Transactional
	public void updateInventoryOrderStatusAsReservedById(Long id) {
		inventoryRepository.updateInventoryOrderStatusAsReservedById(id);
	}

	@Override
	public void updateInventoryOrderStatusAsReadyById(Long inventoryId) {
		inventoryRepository.updateInventoryOrderStatusAsReservedById(inventoryId);

	}

	@Override
	public Inventory findByCartonBarCodeAndStatus(String scanBarcode, OrderStatus orderStatus) {
		return inventoryRepository.findByScanBarcodeAndOrderStatus(scanBarcode, orderStatus);
	}

	@Override
	public List<Inventory> findInventoryByPalletBarCodes(List<String> palletBarCodeList) {
		return inventoryRepository.findInventoryByPalletBarcodeIn(palletBarCodeList);
	}

	@Override
	public List<Object[]> findTOPNDistinctPalletBarCodesByStatus(OrderStatus status, Product product,
			int numberOfPallets) {

		return inventoryRepository.findTOPNDistinctPalletBarCodesByStatus(product.getId(), numberOfPallets);
	}

	@Override
	public Long countByProduct_IdAndOrderStatus(Long id, OrderStatus orderStatus) {
		return inventoryRepository.countByProduct_IdAndOrderStatus(id, orderStatus);
	}


	@Override
	public List<Object[]> findInventoryCreatedToday (){
		return inventoryRepository.findInventoryByDateCreated();
	}

	@Override
	public String countInventoryCreatedToday (){
		return inventoryRepository.countInventoryByDateCreated().toString();
	}
}
