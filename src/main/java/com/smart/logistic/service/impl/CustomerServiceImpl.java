package com.smart.logistic.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.smart.logistic.domain.Customer;
import com.smart.logistic.domain.SalesOrder;
import com.smart.logistic.dto.CustomerDto;
import com.smart.logistic.dto.ImportExternalCustomerDto;
import com.smart.logistic.dto.ImportExternalCustomersJsonDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.ICustomerRepository;
import com.smart.logistic.service.ICustomerService;
import com.smart.logistic.service.ISalesOrderService;
import com.smart.logistic.service.generic.AbstractService;
import com.smart.logistic.transformer.CustomerTransformer;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CustomerServiceImpl extends AbstractService<Customer> implements ICustomerService {

	
	@Autowired
	private ICustomerRepository  customerRepository;
	
	@Autowired
	private ISalesOrderService salesOrderService;
	
	@Autowired
	private CustomerTransformer customerTransformer;
	
	
	@Value("${customer.master.filename}")
	private String customerMasterFileName;
	
	@Value("${import.external.customer.master.url}")
	private String importExternalCustomerMasterUrl;

	@Autowired
	private RestTemplate rest;
	
	@Override
	protected WmsException notFoundException() {
		return new WmsException("Customer not found");
	}
	
	@Override
    @Scheduled(cron = "0 0 5 * * *")
	public List<Customer> importAllCustomersFromRamco() throws WmsException {
		// Set XML content type explicitly to force response in XML (If not spring get
		// response in JSON)
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

		ResponseEntity<String> response = rest.exchange(importExternalCustomerMasterUrl, HttpMethod.GET, entity,
				String.class);

		String responseBody = response.getBody().trim();
		responseBody = responseBody.split(">")[1];

		ImportExternalCustomersJsonDto importExternalCustomersJsonDto = convertToJavaObject(responseBody);
		List<Customer> customer = convertImportedCustomersToCustomerDomain(importExternalCustomersJsonDto);
		return customer;
	}
	
	private ImportExternalCustomersJsonDto convertToJavaObject(String responseBody) {
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(responseBody));
		reader.setLenient(true);

		ImportExternalCustomersJsonDto importExternalCustomersJsonDto = gson.fromJson(reader,
				ImportExternalCustomersJsonDto.class);

		return importExternalCustomersJsonDto;
	}
	
	private List<Customer> convertImportedCustomersToCustomerDomain(
			ImportExternalCustomersJsonDto importExternalCustomersJsonDto) {
		List<Customer> customers  = new ArrayList<Customer>();
		Set<String> customerCodes = new HashSet<String>();
		
		List<ImportExternalCustomerDto> customerList = importExternalCustomersJsonDto.getGetCustomerList();
		for (ImportExternalCustomerDto importExternalCustomerDto : customerList) {
			if(StringUtils.isNotEmpty(importExternalCustomerDto.getClo_cust_code()) && customerRepository.findByCustomerCode(importExternalCustomerDto.getClo_cust_code()) == null && !customerCodes.contains(importExternalCustomerDto.getClo_cust_code())) {
				
				Customer customer = Customer.builder().customerCode(importExternalCustomerDto.getClo_cust_code()).name(importExternalCustomerDto.getClo_cust_name())
				.addressline1(importExternalCustomerDto.getClo_addrline1()).addressline2(importExternalCustomerDto.getClo_addrline2()).addressline3(importExternalCustomerDto.getClo_addrline3())
				.city(importExternalCustomerDto.getClo_city()).state(importExternalCustomerDto.getClo_state()).country(importExternalCustomerDto.getClo_country())
				.customerLongDescription(importExternalCustomerDto.getClo_cust_name_shd()).customerNameShd(importExternalCustomerDto.getClo_cust_name_shd())
				.email(importExternalCustomerDto.getClo_email()).faxNumber(importExternalCustomerDto.getClo_fax()).isDefault(Boolean.TRUE).mobileNumber(importExternalCustomerDto.getClo_mobile())
				.phoneNumber1(importExternalCustomerDto.getClo_phone1()).phoneNumber2(importExternalCustomerDto.getClo_phone2()).pinCode(importExternalCustomerDto.getClo_zip())
				.build();
				customerCodes.add(importExternalCustomerDto.getClo_cust_code());
				customers.add(customer);
			}
		}
		
		if(!CollectionUtils.isEmpty(customers)) {
			customers = save(customers);
		}
		return customers;
	}

	@Override
	@Transactional
	public List<Customer> importCustomerMaster() throws WmsException {
		List<Customer> customers = new ArrayList<Customer>();
		try {

			FileInputStream excelFile = new FileInputStream(new File(customerMasterFileName));
			@SuppressWarnings("resource")
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			int rowNumber = 1;
			DataFormatter formatter = new DataFormatter();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();
				if (rowNumber == 1) {
					rowNumber++;
					continue;
				}
				Iterator<Cell> cellIterator = currentRow.iterator();
				Customer customer = new Customer();

				int colNumber = 1;
				while (cellIterator.hasNext()) {
					Cell currentCell = cellIterator.next();
					switch (colNumber) {

					case 1:
						customer.setCustomerCode(formatter.formatCellValue(currentCell));
						break;
					case 2:
						customer.setName(formatter.formatCellValue(currentCell));
						break;
					case 3:
						customer.setCustomerNameShd(formatter.formatCellValue(currentCell));
						break;
					case 5:
						customer.setAddressline1(formatter.formatCellValue(currentCell));
						break;
					case 6:
						customer.setAddressline2(formatter.formatCellValue(currentCell));
						break;
					case 7:
						customer.setAddressline3(formatter.formatCellValue(currentCell));
						break;
					case 8:
						customer.setCity(formatter.formatCellValue(currentCell));
						break;
					case 9:
						customer.setState(formatter.formatCellValue(currentCell));
						break;
					case 10:
						customer.setCountry(formatter.formatCellValue(currentCell));
						break;
					case 11:
						customer.setPinCode(formatter.formatCellValue(currentCell));
						customer.setIsDefault(Boolean.TRUE);
						break;
					case 12:
						customer.setPhoneNumber1(formatter.formatCellValue(currentCell));
						break;
					case 13:
						customer.setPhoneNumber2(formatter.formatCellValue(currentCell));
						break;
					case 14:
						customer.setMobileNumber(formatter.formatCellValue(currentCell));
						break;
					case 15:
						customer.setFaxNumber(formatter.formatCellValue(currentCell));
						break;
					case 16:
						customer.setEmail(formatter.formatCellValue(currentCell));
						break;
					case 17:
						customer.setWebsiteUrl(formatter.formatCellValue(currentCell));
						break;
					}
					colNumber++;
				}
				customers.add(customer);
				rowNumber++;
			}
		} catch (FileNotFoundException e) {
			log.error(e.getMessage());
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		return save(customers);
	}
	
	@Override
	public Page<Customer> findingAllCustomer(Integer pageNumber, Integer pageSize) throws WmsException {

		Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "dateCreated"));
		
		validatePageNumber(pageNumber, pageSize);
		
		return customerRepository.findAll(pageable);
	}

	private void validatePageNumber(Integer pageNumber, Integer pageSize) throws WmsException {
		if ((pageNumber == null) || (pageSize == null) || (pageNumber < 0) || (pageSize < 0)) {
			throw new WmsException(HttpStatus.BAD_REQUEST, "PageNumber or PageSize not valid");
		}
	}

	@Override
	public Map<String, CustomerDto> getAllCustomersForPendingDeliveryOrders() throws WmsException {
		List<SalesOrder> deliveryOrders = salesOrderService.getListOfDeliveryOrders();
		Map<String, CustomerDto> customerMap = new HashMap<String, CustomerDto>();
		for (SalesOrder salesOrder : deliveryOrders) {
			Customer customer = salesOrder.getCustomer();
			if(!customerMap.containsKey(customer.getCustomerCode())) {
				customerMap.put(customer.getCustomerCode(), customerTransformer.toDto(customer)) ;
			}
		}
		return customerMap;
	}

	@Override
	public Customer findCustomerByCode(String cust_Code) throws WmsException {
		Customer customer = customerRepository.findByCustomerCode(cust_Code);
		if(customer == null) {
			throw new WmsException(HttpStatus.BAD_REQUEST, "Customer not found for the customer Code : " +cust_Code);
		}
		return customer;
	}

	@Override
	public Page<Customer> findTopTenCustomer() throws WmsException {
		int pageNumber = 0;
		int pageSize = 10;
		Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "dateCreated"));
		return findAll(pageable);
	}
}
