package com.smart.logistic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.User;
import com.smart.logistic.exception.UserNotFoundException;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.IUserRepository;
import com.smart.logistic.service.IUserService;
import com.smart.logistic.service.generic.AbstractService;

@Service
public class UserServiceImpl extends AbstractService<User> implements IUserService {

	@Autowired
	private IUserRepository userRepository;

	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	protected WmsException notFoundException() {
		return new UserNotFoundException();
	}

}
