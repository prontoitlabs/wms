package com.smart.logistic.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.smart.logistic.domain.SalesOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.SalesOrderPackslip;
import com.smart.logistic.domain.SalesOrderPackslipItem;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.dto.SalesOrderPackSlipDispatchDetailsDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.IPackslipDispatchDetailsRepository;
import com.smart.logistic.service.IOrderPackslipItemService;
import com.smart.logistic.service.IOrderPackslipService;
import com.smart.logistic.service.IPackslipDispatchDetailsService;
import com.smart.logistic.service.generic.AbstractService;
import com.smart.logistic.transformer.SalesOrderPackslipDispatchDetailsTransformer;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SalesOrderPackslipDispatchDetailsServiceImpl extends AbstractService<SalesOrderPackSlipDispatchDetails>
		implements IPackslipDispatchDetailsService {

	@Override
	protected WmsException notFoundException() {
		return new WmsException("pack slip dispatch detail not found");
	}

	@Autowired
	private IPackslipDispatchDetailsRepository packslipDispatchDetailsRepository;

	@Autowired
	private SalesOrderPackslipDispatchDetailsTransformer packslipDispatchDetailsTransformer;

	@Autowired
	private IOrderPackslipItemService orderPackslipItemService;

	@Autowired
	private IOrderPackslipService orderPackslipService;

	@Override
	public List<SalesOrderPackSlipDispatchDetailsDto> getPackslipDispatchDetailByPackSlipNumberAndSku(
			String packSlipNumber, String sku) {
		log.info("packSlipNumber = " + packSlipNumber);
		return packslipDispatchDetailsTransformer.toDto(packslipDispatchDetailsRepository
				.findAllByOrderPackslipItem_OrderPackslip_PackslipNumberAndProduct_Sku(packSlipNumber, sku));
	}

	@Transactional
	@Override
	public List<SalesOrderPackSlipDispatchDetailsDto> confirmDispatchDetails(List<Long> ids, OrderStatus status)
			throws WmsException {

		List<SalesOrderPackSlipDispatchDetails> listOfPackSlipDispatchDetails = new ArrayList<SalesOrderPackSlipDispatchDetails>();

		Map<Long, Integer> itemMap = new HashMap<>();

		Map<Long, Integer> packSlipMap = new HashMap<>();

		// iterating over dispatchDetail ids
		for (Long id : ids) {

			// finding SalesOrderPackSlip Dispatch Details from id
			SalesOrderPackSlipDispatchDetails packSlipDispatchDetails = findOne(id);

			// if provided status is not equal to saved status
			if (!(status.equals(packSlipDispatchDetails.getStatus()))) {

				// first time when marking done
				SalesOrderPackslipItem salesOrderPackslipItem = packSlipDispatchDetails.getOrderPackslipItem();
				if (status.equals(OrderStatus.DONE)) {

					// updating packSlipDispatchdetail
					packSlipDispatchDetails.setStatus(status);

					/// here updating item map key with transferOrderPackslipItem id
					Long itemkey = salesOrderPackslipItem.getId();

					if (itemMap.containsKey(itemkey)) {
						// if map already contains same key then we are just incrementing value
						itemMap.put(itemkey, itemMap.get(itemkey) + 1);
					} else {
						// if first time entering in map
						itemMap.put(itemkey, 1);
					}

					// here updating packslip map
					Long packSlipkey = salesOrderPackslipItem.getOrderPackslip().getId();

					if (packSlipMap.containsKey(packSlipkey)) {
						// if map already contains same key then we are just incrementing value
						packSlipMap.put(packSlipkey, packSlipMap.get(packSlipkey) + 1);
					} else {
						// if first time entering in map
						packSlipMap.put(packSlipkey, 1);
					}

				}

				// already done to not done
				if (((status != OrderStatus.DONE) && OrderStatus.DONE.equals(packSlipDispatchDetails.getStatus()))) {

					// updating packSlipDispatchdetail
					packSlipDispatchDetails.setStatus(status);

					/// here updating item map
					Long itemkey = salesOrderPackslipItem.getId();

					if (itemMap.containsKey(itemkey)) {
						// if map already contains same key then we are just incrementing
						itemMap.put(itemkey, itemMap.get(itemkey) - 1);
					} else {
						// if first time entering in map
						itemMap.put(itemkey, -1);
					}

					// here updating packslip map
					Long packSlipkey = salesOrderPackslipItem.getOrderPackslip().getId();

					if (packSlipMap.containsKey(packSlipkey)) {

						// if map already contains same key then we are just incrementing
						packSlipMap.put(packSlipkey, packSlipMap.get(packSlipkey) - 1);
					} else {
						// if first time entering in map
						packSlipMap.put(packSlipkey, -1);
					}
				}
			}
			listOfPackSlipDispatchDetails.add(packSlipDispatchDetails);
		}
		// process item and packslip map
		processMapForItemAndPackSlip(itemMap, packSlipMap);
		return packslipDispatchDetailsTransformer.toDto(listOfPackSlipDispatchDetails);
	}

	private void processMapForItemAndPackSlip(Map<Long, Integer> itemMap, Map<Long, Integer> packSlipMap)
			throws WmsException {

		List<SalesOrderPackslipItem> listOfSalesOrderPackslipItem = new ArrayList<SalesOrderPackslipItem>();

		List<SalesOrderPackslip> listOfSalesOrderPackslip = new ArrayList<SalesOrderPackslip>();

		// processing itemList
		for (Map.Entry<Long, Integer> item : itemMap.entrySet()) {

			SalesOrderPackslipItem salesOrderPackslipItem = orderPackslipItemService.findOne(item.getKey());

			salesOrderPackslipItem.setVerifiedCases(salesOrderPackslipItem.getVerifiedCases() + item.getValue());

			listOfSalesOrderPackslipItem.add(salesOrderPackslipItem);
		}

		orderPackslipItemService.save(listOfSalesOrderPackslipItem);

		// processing packSlip
		for (Map.Entry<Long, Integer> packslip : packSlipMap.entrySet()) {

			SalesOrderPackslip salesOrderPackslip = orderPackslipService.findOne(packslip.getKey());

			salesOrderPackslip.setVerifiedCases(salesOrderPackslip.getVerifiedCases() + packslip.getValue());
			if (salesOrderPackslip.getPackslipQuantity().intValue() == salesOrderPackslip.getVerifiedCases()) {
				salesOrderPackslip.setOrderStatus(OrderStatus.DONE);
			}

			listOfSalesOrderPackslip.add(salesOrderPackslip);
		}
		orderPackslipService.save(listOfSalesOrderPackslip);
	}

	public static boolean contains(OrderStatus status) {
		return EnumUtils.isValidEnum(OrderStatus.class, status.toString());
	}

	@Override
	@Transactional
	public List<SalesOrderPackSlipDispatchDetails> findAllPackslipDispatchDetailsByPackslipIdAndStatus(Long packslipId,
			OrderStatus status) {
		List<SalesOrderPackSlipDispatchDetails> transferOrderPackslipDispatchDetails = packslipDispatchDetailsRepository
				.findAllByOrderPackslipItem_OrderPackslip_IdAndStatus(packslipId, status);
		return transferOrderPackslipDispatchDetails;
	}

	@Override
	public List<SalesOrderPackSlipDispatchDetails> findAllPackslipDispatchDetailsByPackslipId(Long id) {
		return packslipDispatchDetailsRepository.findAllByOrderPackslipItem_OrderPackslip_Id(id);
	}
}
