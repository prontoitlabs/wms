package com.smart.logistic.service.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.opencsv.CSVWriter;
import com.smart.logistic.domain.TransferOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.TransferOrderPackslip;
import com.smart.logistic.domain.TransferOrderPackslipItem;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.dto.TransferOrderPackSlipDispatchDetailsDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.ITransferOrderPackSlipDispatchDetailsRepository;
import com.smart.logistic.service.ITransferOrderPackSlipDispatchDetailsService;
import com.smart.logistic.service.ITransferOrderPackslipItemService;
import com.smart.logistic.service.ITransferOrderPackslipService;
import com.smart.logistic.service.generic.AbstractService;
import com.smart.logistic.transformer.TransferOrderPackSlipDispatchDetailsTransformer;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TransferOrderPackSlipDispatchDetailsServiceImpl extends
		AbstractService<TransferOrderPackSlipDispatchDetails> implements ITransferOrderPackSlipDispatchDetailsService {

	@Autowired
	private ITransferOrderPackSlipDispatchDetailsRepository transferOrderPackSlipDispatchDetailsRepository;

	@Autowired
	private TransferOrderPackSlipDispatchDetailsTransformer transferOrderPackSlipDispatchDetailsTransformer;

	@Autowired

	private ITransferOrderPackslipService transferOrderPackslipService;

	@Autowired

	private ITransferOrderPackslipItemService transferOrderPackslipItemService;


	@Override
	@Transactional
	public List<TransferOrderPackSlipDispatchDetails> findAllPackslipDispatchDetailsByPackslipIdAndStatus(
			Long packslipId, OrderStatus status) {
		List<TransferOrderPackSlipDispatchDetails> transferOrderPackslipDispatchDetails = transferOrderPackSlipDispatchDetailsRepository
				.findAllByTransferOrderPackslip_IdAndStatus(packslipId, status);
		return transferOrderPackslipDispatchDetails;
	}

	@Override
	protected WmsException notFoundException() {
		return new WmsException("transfer order pack slip dispatch details not found");
	}

	@Transactional
	@Override
	public List<TransferOrderPackSlipDispatchDetailsDto> confirmListOfPackSlipDispatchDetailAsDone(List<Long> ids,
			OrderStatus status) throws WmsException {

		List<TransferOrderPackSlipDispatchDetails> listOfTransferOrderPackSlipDetail = new ArrayList<TransferOrderPackSlipDispatchDetails>();

		Map<Long, Integer> itemMap = new HashMap<>();

		Map<Long, Integer> packSlipMap = new HashMap<>();

		// iterating over dispatchDetail ids
		for (Long id : ids) {

			// finding TransferOrderPackSlip Dispatch Details from id
			TransferOrderPackSlipDispatchDetails packSlipDispatchDetails = findOne(id);

			if (packSlipDispatchDetails != null) {

				// if provided status is not equal to saved status
				if (!(status.equals(packSlipDispatchDetails.getStatus()))) {

					// first time when marking done
					if (status.equals(OrderStatus.DONE)) {

						// updating packSlipDispatchdetail
						packSlipDispatchDetails.setStatus(status);

						// here updating packslip map
						Long packSlipkey = packSlipDispatchDetails.getTransferOrderPackslip().getId();

						if (packSlipMap.containsKey(packSlipkey)) {
							// if map already contains same key then we are just incrementing value
							packSlipMap.put(packSlipkey, packSlipMap.get(packSlipkey) + 1);
						} else {
							// if first time entering in map
							packSlipMap.put(packSlipkey, 1);
						}

					}

					// already done to not done
					if (((status != OrderStatus.DONE)
							&& OrderStatus.DONE.equals(packSlipDispatchDetails.getStatus()))) {

						// updating packSlipDispatchdetail
						packSlipDispatchDetails.setStatus(status);

						// here updating packslip map
						Long packSlipkey = packSlipDispatchDetails.getTransferOrderPackslip().getId();

						if (packSlipMap.containsKey(packSlipkey)) {

							// if map already contains same key then we are just incrementing
							packSlipMap.put(packSlipkey, packSlipMap.get(packSlipkey) - 1);
						} else {
							// if first time entering in map
							packSlipMap.put(packSlipkey, -1);
						}
					}
				}
			}
			listOfTransferOrderPackSlipDetail.add(packSlipDispatchDetails);
		}
		// process item and packslip map
		processMapForItemAndPackSlip(itemMap, packSlipMap);
		return transferOrderPackSlipDispatchDetailsTransformer
				.toDto(transferOrderPackSlipDispatchDetailsRepository.save(listOfTransferOrderPackSlipDetail));
	}

	private void processMapForItemAndPackSlip(Map<Long, Integer> itemMap, Map<Long, Integer> packSlipMap)
			throws WmsException {

		List<TransferOrderPackslipItem> listOfTransferOrderPackslipItem = new ArrayList<TransferOrderPackslipItem>();

		List<TransferOrderPackslip> listOfTransferOrderPackslip = new ArrayList<TransferOrderPackslip>();

		// processing itemList
		for (Map.Entry<Long, Integer> item : itemMap.entrySet()) {

			TransferOrderPackslipItem transferOrderPackslipItem = transferOrderPackslipItemService
					.findOne(item.getKey());

			transferOrderPackslipItem.setDoneCases(transferOrderPackslipItem.getDoneCases() + item.getValue());

			listOfTransferOrderPackslipItem.add(transferOrderPackslipItem);
		}

		transferOrderPackslipItemService.save(listOfTransferOrderPackslipItem);

		// processing packSlip
		for (Map.Entry<Long, Integer> packslip : packSlipMap.entrySet()) {

			TransferOrderPackslip transferOrderPackslip = transferOrderPackslipService.findOne(packslip.getKey());

			transferOrderPackslip.setDoneCases(transferOrderPackslip.getDoneCases() + packslip.getValue());

			listOfTransferOrderPackslip.add(transferOrderPackslip);
		}
		transferOrderPackslipService.save(listOfTransferOrderPackslip);
	}

	public static boolean contains(OrderStatus status) {
		return EnumUtils.isValidEnum(OrderStatus.class, status.toString());
	}

	@Override
	public Page<TransferOrderPackSlipDispatchDetails> getTransferOrderPackslipDispatchDetailsById(Long transferOrderId,
			Integer pageNumber, Integer pageSize) throws WmsException {
		log.info("inside getTransferOrderPackslipDispatchDetailsByIdAndStatus: pageNumber  pageSize: " + pageNumber
				+ pageSize);
		Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "dateCreated"));
		validatePageNumber(pageNumber, pageSize);
		return transferOrderPackSlipDispatchDetailsRepository.findAllByTransferOrder_Id(transferOrderId, pageable);
	}

	private void validatePageNumber(Integer pageNumber, Integer pageSize) throws WmsException {
		if ((pageNumber == null) || (pageSize == null) || (pageNumber < 0) || (pageSize < 0)) {
			throw new WmsException(HttpStatus.BAD_REQUEST, "PageNumber or PageSize not valid");
		}
	}

	@Override
	public List<TransferOrderPackSlipDispatchDetails> getTransferOrderPackslipDispatchDetailsById(
			Long transferOrderId) {
		return transferOrderPackSlipDispatchDetailsRepository.findAllByTransferOrder_Id(transferOrderId);
	}

	@Override
	public FileSystemResource createCsvFileByTransferOrderId(HttpServletResponse response, Long transferOrderId,
			String csvFileName) throws IOException {
		log.info("inside createCsvFileByTransferOrderId Method ");
		response.setHeader("Content-disposition", "attachment; filename=" + csvFileName);
		List<TransferOrderPackSlipDispatchDetails> transferOrderPackSlipDispatchDetails = transferOrderPackSlipDispatchDetailsRepository
				.findAllByTransferOrder_Id(transferOrderId);
		createCsvFile(transferOrderPackSlipDispatchDetails, csvFileName);
		return new FileSystemResource(new File(csvFileName));
	}

	private void createCsvFile(List<TransferOrderPackSlipDispatchDetails> transferOrderPackSlipDispatchDetails,
			String csvFileName) throws IOException {
		log.info("inside createCsvFile method");
		// initializing csvwriter
		CSVWriter csvWriter = new CSVWriter(new FileWriter(csvFileName));
		createCsvHeader(csvWriter);
		writingDataToCsv(transferOrderPackSlipDispatchDetails, csvWriter);
		csvWriter.close();
	}

	private void writingDataToCsv(List<TransferOrderPackSlipDispatchDetails> transferOrderPackSlipDispatchDetails,
			CSVWriter csvWriter) {
		log.info("inside writingDataToCsv method");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		String stockindate = sdf.format(date);
		for (TransferOrderPackSlipDispatchDetails packSlipDispatchDetails : transferOrderPackSlipDispatchDetails) {
			csvWriter.writeNext(new String[] { packSlipDispatchDetails.getTransferOrderPackslip().getPackslipNumber(),
					packSlipDispatchDetails.getTransferOrder().getTrasferOrderNumber(),
					packSlipDispatchDetails.getProduct().getSku(),
					packSlipDispatchDetails.getProduct().getDescription(), "packing",
					packSlipDispatchDetails.getBatchNumber(), stockindate, "40", "100", "800",
					packSlipDispatchDetails.getScanBarcode(), packSlipDispatchDetails.getPalletBarcode(), "flag" });
		}
	}

	private void createCsvHeader(CSVWriter csvWriter) {
		log.info("inside createCsvHeader method");
		csvWriter.writeNext(new String[] { "Packslipno", "orderno", "itemcode", "itemdesciption", "packing", "batchno",
				"Stockindate(YYYYMMDD)", "nop", "stnetwt", "stgrosswt", "cbbaroce", "plaletbarcode", "flag" });
	}

}
