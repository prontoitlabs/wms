package com.smart.logistic.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.ProductType;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;
import com.smart.logistic.dto.ImportExternalProductDto;
import com.smart.logistic.dto.ImportExternalProductsJsonDto;
import com.smart.logistic.dto.ProductDto;
import com.smart.logistic.dto.ProductStatusOnDashBoardDto;
import com.smart.logistic.exception.EntityAlreadyExistsException;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.IProductRepository;
import com.smart.logistic.service.IInventoryService;
import com.smart.logistic.service.IProductService;
import com.smart.logistic.service.generic.AbstractService;
import com.smart.logistic.transformer.ProductTransformer;
import com.smart.logistic.util.QuantityInHandForProductComparator;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProductServiceImpl extends AbstractService<Product> implements IProductService {

	@Autowired
	private IProductRepository productRepository;

	@Autowired
	private ProductTransformer productTransformer;

	@Value("${item.master.filename}")
	private String itemMasterFileName;

	@Value("${import.external.item.master.url}")
	private String importExternalItemMasterUrl;

	@Autowired
	private IInventoryService inventoryService;

	@Autowired
	private RestTemplate rest;

	@Override
	protected WmsException notFoundException() {
		return null;
	}

	@Override
	public ProductDto saveProduct(ProductDto productDto) throws WmsException {
		checkProductSku(productDto);
		Product product = new Product();
		BeanUtils.copyProperties(productDto, product);
		return productTransformer.toDto(save(product));
	}

	private void checkProductSku(ProductDto productDto) throws EntityAlreadyExistsException {
		if (productRepository.findBySkuIgnoreCase(productDto.getSku()) != null) {
			throw new EntityAlreadyExistsException("Product with SKU " + productDto.getSku() + " already exists !");
		}
	}

	@Override
	public Page<Product> getAllProduct(Integer pageNumber, Integer pageSize) throws WmsException {
		Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "dateCreated"));
		validatePageNumber(pageNumber, pageSize);
		return findAll(pageable);
	}

	private void validatePageNumber(Integer pageNumber, Integer pageSize) throws WmsException {
		if ((pageNumber == null) || (pageSize == null) || (pageNumber < 0) || (pageSize < 0)) {
			throw new WmsException(HttpStatus.BAD_REQUEST, "PageNumber or PageSize not valid");
		}
	}

	@Override
	public Product findBySkuIgnoreCase(String sku) {
		return productRepository.findBySkuIgnoreCase(sku);
	}

	@Override
	public List<ProductStatusOnDashBoardDto> findItemAndQuantityInHand() {
		List<String> listOfProductSkus = productRepository.findDistinctBySku();
		List<Product> products = new ArrayList<Product>();

		for (String sku : listOfProductSkus) {
			Product product = productRepository.findBySkuIgnoreCase(sku);
			products.add(product);
		}
		Collections.sort(products, new QuantityInHandForProductComparator());
		return processAndSortListBasedOnQuantityInhand(products);
	}

	private List<ProductStatusOnDashBoardDto> processAndSortListBasedOnQuantityInhand(List<Product> listOfProducts) {

		List<ProductStatusOnDashBoardDto> productStatusOnDashBoardDtos = new ArrayList<ProductStatusOnDashBoardDto>();

		for (Product product : listOfProducts) {
			ProductStatusOnDashBoardDto ProductStatusOnDashBoardDto = new ProductStatusOnDashBoardDto();
			ProductStatusOnDashBoardDto.setItemName(product.getDescription());
			ProductStatusOnDashBoardDto.setQuantityInHand(product.getQuantityInHand());
			ProductStatusOnDashBoardDto.setSku (product.getSku());
			productStatusOnDashBoardDtos.add(ProductStatusOnDashBoardDto);
		}
		return productStatusOnDashBoardDtos;
	}

	@Override
	@Scheduled(cron = "0 0 6 * * *")
	public List<Product> importAllItemsFromRamco() throws WmsException {
		// Set XML content type explicitly to force response in XML (If not spring get
		// response in JSON)
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

		ResponseEntity<String> response = rest.exchange(importExternalItemMasterUrl, HttpMethod.GET, entity,
				String.class);

		String responseBody = response.getBody().trim();
		responseBody = responseBody.split(">")[1];

		ImportExternalProductsJsonDto importExternalProductsJsonDto = convertToJavaObject(responseBody);
		List<Product> products = convertImportedProductsToProductDomain(importExternalProductsJsonDto);
		return products;
	}

	private List<Product> convertImportedProductsToProductDomain(
			ImportExternalProductsJsonDto importExternalProductsJsonDto) {
		List<Product> products = new ArrayList<Product>();
		Set<String> itemCodes = new HashSet<String>();
		List<ImportExternalProductDto> itemList = importExternalProductsJsonDto.getGetItemList();
		for (ImportExternalProductDto importExternalProductDto : itemList) {
			if (StringUtils.isNotEmpty(importExternalProductDto.getIou_itemcode())
					&& findBySkuIgnoreCase(importExternalProductDto.getIou_itemcode()) == null
					&& !itemCodes.contains(importExternalProductDto.getIou_itemcode())) {
				Product product = Product.builder().name(importExternalProductDto.getLoi_itemdesc())
						.category(importExternalProductDto.getIbu_category())
						.description(importExternalProductDto.getLoi_itemdesc())
						.hsnsacCode(importExternalProductDto.getHSNCode())
						.shortDescription(importExternalProductDto.getLoi_itemdesc())
						.fromUnitsOfMeasurement(UnitsOfMeasurement.CARTONS)
						.toUnitsOfMeasurement(UnitsOfMeasurement.valueOf(importExternalProductDto.getItemUom()))
						.conversionFactor(importExternalProductDto.getKG_CB_Con_Fact())
						.cartonToPacketConversionFactor(importExternalProductDto.getPKT_CB_Con_Fact().intValue())
						.productType(ProductType.STOCKABLE_PRODUCT).quantityInHand(0)
						.quanitityInHandUnitsOfMeasurement(UnitsOfMeasurement.CARTONS)
						.sku(importExternalProductDto.getIou_itemcode())
						.grammage(importExternalProductDto.getGrammage()).build();
				itemCodes.add(importExternalProductDto.getIou_itemcode());
				products.add(product);
			}
		}
		if (!CollectionUtils.isEmpty(products)) {
			products = save(products);
		}
		return products;
	}

	private ImportExternalProductsJsonDto convertToJavaObject(String responseBody) {
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new StringReader(responseBody));
		reader.setLenient(true);

		ImportExternalProductsJsonDto importExternalProductsJsonDto = gson.fromJson(reader,
				ImportExternalProductsJsonDto.class);

		return importExternalProductsJsonDto;
	}

	@Override
	@Transactional
	public List<Product> importItemMaster() throws WmsException {
		List<Product> products = new ArrayList<Product>();
		try {

			FileInputStream excelFile = new FileInputStream(new File(itemMasterFileName));
			@SuppressWarnings("resource")
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();
			int rowNumber = 1;
			while (iterator.hasNext()) {

				Row currentRow = iterator.next();
				if (rowNumber == 1) {
					rowNumber++;
					continue;
				}
				Iterator<Cell> cellIterator = currentRow.iterator();
				Product product = new Product();
				int colNumber = 1;
				while (cellIterator.hasNext()) {
					Cell currentCell = cellIterator.next();
					switch (colNumber) {

					case 1:
						product.setSku(currentCell.getStringCellValue());
						break;
					case 2:
						product.setDescription(currentCell.getStringCellValue());
						product.setQuanitityInHandUnitsOfMeasurement(UnitsOfMeasurement.CARTONS);
						product.setQuantityInHand(0);
						break;
					case 3:
						product.setShortDescription(currentCell.getStringCellValue());
						product.setName(currentCell.getStringCellValue());
						break;
					case 4:
						product.setSalesUnitsOfMeasure(UnitsOfMeasurement.valueOf(currentCell.getStringCellValue()));
						break;
					case 5:
						product.setFromUnitsOfMeasurement(UnitsOfMeasurement.CARTONS);
						product.setToUnitsOfMeasurement(UnitsOfMeasurement.KG);
						product.setConversionFactor(currentCell.getNumericCellValue());
						product.setProductType(ProductType.STOCKABLE_PRODUCT);
						break;
					case 7:
						Double cellValue = new Double(currentCell.getNumericCellValue());
						product.setCartonToPacketConversionFactor(cellValue.intValue());
						break;
					case 10:
						product.setCategory(currentCell.getStringCellValue());
						break;
					}
					colNumber++;
				}
				products.add(product);
				rowNumber++;
			}
		} catch (FileNotFoundException e) {
			log.error(e.getMessage());
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		return save(products);
	}

	@Override
	public List<Product> findByIdIn(List<Long> ids) {
		return productRepository.findByIdIn(ids);
	}

	@Override
	public Page<Product> findTopTenProduct() throws WmsException {
		int pageNumber = 0;
		int pageSize = 10;
		Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "dateCreated"));
		return findAll(pageable);
	}

	@Override
	public void reconcileProductQuantityInHand() {
		List<Product> productList = new ArrayList<>();
		List<Product> products = findAll();
		products.stream().forEach(product -> {
			Long totalProducts = inventoryService.countByProduct_IdAndOrderStatus(product.getId(),
					OrderStatus.AVAILABLE);
			product.setQuantityInHand(totalProducts.intValue());
			productList.add(product);
		});
		 save(productList);
	}



}
