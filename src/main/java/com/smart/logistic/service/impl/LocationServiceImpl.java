package com.smart.logistic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.Location;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.ILocationService;
import com.smart.logistic.service.generic.AbstractService;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class LocationServiceImpl extends AbstractService<Location> implements ILocationService{

	
	
	@Autowired
	ILocationService locationService;
	
	@Override
	protected WmsException notFoundException() {
		return new WmsException("Location not found");
	}

	
	
	
	@Override
	public Page<Location> findingAllLocation(Integer pageNumber, Integer pageSize) throws WmsException {
		log.info("pageNumber::"+pageNumber+"   pageSize::"+pageSize);
		Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "dateCreated"));
		validatePageNumber(pageNumber, pageSize);
		return locationService.findAll(pageable);
	}

	private void validatePageNumber(Integer pageNumber, Integer pageSize) throws WmsException {
		if ((pageNumber == null) || (pageSize == null) || (pageNumber < 0) || (pageSize < 0)) {
			throw new WmsException(HttpStatus.BAD_REQUEST, "PageNumber or PageSize not valid");
		}
	}
	
}
