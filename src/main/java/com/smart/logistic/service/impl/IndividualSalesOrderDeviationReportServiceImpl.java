package com.smart.logistic.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.IndividualSalesOrderDeviationReport;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.IIndividualSalesOrderDeviationReportRepository;
import com.smart.logistic.service.IIndividualSalesOrderDeviationReportService;
import com.smart.logistic.service.generic.AbstractService;

@Service
public class IndividualSalesOrderDeviationReportServiceImpl extends AbstractService<IndividualSalesOrderDeviationReport> implements IIndividualSalesOrderDeviationReportService{
	
	
	@Autowired 
	IIndividualSalesOrderDeviationReportRepository individualSalesOrderDeviationReportRepository;
	
	protected WmsException notFoundException() {
	
	return new WmsException("individualSalesOrderDeviationReport not found");
	}

	@Override
	public List<IndividualSalesOrderDeviationReport> findByDateCreatedBetween(Date startDate, Date endDate) {
		
		return individualSalesOrderDeviationReportRepository.findByDateCreatedBetween(startDate, endDate);
	}

	@Override
	public IndividualSalesOrderDeviationReport findTopDeviationReportByMaxDate() {
		return individualSalesOrderDeviationReportRepository.findTopByOrderByGeneratedDateTimeDesc();
	}

	@Override
	public List<IndividualSalesOrderDeviationReport> findByGeneratedDateTime(Date generatedDateTime) {
		return individualSalesOrderDeviationReportRepository.findByGeneratedDateTime(generatedDateTime);
	}

}
