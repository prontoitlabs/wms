package com.smart.logistic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.TransferOrderPackslip;
import com.smart.logistic.domain.TransferOrderPackslipItem;
import com.smart.logistic.dto.TransferOrderPackslipDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.ITransferOrderPackslipItemService;
import com.smart.logistic.service.ITransferOrderPackslipService;
import com.smart.logistic.service.generic.AbstractService;
import com.smart.logistic.transformer.TransferOrderPackslipTransformer;

@Service
public class TransferOrderPackslipItemServiceImpl extends AbstractService<TransferOrderPackslipItem>
		implements ITransferOrderPackslipItemService {

	@Autowired
	private ITransferOrderPackslipService transferOrderPackslipService;

	@Autowired
	private TransferOrderPackslipTransformer transferOrderPackslipTransformer;

	@Override
	protected WmsException notFoundException() {
		return new WmsException("Transfer Order pack slip item not found");
	}

	@Override
	public TransferOrderPackslipDto getTransferOrderPackslipDetailById(Long id) throws WmsException {
		TransferOrderPackslip transferOrderPackslip = transferOrderPackslipService.findOne(id);
		return transferOrderPackslipTransformer.toDtoEntity(transferOrderPackslip);
	}

}
