package com.smart.logistic.service.impl;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.smart.logistic.domain.Inventory;
import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.SalesOrder;
import com.smart.logistic.domain.SalesOrderDetails;
import com.smart.logistic.domain.SalesOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.SalesOrderPackslip;
import com.smart.logistic.domain.SalesOrderPackslipItem;
import com.smart.logistic.domain.enums.OrderFulfillmentType;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;
import com.smart.logistic.dto.SalesOrderDto;
import com.smart.logistic.dto.SalesOrderPackslipDto;
import com.smart.logistic.dto.SalesOrderPackslipItemDto;
import com.smart.logistic.dto.VehicleAllocationDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.IOrderPackslipItemRepository;
import com.smart.logistic.repository.IOrderPackslipRepository;
import com.smart.logistic.service.IInventoryService;
import com.smart.logistic.service.IOrderPackslipItemService;
import com.smart.logistic.service.IOrderPackslipService;
import com.smart.logistic.service.IPackslipDispatchDetailsService;
import com.smart.logistic.service.IProductService;
import com.smart.logistic.service.ISalesOrderDetailsService;
import com.smart.logistic.service.ISalesOrderService;
import com.smart.logistic.service.generic.AbstractService;
import com.smart.logistic.transformer.SalesOrderPackslipTransformer;
import com.smart.logistic.transformer.SalesOrderTransformer;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SalesOrderPackslipServiceImpl extends AbstractService<SalesOrderPackslip>
        implements IOrderPackslipService {

    @Value("${pallets.to.cartons.conversion.factor}")
    private Integer palletsToCartonsConversionFactor;

    @Autowired
    private ISalesOrderService salesOrderService;

    @Autowired
    private IProductService productService;

    @Autowired
    private IOrderPackslipRepository orderPackslipRepository;

    @Autowired
    private IOrderPackslipItemRepository orderPackslipItemRepository;

    @Autowired
    private IPackslipDispatchDetailsService packSlipDispatchDetailsService;

    @Autowired
    private ISalesOrderDetailsService salesOrderDetailsService;

    @Autowired
    private IInventoryService inventoryService;

    @Autowired
    private SalesOrderPackslipTransformer orderPackslipTransformer;

    @Autowired
    private IOrderPackslipItemService orderPackslipItemService;

    @Autowired
    private SalesOrderTransformer salesOrderTransformer;

    @Value("${hst.local.path}")
    private String hstLocalDirectoryPath;

    @Value(value = "${aws.dispatch.summary.remote.inward.directory}")
    private String awsDispatchSummaryRemoteDirectory;

    @Value(value = "${aws.bucket.name}")
    private String bucketName;


    @Autowired
    private AmazonS3 amazonS3;

    @Override
    public Page<SalesOrderPackslip> getAllPackingSlips(Integer pageNumber, Integer pageSize) throws WmsException {
        Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "dateCreated"));
        validatePageNumber(pageNumber, pageSize);
        return findAll(pageable);
    }

    private void validatePageNumber(Integer pageNumber, Integer pageSize) throws WmsException {
        if ((pageNumber == null) || (pageSize == null) || (pageNumber < 0) || (pageSize < 0)) {
            throw new WmsException(HttpStatus.BAD_REQUEST, "PageNumber or PageSize not valid");
        }
    }

    @Override
    protected WmsException notFoundException() {
        return new WmsException("Packing slip not found");
    }

    @Override
    public SalesOrderPackslip validateSalesOrderPackSlip(Long packslipId) throws WmsException {
        // import hst data for PACK SLIP / PKSH/000492/1819
        SalesOrderPackslip orderPackslip = findOne(new Long(packslipId));
        // check status of packslip
        List<Long> inventoryIds = new ArrayList<Long>();
        if (orderPackslip.getOrderStatus().equals(OrderStatus.DONE)) {
            throw new WmsException(HttpStatus.OK, "The Order Packslip is already validated");
        }

        // more validation
        List<SalesOrderPackSlipDispatchDetails> packslipDispatchDetails = packSlipDispatchDetailsService
                .findAllPackslipDispatchDetailsByPackslipIdAndStatus(packslipId, OrderStatus.DONE);
        if (packslipDispatchDetails.size() != orderPackslip.getPackslipQuantity() || packslipDispatchDetails.size() == 0
                || orderPackslip.getVerifiedCases() == 0) {
            throw new WmsException(HttpStatus.OK,
                    "The Order Packslip cannot be validated before all the Cartons are Processed");
        }

        for (SalesOrderPackslipItem orderPackSlipItem : orderPackslip.getOrderPackslipItems()) {
            List<Inventory> inventories;
            inventories = getAllReservedInventoriesByStatus(orderPackSlipItem).getContent();
            inventories.stream().forEach(inventory -> {
                inventoryIds.add(inventory.getId());
            });
        }
        log.info("Updating Inventory order STATUS as READY");
        for (Long inventoryId : inventoryIds) {
            inventoryService.updateInventoryOrderStatusAsReadyById(inventoryId);
        }
        orderPackslip.setOrderStatus(OrderStatus.DONE);
        orderPackslip = save(orderPackslip);
        List<SalesOrder> salesOrders = new ArrayList<SalesOrder>();
        salesOrders.addAll(orderPackslip.getSalesOrders());
        salesOrders.forEach(order -> order.setStatus(OrderStatus.DONE));
        salesOrderService.save(salesOrders);
        log.info("packslip validated succesfully: " + orderPackslip);
        return orderPackslip;
    }

    private Page<Inventory> getAllReservedInventoriesByStatus(SalesOrderPackslipItem orderPackSlipItem)
            throws WmsException {
        log.info("inside getAllReserviedInventoriesByStatus page size =  : " + orderPackSlipItem.getPackslipQuantity());
        Pageable pageable = new PageRequest(0, orderPackSlipItem.getPackslipQuantity(),
                new Sort(Direction.ASC, "dateCreated"));
        Page<Inventory> inventories = inventoryService.findInventoryByOrderStatusAndProduct(OrderStatus.RESERVED,
                orderPackSlipItem.getProduct(), pageable);
        log.info("Total Reserved Stock : " + inventories.getSize());
        if (inventories.getSize() < orderPackSlipItem.getPackslipQuantity()) {
            throw new WmsException(HttpStatus.OK, "Item's Quanitity in the Stock is not sufficient for Ready Dispatch");
        }
        return inventories;
    }

    @Override
    public SalesOrderPackslipDto createPackSlip() throws WmsException {
        SalesOrderPackslipDto orderPackslipDto = SalesOrderPackslipDto.builder().packslipQuantity(new Integer(0))
                .verifiedCases(new Integer(0)).packslipQuantity(0).unitOfMeasure(UnitsOfMeasurement.CARTONS)
                .orderStatus(OrderStatus.WAITING).packslipDate(DateTime.now().toDate())
                .orderPackSlipItemDto(new ArrayList<SalesOrderPackslipItemDto>()).build();
        return orderPackslipDto;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SalesOrderPackslipDto savePackSlip(SalesOrderPackslipDto orderPackslipDto) throws WmsException {
        log.info("inside Save PackSlip : " + orderPackslipDto);
        SalesOrderPackslip packslip = createOrderPackslip(orderPackslipDto);
        return orderPackslipTransformer.toDtoEntity(packslip);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SalesOrderPackslipDto editPackSlip(SalesOrderPackslipDto orderPackslipDto) throws WmsException {
        log.info("inside Edit PackSlip : " + orderPackslipDto);
        SalesOrderPackslip packslip = editOrderPackslip(orderPackslipDto);
        return orderPackslipTransformer.toDto(packslip);
    }

    private SalesOrderPackslip createOrderPackslip(SalesOrderPackslipDto orderPackslipDto) throws WmsException {
        int totalPendingCases = orderPackslipDto.getPackslipQuantity();
        SalesOrderPackslip salesOrderPackslip = SalesOrderPackslip.builder()
                .packslipNumber(orderPackslipDto.getPackslipNumber()).packslipDate(orderPackslipDto.getPackslipDate())
                .packslipQuantity(orderPackslipDto.getPackslipQuantity())
                .vehicleNumber(orderPackslipDto.getVehicleNumber()).verifiedCases(orderPackslipDto.getVerifiedCases())
                .orderStatus(OrderStatus.WAITING).unitOfMeasure(UnitsOfMeasurement.CARTONS).build();
        Set<SalesOrder> salesOrderDomainList = toSalesOrderDomainList(orderPackslipDto.getSalesOrdersDto(),
                salesOrderPackslip);

        salesOrderPackslip.setSalesOrders(salesOrderDomainList);

        SalesOrder firstSalesOrder = salesOrderDomainList.iterator().next();
        if (firstSalesOrder.getOrderFulfillmentType().equals(OrderFulfillmentType.CARTON)
                && CollectionUtils.isNotEmpty(orderPackslipDto.getOrderPackSlipItemDto())) {
            List<SalesOrderPackslipItem> packSlipItemDomainList = toOrderPackSlipItemDomainList(
                    orderPackslipDto.getOrderPackSlipItemDto(), salesOrderPackslip);
            salesOrderPackslip.setOrderPackslipItems(packSlipItemDomainList);
        } else if (firstSalesOrder.getOrderFulfillmentType().equals(OrderFulfillmentType.PALLET)) {

            List<SalesOrderPackslipItem> salesOrderPackslipItems = new ArrayList<>();
            for (SalesOrder salesOrder : salesOrderDomainList) {
                for (SalesOrderDetails salesOrderDetail : salesOrder.getSalesOrderDetails()) {
                    // Case : No More Pending Cartons to fill the truck
                    if (totalPendingCases == 0) {
                        break;
                    }
                    if (salesOrderDetail.getActualReservedQuantityInCB() <= 0) {
                        continue;
                    }
                    SalesOrderPackslipItem orderPackslipItem = SalesOrderPackslipItem.builder()
                            .product(salesOrderDetail.getProduct()).verifiedCases(0).salesOrder(salesOrder)
                            .unitOfMeasure(UnitsOfMeasurement.CARTONS).orderPackslip(salesOrderPackslip).build();

                    if (totalPendingCases >= salesOrderDetail.getActualReservedQuantityInCB()) {
                        orderPackslipItem.setPackslipQuantity(salesOrderDetail.getActualReservedQuantityInCB());
                    } else {
                        orderPackslipItem.setPackslipQuantity(totalPendingCases);
                        salesOrderDetail.setActualReservedQuantityInCB(totalPendingCases);
                        salesOrderDetail.setActualReservedQuantityInKG(totalPendingCases * salesOrderDetail.getConversionFactor());
                        salesOrderDetailsService.save(salesOrderDetail);
                    }
                    salesOrderPackslipItems.add(orderPackslipItem);
                    totalPendingCases = totalPendingCases - salesOrderDetail.getActualReservedQuantityInCB();
                }
            }
            salesOrderPackslip.setOrderPackslipItems(salesOrderPackslipItems);
        }

        return save(salesOrderPackslip);
    }

    private SalesOrderPackslip editOrderPackslip(SalesOrderPackslipDto orderPackslipDto) throws WmsException {
        SalesOrderPackslip salesOrderPackslip = findOne(orderPackslipDto.getId());
        salesOrderPackslip.setPackslipNumber(orderPackslipDto.getPackslipNumber());
        salesOrderPackslip.setPackslipDate(orderPackslipDto.getPackslipDate());
        salesOrderPackslip.setPackslipQuantity(orderPackslipDto.getPackslipQuantity());
        salesOrderPackslip.setVerifiedCases(orderPackslipDto.getVerifiedCases());
        salesOrderPackslip.setVehicleNumber(orderPackslipDto.getVehicleNumber());

        List<SalesOrderPackslipItem> packSlipItemDomainList = transformOrderPackSlipItemDtoToDomain(
                orderPackslipDto.getOrderPackSlipItemDto(), salesOrderPackslip);
        salesOrderPackslip.getOrderPackslipItems().addAll(packSlipItemDomainList);

        Set<SalesOrder> salesOrderDomainList = toSalesOrderDomainList(orderPackslipDto.getSalesOrdersDto(),
                salesOrderPackslip);
        salesOrderPackslip.setSalesOrders(salesOrderDomainList);

        return save(salesOrderPackslip);
    }

    private List<SalesOrderPackslipItem> transformOrderPackSlipItemDtoToDomain(
            List<SalesOrderPackslipItemDto> orderPackSlipItemDto, SalesOrderPackslip salesOrderPackslip)
            throws WmsException {
        List<SalesOrderPackslipItem> orderPackslipItems = new ArrayList<SalesOrderPackslipItem>();
        for (SalesOrderPackslipItemDto salesOrderPackslipItemDto : orderPackSlipItemDto) {
            if (salesOrderPackslipItemDto.getId() != null) {
                SalesOrderPackslipItem salesOrderPackslipItem = orderPackslipItemService
                        .findOne(salesOrderPackslipItemDto.getId());
                Product product = productService.findBySkuIgnoreCase(salesOrderPackslipItemDto.getSku());
                if (product == null) {
                    throw new WmsException(HttpStatus.BAD_REQUEST,
                            "Product sku " + salesOrderPackslipItemDto.getSku() + " doesn't exist in our database");
                }
                salesOrderPackslipItem.setProduct(product);
                salesOrderPackslipItem.setPackslipQuantity(salesOrderPackslipItemDto.getPackslipQuantity());
                salesOrderPackslipItem.setPackslipQuantity(salesOrderPackslipItemDto.getPackslipQuantity());
                salesOrderPackslipItem.setVerifiedCases(salesOrderPackslipItemDto.getVerifiedCases());
                salesOrderPackslipItem.setSalesOrder(
                        salesOrderService.findSalesOrderByNumber(salesOrderPackslipItemDto.getSalesOrderNumber()));
                orderPackslipItems.add(salesOrderPackslipItem);
            } else {
                orderPackslipItems.add(toOrderPackslipItemDomain(salesOrderPackslipItemDto, salesOrderPackslip));
            }
        }
        return orderPackslipItems;
    }

    private Set<SalesOrder> toSalesOrderDomainList(Set<SalesOrderDto> salesOrdersDto,
                                                   SalesOrderPackslip salesOrderPackslip) throws WmsException {
        Set<SalesOrder> salesOrders = new HashSet<SalesOrder>();
//		int pendingCases = 0;
        for (SalesOrderDto salesOrderDto : salesOrdersDto) {
            SalesOrder salesOrder = salesOrderService.findOne(salesOrderDto.getId());
            salesOrder.getPackslips().add(salesOrderPackslip);

//			List<SalesOrderDetails> salesOrderDetails = salesOrder.getSalesOrderDetails();
//			for (SalesOrderDetails salesOrderDetail : salesOrderDetails) {
//				pendingCases = pendingCases + salesOrderDetail.getPendingQuantityInCB();
//			}
            salesOrders.add(salesOrder);
        }
        return salesOrders;
    }

    private List<SalesOrderPackslipItem> toOrderPackSlipItemDomainList(
            List<SalesOrderPackslipItemDto> orderPackSlipItemDto, SalesOrderPackslip salesOrderPackslip)
            throws WmsException {
        List<SalesOrderPackslipItem> orderPackslipItems = new ArrayList<SalesOrderPackslipItem>();
        for (SalesOrderPackslipItemDto dto : orderPackSlipItemDto) {
            orderPackslipItems.add(toOrderPackslipItemDomain(dto, salesOrderPackslip));
        }
        return orderPackslipItems;
    }

    private SalesOrderPackslipItem toOrderPackslipItemDomain(SalesOrderPackslipItemDto orderPackslipItemDto,
                                                             SalesOrderPackslip salesOrderPackslip) throws WmsException {
        Product product = productService.findBySkuIgnoreCase(orderPackslipItemDto.getSku());
        if (product == null) {
            throw new WmsException(HttpStatus.BAD_REQUEST,
                    "Product sku " + orderPackslipItemDto.getSku() + " doesn't exist in our database");
        }
        SalesOrderPackslipItem orderPackslipItem = SalesOrderPackslipItem.builder().product(product).verifiedCases(0)
                .salesOrder(salesOrderService.findSalesOrderByNumber(orderPackslipItemDto.getSalesOrderNumber()))
                .packslipQuantity(orderPackslipItemDto.getPackslipQuantity()).orderPackslip(salesOrderPackslip)
                .unitOfMeasure(UnitsOfMeasurement.CARTONS).build();
        return orderPackslipItem;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SalesOrderPackslip allocateInventoryForOrderPackslip(Long id) throws WmsException {
        SalesOrderPackslip salesOrderPackslip = findOne(id);
        validatePackslipBeforeAllocation(salesOrderPackslip);
        salesOrderPackslip = createPackslipDispatchDetails(salesOrderPackslip);

        return salesOrderPackslip;
    }

    private void validatePackslipBeforeAllocation(SalesOrderPackslip salesOrderPackslip) throws WmsException {
        if (!salesOrderPackslip.getOrderStatus().equals(OrderStatus.WAITING)) {
            throw new WmsException(HttpStatus.BAD_REQUEST,
                    "Sales Order Packslip Has to be in a Waiting State for allocation to be performed");
        }
    }

    private SalesOrderPackslip createPackslipDispatchDetails(SalesOrderPackslip packslip) throws WmsException {
        List<SalesOrderPackslipItem> orderPackslipItems = packslip.getOrderPackslipItems();
        List<Inventory> inventoryToUpdate = new ArrayList<Inventory>();

        for (SalesOrderPackslipItem orderPackSlipItem : orderPackslipItems) {
            if (orderPackSlipItem.getPackslipQuantity() == 0) {
                continue;
            }
            List<Inventory> inventories = getAllInventoriesByStatus(orderPackSlipItem);

            List<SalesOrderPackSlipDispatchDetails> packSlipDispatchDetailList = new ArrayList<SalesOrderPackSlipDispatchDetails>();
            inventories.stream().forEach(inventory -> {
                SalesOrderPackSlipDispatchDetails packSlipDispatchDetails = SalesOrderPackSlipDispatchDetails.builder()
                        .scanBarcode(inventory.getScanBarcode()).palletBarcode(inventory.getPalletBarcode())
                        .grossWeight(inventory.getGrossWeight()).netWeight(inventory.getNetWeight())
                        .status(OrderStatus.DONE).batchNumber(inventory.getBatchNumber())
                        .scanDate(inventory.getInventoryDate()).product(inventory.getProduct())
                        .orderPackslipItem(orderPackSlipItem).build();

                packSlipDispatchDetailList.add(packSlipDispatchDetails);
                inventory.setOrderStatus(OrderStatus.DONE);
                inventoryToUpdate.add(inventory);
            });
            orderPackSlipItem.setPackslipDispatchDetails(packSlipDispatchDetailList);
        }

        packslip.setOrderStatus(OrderStatus.DONE);
        packslip.setOrderPackslipItems(new ArrayList<SalesOrderPackslipItem>(orderPackslipItems));

        updateSalesOrderQuantityForPalletizedOrder(orderPackslipItems);
        inventoryService.save(inventoryToUpdate);
        return save(packslip);
    }

    private void updateSalesOrderQuantityForPalletizedOrder(List<SalesOrderPackslipItem> orderPackslipItems) throws WmsException {
        List<SalesOrderDetails> salesOrderDetailList = new ArrayList<SalesOrderDetails>();
        Map<Long, Integer> salesOrderPendingQuantityInCB = new HashMap<>();
        List<Product> productToSave = new ArrayList<>();
        for (SalesOrderPackslipItem salesOrderPackslipItem : orderPackslipItems) {
            Long salesOrderId = salesOrderPackslipItem.getSalesOrder().getId();
            List<SalesOrderDetails> salesOrderDetailsListByItems = salesOrderService
                    .findSalesOrderDetailByProductAndSalesOrder(salesOrderPackslipItem.getProduct().getId(),
                            salesOrderId);
            Product product = salesOrderPackslipItem.getProduct();

            for (SalesOrderDetails salesOrderDetail : salesOrderDetailsListByItems) {
                int deviation = salesOrderPackslipItem.getPackslipQuantity()
                        - salesOrderDetail.getActualReservedQuantityInCB();
                if (deviation > 0) {
                    product.setQuantityInHand(product.getQuantityInHand() - deviation);
                    productToSave.add(product);
                }
                Integer actualShippedQuantityInCB = salesOrderDetail.getActualShippedQuantityInCB() != null
                        ? salesOrderDetail.getActualShippedQuantityInCB()
                        : 0;
                salesOrderDetail.setActualShippedQuantityInCB(
                        actualShippedQuantityInCB + salesOrderPackslipItem.getPackslipQuantity());
                salesOrderDetail.setActualShippedQuantityInKG(
                        salesOrderDetail.getActualShippedQuantityInCB() * salesOrderDetail.getConversionFactor());

                if (salesOrderDetail.getActualShippedQuantityInCB() < salesOrderDetail.getTotalOrderQuanityInCB()) {
                    salesOrderDetail.setActualReservedQuantityInCB(salesOrderDetail.getActualReservedQuantityInCB() - salesOrderDetail.getActualShippedQuantityInCB());
                    salesOrderDetail.setActualReservedQuantityInKG(salesOrderDetail.getActualReservedQuantityInCB() * salesOrderDetail.getConversionFactor());
                } else {
                    salesOrderDetail.setActualReservedQuantityInCB(0);
                    salesOrderDetail.setActualReservedQuantityInKG(0D);
                }

                salesOrderDetailList.add(salesOrderDetail);

                if (!salesOrderPendingQuantityInCB.containsKey(salesOrderId)) {
                    salesOrderPendingQuantityInCB.put(salesOrderId, salesOrderDetail.getTotalOrderQuanityInCB()
                            - salesOrderDetail.getActualShippedQuantityInCB());
                } else {
                    salesOrderPendingQuantityInCB.put(salesOrderId,
                            salesOrderPendingQuantityInCB.get(salesOrderId)
                                    + (salesOrderDetail.getTotalOrderQuanityInCB()
                                    - salesOrderDetail.getActualShippedQuantityInCB()));
                }
            }

        }
        if (CollectionUtils.isNotEmpty(productToSave)) {
            productService.save(productToSave);
        }
        salesOrderDetailsService.save(salesOrderDetailList);
        for (Entry<Long, Integer> entry : salesOrderPendingQuantityInCB.entrySet()) {
            SalesOrder salesOrder = salesOrderService.findOne(entry.getKey());
            SalesOrderDto salesOrderDto = salesOrderTransformer.toDto(salesOrder);
            if (salesOrderDto.getActualShippedQuantityInCB() < salesOrderDto.getTotalOrderQuanityInCB()) {
                salesOrder.setStatus(OrderStatus.PARTIALLY_FULFILLMENT_DONE);
            } else {
                salesOrder.setStatus(OrderStatus.FULFILLED);
            }
            salesOrderService.save(salesOrder);
        }
    }

    private List<Inventory> getAllInventoriesByStatus(SalesOrderPackslipItem orderPackSlipItem) throws WmsException {
        Integer packslipQuantity = orderPackSlipItem.getPackslipQuantity();
        int numberOfPallets = packslipQuantity / palletsToCartonsConversionFactor;

        List<Object[]> queryData = inventoryService.findTOPNDistinctPalletBarCodesByStatus(OrderStatus.AVAILABLE,
                orderPackSlipItem.getProduct(), numberOfPallets + 1);
        List<String> palletBarCodeList = new ArrayList<>();
        for (Object[] objects : queryData) {
            if (objects[0] != null && objects[0] != "") {
                palletBarCodeList.add(objects[0].toString());
            }
        }

        if (CollectionUtils.isEmpty(palletBarCodeList) || palletBarCodeList.size() < numberOfPallets) {
            throw new WmsException(HttpStatus.BAD_REQUEST,
                    "Item's Quanitity in the Stock is not sufficient for Reserving Dispatch");
        }

        List<Inventory> inventories = inventoryService.findInventoryByPalletBarCodes(palletBarCodeList);
        log.info("Total Available Stock : " + inventories.size());

        if (inventories.size() < packslipQuantity) {
            throw new WmsException(HttpStatus.BAD_REQUEST,
                    "Item's Quanitity in the Stock is not sufficient for Reserving Dispatch");
        }
        return inventories;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<SalesOrderPackslip> allocateVehicle(List<VehicleAllocationDto> vehicles) throws WmsException {
        List<SalesOrderPackslip> packslipsCreated = new ArrayList<SalesOrderPackslip>();
        for (VehicleAllocationDto vehicleAllocationDto : vehicles) {
            List<SalesOrderPackslipItem> salesOrderPackslipItems = new ArrayList<SalesOrderPackslipItem>();
            List<String> salesOrderNumbers = vehicleAllocationDto.getSalesOrderNumbers();
            Integer totalPackslipQuantityInCartons = vehicleAllocationDto.getTotalPackslipQuantityInCartons();
            SalesOrderPackslip salesOrderPackslip = SalesOrderPackslip.builder().packslipDate(DateTime.now().toDate())
                    .packslipQuantity(totalPackslipQuantityInCartons)
                    .vehicleNumber(vehicleAllocationDto.getVehicleNumber()).verifiedCases(0)
                    .orderStatus(OrderStatus.WAITING).unitOfMeasure(UnitsOfMeasurement.CARTONS).build();

            int totalPendingCases = totalPackslipQuantityInCartons;
            boolean isCartonizedPackslip = false;
            boolean isPalletizedPackslip = false;

            for (String salesOrderNumber : salesOrderNumbers) {
                SalesOrder salesOrder = salesOrderService.findSalesOrderByNumber(salesOrderNumber);

                salesOrder.getPackslips().add(salesOrderPackslip);
                salesOrder.setStatus(OrderStatus.VEHICLE_ALLOCATED);
                salesOrderPackslip.getSalesOrders().add(salesOrder);

                if (!salesOrder.getStatus().equals(OrderStatus.QUEUED_FOR_VEHICLE_ALLOCATION)
                        && !salesOrder.getStatus().equals(OrderStatus.VEHICLE_ALLOCATED)
                        && !salesOrder.getStatus().equals(OrderStatus.PARTIALLY_FULFILLMENT_DONE)) {
                    throw new WmsException(HttpStatus.BAD_REQUEST, "The Sales Order Number : "
                            + salesOrder.getOrderNumber() + " is not queued for Dispatch with the Transporter");
                }
                if (salesOrder.getOrderFulfillmentType().equals(OrderFulfillmentType.CARTON)) {
                    isCartonizedPackslip = true;
                    if (isPalletizedPackslip) {
                        throw new WmsException(HttpStatus.BAD_REQUEST, "The Sales Order Number : "
                                + salesOrder.getOrderNumber() + " is not queued for Dispatch with the Transporter");
                    }
                    for (SalesOrderDetails salesOrderDetail : salesOrder.getSalesOrderDetails()) {
                        if (salesOrderDetail.getActualReservedQuantityInCB() > 0) {
                            SalesOrderPackslipItem orderPackslipItem = SalesOrderPackslipItem.builder()
                                    .product(salesOrderDetail.getProduct()).verifiedCases(0).packslipQuantity(0)
                                    .salesOrder(salesOrder).unitOfMeasure(UnitsOfMeasurement.CARTONS)
                                    .orderPackslip(salesOrderPackslip).build();
                            salesOrderPackslipItems.add(orderPackslipItem);
                        }
                    }
                    salesOrderPackslip.getOrderPackslipItems().addAll(salesOrderPackslipItems);
                    packslipsCreated.add(salesOrderPackslip);
                } else if (salesOrder.getOrderFulfillmentType().equals(OrderFulfillmentType.PALLET)) {
                    isPalletizedPackslip = true;
                    if (isCartonizedPackslip) {
                        throw new WmsException(HttpStatus.BAD_REQUEST,
                                "A Packslip can either contain All Palletized Orders or All Cartonized Orders!!");
                    }
                    for (SalesOrderDetails salesOrderDetail : salesOrder.getSalesOrderDetails()) {
                        // Case : No More Pending Cartons to fill the truck
                        if (totalPendingCases <= 0) {
                            break;
                        }
                        if (salesOrderDetail.getActualReservedQuantityInCB() > 0) {
                            SalesOrderPackslipItem orderPackslipItem = SalesOrderPackslipItem.builder()
                                    .product(salesOrderDetail.getProduct()).verifiedCases(0).salesOrder(salesOrder)
                                    .unitOfMeasure(UnitsOfMeasurement.CARTONS).orderPackslip(salesOrderPackslip)
                                    .build();

                            if (totalPendingCases >= salesOrderDetail.getActualReservedQuantityInCB()) {
                                orderPackslipItem.setPackslipQuantity(salesOrderDetail.getActualReservedQuantityInCB());
                            } else {
                                orderPackslipItem.setPackslipQuantity(totalPendingCases);
                                salesOrderDetail.setActualReservedQuantityInCB(totalPendingCases);
                                salesOrderDetail.setActualReservedQuantityInKG(totalPendingCases * salesOrderDetail.getConversionFactor());
                                salesOrderDetailsService.save(salesOrderDetail);
                            }
                            salesOrderPackslipItems.add(orderPackslipItem);
                            totalPendingCases = totalPendingCases - salesOrderDetail.getActualReservedQuantityInCB();
                        }
                    }
                    salesOrderPackslip.getOrderPackslipItems().addAll(salesOrderPackslipItems);
                    packslipsCreated.add(salesOrderPackslip);
                }
            }
        }
        return save(packslipsCreated);
    }

    @Override
    public List<SalesOrderPackslipItem> getAllPendingItemsFromSalesOrders(List<Long> salesOrderIds)
            throws WmsException {
        List<SalesOrderPackslipItem> salesOrderPackslipItems = new ArrayList<SalesOrderPackslipItem>();
        for (Long salesOrderId : salesOrderIds) {
            SalesOrder salesOrder = salesOrderService.findOne(salesOrderId);
            List<SalesOrderDetails> salesOrderDetails = salesOrder.getSalesOrderDetails();
            for (SalesOrderDetails salesOrderDetail : salesOrderDetails) {
//                if (salesOrderDetail.getActualReservedQuantityInCB() > 0)
                {
                    SalesOrderPackslipItem orderPackslipItem = SalesOrderPackslipItem.builder()
                            .product(salesOrderDetail.getProduct()).verifiedCases(0).salesOrder(salesOrder)
                            .packslipQuantity(0).unitOfMeasure(UnitsOfMeasurement.CARTONS).build();
                    salesOrderPackslipItems.add(orderPackslipItem);
                }
            }
        }
        return salesOrderPackslipItems;
    }

    @Override
    public List<SalesOrderPackslip> findByLastModifiedBetweenAndOrderStatus(Date startDate, Date endDate,
                                                                            OrderStatus orderStatus) {
        return orderPackslipRepository.findByLastModifiedBetweenAndOrderStatus(startDate, endDate, orderStatus);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SalesOrderPackslip importHSTDataForPackslip(Long packSlipId) throws WmsException {

        // get the packslip number from api
        SalesOrderPackslip salesOrderPackslip = findOne(packSlipId);

        Map<Long, SalesOrderPackslipItem> salesOrderPackslipItemMap = new HashMap<>();

        // validation
        validatePackslipForHSTImport(salesOrderPackslip);

        // sku map
        Map<String, SalesOrderPackslipItem> packslipItemSkuMap = extractPackslipItemSkuMap(salesOrderPackslip);

        // products
        Map<String, Product> productMap = new HashMap<String, Product>();
        // generate file number
        String fileName = StringUtils.replaceChars(salesOrderPackslip.getPackslipNumber(), "/", "-")
                + ".xlsx";


        String srcKey = (awsDispatchSummaryRemoteDirectory + "/" + fileName).toLowerCase();
        log.info("validating " + srcKey + " in bucket " + bucketName);

        List<Inventory> inventoryList = new ArrayList<Inventory>();

        int verifiedCasesCount = 0;
        try {

            boolean doesObjectExist = amazonS3.doesObjectExist(bucketName, srcKey );
            InputStream stream = null;
            if (doesObjectExist) {
                S3Object fullObject = amazonS3.getObject(new GetObjectRequest(bucketName, srcKey));
                stream = fullObject.getObjectContent();
                log.info("contents read from s3");
            } else {
                log.info("proceeding with local read");
                stream = new FileInputStream(new File(hstLocalDirectoryPath + "/" + fileName));
            }

            // FileInputStream excelFile = new FileInputStream(new File(hstLocalDirectoryPath + "/" + fileName));
            // FileInputStream excelFile = new FileInputStream(stream);

            @SuppressWarnings("resource")
            Workbook workbook = new XSSFWorkbook(stream);
            Sheet datatypeSheet = workbook.getSheetAt(0); // get first sheet
            Iterator<Row> iterator = datatypeSheet.iterator();
            int rowNumber = 0;
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellIterator = currentRow.iterator();
                SalesOrderPackSlipDispatchDetails salesOrderPackSlipDispatchDetail = new SalesOrderPackSlipDispatchDetails();
                salesOrderPackSlipDispatchDetail.setStatus(OrderStatus.DONE);
                int colNumber = 1;
                String ramcoPackslipNumber = "";
                Date scanDate = null;
                String ramcoOrderNumber = "";
                String skuCode = "";
                String batchNumber = "";
                String scanBarcode = "";
                Product product = null;
                DataFormatter formatter = new DataFormatter();
                // for each cell in a row
                while (cellIterator.hasNext()) {
                    Cell currentCell = cellIterator.next();
                    switch (colNumber) {

                        case 0:
                            break;
                        case 1:
                            currentCell.getStringCellValue();
                            break;
                        case 2:
                            ramcoPackslipNumber = currentCell.getStringCellValue();
                            break;
                        case 3:
                            scanDate = DateTime.parse(formatter.formatCellValue(currentCell)).toDate();
                            salesOrderPackSlipDispatchDetail.setScanDate(scanDate);
                            break;
                        case 4:
                            ramcoOrderNumber = currentCell.getStringCellValue();
                            break;
                        case 5:
                            skuCode = currentCell.getStringCellValue();
                            if (productMap.containsKey(skuCode)) {
                                product = productMap.get(skuCode);
                            } else {
                                product = productService.findBySkuIgnoreCase(skuCode);
                                productMap.put(skuCode, product);
                            }
                            salesOrderPackSlipDispatchDetail.setProduct(product);
                            break;
                        case 6:
                            break;
                        case 7:
                            break;
                        case 8:
                            scanBarcode = currentCell.getStringCellValue();
                            // log.info("Row Number  :" + rowNumber);
                            // log.info("ScanBarCode  :" + scanBarcode);
                            salesOrderPackSlipDispatchDetail.setScanBarcode(scanBarcode);
                            break;
                        case 9:
                            batchNumber = currentCell.getStringCellValue();
                            salesOrderPackSlipDispatchDetail.setBatchNumber(batchNumber);
                            break;
                        case 10:
                            salesOrderPackSlipDispatchDetail.setNetWeight(currentCell.getNumericCellValue());
                            break;

                    }
                    colNumber++;
                }
                if (!ramcoPackslipNumber.equals(salesOrderPackslip.getPackslipNumber())) {
                    log.error("Packslip Number not matching with the Ramco Packslip Number in the HST File");
                    throw new WmsException(HttpStatus.BAD_REQUEST,
                            "Packslip Number not matching with the Ramco Packslip Number in the HST File");
                }
                // Update Inventory with the Status
                Inventory inventory = inventoryService.findByCartonBarCodeAndStatus(scanBarcode, OrderStatus.AVAILABLE);
                if (inventory == null) {
                    log.error("Carton cannot be found in row number " + rowNumber + " for the specified barCode : "
                            + scanBarcode + ". Please fix the Code and try again later!!");


                } else {
                    inventory.setOrderStatus(OrderStatus.DONE);
                    inventory.setBatchNumber(batchNumber);
                    inventoryList.add(inventory);
                    salesOrderPackSlipDispatchDetail.setGrossWeight(inventory.getGrossWeight());
                }

                SalesOrderPackslipItem salesOrderPackslipItem = updatePackslipItem(salesOrderPackslip, productMap,
                        packslipItemSkuMap, salesOrderPackSlipDispatchDetail, skuCode, ramcoOrderNumber);

                if (!salesOrderPackslipItemMap.containsKey(salesOrderPackslipItem.getId())) {
                    salesOrderPackslipItemMap.put(salesOrderPackslipItem.getId(), salesOrderPackslipItem);
                }
                salesOrderPackSlipDispatchDetail.setOrderPackslipItem(salesOrderPackslipItem);
                verifiedCasesCount++;
                rowNumber++;
            }
        } catch (FileNotFoundException e) {
            throw new WmsException(HttpStatus.BAD_REQUEST, "No file exists for this Packslip Number -> " + packSlipId);
        } catch (IOException e) {
            throw new WmsException(HttpStatus.BAD_REQUEST, "No file exists for this Packslip Number -> " + packSlipId);
        } catch (Exception e){
            e.printStackTrace();
            throw new WmsException(HttpStatus.BAD_REQUEST, "Please check if HST file is in correct format -> " + packSlipId);
        }

        salesOrderPackslip.setOrderStatus(OrderStatus.DONE);
        salesOrderPackslip.setPackslipQuantity(verifiedCasesCount);
        salesOrderPackslip.setVerifiedCases(verifiedCasesCount);
        salesOrderPackslip.getOrderPackslipItems().addAll(packslipItemSkuMap.values());
        inventoryService.save(inventoryList);
        updateSalesOrderQuantity(new ArrayList<SalesOrderPackslipItem>(salesOrderPackslipItemMap.values()));

        return save(salesOrderPackslip);
    }

    private SalesOrderPackslipItem updatePackslipItem(SalesOrderPackslip salesOrderPackslip, Map<String, Product> productMap,
                                                      Map<String, SalesOrderPackslipItem> packslipItemSkuMap,
                                                      SalesOrderPackSlipDispatchDetails salesOrderPackSlipDispatchDetail, String skuCode,
                                                      String ramcoOrderNumber) throws WmsException{

        SalesOrderPackslipItem salesOrderPackslipItem = packslipItemSkuMap.get(skuCode + "~" + ramcoOrderNumber);

        if (salesOrderPackslipItem == null || CollectionUtils.isEmpty(salesOrderPackslipItem.getPackslipDispatchDetails())) {
            List<SalesOrderPackSlipDispatchDetails> dispatchDetailsList = new ArrayList<SalesOrderPackSlipDispatchDetails>();
            dispatchDetailsList.add(salesOrderPackSlipDispatchDetail);
            if (salesOrderPackslipItem == null) {
                // TODO
                salesOrderPackslipItem = SalesOrderPackslipItem.builder()
                        .product(productMap.get(skuCode)).verifiedCases(1).salesOrder(salesOrderService.findSalesOrderByNumber(ramcoOrderNumber))
                        .unitOfMeasure(UnitsOfMeasurement.CARTONS).orderPackslip(salesOrderPackslip).build();
            }
            salesOrderPackslipItem.setPackslipDispatchDetails(dispatchDetailsList);
            salesOrderPackslipItem.setPackslipQuantity(1);
            salesOrderPackslipItem.setVerifiedCases(1);
            packslipItemSkuMap.put(skuCode + "~" + ramcoOrderNumber, salesOrderPackslipItem);
        } else {
            salesOrderPackslipItem.getPackslipDispatchDetails().add(salesOrderPackSlipDispatchDetail);
            salesOrderPackslipItem.setPackslipQuantity(salesOrderPackslipItem.getVerifiedCases() + 1);
            salesOrderPackslipItem.setVerifiedCases(salesOrderPackslipItem.getVerifiedCases() + 1);
        }
        return salesOrderPackslipItem;
    }

    private void validatePackslipForHSTImport(SalesOrderPackslip salesOrderPackslip) throws WmsException {
        Set<SalesOrder> salesOrders = salesOrderPackslip.getSalesOrders();
        for (SalesOrder salesOrder : salesOrders) {
            if (salesOrder.getOrderFulfillmentType() != null
                    && salesOrder.getOrderFulfillmentType().equals(OrderFulfillmentType.PALLET)) {
                log.error("Importing HST Data is only applicable for Cartonized Orders. Sales Order Number : "
                        + salesOrder.getOrderNumber() + " is Palletized");
                throw new WmsException(HttpStatus.BAD_REQUEST,
                        "Importing HST Data is only applicable for Cartonized Orders. Sales Order Number : "
                                + salesOrder.getOrderNumber() + " is Palletized");
            }
        }
        if (StringUtils.isEmpty(salesOrderPackslip.getPackslipNumber())) {
            log.error("Packslip doesn't have the Packslip Number assigned to it");
            throw new WmsException(HttpStatus.BAD_REQUEST, "Packslip doesn't have the Packslip Number assigned to it");
        }
    }

    private void updateSalesOrderQuantity(List<SalesOrderPackslipItem> orderPackslipItems) throws WmsException {
        List<SalesOrderDetails> salesOrderDetailList = new ArrayList<SalesOrderDetails>();
        Map<Long, Integer> salesOrderPendingQuantityInCB = new HashMap<>();
        List<Product> productToSave = new ArrayList<>();
        for (SalesOrderPackslipItem salesOrderPackslipItem : orderPackslipItems) {
            Long salesOrderId = salesOrderPackslipItem.getSalesOrder().getId();
            List<SalesOrderDetails> salesOrderDetailsByItems = salesOrderService
                    .findSalesOrderDetailByProductAndSalesOrder(salesOrderPackslipItem.getProduct().getId(),
                            salesOrderId);
            Product product = salesOrderPackslipItem.getProduct();
// was preventing import even for valid quantity
//			 if(product.getQuantityInHand() < salesOrderPackslipItem.getVerifiedCases()) {
//			 	log.info ("Quantity in hand " + product.getQuantityInHand());
//				log.info ("Verified cases " + salesOrderPackslipItem.getVerifiedCases());
//				 throw new WmsException(HttpStatus.BAD_REQUEST, "Available Quantity for Product SKU : " + product.getSku() + " is less than the desired quantity. Cannot Proceed with Import.");
//			 }

            for (SalesOrderDetails salesOrderDetail : salesOrderDetailsByItems) {
                int deviation = salesOrderPackslipItem.getVerifiedCases()
                        - salesOrderDetail.getActualReservedQuantityInCB();
                if (deviation > 0 && product.getQuantityInHand() >= deviation) {
                    product.setQuantityInHand(product.getQuantityInHand() - deviation);
                    productToSave.add(product);
                }
                Integer actualShippedQuantityInCB = salesOrderDetail.getActualShippedQuantityInCB() != null
                        ? salesOrderDetail.getActualShippedQuantityInCB()
                        : 0;
                salesOrderDetail.setActualShippedQuantityInCB(
                        actualShippedQuantityInCB + salesOrderPackslipItem.getVerifiedCases());
                salesOrderDetail.setActualShippedQuantityInKG(
                        salesOrderDetail.getActualShippedQuantityInCB() * salesOrderDetail.getConversionFactor());

                if (salesOrderDetail.getActualShippedQuantityInCB() < salesOrderDetail.getTotalOrderQuanityInCB()) {
                    salesOrderDetail.setActualReservedQuantityInCB(salesOrderDetail.getActualReservedQuantityInCB() - salesOrderPackslipItem.getVerifiedCases());
                    salesOrderDetail.setActualReservedQuantityInKG(salesOrderDetail.getActualReservedQuantityInCB() * salesOrderDetail.getConversionFactor());
                } else {
                    salesOrderDetail.setActualReservedQuantityInCB(0);
                    salesOrderDetail.setActualReservedQuantityInKG(0D);
                }

                salesOrderDetailList.add(salesOrderDetail);

                if (!salesOrderPendingQuantityInCB.containsKey(salesOrderId)) {
                    salesOrderPendingQuantityInCB.put(salesOrderId, salesOrderDetail.getTotalOrderQuanityInCB()
                            - salesOrderDetail.getActualShippedQuantityInCB());
                } else {
                    salesOrderPendingQuantityInCB.put(salesOrderId,
                            salesOrderPendingQuantityInCB.get(salesOrderId)
                                    + (salesOrderDetail.getTotalOrderQuanityInCB()
                                    - salesOrderDetail.getActualShippedQuantityInCB()));
                }
            }

        }
        if (CollectionUtils.isNotEmpty(productToSave)) {
            productService.save(productToSave);
        }
        salesOrderDetailsService.save(salesOrderDetailList);
        for (Entry<Long, Integer> entry : salesOrderPendingQuantityInCB.entrySet()) {
            SalesOrder salesOrder = salesOrderService.findOne(entry.getKey());
            SalesOrderDto salesOrderDto = salesOrderTransformer.toDto(salesOrder);
            if (salesOrderDto.getActualShippedQuantityInCB() < salesOrderDto.getTotalOrderQuanityInCB()) {
                salesOrder.setStatus(OrderStatus.PARTIALLY_FULFILLMENT_DONE);
            } else {
                salesOrder.setStatus(OrderStatus.FULFILLED);
            }
            salesOrderService.save(salesOrder);
        }
    }

    private Map<String, SalesOrderPackslipItem> extractPackslipItemSkuMap(SalesOrderPackslip salesOrderPackslip) {
        Map<String, SalesOrderPackslipItem> packslipItemSkuMap = new HashMap<String, SalesOrderPackslipItem>();
        List<SalesOrderPackslipItem> packslipItems = salesOrderPackslip.getOrderPackslipItems();
        for (SalesOrderPackslipItem salesOrderPackslipItem : packslipItems) {
            packslipItemSkuMap.put(salesOrderPackslipItem.getProduct().getSku() + "~"
                    + salesOrderPackslipItem.getSalesOrder().getOrderNumber(), salesOrderPackslipItem);
        }
        return packslipItemSkuMap;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deletePackSlip(Long packslipId) throws WmsException {
        SalesOrderPackslip salesOrderPackslip = findOne(packslipId);
        List<SalesOrderPackslipItem> orderPackslipItems = salesOrderPackslip.getOrderPackslipItems();
        List<SalesOrderPackSlipDispatchDetails> dispatchDetailsListForPackslip = new ArrayList<>();
        for (SalesOrderPackslipItem salesOrderPackslipItem : orderPackslipItems) {
            dispatchDetailsListForPackslip.addAll(salesOrderPackslipItem.getPackslipDispatchDetails());
        }
        if (CollectionUtils.isNotEmpty(dispatchDetailsListForPackslip)) {
            throw new WmsException(HttpStatus.BAD_REQUEST, "Cannot Delete a Packslip which has been Dispatched");
        }
//		orderPackslipItemRepository.deleteInBatch(orderPackslipItems);
        salesOrderPackslip.getOrderPackslipItems().clear();
        Set<SalesOrder> salesOrders = salesOrderPackslip.getSalesOrders();
        for (SalesOrder salesOrder : salesOrders) {
            salesOrder.getPackslips().remove(salesOrderPackslip);
            salesOrderService.save(salesOrder);

        }

        salesOrderPackslip.getSalesOrders().clear();
        save(salesOrderPackslip);
        delete(packslipId);
        return Boolean.TRUE;
    }
}
