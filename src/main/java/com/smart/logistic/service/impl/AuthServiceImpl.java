package com.smart.logistic.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.User;
import com.smart.logistic.domain.enums.Role;
import com.smart.logistic.dto.AuthDto;
import com.smart.logistic.dto.LoginDto;
import com.smart.logistic.exception.UserNotAuthenticatedException;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.IAuthService;
import com.smart.logistic.service.IUserService;
import com.smart.logistic.transformer.UserTransformer;
import com.smart.logistic.util.AuthUtils;
import com.smart.logistic.util.EncryptionUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AuthServiceImpl implements IAuthService {

	@Autowired
	private IUserService userService;

	@Autowired
	private UserTransformer userTransformer;

	@Override
	public AuthDto login(LoginDto loginDto) throws UserNotAuthenticatedException {
		log.info("inside log in method : " + loginDto);
		User user = hasCredentials(loginDto);
		String token = AuthUtils.createToken(String.valueOf(user.getId()));
		return transform(token, user);
	}

	private AuthDto transform(String token, User user) {
		return new AuthDto(token, userTransformer.toDto(user));
	}

	private User hasCredentials(LoginDto loginDto) throws UserNotAuthenticatedException {
		User user = userService.findByEmail(loginDto.getEmail());
		if (user == null) {
			throw new UserNotAuthenticatedException(HttpStatus.UNAUTHORIZED,
					"Given email is not Registered with us ,Please register First");
		}
		if (!(user.getRole().equals(Role.ADMIN) || user.getRole().equals(Role.USER))) {
			throw new UserNotAuthenticatedException(HttpStatus.UNAUTHORIZED, "Role doesn't match");
		}
		String password = EncryptionUtil.encrypt(loginDto.getPassword());
		if (!user.getPassword().equals(password)) {
			throw new UserNotAuthenticatedException(HttpStatus.UNAUTHORIZED, "Password Incorrect");
		}
		log.info("Successfully logged in : " + user);
		return user;
	}

	@Override
	public void logout(User user, String token) throws WmsException {
		if (user.getInvalidTokens() == null) {
			user.setInvalidTokens(new ArrayList<String>());
		}
		user.getInvalidTokens().add(token);
		userService.save(user);
	}

}
