package com.smart.logistic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.SalesOrderDetails;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.ISalesOrderDetailsRepository;
import com.smart.logistic.service.ISalesOrderDetailsService;
import com.smart.logistic.service.generic.AbstractService;

@Service
public class SalesOrderDetailsServiceImpl extends AbstractService<SalesOrderDetails>
		implements ISalesOrderDetailsService {

	@Autowired
	private ISalesOrderDetailsRepository salesOrderDetailsRepository;

	@Override
	protected WmsException notFoundException() {
		return new WmsException("Sales order detail not found");
	}

	@Override
	public List<SalesOrderDetails> findBySalesOrderIdAndProductId(Long salesOrderId, Long productId) {
		return salesOrderDetailsRepository.findBySalesOrder_IdAndProduct_Id(salesOrderId, productId);
	}

	@Override
	public List<SalesOrderDetails> findByIdIn(List<Long> salesOrderIds) {
		return salesOrderDetailsRepository.findByIdIn(salesOrderIds);
	}
}