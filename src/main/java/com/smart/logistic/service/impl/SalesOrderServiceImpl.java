package com.smart.logistic.service.impl;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.smart.logistic.dto.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.opencsv.CSVWriter;
import com.smart.logistic.domain.Customer;
import com.smart.logistic.domain.DeviationReport;
import com.smart.logistic.domain.IndividualSalesOrderDeviationReport;
import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.SalesOrder;
import com.smart.logistic.domain.SalesOrderDetails;
import com.smart.logistic.domain.SalesOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.SalesOrderPackslip;
import com.smart.logistic.domain.enums.OrderFulfillmentType;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.RamcoOrderStatus;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.ISalesOrderDetailsRepository;
import com.smart.logistic.repository.ISalesOrderRepository;
import com.smart.logistic.service.ICustomerService;
import com.smart.logistic.service.IInventoryService;
import com.smart.logistic.service.IOrderPackslipService;
import com.smart.logistic.service.IPackslipDispatchDetailsService;
import com.smart.logistic.service.IProductService;
import com.smart.logistic.service.IReportService;
import com.smart.logistic.service.ISalesOrderDetailsService;
import com.smart.logistic.service.ISalesOrderService;
import com.smart.logistic.service.generic.AbstractService;
import com.smart.logistic.transformer.DeliveryOrderTransformer;
import com.smart.logistic.transformer.SalesOrderDetailsTransformer;
import com.smart.logistic.transformer.SalesOrderTransformer;

import lombok.extern.slf4j.Slf4j;
import static java.lang.Math.toIntExact;

@Slf4j
@Service
public class SalesOrderServiceImpl extends AbstractService<SalesOrder> implements ISalesOrderService {

    @Autowired
    private ISalesOrderRepository salesOrderRepository;

    @Autowired
    private SalesOrderTransformer salesOrderTransformer;

    @Autowired
    private SalesOrderDetailsTransformer salesOrderDetailsTransformer;

    @Autowired
    private ICustomerService customerService;

    @Autowired
    private IProductService productService;

    @Value("${import.external.sales.order}")
    private String importExternalOrderUrl;

    @Autowired
    private RestTemplate rest;

    @Autowired
    private AsyncRestTemplate asyncRestTemplate;

    @Autowired
    private DeliveryOrderTransformer deliveryOrderTransformer;

    @Value("${transporter.api.url}")
    private String transportApiUrl;

    @Autowired
    private IPackslipDispatchDetailsService packslipDispatchDetailsService;

    @Autowired
    private ISalesOrderDetailsRepository salesOrderDetailsReposiory;

    @Autowired
    private IReportService reportService;

    @Autowired
    private S3Service s3Service;

    @Autowired
    private ISalesOrderDetailsService salesOrderDetailsService;

    @Value(value = "${aws.bucket.name}")
    private String awsBucketName;

    @Value(value = "${aws.hst-dispatch.summary.remote.outward.directory}")
    private String awsHSTDispatchSummaryExportRemoteDirectory;

    @Autowired
    private IOrderPackslipService salesOrderPackslipService;

    @Value("${pallets.to.cartons.conversion.factor}")
    private Integer palletsToCartonsConversionFactor;

    @Autowired
    private IInventoryService inventoryService;

    @Override
    protected WmsException notFoundException() {
        return new WmsException("Sales order not found");
    }


    @Autowired
    private AmazonS3 amazonS3;





    @Override
    public Page<SalesOrder> getListOfSalesOrder(Integer pageNumber, Integer pageSize) throws WmsException {
        log.info("getting list of sales order: " + pageNumber + pageSize);
        Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "orderDate"));
        validatePageNumber(pageNumber, pageSize);
        ImmutableList<OrderStatus> orderStatuses = ImmutableList.of(OrderStatus.WAITING, OrderStatus.FULFILLED);
        return salesOrderRepository.findByStatusInAndActiveTrue(orderStatuses, pageable);
    }

    private void validatePageNumber(Integer pageNumber, Integer pageSize) throws WmsException {
        if ((pageNumber == null) || (pageSize == null) || (pageNumber < 0) || (pageSize < 0)) {
            throw new WmsException(HttpStatus.BAD_REQUEST, "PageNumber or PageSize not valid");
        }
    }

    @Override
    public List<DistinctProductDto> findDistinctSkuAndProductBySalesOrderId(Long salesOrderId) throws WmsException {
        log.info("inside findDistinctSkuAndProduct : " + salesOrderId);
        SalesOrder salesOrder = findOne(salesOrderId);
        List<DistinctProductDto> distinctProducts = new ArrayList<DistinctProductDto>();
        salesOrder.getSalesOrderDetails().stream().forEach(salaesOrderDetails -> {
            DistinctProductDto distinctProductDto = DistinctProductDto.builder()
                    .sku(salaesOrderDetails.getProduct().getSku()).name(salaesOrderDetails.getProduct().getName())
                    .build();
            distinctProducts.add(distinctProductDto);
        });
        log.info("distinctProducts : " + distinctProducts);
        return distinctProducts;
    }

    @Override
    public List<SalesOrderDto> findSalesOrderDeliveryDetail(List<Long> ids) throws WmsException {
        List<SalesOrder> salesorders = new ArrayList<SalesOrder>();
        for (Long salesOrderId : ids) {
            SalesOrder salesOrder = findOne(salesOrderId);
            salesorders.add(salesOrder);
        }

        return salesOrderTransformer.toDto(salesorders);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SalesOrderDto addSalesOrderToDispatch(Long id, OrderFulfillmentType orderFulfillmentType)
            throws WmsException {
        SalesOrder salesOrder = findOne(id);

        if (!salesOrder.getStatus().equals(OrderStatus.WAITING)) {
            throw new WmsException(HttpStatus.BAD_REQUEST,
                    "Sales Order must be in a WAITING state in order to be DISPATCHED");
        }
        if (orderFulfillmentType == null) {
            throw new WmsException(HttpStatus.BAD_REQUEST,
                    "Sales Order must be either in Carton or Pallet. Please choose one option to move it to Delivery Order");
        }
        salesOrder.setStatus(OrderStatus.ADDED_TO_DISPATCH);
        salesOrder.setOrderFulfillmentType(orderFulfillmentType);

        // Checking Whether This Order can be added to Delivery Order based on
        // Availability of Items Ordered
        List<SalesOrderDetailsDto> salesOrderDetailsListDto = salesOrderDetailsTransformer
                .toDto(salesOrder.getSalesOrderDetails());
        int totalReservedQuantity = 0;
        int totalPendingQuantity = 0;
        List<Product> productsToSave = new ArrayList<Product>();
        for (SalesOrderDetailsDto salesOrderDetailsDto : salesOrderDetailsListDto) {
            totalReservedQuantity = totalReservedQuantity + salesOrderDetailsDto.getReservedQuantity();
            totalPendingQuantity = totalPendingQuantity + salesOrderDetailsDto.getPendingQuantityInCB();
            Product product = productService.findBySkuIgnoreCase(salesOrderDetailsDto.getProductDto().getSku());
            // Changed to book Total Order Quantity instead of only reserved Quantity
            if (salesOrderDetailsDto.getAvailableQuantity() >= salesOrderDetailsDto.getTotalOrderQuanityInCB()) {
                product.setQuantityInHand(
                        product.getQuantityInHand() - salesOrderDetailsDto.getTotalOrderQuanityInCB());
            }
            productsToSave.add(product);
        }

        productService.save(productsToSave);
        return salesOrderTransformer.toDto(save(salesOrder));
    }

    @Override
    public Page<SalesOrder> getListOfDeliveryOrders(Integer pageNumber, Integer pageSize) throws WmsException {
        log.info("getting list of sales order: " + pageNumber + pageSize);
        Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "dateCreated"));
        validatePageNumber(pageNumber, pageSize);
        ImmutableList<OrderStatus> orderStatuses = ImmutableList.of(OrderStatus.ADDED_TO_DISPATCH,
                OrderStatus.QUEUED_FOR_VEHICLE_ALLOCATION, OrderStatus.VEHICLE_ALLOCATED,
                OrderStatus.PARTIALLY_FULFILLMENT_DONE);
        return salesOrderRepository.findByStatusInAndActiveTrue(orderStatuses, pageable);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SalesOrderDto removeSalesOrderFromDispatch(Long id) throws WmsException {
        SalesOrder salesOrder = findOne(id);
        if (!salesOrder.getStatus().equals(OrderStatus.ADDED_TO_DISPATCH)) {
            throw new WmsException(HttpStatus.BAD_REQUEST,
                    "Sales Order must be in a ADDED_TO_DISPATCH state in order to be moved back to WAITING");
        }
        salesOrder.setStatus(OrderStatus.WAITING);
        List<Product> productsToSave = new ArrayList<Product>();
        List<SalesOrderDetails> salesOrderDetailsList = new ArrayList<SalesOrderDetails>();
        for (SalesOrderDetails salesOrderDetails : salesOrder.getSalesOrderDetails()) {
            Product product = salesOrderDetails.getProduct();
            if (salesOrderDetails.getActualReservedQuantityInCB() > 0) {
                product.setQuantityInHand(
                        product.getQuantityInHand() + salesOrderDetails.getActualReservedQuantityInCB());
                productsToSave.add(product);
            }
            salesOrderDetails.setActualReservedQuantityInCB(0);
            salesOrderDetails.setActualReservedQuantityInKG(0D);
            salesOrderDetailsList.add(salesOrderDetails);
        }
        productService.save(productsToSave);
        salesOrder = save(salesOrder);
        salesOrderDetailsService.save(salesOrderDetailsList);
        return salesOrderTransformer.toDto(salesOrder);
    }

    @Override
    public List<SalesOrder> getListOfDeliveryOrders() throws WmsException {
        ImmutableList<OrderStatus> orderStatuses = ImmutableList.of(OrderStatus.ADDED_TO_DISPATCH,
                OrderStatus.QUEUED_FOR_VEHICLE_ALLOCATION, OrderStatus.VEHICLE_ALLOCATED,
                OrderStatus.PARTIALLY_FULFILLMENT_DONE);
        return salesOrderRepository.findByStatusInAndActiveTrue(orderStatuses);
    }

    @Override
    public List<SalesOrder> getListOfShippedOrders() throws WmsException {
        ImmutableList<OrderStatus> orderStatuses = ImmutableList.of(OrderStatus.ADDED_TO_DISPATCH,
                OrderStatus.QUEUED_FOR_VEHICLE_ALLOCATION, OrderStatus.VEHICLE_ALLOCATED,
                OrderStatus.PARTIALLY_FULFILLMENT_DONE, OrderStatus.FULFILLED);
        return salesOrderRepository.findByStatusInAndActiveTrue(orderStatuses);
    }

    @Override
    public List<SalesOrder> getAllDeliveryOrdersByCustomerCode(String customerCode) throws WmsException {
        ImmutableList<OrderStatus> orderStatuses = ImmutableList.of(OrderStatus.ADDED_TO_DISPATCH,
                OrderStatus.QUEUED_FOR_VEHICLE_ALLOCATION, OrderStatus.VEHICLE_ALLOCATED,
                OrderStatus.PARTIALLY_FULFILLMENT_DONE);
        return salesOrderRepository.findByStatusInAndCustomer_CustomerCode(orderStatuses, customerCode);
    }

    @Override
    public SalesOrder findSalesOrderByNumber(String salesOrderNumber) throws WmsException {
        SalesOrder salesOrder = salesOrderRepository.findByOrderNumber(salesOrderNumber);
        if (salesOrder == null) {
            throw new WmsException(HttpStatus.BAD_REQUEST, "Sales Order not found for the provided Sales Order Number");
        }
        return salesOrder;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<SalesOrder> importAllOpenSalesOrdersFromRamco() throws WmsException {

        productService.importAllItemsFromRamco();
        customerService.importAllCustomersFromRamco();

        // Set XML content type explicitly to force response in XML (If not spring get
        // response in JSON)
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<String> response = rest.exchange(importExternalOrderUrl, HttpMethod.GET, entity, String.class);

        String responseBody = response.getBody().trim();
        responseBody = responseBody.split(">")[1];
        File tempFile = null;
        List<SalesOrder> salesOrders = null;

        try{
            tempFile = File.createTempFile("salesorder-", ".tmp");
            log.info ("creating temp file for writing ->" + tempFile.getAbsolutePath());
            FileWriter writer = new FileWriter (tempFile);
            writer.write(responseBody);
            writer.close();
            salesOrders = convertToJavaObjectFromFile(tempFile);
            tempFile.delete();
        } catch (IOException ioe){
            throw new WmsException(ioe.getMessage());
        }

//        ImportExternalSalesOrderJsonDto importExternalSalesOrderJsonDto = convertToJavaObject(responseBody);
//        List<SalesOrder> salesOrders = createSalesOrdersFromImportedOrders(importExternalSalesOrderJsonDto);
        return salesOrders;
    }


    private List <SalesOrderDetails> getAllUpdates (Map <String, SalesOrder> salesOrderToIterate,
                                                    Map <String, Map <String, List<ImportExternalSalesOrderDto>>> ramcoMap,
                                                    Map <String, Product> productMap) throws WmsException{

        List<SalesOrderDetails> salesOrderDetailsForUpdates = new ArrayList<>();

        for (String orderNumber : ramcoMap.keySet()){

            SalesOrder salesOrder = salesOrderToIterate.get(orderNumber);

            if (salesOrder == null) {
                log.info ("ignoring " + orderNumber + " for updates");
                continue;
            }


            // for orders that exist look at all the items
            for(String itemCode : ramcoMap.get(orderNumber).keySet()){
                List <ImportExternalSalesOrderDto > importedOrderList = ramcoMap.get(orderNumber).get (itemCode);
                Product product = productMap.get(itemCode);
                if (product == null){
                    log.info ("Could not locate -> "+ itemCode);
                    product = productService.findBySkuIgnoreCase(itemCode);
                    productMap.put(itemCode, product);
                }


                List<SalesOrderDetails> salesOrderDetailsListByItem = findSalesOrderDetailByProductAndSalesOrder(
                        product.getId(), salesOrder.getId());
                // list from order and sales order detail has to be compared per item
                updateSalesOrderDetailsIfChangeDetected (salesOrderDetailsForUpdates, importedOrderList,
                        salesOrder, productMap.get(itemCode), salesOrderDetailsListByItem);
            }

        }


        return salesOrderDetailsForUpdates;
    }


    private void populateSalesOrderDetailsForUpdate (List<SalesOrderDetails> salesOrderDetailsForUpdates,
                                                     List<ImportExternalSalesOrderDto> importExternalSalesOrderDtos,
                                                     SalesOrder salesOrderFromDB, Product product){

        for(ImportExternalSalesOrderDto importExternalSalesOrderDto : importExternalSalesOrderDtos) {
            int quantityInCB = toIntExact(Math.round(importExternalSalesOrderDto.getOrder_Qty()/importExternalSalesOrderDto.getKG_CB_Con_Fact()));
            int pendingQuantityInCB = quantityInCB - toIntExact(Math.round(importExternalSalesOrderDto.getShp_Qty_in_CB()));

            SalesOrderDetails newSalesOrderDetails = SalesOrderDetails.builder()
                    .shippedQuantityInCB(new Double(importExternalSalesOrderDto.getShp_Qty_in_CB()).intValue())
                    .shippedQuantityInKG(importExternalSalesOrderDto.getShipped_Qty()).product(product)
                    .totalOrderQuanityInCB(quantityInCB)
                    .totalOrderQuanityInKG(importExternalSalesOrderDto.getOrder_Qty())
                    .pendingQuantityInCB(pendingQuantityInCB)
                    .pendingQuantityInKG(importExternalSalesOrderDto.getPending_Qty())
                    .conversionFactor(importExternalSalesOrderDto.getKG_CB_Con_Fact()).salesOrder(salesOrderFromDB)
                    .unitPrice(BigDecimal.valueOf(importExternalSalesOrderDto.getUnit_Price()))
                    .taxes(BigDecimal.valueOf(importExternalSalesOrderDto.getTax_Amount()))
                    .subTotal(BigDecimal.valueOf(importExternalSalesOrderDto.getTotal_Price()))
                    .unitsOfMeasure(UnitsOfMeasurement.valueOf(importExternalSalesOrderDto.getItemUOM())).build();
            salesOrderDetailsForUpdates.add(newSalesOrderDetails);
        }

    }
    private void updateSalesOrderDetailsIfChangeDetected(List<SalesOrderDetails> salesOrderDetailsForUpdates,
                                                         List<ImportExternalSalesOrderDto> importExternalSalesOrderDtos, SalesOrder salesOrderFromDB,
                                                         Product product,
                                                         List<SalesOrderDetails> salesOrderDetailsListByItem) {

        // Adding new sales order details if detected as a new item while updating
        if (CollectionUtils.isEmpty(salesOrderDetailsListByItem)) {
            log.info("New Order Line Detected for Order Number : " + salesOrderFromDB.getOrderNumber()
                    + " And Item Code " + product.getSku());

            populateSalesOrderDetailsForUpdate(salesOrderDetailsForUpdates,
                importExternalSalesOrderDtos, salesOrderFromDB, product);


            return;
        }

        // update all orders that are in waiting.
        if(importExternalSalesOrderDtos.size() == salesOrderDetailsListByItem.size()){
            int i = 0;
            for(ImportExternalSalesOrderDto importExternalSalesOrderDto : importExternalSalesOrderDtos) {

                int quantityInCB = toIntExact(Math.round(importExternalSalesOrderDto.getOrder_Qty()/importExternalSalesOrderDto.getKG_CB_Con_Fact()));
                int pendingQuantityInCB = quantityInCB - toIntExact(Math.round(importExternalSalesOrderDto.getShp_Qty_in_CB()));

                SalesOrderDetails salesOrderDetails = salesOrderDetailsListByItem.get(i);
                salesOrderDetails
                        .setShippedQuantityInCB(new Double(importExternalSalesOrderDto.getShp_Qty_in_CB()).intValue());
                salesOrderDetails.setShippedQuantityInKG(importExternalSalesOrderDto.getShipped_Qty());

                salesOrderDetails.setTotalOrderQuanityInCB(quantityInCB);
                salesOrderDetails.setTotalOrderQuanityInKG(importExternalSalesOrderDto.getOrder_Qty());

                salesOrderDetails
                        .setPendingQuantityInCB(pendingQuantityInCB);
                salesOrderDetails.setPendingQuantityInKG(importExternalSalesOrderDto.getKG_CB_Con_Fact() * pendingQuantityInCB);

                salesOrderDetails.setUnitPrice(BigDecimal.valueOf(importExternalSalesOrderDto.getUnit_Price()));
                salesOrderDetails.setTaxes(BigDecimal.valueOf(importExternalSalesOrderDto.getTax_Amount()));
                salesOrderDetails.setSubTotal(BigDecimal.valueOf(importExternalSalesOrderDto.getTotal_Price()));

                salesOrderDetailsForUpdates.add(salesOrderDetails);
                i++;

            }

        } else if (importExternalSalesOrderDtos.size () > salesOrderDetailsListByItem.size()){
            // add new sales order details TODO
            log.info ("Import has more line items for order " + salesOrderFromDB.getOrderNumber() + " and item " + product.getSku());
            // Iterator <SalesOrderDetails> details = unmatchedSalesOrderDetails.iterator();
            int i = 0;
            for(ImportExternalSalesOrderDto importExternalSalesOrderDto : importExternalSalesOrderDtos) {

                int quantityInCB = toIntExact(Math.round(importExternalSalesOrderDto.getOrder_Qty()/importExternalSalesOrderDto.getKG_CB_Con_Fact()));
                int pendingQuantityInCB = quantityInCB - toIntExact(Math.round(importExternalSalesOrderDto.getShp_Qty_in_CB()));

                if(i < salesOrderDetailsListByItem.size()) {
                    // ImportExternalSalesOrderDto importExternalSalesOrderDto = unmatchedExternal.get (i);
                    SalesOrderDetails salesOrderDetails = salesOrderDetailsListByItem.get(i);
                    salesOrderDetails
                            .setShippedQuantityInCB(new Double(importExternalSalesOrderDto.getShp_Qty_in_CB()).intValue());
                    salesOrderDetails.setShippedQuantityInKG(importExternalSalesOrderDto.getShipped_Qty());

                    salesOrderDetails.setTotalOrderQuanityInCB(quantityInCB);
                    salesOrderDetails.setTotalOrderQuanityInKG(importExternalSalesOrderDto.getOrder_Qty());

                    salesOrderDetails
                            .setPendingQuantityInCB(pendingQuantityInCB);
                    salesOrderDetails.setPendingQuantityInKG(importExternalSalesOrderDto.getKG_CB_Con_Fact() * pendingQuantityInCB);

                    salesOrderDetails.setUnitPrice(BigDecimal.valueOf(importExternalSalesOrderDto.getUnit_Price()));
                    salesOrderDetails.setTaxes(BigDecimal.valueOf(importExternalSalesOrderDto.getTax_Amount()));
                    salesOrderDetails.setSubTotal(BigDecimal.valueOf(importExternalSalesOrderDto.getTotal_Price()));

                    salesOrderDetailsForUpdates.add(salesOrderDetails);

                }
                else {



                    SalesOrderDetails newSalesOrderDetails = SalesOrderDetails.builder()
                            .shippedQuantityInCB(new Double(importExternalSalesOrderDto.getShp_Qty_in_CB()).intValue())
                            .shippedQuantityInKG(importExternalSalesOrderDto.getShipped_Qty()).product(product)
                            .totalOrderQuanityInCB(quantityInCB)
                            .totalOrderQuanityInKG(importExternalSalesOrderDto.getOrder_Qty())
                            .pendingQuantityInCB(pendingQuantityInCB)
                            .pendingQuantityInKG(pendingQuantityInCB*importExternalSalesOrderDto.getKG_CB_Con_Fact())
                            .conversionFactor(importExternalSalesOrderDto.getKG_CB_Con_Fact()).salesOrder(salesOrderFromDB)
                            .unitPrice(BigDecimal.valueOf(importExternalSalesOrderDto.getUnit_Price()))
                            .taxes(BigDecimal.valueOf(importExternalSalesOrderDto.getTax_Amount()))
                            .subTotal(BigDecimal.valueOf(importExternalSalesOrderDto.getTotal_Price()))
                            .unitsOfMeasure(UnitsOfMeasurement.valueOf(importExternalSalesOrderDto.getItemUOM())).build();
                    salesOrderDetailsForUpdates.add(newSalesOrderDetails);
                }
                i++;
            }

            populateSalesOrderDetailsForUpdate(salesOrderDetailsForUpdates,
                    importExternalSalesOrderDtos.subList(i, importExternalSalesOrderDtos.size()), salesOrderFromDB, product);
            // TODO iterate over remaining unmatchedExternal and create new entries.


        } else if (importExternalSalesOrderDtos.size () < salesOrderDetailsListByItem.size()){
            // remove sales order details
            // TODO : not sure about deletions yet // most likely handled by short close
            log.info ("Import has less line items for order " + importExternalSalesOrderDtos.get(0).getOrder_No() + " and item " + product.getSku());

        }
    }


    private List<SalesOrder> convertToJavaObjectFromFile(File tempFile) throws IOException, WmsException {


        Map<String, SalesOrder> salesOrderMap = new HashMap<String, SalesOrder>();
        Map<String, Product> productMap = new HashMap<>();
        List<SalesOrder> salesOrdersToSave = new ArrayList<SalesOrder>();

        Gson gson = new Gson();
        FileInputStream fis = new FileInputStream(tempFile);
        JsonReader reader = new JsonReader(new InputStreamReader(fis));
        reader.setLenient(true);

        reader.beginObject();

        Map<String, Map<String, List<ImportExternalSalesOrderDto>>> ramcoMap = new HashMap<String, Map<String, List<ImportExternalSalesOrderDto>>>();
        Map<String, SalesOrder> mapToIterateForUpdates = new HashMap<String, SalesOrder>();

    // Implicitly uses system time zone and system clock
        ZonedDateTime now = ZonedDateTime.now();
        ZonedDateTime ninetyDaysAgo = now.plusDays(-60);

        while (reader.hasNext()) {
            reader.nextName();
            reader.beginArray();
            while(reader.hasNext())
            {

            ImportExternalSalesOrderDto importExternalSalesOrderDto = gson.fromJson(reader, ImportExternalSalesOrderDto.class);

            Date eventStartDate = parseDate(importExternalSalesOrderDto.getOrder_Date());

            if (eventStartDate.toInstant().isBefore(ninetyDaysAgo.toInstant())) {
                // log.info ("eventStartDate is ninety days before" + eventStartDate.toString());
                continue;
            }

            SalesOrder salesOrderFromDB = null;
            Product product = null;
            // Create Product Map To avoid Data Lookup each Time
            if (!productMap.containsKey(importExternalSalesOrderDto.getItemcode())) {
                product = productService.findBySkuIgnoreCase(importExternalSalesOrderDto.getItemcode());
                productMap.put(importExternalSalesOrderDto.getItemcode(), product);
            } else {
                product = productMap.get(importExternalSalesOrderDto.getItemcode());
            }
            // Create Sales Order Map To avoid Data Lookup each Time

            salesOrderFromDB = salesOrderRepository.findByOrderNumber(importExternalSalesOrderDto.getOrder_No());


            if (salesOrderFromDB != null) {
                // log.info("Sales Order Number : " + salesOrderFromDB.getOrderNumber() + " And
                // Item SKU : "
                // + product.getId());
                // TODO : directly short close from behind
                // TODO : holding or pending
                if (!(salesOrderFromDB.getStatus() == OrderStatus.WAITING || salesOrderFromDB.getStatus() == OrderStatus.PARTIALLY_FULFILLMENT_DONE)) {
                    // log and continue
                    log.debug("Not updating " + importExternalSalesOrderDto.getOrder_No() + " as current status is " + salesOrderFromDB.getStatus());
                    continue;
                }

                // should get only the orders to update
                Map<String, List<ImportExternalSalesOrderDto>> orderMap = ramcoMap.get(importExternalSalesOrderDto.getOrder_No());
                if (orderMap == null) {
                    orderMap = new HashMap<String, List<ImportExternalSalesOrderDto>>();
                    ramcoMap.put(importExternalSalesOrderDto.getOrder_No(), orderMap);
                }
                List<ImportExternalSalesOrderDto> itemList = orderMap.get(importExternalSalesOrderDto.getItemcode());
                if (itemList == null) {
                    itemList = new ArrayList<ImportExternalSalesOrderDto>();
                    orderMap.put(importExternalSalesOrderDto.getItemcode(), itemList);
                }

                itemList.add(importExternalSalesOrderDto);

                mapToIterateForUpdates.put(importExternalSalesOrderDto.getOrder_No(), salesOrderFromDB);

//				List<SalesOrderDetails> salesOrderDetailsListByItem = findSalesOrderDetailByProductAndSalesOrder(
//						product.getId(), salesOrderFromDB.getId());
//				updateSalesOrderDetailsIfChangeDetected(salesOrderDetailsForUpdates, importExternalSalesOrderDto,
//						salesOrderFromDB, product, salesOrderDetailsListByItem);

                if (!salesOrderMap.containsKey(importExternalSalesOrderDto.getOrder_No())) {
                    salesOrderFromDB.setRamcoOrderStatus(importExternalSalesOrderDto.getOrder_Status());
                    salesOrderFromDB.setUntaxedAmount(BigDecimal.valueOf(importExternalSalesOrderDto.getTotal_Price()));
                    salesOrderFromDB.setTaxes(BigDecimal.valueOf(importExternalSalesOrderDto.getTax_Amount()));
                    salesOrderFromDB
                            .setGrandTotal(salesOrderFromDB.getUntaxedAmount().add(salesOrderFromDB.getTaxes()));
                    salesOrderMap.put(importExternalSalesOrderDto.getOrder_No(), salesOrderFromDB);
                } else {
                    salesOrderFromDB = salesOrderMap.get(importExternalSalesOrderDto.getOrder_No());
                    salesOrderFromDB.setUntaxedAmount(salesOrderFromDB.getUntaxedAmount()
                            .add(BigDecimal.valueOf(importExternalSalesOrderDto.getTotal_Price())));
                    salesOrderFromDB.setTaxes(salesOrderFromDB.getTaxes()
                            .add(BigDecimal.valueOf(importExternalSalesOrderDto.getTax_Amount())));
                    salesOrderFromDB
                            .setGrandTotal(salesOrderFromDB.getUntaxedAmount().add(salesOrderFromDB.getTaxes()));

                    salesOrderMap.put(importExternalSalesOrderDto.getOrder_No(), salesOrderFromDB);
                }
            } else {
                if (!salesOrderMap.containsKey(importExternalSalesOrderDto.getOrder_No())) {
                    Customer customer = null;
                    try {
                        customer = customerService.findCustomerByCode(importExternalSalesOrderDto.getCust_Code());
                    } catch (WmsException wmsException) {
                        log.error(wmsException.getMessage());
                        continue;
                    }
                    SalesOrder salesOrder = SalesOrder.builder().customer(customer).status(OrderStatus.WAITING)
                            .orderNumber(importExternalSalesOrderDto.getOrder_No())
                            .orderDate(parseDate(importExternalSalesOrderDto.getOrder_Date()))
                            .untaxedAmount(BigDecimal.valueOf(importExternalSalesOrderDto.getTotal_Price()))
                            .ramcoOrderStatus(importExternalSalesOrderDto.getOrder_Status()).build();
                    salesOrder.setTaxes(BigDecimal.valueOf(importExternalSalesOrderDto.getTax_Amount()));
                    salesOrder.setGrandTotal(salesOrder.getUntaxedAmount().add(salesOrder.getTaxes()));

                    List<SalesOrderDetails> salesOrderDetailsList = new ArrayList<SalesOrderDetails>();
                    int quantityInCB = toIntExact(Math.round(importExternalSalesOrderDto.getOrder_Qty()/importExternalSalesOrderDto.getKG_CB_Con_Fact()));
                    int pendingQuantityInCB = quantityInCB - toIntExact(Math.round(importExternalSalesOrderDto.getShp_Qty_in_CB()));

                    SalesOrderDetails salesOrderDetails = SalesOrderDetails.builder()
                            .shippedQuantityInCB(new Double(importExternalSalesOrderDto.getShp_Qty_in_CB()).intValue())
                            .shippedQuantityInKG(importExternalSalesOrderDto.getShipped_Qty()).product(product)
                            .totalOrderQuanityInCB(quantityInCB)
                            .totalOrderQuanityInKG(importExternalSalesOrderDto.getOrder_Qty())
                            .pendingQuantityInCB(pendingQuantityInCB)
                            .pendingQuantityInKG(pendingQuantityInCB*importExternalSalesOrderDto.getKG_CB_Con_Fact())
                            .conversionFactor(importExternalSalesOrderDto.getKG_CB_Con_Fact()).salesOrder(salesOrder)
                            .unitPrice(BigDecimal.valueOf(importExternalSalesOrderDto.getUnit_Price()))
                            .taxes(BigDecimal.valueOf(importExternalSalesOrderDto.getTax_Amount()))
                            .subTotal(BigDecimal.valueOf(importExternalSalesOrderDto.getTotal_Price()))
                            .unitsOfMeasure(UnitsOfMeasurement.valueOf(importExternalSalesOrderDto.getItemUOM()))
                            .build();

                    salesOrderDetailsList.add(salesOrderDetails);
                    salesOrder.setSalesOrderDetails(salesOrderDetailsList);
                    salesOrderMap.put(importExternalSalesOrderDto.getOrder_No(), salesOrder);
                } else {

                    int quantityInCB = toIntExact(Math.round(importExternalSalesOrderDto.getOrder_Qty()/importExternalSalesOrderDto.getKG_CB_Con_Fact()));
                    int pendingQuantityInCB = quantityInCB - toIntExact(Math.round(importExternalSalesOrderDto.getShp_Qty_in_CB()));


                    SalesOrder salesOrder = salesOrderMap.get(importExternalSalesOrderDto.getOrder_No());

                    salesOrder.setUntaxedAmount(salesOrder.getUntaxedAmount()
                            .add(BigDecimal.valueOf(importExternalSalesOrderDto.getTotal_Price())));
                    salesOrder.setTaxes(
                            salesOrder.getTaxes().add(BigDecimal.valueOf(importExternalSalesOrderDto.getTax_Amount())));
                    salesOrder.setGrandTotal(salesOrder.getUntaxedAmount().add(salesOrder.getTaxes()));
                    List<SalesOrderDetails> salesOrderDetailsList = salesOrder.getSalesOrderDetails();
                    SalesOrderDetails salesOrderDetails = SalesOrderDetails.builder()
                            .shippedQuantityInCB(new Double(importExternalSalesOrderDto.getShp_Qty_in_CB()).intValue())
                            .shippedQuantityInKG(importExternalSalesOrderDto.getShipped_Qty()).product(product)
                            .totalOrderQuanityInCB(quantityInCB)
                            .totalOrderQuanityInKG(importExternalSalesOrderDto.getOrder_Qty())
                            .pendingQuantityInCB(pendingQuantityInCB)
                            .pendingQuantityInKG(pendingQuantityInCB*importExternalSalesOrderDto.getKG_CB_Con_Fact())
                            .conversionFactor(importExternalSalesOrderDto.getKG_CB_Con_Fact()).salesOrder(salesOrder)
                            .unitPrice(BigDecimal.valueOf(importExternalSalesOrderDto.getUnit_Price()))
                            .taxes(BigDecimal.valueOf(importExternalSalesOrderDto.getTax_Amount()))
                            .subTotal(BigDecimal.valueOf(importExternalSalesOrderDto.getTotal_Price()))
                            .unitsOfMeasure(UnitsOfMeasurement.valueOf(importExternalSalesOrderDto.getItemUOM()))
                            .build();

                    salesOrderDetailsList.add(salesOrderDetails);
                    salesOrder.setSalesOrderDetails(salesOrderDetailsList);
                    salesOrderMap.put(importExternalSalesOrderDto.getOrder_No(), salesOrder);
                }
            }

        }
        reader.endArray();
    }

        reader.endObject();


        salesOrdersToSave = salesOrderMap.values().stream().collect(Collectors.toList());

        salesOrderDetailsReposiory.save(getAllUpdates(mapToIterateForUpdates, ramcoMap, productMap));

        // clear the maps
        for (String orderNumber : ramcoMap.keySet()) {

            for(String itemCode : ramcoMap.get(orderNumber).keySet()){
                ramcoMap.get(orderNumber).get(itemCode).clear();
            }
            ramcoMap.get(orderNumber).clear();
        }
        ramcoMap.clear();

        List<SalesOrder> savedSalesOrders = save(salesOrdersToSave);
        return savedSalesOrders;

    }


    private ImportExternalSalesOrderJsonDto convertToJavaObject(String responseBody) {
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new StringReader(responseBody));
        reader.setLenient(true);

        ImportExternalSalesOrderJsonDto importExternalSalesOrderJsonDto = gson.fromJson(reader,
                ImportExternalSalesOrderJsonDto.class);

        return importExternalSalesOrderJsonDto;
    }

    private Date parseDate(String dateString) {
        String milis = StringUtils.substringBetween(dateString, "(", ")");
        return new Date(Long.parseLong(milis));

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String sendDeliveryOrdersForAllocation(DeliveryDetailsRequestDto deliveryDetailsRequestDto)
            throws WmsException {
        List<Long> deliveryOrderIds = deliveryDetailsRequestDto.getIds();
        if (CollectionUtils.isEmpty(deliveryOrderIds)) {
            throw new WmsException(HttpStatus.BAD_REQUEST,
                    "Dispatch Plan cannot be Empty. It should have atleast one Delivery Order");
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        List<SalesOrderDto> salesOrderDeliveryDetail = findSalesOrderDeliveryDetail(deliveryOrderIds);
        List<DeliveryOrderDto> deliveryOrders = deliveryOrderTransformer.toDto(salesOrderDeliveryDetail);
        HttpEntity<List<DeliveryOrderDto>> httpEntity = new HttpEntity<List<DeliveryOrderDto>>(deliveryOrders, headers);
        Gson gson = new Gson();
        String jsonInString = gson.toJson(deliveryOrders);
        log.info(jsonInString);
        ListenableFuture<ResponseEntity<String>> future = asyncRestTemplate.exchange(transportApiUrl, HttpMethod.POST,
                httpEntity, String.class, deliveryOrders);
        String body = "";
        try {
            // waits for the result
            ResponseEntity<String> entity = future.get();
            List<SalesOrder> salesOrders = new ArrayList<SalesOrder>();
            if (entity.getStatusCode().equals(HttpStatus.CREATED)) {
                body = entity.getBody();
                log.info(body);
                for (DeliveryOrderDto deliveryOrderDto : deliveryOrders) {
                    SalesOrder salesOrder = findSalesOrderByNumber(deliveryOrderDto.getOrderNumber());
                    salesOrder.setStatus(OrderStatus.QUEUED_FOR_VEHICLE_ALLOCATION);
                    salesOrders.add(salesOrder);
                }
                save(salesOrders);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return body;
    }

    @Override
    public FileSystemResource createCsvFileBySalesOrderPackSlipId(HttpServletResponse response,
                                                                  Long salesOrderPackslipId, String csvFileName) throws IOException, WmsException {
        log.info("inside createCsvFileBySalesOrderPackSlipId Method ");
        SalesOrderPackslip salesOrderPackslip = salesOrderPackslipService.findOne(salesOrderPackslipId);
        String dispatchHSTfileName = StringUtils.replaceChars(salesOrderPackslip.getPackslipNumber().toUpperCase(), "/",
                "-") + "-HST-DISPATCH.csv";
        List<SalesOrderPackSlipDispatchDetails> salesOrderPackSlipDispatchDetails = null;
        response.setHeader("Content-disposition", "attachment; filename=" + csvFileName);
        salesOrderPackSlipDispatchDetails = packslipDispatchDetailsService
                .findAllPackslipDispatchDetailsByPackslipId(salesOrderPackslipId);
        createCsvFile(salesOrderPackSlipDispatchDetails, csvFileName);
        MultipartFile multiPartFile = convertFileToMultiPartFile(new File(csvFileName), dispatchHSTfileName);
        s3Service.pushToS3(multiPartFile, awsBucketName, awsHSTDispatchSummaryExportRemoteDirectory);
        return new FileSystemResource(new File(csvFileName));
    }

    private MultipartFile convertFileToMultiPartFile(File file, String dispatchHSTfileName)
            throws FileNotFoundException, IOException {
        MockMultipartFile multipartFile = new MockMultipartFile(dispatchHSTfileName, new FileInputStream(file));
        log.info("File Name To Be Uplaaded  : " + multipartFile.getName());
        log.info("Original File Name To Be Uplaaded  : " + multipartFile.getOriginalFilename());
        return multipartFile;
    }

    private void createCsvFile(List<SalesOrderPackSlipDispatchDetails> salesOrderPackSlipDispatchDetails,
                               String csvFileName) throws IOException {
        log.info("inside createCsvFile method");
        // initializing csvwriter
        CSVWriter csvWriter = new CSVWriter(new FileWriter(csvFileName));
        createCsvHeader(csvWriter);
        writingDataToCsv(salesOrderPackSlipDispatchDetails, csvWriter);
        csvWriter.close();
    }

    private void writingDataToCsv(List<SalesOrderPackSlipDispatchDetails> salesOrderPackSlipDispatchDetails,
                                  CSVWriter csvWriter) {
        log.info("inside writingDataToCsv method");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String stockindate = sdf.format(date);
        for (SalesOrderPackSlipDispatchDetails packSlipDispatchDetails : salesOrderPackSlipDispatchDetails) {
            csvWriter.writeNext(new String[] {
                    packSlipDispatchDetails.getOrderPackslipItem().getOrderPackslip().getPackslipNumber(),
                    packSlipDispatchDetails.getOrderPackslipItem().getSalesOrder().getOrderNumber(),
                    packSlipDispatchDetails.getProduct().getSku(),
                    packSlipDispatchDetails.getProduct().getDescription(), "FP",
                    packSlipDispatchDetails.getBatchNumber(), stockindate,
                    packSlipDispatchDetails.getProduct().getCartonToPacketConversionFactor().toString(),
                    packSlipDispatchDetails.getNetWeight().toString(),
                    packSlipDispatchDetails.getGrossWeight() != null
                            ? packSlipDispatchDetails.getGrossWeight().toString()
                            : "0D",
                    packSlipDispatchDetails.getScanBarcode(), packSlipDispatchDetails.getPalletBarcode(), "0" });
        }
    }

    private void createCsvHeader(CSVWriter csvWriter) {
        log.info("inside createCsvHeader method");
        csvWriter.writeNext(new String[] { "Packslipno", "orderno", "itemcode", "itemdesciption", "packing", "batchno",
                "Stockindate(YYYYMMDD)", "nop", "stnetwt", "stgrosswt", "cbbaroce", "plaletbarcode", "flag" });
    }

    @Override
    public Page<SalesOrder> findTopTenSalesOrder() throws WmsException {
        int pageNumber = 0;
        int pageSize = 10;
        return getListOfSalesOrder(pageNumber, pageSize);
    }

    @Override
    public List<SalesOrder> findListOfSalesOrderWhichIsNotDispatched(OrderStatus status) {
        return salesOrderRepository.findByStatusNotAndRamcoOrderStatusNot(status, RamcoOrderStatus.SC);
    }

    @Override
    public List<SalesOrderDetails> findSalesOrderDetailByProductAndSalesOrder(Long productId, Long salesOrderId)
            throws WmsException {
        return salesOrderDetailsReposiory.findBySalesOrder_IdAndProduct_Id(salesOrderId, productId);
    }

    @Override
    public FileSystemResource exportSalesOrderDeviationReport(HttpServletResponse response, String csvFileName)
            throws WmsException, IOException {
        log.info("inside exportSalesOrderDeviationReport Method ");
        List<IndividualSalesOrderDeviationReport> reportForIndividualSalesOrder = reportService
                .getDeviationReportForIndividualSalesOrder();
        response.setHeader("Content-disposition", "attachment; filename=" + csvFileName);
        createCsvFileForSalesOrderDeviationReport(reportForIndividualSalesOrder, csvFileName);
        // Sendind indentation report on specified email
        if (new File(csvFileName).exists() && new File(csvFileName).length() != 0) {
            sendAttachment(csvFileName);
        }
        return new FileSystemResource(new File(csvFileName));
    }

    @Override
    public FileSystemResource exportSalesOrderIdDetails(HttpServletResponse response, String csvFileName, Long salesOrderId) throws WmsException, IOException {
        response.setHeader("Content-disposition", "attachment; filename=" + csvFileName);
        CSVWriter csvWriter = new CSVWriter(new FileWriter(csvFileName));
        csvWriter.writeNext(new String[] { "SKU Code", "Product", "Description", "Actual Shipped",
                "Plan Dispatch", "Total Order Quantity(CARTONS)", "Total Shipped Quantity(CARTONS)", "Total Pending Quantity(CARTONS)" });
        List <Long> list = new ArrayList<Long>();
        list.add(salesOrderId);
        List <SalesOrderDetails> salesOrderDetails = salesOrderRepository.findOne(salesOrderId).getSalesOrderDetails ();

        for (SalesOrderDetails salesOrderDetail : salesOrderDetails){
            List<String> temp = new ArrayList<String>();
            temp.add(salesOrderDetail.getProduct().getSku());
            temp.add(salesOrderDetail.getProduct().getName());
            temp.add(salesOrderDetail.getProduct().getDescription());
            temp.add(salesOrderDetail.getActualShippedQuantityInCB().toString ());
            temp.add("NAN");
            temp.add(salesOrderDetail.getTotalOrderQuanityInCB().toString ());
            temp.add(salesOrderDetail.getShippedQuantityInCB().toString ());
            temp.add(salesOrderDetail.getPendingQuantityInCB().toString ());

            csvWriter.writeNext(temp.toArray(new String[0]));
        }
        csvWriter.close();
//        if (new File(csvFileName).exists() && new File(csvFileName).length() != 0) {
//            sendAttachment(csvFileName);
//        }
        return new FileSystemResource(new File(csvFileName));
    }

    private void createCsvFileForSalesOrderDeviationReport(
            List<IndividualSalesOrderDeviationReport> reportForIndividualSalesOrder, String csvFileName)
            throws IOException {
        log.info("inside createCsvFileForSalesOrderDeviationReport method");
        // initializing csvwriter
        CSVWriter csvWriter = new CSVWriter(new FileWriter(csvFileName));
        createCsvHeaderForSalesOrderDeviationReport(csvWriter);
        writingSalesOrderDeviationReportToCsv(reportForIndividualSalesOrder, csvWriter);
        csvWriter.close();
    }

    private void createCsvHeaderForSalesOrderDeviationReport(CSVWriter csvWriter) {
        log.info("inside createCsvHeaderForSalesOrderDeviationReport method");
        csvWriter.writeNext(new String[] { "Sales Order Number", "Sales Order Date", "Customer Name", "Item Code",
                "Item Desciption", "Pending Quantity(CARTONS)" });
    }

    private void writingSalesOrderDeviationReportToCsv(
            List<IndividualSalesOrderDeviationReport> reportForIndividualSalesOrder, CSVWriter csvWriter) {
        log.info("inside writingSalesOrderDeviationReportToCsv method");
        for (IndividualSalesOrderDeviationReport individualSalesOrder : reportForIndividualSalesOrder) {
            List<String> temp = new ArrayList<String>();
            List<DeviationReport> deviationReport = individualSalesOrder.getListOfDeviationReport();
            for (DeviationReport deviation : deviationReport) {
                temp.add(individualSalesOrder.getSalesOrder().getOrderNumber());
                temp.add(individualSalesOrder.getSalesOrder().getOrderDate().toString());
                temp.add(individualSalesOrder.getSalesOrder().getCustomer().getName());
                temp.add(deviation.getProductSku());
                temp.add(deviation.getProductDescription());
                temp.add(Integer.valueOf(deviation.getPendingQuantity()).toString());
            }

            csvWriter.writeNext(temp.toArray(new String[0]));

        }
    }

    @Override
    public SalesOrderDto addSalesOrderForDispatch(List<SalesOrderDetailsDto> salesOrderDetailsDtoList, Long id,
                                                  OrderFulfillmentType orderFulfillmentType) throws WmsException {
        if (CollectionUtils.isEmpty(salesOrderDetailsDtoList)) {
            throw new WmsException(HttpStatus.BAD_REQUEST,
                    "Sales Order must have at least one order line in order to be DISPATCHED");
        }
        SalesOrder salesOrder = findOne(id);
        List<Product> products = new ArrayList<Product>();
        if (!OrderStatus.WAITING.equals(salesOrder.getStatus())
                && !OrderStatus.PARTIALLY_FULFILLMENT_DONE.equals(salesOrder.getStatus())) {
            throw new WmsException(HttpStatus.BAD_REQUEST,
                    "Sales Order must be in a WAITING or PARTIALLY_FULFILLMENT_DONE state in order to be DISPATCHED");
        }
        if (orderFulfillmentType == null) {
            throw new WmsException(HttpStatus.BAD_REQUEST,
                    "Sales Order must be either in Carton or Pallet. Please choose one option to move it to Delivery Order");
        }
        salesOrder.setStatus(OrderStatus.ADDED_TO_DISPATCH);
        salesOrder.setOrderFulfillmentType(orderFulfillmentType);

        List<SalesOrderDetails> salesOrderDetailsList = new ArrayList<SalesOrderDetails>();
        for (SalesOrderDetailsDto salesOrderDetailsDto : salesOrderDetailsDtoList) {
            if (salesOrderDetailsDto.getActualReservedQuantityInCB() > (salesOrderDetailsDto.getTotalOrderQuanityInCB()
                    - salesOrderDetailsDto.getActualShippedQuantityInCB())) {
                throw new WmsException(HttpStatus.BAD_REQUEST,
                        "Plan Dispatch Quantity can't exceed pending shippable quantity");
            }
            if (salesOrderDetailsDto.getAvailableQuantity() < salesOrderDetailsDto.getActualReservedQuantityInCB()) {
                throw new WmsException(HttpStatus.BAD_REQUEST,
                        "Plan Dispatch Quantity can't exceed Product available quantity");
            }
            String sku = salesOrderDetailsDto.getProductDto().getSku();
            if (salesOrderDetailsDto.getActualReservedQuantityInCB() <= 0) {
                throw new WmsException(HttpStatus.BAD_REQUEST,
                        "Plan Dispatch Quantity must be a greater than 0 for Item SKU : " + sku);
            }
            Product product = productService.findBySkuIgnoreCase(sku);
            SalesOrderDetails salesOrderDetails = salesOrderDetailsService.findOne(salesOrderDetailsDto.getId());

            if (orderFulfillmentType.equals(OrderFulfillmentType.CARTON)) {
                if (salesOrderDetailsDto.getActualReservedQuantityInCB() != salesOrderDetails
                        .getActualReservedQuantityInCB()) {
                    product.setQuantityInHand(
                            product.getQuantityInHand() - (salesOrderDetailsDto.getActualReservedQuantityInCB()
                                    - salesOrderDetails.getActualReservedQuantityInCB()));

                    products.add(product);
                }
                salesOrderDetails.setActualReservedQuantityInCB(salesOrderDetailsDto.getActualReservedQuantityInCB());
                salesOrderDetails.setActualReservedQuantityInKG(
                        product.getConversionFactor() * salesOrderDetailsDto.getActualReservedQuantityInCB());
                salesOrderDetailsList.add(salesOrderDetails);
            } else if (orderFulfillmentType.equals(OrderFulfillmentType.PALLET)) {
                int numberOfPallets = salesOrderDetailsDto.getActualReservedQuantityInCB()
                        / palletsToCartonsConversionFactor;

                if (numberOfPallets <= 0) {
                    throw new WmsException(HttpStatus.BAD_REQUEST,
                            "Plan Dispatch Quanitity of the Item with SKU CODE : " + product.getSku()
                                    + " must be greater than or equal to " + palletsToCartonsConversionFactor
                                    + " for Reserving Dispatch");
                }

                List<Object[]> queryData = inventoryService
                        .findTOPNDistinctPalletBarCodesByStatus(OrderStatus.AVAILABLE, product, numberOfPallets + 1);
                List<String> palletBarCodeList = new ArrayList<>();
                for (Object[] objects : queryData) {
                    if (objects[0] != null && objects[0] != "") {
                        palletBarCodeList.add(objects[0].toString());
                    }
                }

                if (CollectionUtils.isEmpty(palletBarCodeList) || palletBarCodeList.size() < numberOfPallets) {
                    throw new WmsException(HttpStatus.BAD_REQUEST, "Pallet Quanitity of the Item with SKU CODE : "
                            + product.getSku() + " in the Stock is not sufficient for Reserving Dispatch");
                }

                int reservableQuantityInCB = numberOfPallets * palletsToCartonsConversionFactor;
                if (reservableQuantityInCB != salesOrderDetails.getActualReservedQuantityInCB()) {
                    product.setQuantityInHand(product.getQuantityInHand()
                            - (reservableQuantityInCB - salesOrderDetails.getActualReservedQuantityInCB()));

                    products.add(product);
                }
                salesOrderDetails.setActualReservedQuantityInCB(reservableQuantityInCB);
                salesOrderDetails.setActualReservedQuantityInKG(
                        product.getConversionFactor() * salesOrderDetails.getActualReservedQuantityInCB());
                salesOrderDetailsList.add(salesOrderDetails);
            }
        }
        productService.save(products);
        save(salesOrder);
        salesOrderDetailsService.save(salesOrderDetailsList);
        return salesOrderTransformer.toDto(salesOrder);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SalesOrder releaseReservedProductFromSalesOrderDetails(Long salesOrderId) throws WmsException {
        SalesOrder salesOrder = findOne(salesOrderId);
        List<SalesOrderDetails> salesOrderDetails = salesOrder.getSalesOrderDetails();
        List<SalesOrderDetails> salesOrderDetailList = new ArrayList<>();
        List<Product> products = new ArrayList<>();
        salesOrderDetails.stream().forEach(salesOrderDetail -> {
            if (salesOrderDetail.getActualReservedQuantityInCB() > 0) {
                Product product = salesOrderDetail.getProduct();
                product.setQuantityInHand(
                        product.getQuantityInHand() + salesOrderDetail.getActualReservedQuantityInCB());
                products.add(product);
                salesOrderDetail.setActualReservedQuantityInCB(0);
                salesOrderDetail.setActualReservedQuantityInKG(new Double(0));
                salesOrderDetailList.add(salesOrderDetail);
            }
        });
        productService.save(products);
        salesOrderDetailsService.save(salesOrderDetailList);
        return salesOrder;
    }

    @Override
    public SalesOrder updateSalesOrderStatus(UpdateStatusDto updateStatusDto) throws WmsException {
        SalesOrder salesOrder = findOne(updateStatusDto.getId());
        salesOrder.setStatus(updateStatusDto.getOrderStatus());
        return save(salesOrder);
    }

    @Override
    public Page<SalesOrder> getAllFulfilledSalesOrders(Integer pageNumber, Integer pageSize) throws WmsException {
        log.info("inside getAllFulfilledSalesOrders method in service: " + pageNumber + pageSize);
        Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "orderDate"));
        validatePageNumber(pageNumber, pageSize);
        return salesOrderRepository.findByStatusOrRamcoOrderStatus(OrderStatus.FULFILLED, RamcoOrderStatus.CL,pageable);
    }

    @Override
    public Page<SalesOrder> getAllPartiallyFulfilledSalesOrders(Integer pageNumber, Integer pageSize) throws WmsException {
        log.info("inside getAllPartiallyFulfilledSalesOrders method in service: " + pageNumber + pageSize);
        Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "orderDate"));
        validatePageNumber(pageNumber, pageSize);
        return salesOrderRepository.findByStatusOrRamcoOrderStatus(OrderStatus.PARTIALLY_FULFILLMENT_DONE, RamcoOrderStatus.SC, pageable);
    }

    @Override
    public Page<SalesOrder> getAllPendingSalesOrders(Integer pageNumber, Integer pageSize) throws WmsException {
        log.info("inside getAllPendingSalesOrders method in service: " + pageNumber + pageSize);
        Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "orderDate"));
        validatePageNumber(pageNumber, pageSize);
        return salesOrderRepository.findSalesOrderByRamcoOrderStatus(RamcoOrderStatus.AU, pageable);
    }

    public Long getCountOfPendingOrders () throws WmsException{
        return salesOrderRepository.countByRamcoOrderStatusInAndActiveTrue(RamcoOrderStatus.AU);
    }

    public List <OrderStatusDto> getOrderStatusToday () throws WmsException{
        log.info("inside getOrderStatusToday method in service: " + this.getClass().getName());

        List <OrderStatusDto> orders = new ArrayList<OrderStatusDto>();

        for (Object[] object : salesOrderRepository.orderReportForToday()){
            OrderStatusDto osd = new OrderStatusDto ();
            osd.setStatus (object [1].toString());
            osd.setCount (Integer.parseInt(object [0].toString()));
            orders.add (osd);
        }


        return orders;
    }




}
