package com.smart.logistic.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.IndentReport;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.IIndentReportRepository;
import com.smart.logistic.service.IIndentReportService;
import com.smart.logistic.service.generic.AbstractService;

@Service
public class IndentReportServiceImpl extends AbstractService<IndentReport> implements IIndentReportService{

	@Autowired
	IIndentReportRepository indentReportRepository;
	
	@Override
	protected WmsException notFoundException() {
		// TODO Auto-generated method stub
		return new WmsException("IndentReport not found");
	}

	@Override
	public List<IndentReport> findByDateCreatedBetween(Date startDate, Date endDate) {
		return indentReportRepository.findByDateCreatedBetween(startDate, endDate);
	}

	@Override
	public IndentReport findTopByOrderByFromDateDesc() {
		return indentReportRepository.findTopByOrderByFromDateDesc();
	}

	@Override
	public List<IndentReport> findByFromDate(Date generatedDateTime) {
		return indentReportRepository.findByFromDate(generatedDateTime);
	}

	@Override
	public IndentReport findTopByOrderByTillDateDesc() {
		return indentReportRepository.findTopByOrderByFromDateDesc();
	}

	

}
