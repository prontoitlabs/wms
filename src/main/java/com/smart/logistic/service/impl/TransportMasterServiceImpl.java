package com.smart.logistic.service.impl;

import org.springframework.stereotype.Service;

import com.smart.logistic.domain.TransportMaster;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.ITransportMasterService;
import com.smart.logistic.service.generic.AbstractService;


@Service
public class TransportMasterServiceImpl extends AbstractService<TransportMaster> implements ITransportMasterService{

	@Override
	protected WmsException notFoundException() {
		return new WmsException("Transport master not found");	}

}
