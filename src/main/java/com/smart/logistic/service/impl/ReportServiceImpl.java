package com.smart.logistic.service.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import com.opencsv.CSVWriter;
import com.smart.logistic.domain.DeviationReport;
import com.smart.logistic.domain.IndentReport;
import com.smart.logistic.domain.IndividualSalesOrderDeviationReport;
import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.SalesOrder;
import com.smart.logistic.domain.SalesOrderDetails;
import com.smart.logistic.domain.SalesOrderPackslip;
import com.smart.logistic.domain.SalesOrderPackslipItem;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.IIndentReportService;
import com.smart.logistic.service.IIndividualSalesOrderDeviationReportService;
import com.smart.logistic.service.IOrderPackslipService;
import com.smart.logistic.service.IReportService;
import com.smart.logistic.service.ISalesOrderService;
import com.smart.logistic.service.generic.AbstractService;
import com.smart.logistic.util.DateUtil;
import com.smart.logistic.util.DeviatedByForLossLeaderComparator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ReportServiceImpl extends AbstractService<DeviationReport> implements IReportService {

	@Autowired
	private ISalesOrderService salesOrderService;

	@Autowired
	private IIndentReportService indentReportService;

	@Autowired
	private IIndividualSalesOrderDeviationReportService individualSalesOrderDeviationReportService;

	@Autowired
	private IOrderPackslipService orderPackslipService;

	@Autowired
	private IReportService reportService;

	@Override
	protected WmsException notFoundException() {
		return new WmsException("Deviation report not found");
	}

	@Transactional
	@Override
	public List<DeviationReport> getdeviationReportForLossLeader() throws WmsException {

		List<IndividualSalesOrderDeviationReport> deviationReportForIndividualSalesOrder = getDeviationReportForIndividualSalesOrder();

		List<DeviationReport> listOfDeviationReport = new ArrayList<DeviationReport>();

		Map<String, DeviationReport> productMap = constructMapForIndividualReport(
				deviationReportForIndividualSalesOrder);

		listOfDeviationReport = mapValuesToDeviationReportList(productMap);

		Collections.sort(listOfDeviationReport, new DeviatedByForLossLeaderComparator());

		return listOfDeviationReport;
	}

	private Map<String, DeviationReport> constructMapForIndividualReport(
			List<IndividualSalesOrderDeviationReport> individualSalesOrderDeviationReports) {

		Map<String, DeviationReport> deviationReportMap = new HashMap<String, DeviationReport>();

		for (IndividualSalesOrderDeviationReport individualSalesOrderDeviationReport : individualSalesOrderDeviationReports) {
			for (DeviationReport deviationReport : individualSalesOrderDeviationReport.getListOfDeviationReport()) {

				// this product sku already exist
				if (deviationReportMap.containsKey(deviationReport.getProductSku())) {

					DeviationReport deviationReportToBeModified = deviationReportMap
							.get(deviationReport.getProductSku());

					deviationReportToBeModified.setDeviatedBy(
							deviationReportToBeModified.getDeviatedBy() + deviationReport.getDeviatedBy());

					deviationReportToBeModified.setPendingQuantity(
							deviationReportToBeModified.getPendingQuantity() + deviationReport.getPendingQuantity());

					// if map already contains same key then we are just incrementing deviated by
					// value
					deviationReportMap.put(deviationReport.getProductSku(), deviationReportToBeModified);

				} else {

					deviationReportMap.put(deviationReport.getProductSku(), deviationReport);
				}

			}

		}
		return deviationReportMap;
	}

	@Override
	public List<IndividualSalesOrderDeviationReport> getDeviationReportForIndividualSalesOrder() throws WmsException {

		IndividualSalesOrderDeviationReport individualSalesOrderDeviationReport = individualSalesOrderDeviationReportService
				.findTopDeviationReportByMaxDate();
		List<IndividualSalesOrderDeviationReport> individualSalesOrderDeviationReports = new ArrayList<>();
		if (individualSalesOrderDeviationReport == null) {
			return individualSalesOrderDeviationReports;
		}
		Date generatedDateTime = individualSalesOrderDeviationReport.getGeneratedDateTime();
		individualSalesOrderDeviationReports = individualSalesOrderDeviationReportService
				.findByGeneratedDateTime(generatedDateTime);
		return individualSalesOrderDeviationReports;

	}

	@Override
	@Transactional
	public List<IndividualSalesOrderDeviationReport> saveDeviationReportForIndividualSalesOrder() throws WmsException {

		List<IndividualSalesOrderDeviationReport> individualSalesOrderDeviationReports = new ArrayList<IndividualSalesOrderDeviationReport>();

		List<SalesOrder> listOfUnDispatchedSalesOrder = salesOrderService
				.findListOfSalesOrderWhichIsNotDispatched(OrderStatus.FULFILLED);

		Date generatedDateTime = DateTime.now().toDate();

		for (SalesOrder salesOrder : listOfUnDispatchedSalesOrder) {

			IndividualSalesOrderDeviationReport individualSalesOrderDeviationReport = new IndividualSalesOrderDeviationReport();

			Map<String, DeviationReport> individualMap = constructMapForIndividualSalesOrderDeviation(salesOrder,
					individualSalesOrderDeviationReport);

			List<DeviationReport> deviationReportList = mapValuesToDeviationReportList(individualMap);
			if (!CollectionUtils.isEmpty(deviationReportList)) {
				individualSalesOrderDeviationReport.setListOfDeviationReport(deviationReportList);
				individualSalesOrderDeviationReport.setSalesOrder(salesOrder);
				individualSalesOrderDeviationReport.setGeneratedDateTime(generatedDateTime);
				individualSalesOrderDeviationReports.add(individualSalesOrderDeviationReport);
			}
		}
		if (!CollectionUtils.isEmpty(individualSalesOrderDeviationReports)) {
			individualSalesOrderDeviationReportService.save(individualSalesOrderDeviationReports);
		}
		return individualSalesOrderDeviationReports;
	}

	private List<DeviationReport> mapValuesToDeviationReportList(Map<String, DeviationReport> individualMap) {

		List<DeviationReport> deviationReportList = new ArrayList<DeviationReport>();

		for (Map.Entry<String, DeviationReport> item : individualMap.entrySet()) {
			if (item.getValue() != null && item.getValue().getDeviatedBy() != null
					&& item.getValue().getDeviatedBy() != 0) {
				deviationReportList.add(item.getValue());

			}
		}
		return deviationReportList;
	}

	private Map<String, DeviationReport> constructMapForIndividualSalesOrderDeviation(SalesOrder salesOrder,
			IndividualSalesOrderDeviationReport individualSalesOrderDeviationReport) throws WmsException {
		Map<String, DeviationReport> productDerivationMap = new HashMap<>();

		for (SalesOrderDetails salesOrderDetails : salesOrder.getSalesOrderDetails()) {

			if (salesOrderDetails.getProduct() != null && salesOrderDetails.getProduct().getSku() != null) {
				String productSku = salesOrderDetails.getProduct().getSku();
				DeviationReport deviationReport = createDeviationReport(salesOrderDetails,
						individualSalesOrderDeviationReport);
				productDerivationMap.put(productSku, deviationReport);
			}
		}
		return productDerivationMap;
	}

	private DeviationReport createDeviationReport(SalesOrderDetails salesOrderDetails,
			IndividualSalesOrderDeviationReport individualSalesOrderDeviationReport) throws WmsException {

		DeviationReport deviationReport = new DeviationReport();
		Product product = salesOrderDetails.getProduct();

		// here setting product Specific attributes
		deviationReport.setProductSku(product.getSku());
		deviationReport.setProductDescription(product.getDescription());
		deviationReport.setProductName(product.getName());
		deviationReport.setAvailableQuantity(product.getQuantityInHand());

		// incrementing product pendingQuantity
		deviationReport.setPendingQuantity(salesOrderDetails.getPendingQuantityInCB());

		// calculating deviation
		if (deviationReport.getPendingQuantity() > deviationReport.getAvailableQuantity()) {
			deviationReport
					.setDeviatedBy(deviationReport.getPendingQuantity() - deviationReport.getAvailableQuantity());
		}
		deviationReport.setIndividualSalesOrderDeviationReport(individualSalesOrderDeviationReport);
		return deviationReport;
	}

	@Override
	public List<IndentReport> getIndentReport() {
		Date fromDate = null;
		IndentReport indentReport = indentReportService.findTopByOrderByFromDateDesc();
		if (indentReport == null) {
			fromDate = DateTime.now().minusHours(24).toDate();
		} else {
			fromDate = indentReport.getFromDate();
		}
		List<IndentReport> listofIndentReport = indentReportService.findByFromDate(fromDate);
		return listofIndentReport;
	}

	@Override
	public List<IndentReport> saveIndentReport() {
		IndentReport indentReport = indentReportService.findTopByOrderByTillDateDesc();
		Date endDate = DateTime.now().toDate();
		Date startDate = null;
		if (indentReport == null) {
			startDate = DateTime.now().minusHours(24).toDate();
		} else {
			startDate = indentReport.getTillDate();
		}
		List<SalesOrderPackslip> listOfSalesOrderPackSlip = orderPackslipService
				.findByLastModifiedBetweenAndOrderStatus(startDate, endDate, OrderStatus.DONE);
		return indentReportService
				.save(processMapForIndent(constructMapForProductAndDispatchedQuantity(listOfSalesOrderPackSlip)));
	}

	private Map<Product, Integer> constructMapForProductAndDispatchedQuantity(
			List<SalesOrderPackslip> listOfSalesOrderpackSlip) {

		// List<SalesOrderPackslipItem> listOfSalesOrderPackslipItem
		Map<Product, Integer> productMap = new HashMap<>();

		for (SalesOrderPackslip salesOrderPackslip : listOfSalesOrderpackSlip) {

			for (SalesOrderPackslipItem SalesOrderPackslipItem : salesOrderPackslip.getOrderPackslipItems()) {

				Product product = SalesOrderPackslipItem.getProduct();

				if (productMap.containsKey(product)) {

					// if map already contains same key then we are just incrementing value
					productMap.put(product, productMap.get(product) + SalesOrderPackslipItem.getVerifiedCases());

				} else {
					// if first time entering in map
					productMap.put(product, SalesOrderPackslipItem.getVerifiedCases());
				}
			}
		}
		return productMap;
	}

	private List<IndentReport> processMapForIndent(Map<Product, Integer> productmap) {

		List<IndentReport> listOfIndentReport = new ArrayList<IndentReport>();

		// processing itemList
		for (Map.Entry<Product, Integer> item : productmap.entrySet()) {

			IndentReport indentReport = new IndentReport();

			indentReport.setName(item.getKey().getDescription());

			indentReport.setSku(item.getKey().getSku());

			indentReport.setTotalDispatchedItem(item.getValue());

			indentReport.setFromDate(DateUtil.calculateDateBeforeCertainHourOfToday(24));

			indentReport.setTillDate(DateUtil.calculateDateAfterCertainHourOfToday(21));

			listOfIndentReport.add(indentReport);
		}

		return listOfIndentReport;

	}

	@Override
	public FileSystemResource exportIndentReport(HttpServletResponse response, String csvFileName)
			throws WmsException, IOException {
		log.info("inside exportIndentReport Method ");
		List<IndentReport> indentReportList = reportService.getIndentReport();
		response.setHeader("Content-disposition", "attachment; filename=" + csvFileName);
		createCsvFileForIndentReport(indentReportList, csvFileName);
		// Sendind indentation report on specified email
		if (new File(csvFileName).exists() && new File(csvFileName).length() != 0) {
			sendAttachment(csvFileName);
		}
		return new FileSystemResource(new File(csvFileName));
	}

	private void createCsvFileForIndentReport(List<IndentReport> indentReportList, String csvFileName)
			throws IOException {
		log.info("inside createCsvFileForIndentReport method");
		// initializing csvwriter
		CSVWriter csvWriter = new CSVWriter(new FileWriter(csvFileName));
		createCsvHeaderForIndentReport(csvWriter);
		writingIndentReportToCsv(indentReportList, csvWriter);
		csvWriter.close();
	}

	private void writingIndentReportToCsv(List<IndentReport> indentReportList, CSVWriter csvWriter) {
		log.info("inside writingIndentReportToCsv method");
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMYYYY hh:mm:ss");
		for (IndentReport indentReport : indentReportList) {
			csvWriter.writeNext(new String[] { indentReport.getSku(), indentReport.getName(),
					indentReport.getTotalDispatchedItem().toString(), sdf.format(indentReport.getFromDate()),
					sdf.format(indentReport.getTillDate()) });
		}
	}

	private void createCsvHeaderForIndentReport(CSVWriter csvWriter) {
		log.info("inside createCsvHeaderForIndentReport method");
		csvWriter.writeNext(new String[] { "SKU", "Name", "Total Dispatched Item", "From Date", "Till Date" });
	}

}
