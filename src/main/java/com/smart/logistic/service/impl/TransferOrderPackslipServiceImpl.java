package com.smart.logistic.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.smart.logistic.domain.Inventory;
import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.TransferOrder;
import com.smart.logistic.domain.TransferOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.TransferOrderPackslip;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;
import com.smart.logistic.dto.TransferOrderDto;
import com.smart.logistic.dto.TransferOrderPackslipDto;
import com.smart.logistic.dto.TransferOrderReceiptDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.ITransferOrderPackslipRepository;
import com.smart.logistic.service.IInventoryService;
import com.smart.logistic.service.IProductService;
import com.smart.logistic.service.ITransferOrderPackSlipDispatchDetailsService;
import com.smart.logistic.service.ITransferOrderPackslipService;
import com.smart.logistic.service.ITransferOrderService;
import com.smart.logistic.service.generic.AbstractService;
import com.smart.logistic.transformer.TransferOrderPackslipTransformer;
import com.smart.logistic.transformer.TransferOrdersTransformer;

@Service
public class TransferOrderPackslipServiceImpl extends AbstractService<TransferOrderPackslip>
		implements ITransferOrderPackslipService {

	@Autowired
	private ITransferOrderPackslipRepository transferOrderPackslipRepository;

	@Autowired
	private ITransferOrderPackSlipDispatchDetailsService transferOrderPackSlipDispatchDetailsService;

	@Autowired
	private IInventoryService inventoryService;

	@Autowired
	private IProductService productService;

	@Autowired
	private ITransferOrderService transferOrderService;

	@Autowired
	private TransferOrderPackslipTransformer transferOrderPackslipTransformer;
	
	@Autowired
	private TransferOrdersTransformer transferOrdersTransformer;
	

	@Override
	public Page<TransferOrderPackslip> getAllTransferOrderPackingSlips(Integer pageNumber, Integer pageSize)
			throws WmsException {
		Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "dateCreated"));
		validatePageNumber(pageNumber, pageSize);
		return findAll(pageable);
	}

	private void validatePageNumber(Integer pageNumber, Integer pageSize) throws WmsException {
		if ((pageNumber == null) || (pageSize == null) || (pageNumber < 0) || (pageSize < 0)) {
			throw new WmsException(HttpStatus.BAD_REQUEST, "PageNumber or PageSize not valid");
		}
	}

	@Override
	public List<TransferOrderPackslipDto> getTransferOrderPackslipsByTransferOrderId(Long transferOrderId) {
		List<TransferOrderPackslip> transferOrderPackslips = transferOrderPackslipRepository
				.findByTransferOrder_Id(transferOrderId);
		return transferOrderPackslipTransformer.toDto(transferOrderPackslips);
	}

	@Override
	protected WmsException notFoundException() {
		return new WmsException("Packing slip not found");
	}

	@Override
	@Transactional
	public TransferOrderPackslip validatePackslip(Long packslipId) throws WmsException {
		TransferOrderPackslip transferOrderPackslip = findOne(new Long(packslipId));
		// Validation Logic
		if (transferOrderPackslip.getOrderStatus().equals(OrderStatus.DONE)) {
			throw new WmsException(HttpStatus.OK, "The Transfer Order Packslip is already validated");
		}

		// Find all Packslip Dispatch Details whose status is DONE
		List<TransferOrderPackSlipDispatchDetails> packslipDispatchDetails = transferOrderPackSlipDispatchDetailsService
				.findAllPackslipDispatchDetailsByPackslipIdAndStatus(packslipId, OrderStatus.DONE);
		if (packslipDispatchDetails.size() != transferOrderPackslip.getReservedCases()
				|| packslipDispatchDetails.size() == 0 || transferOrderPackslip.getDoneCases() == 0) {
			throw new WmsException(HttpStatus.OK,
					"The Transfer Order Packslip cannot be validated before all the Cartons are Processed");
		}

		// Save All Dispatch Details Items in Inventory
		List<Inventory> stock = new ArrayList<Inventory>();
		Map<String, Integer> productQuantity = new HashMap<String, Integer>();
		for (TransferOrderPackSlipDispatchDetails transferOrderPackSlipDispatchDetails : packslipDispatchDetails) {

			Inventory inventory = Inventory.builder()
					.batchNumber(transferOrderPackSlipDispatchDetails.getBatchNumber())
					.inventoryDate(DateTime.now().toDate()).orderStatus(OrderStatus.AVAILABLE)
					.palletBarcode(transferOrderPackSlipDispatchDetails.getPalletBarcode())
					.product(transferOrderPackSlipDispatchDetails.getProduct()).quantity(1.0)
					.scanBarcode(transferOrderPackSlipDispatchDetails.getScanBarcode())
					.unitOfMeasure(UnitsOfMeasurement.CARTONS).build();
			stock.add(inventory);

			String sku = transferOrderPackSlipDispatchDetails.getProduct().getSku();
			if (!productQuantity.containsKey(sku)) {
				productQuantity.put(sku, 1);
			} else {
				productQuantity.put(sku, productQuantity.get(sku) + 1);
			}
		}
		inventoryService.save(stock);

		// Update Product Quantity
		for (Entry<String, Integer> productQuantityMap : productQuantity.entrySet()) {
			Product product = productService.findBySkuIgnoreCase(productQuantityMap.getKey());
			product.setQuantityInHand(product.getQuantityInHand() + productQuantityMap.getValue());
			productService.save(product);
		}

		transferOrderPackslip.setOrderStatus(OrderStatus.DONE);
		transferOrderPackslip = save(transferOrderPackslip);

		TransferOrder transferOrder = transferOrderPackslip.getTransferOrder();
		transferOrder.setStatus(OrderStatus.DONE);
		transferOrderService.save(transferOrder);

		return transferOrderPackslip;
	}

	@Override
	@Transactional
	public List<TransferOrderPackslip> validateTransferOrderPackSlips(Long transferOrderId) throws WmsException {
		List<TransferOrderPackslip> transferOrderPackslips = transferOrderPackslipRepository
				.findByTransferOrder_Id(new Long(transferOrderId));
		List<TransferOrderPackslip> transferOrderPackslipList = new ArrayList<TransferOrderPackslip>();
		for (TransferOrderPackslip transferOrderPackslip : transferOrderPackslips) {
			// Validation Logic
			if (transferOrderPackslip.getOrderStatus().equals(OrderStatus.DONE)) {
				throw new WmsException(HttpStatus.OK, "The Transfer Order Packslip is already validated");
			}

			// Find all Packslip Dispatch Details of indivudual packslip whose status is
			// DONE
			List<TransferOrderPackSlipDispatchDetails> packslipDispatchDetails = transferOrderPackSlipDispatchDetailsService
					.findAllPackslipDispatchDetailsByPackslipIdAndStatus(transferOrderPackslip.getId(),
							OrderStatus.DONE);
			if (packslipDispatchDetails.size() != transferOrderPackslip.getReservedCases()
					|| packslipDispatchDetails.size() == 0 || transferOrderPackslip.getDoneCases() == 0) {
				throw new WmsException(HttpStatus.OK,
						"The Transfer Order Packslip cannot be validated before all the Cartons are Processed");
			}
			// Save All Dispatch Details Items of indivudual packslip in Inventory
			List<Inventory> stock = new ArrayList<Inventory>();
			Map<String, Integer> productQuantity = new HashMap<String, Integer>();
			for (TransferOrderPackSlipDispatchDetails transferOrderPackSlipDispatchDetails : packslipDispatchDetails) {
				Inventory inventory = Inventory.builder()
						.batchNumber(transferOrderPackSlipDispatchDetails.getBatchNumber())
						.inventoryDate(DateTime.now().toDate()).orderStatus(OrderStatus.AVAILABLE)
						.palletBarcode(transferOrderPackSlipDispatchDetails.getPalletBarcode())
						.product(transferOrderPackSlipDispatchDetails.getProduct()).quantity(1.0)
						.scanBarcode(transferOrderPackSlipDispatchDetails.getScanBarcode())
						.unitOfMeasure(UnitsOfMeasurement.CARTONS).build();
				stock.add(inventory);

				String sku = transferOrderPackSlipDispatchDetails.getProduct().getSku();
				if (!productQuantity.containsKey(sku)) {
					productQuantity.put(sku, 1);
				} else {
					productQuantity.put(sku, productQuantity.get(sku) + 1);
				}
			}
			inventoryService.save(stock);
			// Update Product Quantity
			for (Entry<String, Integer> productQuantityMap : productQuantity.entrySet()) {
				Product product = productService.findBySkuIgnoreCase(productQuantityMap.getKey());
				product.setQuantityInHand(product.getQuantityInHand() + productQuantityMap.getValue());
				productService.save(product);
			}
			transferOrderPackslip.setOrderStatus(OrderStatus.DONE);
			transferOrderPackslip = save(transferOrderPackslip);
			TransferOrder transferOrder = transferOrderPackslip.getTransferOrder();
			transferOrder.setStatus(OrderStatus.DONE);
			transferOrderPackslipList.add(transferOrderPackslip);
			transferOrderService.save(transferOrder);
		}
		return transferOrderPackslipList;
	}

	@Override
	public TransferOrderReceiptDto getPackslipsOfTransferOrderByTransferOrderId(Long transferOrderId)
			throws WmsException {
		TransferOrderReceiptDto transferOrderPackingSlipDto = new TransferOrderReceiptDto();
		
		transferOrderPackingSlipDto.setTransferOrderPackslipDto(
				transformToDto(transferOrderPackslipRepository.findByTransferOrder_Id(transferOrderId)));
		transferOrderPackingSlipDto.setTransferOrderDto((transferOrderService.getTransferOrderById(transferOrderId)));
	TransferOrderDto 	transferOrderDto = transferOrdersTransformer.toDto(transferOrderService.findOne(transferOrderId));
	transferOrderPackingSlipDto.setTransferOrderDate(transferOrderDto.getTransferOrderDate());
		transferOrderPackingSlipDto.setTransferQuantity(transferOrderDto.getTransferQuantity());
		return transferOrderPackingSlipDto;
	}

	private List<TransferOrderPackslipDto> transformToDto(List<TransferOrderPackslip> transferOrderPackslip) {
		return transferOrderPackslipTransformer.toDto(transferOrderPackslip);
	}
}