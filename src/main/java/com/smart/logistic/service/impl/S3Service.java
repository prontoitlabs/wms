package com.smart.logistic.service.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.smart.logistic.exception.WmsException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class S3Service {

	@Autowired
	private AmazonS3 amazonS3;

	@Async
	public void pushToS3(MultipartFile file, String awsBucketName, String awsRemoteDirectory) throws WmsException {
		log.info("inside pushToS3 method : " + file, awsBucketName, awsRemoteDirectory);
		String fileNameForS3 =file.getName();
		String contentType = file.getContentType();
		InputStream inputStream = null;
		try {
			Long contentLength = file.getSize();
			ObjectMetadata meta = new ObjectMetadata();
			meta.setContentLength(contentLength);
			meta.setContentType(contentType);
			inputStream = new ByteArrayInputStream(file.getBytes());
			amazonS3.putObject(
					new PutObjectRequest(awsBucketName, awsRemoteDirectory + "/" + fileNameForS3, inputStream, meta)
							.withCannedAcl(CannedAccessControlList.PublicRead));
		} catch (Exception e) {
			throw new WmsException(HttpStatus.BAD_REQUEST, "Upload failed for this file " + file);
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					throw new WmsException(HttpStatus.BAD_REQUEST, "Upload failed for this file " + file);
				}
			}
		}
	}

}
