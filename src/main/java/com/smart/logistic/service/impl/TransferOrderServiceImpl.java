package com.smart.logistic.service.impl;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;

import com.google.gson.Gson;
import com.opencsv.CSVReader;
import com.smart.logistic.domain.Inventory;
import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.TransferOrder;
import com.smart.logistic.domain.TransferOrderDetails;
import com.smart.logistic.domain.TransferOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.TransferOrderPackslip;
import com.smart.logistic.domain.TransferOrderPackslipItem;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;
import com.smart.logistic.dto.ReceiptTransferOrderDto;
import com.smart.logistic.dto.TransferOrderDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.ITransferOrderRepository;
import com.smart.logistic.service.IInventoryService;
import com.smart.logistic.service.IProductService;
import com.smart.logistic.service.ITransferOrderPackslipService;
import com.smart.logistic.service.ITransferOrderService;
import com.smart.logistic.service.generic.AbstractService;
import com.smart.logistic.transformer.ReceiptOrderTransformer;
import com.smart.logistic.transformer.TransferOrdersTransformer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TransferOrderServiceImpl extends AbstractService<TransferOrder> implements ITransferOrderService {

	@Autowired
	private ITransferOrderRepository transferOrderRepository;

	@Autowired
	private TransferOrdersTransformer transferOrderTransformer;

	@Autowired
	private AsyncRestTemplate asyncRestTemplate;

	@Autowired
	private IProductService productService;

	@Autowired
	private ITransferOrderPackslipService transferOrderPackslipService;
	
	@Autowired
	private IInventoryService inventoryService;
	
	@Autowired
	private ReceiptOrderTransformer receiptOrderTransformer;

	@Value("${transporter.transferorder.api.url}")
	private String transferOrderApiUrl;

	@Override
	protected WmsException notFoundException() {
		return new WmsException("Transfer Order not found");
	}

	@Override
	public Page<TransferOrder> getListOfTransferOrder(Integer pageNumber, Integer pageSize) throws WmsException {
		log.info("getting list of transfer orders: " + pageNumber + pageSize);
		Pageable pageable = new PageRequest(pageNumber, pageSize, new Sort(Direction.DESC, "transferOrderDate"));
		validatePageNumber(pageNumber, pageSize);
		return findAll(pageable);
	}

	private void validatePageNumber(Integer pageNumber, Integer pageSize) throws WmsException {
		if ((pageNumber == null) || (pageSize == null) || (pageNumber < 0) || (pageSize < 0)) {
			throw new WmsException(HttpStatus.BAD_REQUEST, "PageNumber or PageSize not valid");
		}
	}

	@Override
	public TransferOrderDto getTransferOrderById(Long id) throws WmsException {
		return transferOrderTransformer.toDtoEntity(transferOrderRepository.findById(id));
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public String sendDispatchPlanforPendingTransferOrders() throws WmsException {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		List<TransferOrder> waitingTransferOrders = transferOrderRepository.findByStatus(OrderStatus.RESERVED);
		if (CollectionUtils.isEmpty(waitingTransferOrders)) {
			throw new WmsException(HttpStatus.OK, "There are no Pending Transfer Orders");
		}

		List<TransferOrderDto> tranferOrdersDto = transferOrderTransformer.toDto(waitingTransferOrders);
		List<ReceiptTransferOrderDto> receiptOrders = receiptOrderTransformer.toDto(tranferOrdersDto);
		
		HttpEntity<List<ReceiptTransferOrderDto>> httpEntity = new HttpEntity<List<ReceiptTransferOrderDto>>(receiptOrders, headers);
		Gson gson = new Gson();
		String jsonInString = gson.toJson(receiptOrders);
		log.info(jsonInString);
		ListenableFuture<ResponseEntity<String>> future = asyncRestTemplate.exchange(transferOrderApiUrl,
				HttpMethod.POST, httpEntity, String.class, receiptOrders);
		String body = "";
		try {
			// waits for the result
			ResponseEntity<String> entity = future.get();
			List<TransferOrder> transferOrders = new ArrayList<TransferOrder>();
			if (entity.getStatusCode().equals(HttpStatus.CREATED)) {
				body = entity.getBody();
				log.info(body);
				for (ReceiptTransferOrderDto reciptTransferOrderDto : receiptOrders) {
					TransferOrder transferOrder = findTransferOrderByOrderNumber(
							reciptTransferOrderDto.getOrderNumber());
					transferOrder.setStatus(OrderStatus.QUEUED_FOR_VEHICLE_ALLOCATION);
					transferOrders.add(transferOrder);
				}
				save(transferOrders);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return body;
	}

	@Override
	public TransferOrder findTransferOrderByOrderNumber(String transferOrderNumber) throws WmsException {
		TransferOrder transferOrder = transferOrderRepository.findByTrasferOrderNumber(transferOrderNumber);
		if (transferOrder == null) {
			throw new WmsException(HttpStatus.BAD_REQUEST,
					"Transfer Order not found for the provided Transfer Order Number");
		}
		return transferOrder;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public TransferOrder importTransferOrdersFromCsv(File file) throws IOException, WmsException {
		List<String[]> lines = csvToStringList(file);
		log.info("process file from directory : " + file);
		return populatingDataInDataBase(lines);
	}

	private List<String[]> csvToStringList(File file) throws IOException {
		@SuppressWarnings("resource")
		CSVReader reader = new CSVReader(new FileReader(file), ',');
		List<String[]> csvEntries = reader.readAll();
		return csvEntries;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public TransferOrder importTransferOrdersFromLines (List<String[]> lines) throws IOException, WmsException {
		return populatingDataInDataBase(lines);
	}


	private TransferOrder populatingDataInDataBase(List<String[]> lines) throws WmsException {
		log.info("inside  populatingDataInDataBase method");
		Map<String, Integer> transferOrderMap = new HashMap<String, Integer>();
		TransferOrder transferOrder = null;
		TransferOrderPackslip transferOrderPackslip = null;
		Map<String, Product> productMap = new HashMap<String, Product>();
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
		for (int i = 3; i < lines.size(); i++) {
			/*
			 * TransNO,StockINDate,RAMCO_Order_No,RAMCO_Packslip_No,ItemCode,ItemeDecription
			 * ,Batch,NETweight,grossWeight,CBUID,PalletBarcode
			 * STNC/000409/1718,21/03/2018,STOC/001630/1718,PKSC/019559/1718,H002793,BHUJIA
			 * 20*10.08,H07BB170318A,10.080,12.079,H0027934BD078CX00335,
			 */
			String dataRow = lines.get(i)[0];
			//String[] tokens = dataRow.split(",");
			String transferOrderNumber = lines.get(i)[0];
			if (transferOrder == null) {
				TransferOrder savedTransferOrderNumber = transferOrderRepository.findByTrasferOrderNumber(transferOrderNumber);
				if(savedTransferOrderNumber != null) {
					log.info("This Stock In File for Transfer Order Number " + transferOrderNumber  + " is already processed. Skipping the File Processing...");
					throw new WmsException(HttpStatus.ALREADY_REPORTED,
							"This Stock In File for Transfer Order Number " + transferOrderNumber  + " is already processed. Skipping the File Processing...");
				}
				transferOrder = new TransferOrder();
				transferOrder.setTrasferOrderNumber(transferOrderNumber);
				DateTime stockInDate = formatter.parseDateTime(lines.get(i)[1]);
				transferOrder.setTransferOrderDate(stockInDate.toDate());
				transferOrder.setStatus(OrderStatus.DISPATCHED);
				transferOrder.setRamcoOrderNumber(lines.get(i)[2]);
			}
			if (transferOrderPackslip == null) {
				transferOrderPackslip = TransferOrderPackslip.builder().packslipNumber(lines.get(i)[3])
						.packslipDate(formatter.parseDateTime(lines.get(i)[1]).toDate()).orderStatus(OrderStatus.DONE)
						.doneCases(1).reservedCases(1).packslipQuantity(1D).pendingCases(0)
						.unitOfMeasure(UnitsOfMeasurement.CARTONS).build();
			} else {
				transferOrderPackslip.setPackslipQuantity(transferOrderPackslip.getPackslipQuantity() + 1);
				transferOrderPackslip.setDoneCases(transferOrderPackslip.getDoneCases() + 1);
				transferOrderPackslip.setReservedCases(transferOrderPackslip.getReservedCases() + 1);
			}

			Product product = null;
			String sku = lines.get(i)[4];
			if (productMap.containsKey(sku)) {
				product = productMap.get(sku);
			} else {
				product = productService.findBySkuIgnoreCase(sku);
				productMap.put(sku, product);
			}

			if (!transferOrderMap.containsKey(sku)) {
				transferOrderMap.put(sku, 1);
			} else {
				transferOrderMap.put(sku, transferOrderMap.get(sku) + 1);
			}
		}

		List<TransferOrderDetails> transferOrderDetailsList = new ArrayList<TransferOrderDetails>();
		List<TransferOrderPackslipItem> transferOrderPackslipItemList = new ArrayList<TransferOrderPackslipItem>();
		List<Product> productsToSave = new ArrayList<Product>();
		for (Map.Entry<String, Integer> entry : transferOrderMap.entrySet()) {
			Product product = productMap.get(entry.getKey());
			TransferOrderDetails transferOrderDetails = TransferOrderDetails.builder()
					.product(product).transferOrder(transferOrder)
					.unitsOfMeasure(UnitsOfMeasurement.CARTONS).transferQuantity(new Double(entry.getValue())).build();

			TransferOrderPackslipItem transferOrderPackslipItem = TransferOrderPackslipItem.builder()
					.transferOrderPackslip(transferOrderPackslip).product(product)
					.packslipQuantity(new Double(entry.getValue())).reservedCases(entry.getValue())
					.doneCases(entry.getValue()).pendingCases(0).build();

			transferOrderDetailsList.add(transferOrderDetails);
			transferOrderPackslipItemList.add(transferOrderPackslipItem);
			
			product.setQuantityInHand(product.getQuantityInHand() + entry.getValue());
			productsToSave.add(product);
		}
		transferOrder.setTransferOrderDetails(transferOrderDetailsList);
		transferOrder = save(transferOrder);
		transferOrderPackslip.setTransferOrder(transferOrder);
		transferOrderPackslip.setTransferOrderPackslipItem(transferOrderPackslipItemList);
		
		
		// Second Iteration to create the TransferOrderPackSlipDispatchDetails
		List<Inventory> inventoryList = new ArrayList<Inventory>();
		List<TransferOrderPackSlipDispatchDetails> transferOrderPackSlipDispatchDetailsList = new ArrayList<TransferOrderPackSlipDispatchDetails>();
		for (int i = 3; i < lines.size(); i++) {
			String dataRow = lines.get(i)[0];
//			String[] tokens = dataRow.split(",");
			String sku = lines.get(i)[4];
			Date stockInDate = formatter.parseDateTime(lines.get(i)[1]).toDate();
			TransferOrderPackSlipDispatchDetails transferOrderPackSlipDispatchDetails = TransferOrderPackSlipDispatchDetails
					.builder().batchNumber(lines.get(i)[6]).scanBarcode(lines.get(i)[9])
					.scanDate(stockInDate).status(OrderStatus.DONE).product(productMap.get(sku))
					.netWeight(Double.valueOf(lines.get(i)[7])).grossWeight(Double.valueOf(lines.get(i)[8]))
					.transferOrder(transferOrder).transferOrderPackslip(transferOrderPackslip).build();
			
			String palletBarcode = lines.get(i)[10];
			transferOrderPackSlipDispatchDetails.setPalletBarcode(palletBarcode);
			transferOrderPackSlipDispatchDetailsList.add(transferOrderPackSlipDispatchDetails);
			
			Inventory inventory = Inventory.builder().batchNumber(lines.get(i)[6])
					.inventoryDate(stockInDate).orderStatus(OrderStatus.AVAILABLE)
					.palletBarcode(palletBarcode).product(productMap.get(sku)).quantity(1D).scanBarcode(lines.get(i)[9])
					.unitOfMeasure(UnitsOfMeasurement.CARTONS).netWeight(Double.valueOf(lines.get(i)[7])).grossWeight(Double.valueOf(lines.get(i)[8])).build();
			inventory.setPalletBarcode(lines.get(i)[10]);
			inventoryList.add(inventory);
		}
		
		inventoryService.save(inventoryList);
		productService.save(productsToSave);
		transferOrderPackslip.setTransferOrderPackSlipDispatchDetails(transferOrderPackSlipDispatchDetailsList);
		transferOrderPackslipService.save(transferOrderPackslip);
		return transferOrder;
	}


	public TransferOrder importManualTransferOrder(List<String[]> lines) throws WmsException {
		log.info("inside  importManualTransferOrder method");
		Map<String, Integer> transferOrderMap = new HashMap<String, Integer>();
		TransferOrder transferOrder = null;
		TransferOrderPackslip transferOrderPackslip = null;
		Map<String, Product> productMap = new HashMap<String, Product>();
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
		for (int i = 3; i < lines.size(); i++) {
			/*
			 * TransNO,StockINDate,RAMCO_Order_No,RAMCO_Packslip_No,ItemCode,ItemeDecription
			 * ,Batch,NETweight,grossWeight,CBUID,PalletBarcode
			 * STNC/000409/1718,21/03/2018,STOC/001630/1718,PKSC/019559/1718,H002793,BHUJIA
			 * 20*10.08,H07BB170318A,10.080,12.079,H0027934BD078CX00335,
			 */
			String dataRow = lines.get(i)[0];
			//String[] tokens = dataRow.split(",");
			String transferOrderNumber = lines.get(i)[0];
			if (transferOrder == null) {
				TransferOrder savedTransferOrderNumber = transferOrderRepository.findByTrasferOrderNumber(transferOrderNumber);
				if(savedTransferOrderNumber != null) {
					log.info("This Stock In File for Transfer Order Number " + transferOrderNumber  + " is already processed. Skipping the File Processing...");
					throw new WmsException(HttpStatus.ALREADY_REPORTED,
							"This Stock In File for Transfer Order Number " + transferOrderNumber  + " is already processed. Skipping the File Processing...");
				}
				transferOrder = new TransferOrder();
				transferOrder.setTrasferOrderNumber(transferOrderNumber);
				DateTime stockInDate = formatter.parseDateTime(lines.get(i)[1]);
				transferOrder.setTransferOrderDate(stockInDate.toDate());
				transferOrder.setStatus(OrderStatus.DISPATCHED);
				transferOrder.setRamcoOrderNumber(lines.get(i)[2]);
			}
			if (transferOrderPackslip == null) {
				transferOrderPackslip = TransferOrderPackslip.builder().packslipNumber(lines.get(i)[3])
						.packslipDate(formatter.parseDateTime(lines.get(i)[1]).toDate()).orderStatus(OrderStatus.DONE)
						.doneCases(1).reservedCases(1).packslipQuantity(1D).pendingCases(0) // done and reserved cases should be read from quantityInCB column
						.unitOfMeasure(UnitsOfMeasurement.CARTONS).build();
			} else {
				transferOrderPackslip.setPackslipQuantity(transferOrderPackslip.getPackslipQuantity() + 1);
				transferOrderPackslip.setDoneCases(transferOrderPackslip.getDoneCases() + 1);
				transferOrderPackslip.setReservedCases(transferOrderPackslip.getReservedCases() + 1);
			}


			String sku = lines.get(i)[4];
			Product product = productMap.get(sku);
			if (product == null) {
				product = productService.findBySkuIgnoreCase(sku);
				productMap.put(sku, product);
			}

			if (!transferOrderMap.containsKey(sku)) {
				transferOrderMap.put(sku, Integer.parseInt(lines.get(i)[8]));
			} else {
				transferOrderMap.put(sku, transferOrderMap.get(sku) + Integer.parseInt(lines.get(i)[8]));
			}
		}

		List<TransferOrderDetails> transferOrderDetailsList = new ArrayList<TransferOrderDetails>();
		List<TransferOrderPackslipItem> transferOrderPackslipItemList = new ArrayList<TransferOrderPackslipItem>();
		List<Product> productsToSave = new ArrayList<Product>();
		for (Map.Entry<String, Integer> entry : transferOrderMap.entrySet()) {
			Product product = productMap.get(entry.getKey());
			TransferOrderDetails transferOrderDetails = TransferOrderDetails.builder()
					.product(product).transferOrder(transferOrder)
					.unitsOfMeasure(UnitsOfMeasurement.CARTONS).transferQuantity(new Double(entry.getValue())).build();

			TransferOrderPackslipItem transferOrderPackslipItem = TransferOrderPackslipItem.builder()
					.transferOrderPackslip(transferOrderPackslip).product(product)
					.packslipQuantity(new Double(entry.getValue())).reservedCases(entry.getValue())
					.doneCases(entry.getValue()).pendingCases(0).build();

			transferOrderDetailsList.add(transferOrderDetails);
			transferOrderPackslipItemList.add(transferOrderPackslipItem);

			product.setQuantityInHand(product.getQuantityInHand() + entry.getValue());
			productsToSave.add(product);
		}
		transferOrder.setTransferOrderDetails(transferOrderDetailsList);
		transferOrder = save(transferOrder);
		transferOrderPackslip.setTransferOrder(transferOrder);
		transferOrderPackslip.setTransferOrderPackslipItem(transferOrderPackslipItemList);


		// Second Iteration to create the TransferOrderPackSlipDispatchDetails
		List<Inventory> inventoryList = new ArrayList<Inventory>();
		List<TransferOrderPackSlipDispatchDetails> transferOrderPackSlipDispatchDetailsList = new ArrayList<TransferOrderPackSlipDispatchDetails>();
		try {
			for (int i = 3; i < lines.size(); i++) {

				String sku = lines.get(i)[4];
				Date stockInDate = formatter.parseDateTime(lines.get(i)[1]).toDate();
				TransferOrderPackSlipDispatchDetails transferOrderPackSlipDispatchDetails = TransferOrderPackSlipDispatchDetails
						.builder().batchNumber(lines.get(i)[6]).scanBarcode(lines.get(i)[9])
						.scanDate(stockInDate).status(OrderStatus.DONE).product(productMap.get(sku))
						.netWeight(Double.valueOf(lines.get(i)[7])).grossWeight(Double.valueOf(lines.get(i)[8]))
						.transferOrder(transferOrder).transferOrderPackslip(transferOrderPackslip).build();

//			String palletBarcode = lines.get(i)[10];
//			transferOrderPackSlipDispatchDetails.setPalletBarcode(palletBarcode);
				transferOrderPackSlipDispatchDetailsList.add(transferOrderPackSlipDispatchDetails);

				Inventory inventory = Inventory.builder().batchNumber(lines.get(i)[6])
						.inventoryDate(stockInDate).orderStatus(OrderStatus.AVAILABLE)
						// .palletBarcode(palletBarcode)
						.product(productMap.get(sku)).quantity(1D).scanBarcode(lines.get(i)[9])
						.unitOfMeasure(UnitsOfMeasurement.CARTONS).netWeight(Double.valueOf(lines.get(i)[7])).grossWeight(Double.valueOf(lines.get(i)[8])).build();
//			inventory.setPalletBarcode(lines.get(i)[10]);
				inventoryList.add(inventory);
			}
		}catch (Exception e ){
			e.printStackTrace();
			throw new WmsException(e.getMessage());
		}

		inventoryService.save(inventoryList);
		productService.save(productsToSave);
		transferOrderPackslip.setTransferOrderPackSlipDispatchDetails(transferOrderPackSlipDispatchDetailsList);
		transferOrderPackslipService.save(transferOrderPackslip);
		return transferOrder;
	}
}
