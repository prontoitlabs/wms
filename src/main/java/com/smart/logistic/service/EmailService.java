package com.smart.logistic.service;

import com.smart.logistic.dto.EmailDto;

public interface EmailService {
	/**
	 * 
	 * @param email
	 * @return successfull message
	 */
	String send(EmailDto email);

}
