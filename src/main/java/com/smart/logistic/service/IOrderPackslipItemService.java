package com.smart.logistic.service;

import com.smart.logistic.domain.SalesOrderPackslipItem;
import com.smart.logistic.dto.SalesOrderPackslipDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.generic.IGenericService;

public interface IOrderPackslipItemService extends IGenericService<SalesOrderPackslipItem> {

	SalesOrderPackslipDto getOrderPackslipDetailById(Long id) throws WmsException;

}
