package com.smart.logistic.service;

import com.smart.logistic.domain.TransferOrderPackslipItem;
import com.smart.logistic.dto.TransferOrderPackslipDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.generic.IGenericService;

public interface ITransferOrderPackslipItemService extends IGenericService<TransferOrderPackslipItem> {

	TransferOrderPackslipDto getTransferOrderPackslipDetailById(Long id) throws WmsException;

}
