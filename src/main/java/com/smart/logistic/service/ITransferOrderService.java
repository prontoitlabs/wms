package com.smart.logistic.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.data.domain.Page;

import com.smart.logistic.domain.TransferOrder;
import com.smart.logistic.dto.TransferOrderDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.generic.IGenericService;

public interface ITransferOrderService extends IGenericService<TransferOrder> {

	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @return Page<TransferOrder>
	 * @throws WmsException
	 */
	Page<TransferOrder> getListOfTransferOrder(Integer pageNumber, Integer pageSize) throws WmsException;

	/**
	 * 
	 * @param id
	 * @return TransferOrderDto
	 */
	TransferOrderDto getTransferOrderById(Long id) throws WmsException;

	String sendDispatchPlanforPendingTransferOrders() throws WmsException;

	TransferOrder findTransferOrderByOrderNumber(String transferOrderNumber) throws WmsException;

	TransferOrder importTransferOrdersFromCsv(File file) throws IOException, WmsException;

	TransferOrder importTransferOrdersFromLines(List<String[]> lines) throws IOException, WmsException;

	TransferOrder importManualTransferOrder(List<String[]> lines) throws WmsException ;

}
