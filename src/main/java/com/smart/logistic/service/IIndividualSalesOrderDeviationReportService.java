package com.smart.logistic.service;

import java.util.Date;
import java.util.List;

import com.smart.logistic.domain.IndividualSalesOrderDeviationReport;
import com.smart.logistic.service.generic.IGenericService;

public interface IIndividualSalesOrderDeviationReportService extends IGenericService<IndividualSalesOrderDeviationReport>{

	List<IndividualSalesOrderDeviationReport> findByDateCreatedBetween(Date startDate, Date endDate);

	IndividualSalesOrderDeviationReport findTopDeviationReportByMaxDate();

	List<IndividualSalesOrderDeviationReport> findByGeneratedDateTime(Date generatedDateTime);
	
}
