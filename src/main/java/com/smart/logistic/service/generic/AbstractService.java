package com.smart.logistic.service.generic;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.smart.logistic.domain.AbstractEntity;
import com.smart.logistic.dto.EmailDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.EmailService;

public abstract class AbstractService<T extends AbstractEntity> implements IGenericService<T> {

	@Autowired
	private JpaRepository<T, Long> jpaRepository;

	@Autowired
	private EmailService emailService;
	
	@Value(value = "${email.to}")
	private String emailTo;

	@Override
	public T findOne(Long id) throws WmsException {
		T entity = jpaRepository.findOne(id);
		if (entity == null) {
			throw notFoundException();
		}
		return entity;
	}

	@Override
	public List<T> findAll() {
		return jpaRepository.findAll();
	}

	@Override
	public Page<T> findAll(Pageable page) {
		return jpaRepository.findAll(page);
	}

	@Override
	public void delete(Long id) {
		jpaRepository.delete(id);
	}

	@Override
	public T save(T domain) {
		return jpaRepository.save(domain);
	}

	@Override
	public List<T> save(List<T> domains) {
		return jpaRepository.save(domains);
	}

	@Override
	public T update(Long id, T domain) throws WmsException {
		T fromDb = findOne(id);
		domain.copyEntityFrom(fromDb);
		return save(domain);
	}

	public void sendAttachment(String csvFileName) {
		EmailDto emailDto = EmailDto.builder().to(emailTo).file(new File(csvFileName))
				.subject("WMS Sales Order Deviation Report").build();
		emailService.send(emailDto);
	}

	protected abstract WmsException notFoundException();
}
