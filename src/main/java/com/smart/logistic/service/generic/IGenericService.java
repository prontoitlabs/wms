package com.smart.logistic.service.generic;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.smart.logistic.domain.AbstractEntity;
import com.smart.logistic.exception.WmsException;

/**
 * This interface abstracts out the CRUD services for any domain.
 * 
 * @param <T>
 *            specifies the Domain object it is working on. It assumes a Long
 *            type of ID.
 */
public interface IGenericService<T extends AbstractEntity> {

	T findOne(Long id) throws WmsException;

	T save(T domain);

	List<T> save(List<T> domains);

	Page<T> findAll(Pageable page);

	List<T> findAll();

	void delete(Long id);

	T update(Long id, T domain) throws WmsException;

}
