package com.smart.logistic.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.smart.logistic.domain.Inventory;
import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.dto.InventoryDto;
import com.smart.logistic.dto.InventoryStatusDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.generic.IGenericService;

public interface IInventoryService extends IGenericService<Inventory> {

	Inventory findByProductId(Long productId);

	Inventory findByProductSku(String productSku);

	InventoryDto saveInventory(InventoryDto inventoryDto) throws WmsException;

	InventoryDto updateInventory(Long inventoryId, InventoryDto inventoryDto) throws WmsException;

    Page<Inventory> fidAllPaginatedProdct(Integer pageNumber, Integer pageSize) throws WmsException;

	List<InventoryStatusDto> findIvetoryByOrderStatus() throws WmsException;

	Page<Inventory> findInventoryByOrderStatusAndProduct(OrderStatus orderStatus, Product product, Pageable page);
	
	void updateInventoryOrderStatusAsReservedById(Long id);

	void updateInventoryOrderStatusAsReadyById(Long inventoryId);

	Inventory findByCartonBarCodeAndStatus(String scanBarcode, OrderStatus orderStatus);

	List<Object[]> findTOPNDistinctPalletBarCodesByStatus(OrderStatus status, Product product, int numberOfPallets);

	List<Inventory> findInventoryByPalletBarCodes(List<String> palletBarCodeList);
	
	Long countByProduct_IdAndOrderStatus(Long id, OrderStatus orderStatus);

    void updateBarCode(String oldBarCode, String newBarCode) throws WmsException;

	public List<Object[]> findInventoryCreatedToday ();

	public String countInventoryCreatedToday ();
}
