package com.smart.logistic.service;

import com.smart.logistic.domain.TransportMaster;
import com.smart.logistic.service.generic.IGenericService;

public interface ITransportMasterService extends IGenericService<TransportMaster> {

}
