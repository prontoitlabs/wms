package com.smart.logistic.service;

import java.util.Date;
import java.util.List;

import com.smart.logistic.domain.IndentReport;
import com.smart.logistic.service.generic.IGenericService;

public interface IIndentReportService extends IGenericService<IndentReport>{

	List<IndentReport>	findByDateCreatedBetween(Date startDate,Date endDate);

	IndentReport findTopByOrderByFromDateDesc();

	List<IndentReport> findByFromDate(Date fromDate);	
	
	IndentReport findTopByOrderByTillDateDesc();
}
