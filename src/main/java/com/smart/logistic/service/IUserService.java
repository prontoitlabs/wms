package com.smart.logistic.service;

import com.smart.logistic.domain.User;
import com.smart.logistic.service.generic.IGenericService;

public interface IUserService extends IGenericService<User> {
	/**
	 * 
	 * @param email
	 * @return User
	 */
	User findByEmail(String email);

}
