package com.smart.logistic.service;

import java.util.List;

import com.smart.logistic.domain.SalesOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.dto.SalesOrderPackSlipDispatchDetailsDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.generic.IGenericService;

public interface IPackslipDispatchDetailsService extends IGenericService<SalesOrderPackSlipDispatchDetails> {

	/**
	 * 
	 * @param packSlipNumber
	 * @param sku
	 * @return List<PackslipDispatchDetailsDto>
	 */
	List<SalesOrderPackSlipDispatchDetailsDto> getPackslipDispatchDetailByPackSlipNumberAndSku(String packSlipNumber,
			String sku);

	List<SalesOrderPackSlipDispatchDetailsDto> confirmDispatchDetails(List<Long> ids, OrderStatus status)
			throws WmsException;

	/**
	 * 
	 * @param packslipId
	 * @param status
	 * @return List<PackSlipDispatchDetails>
	 */
	List<SalesOrderPackSlipDispatchDetails> findAllPackslipDispatchDetailsByPackslipIdAndStatus(Long packslipId,
			OrderStatus status);

	/**
	 * 
	 * @param packslipId
	 * @return List<PackSlipDispatchDetails>
	 */
	List<SalesOrderPackSlipDispatchDetails> findAllPackslipDispatchDetailsByPackslipId(Long id);

}
