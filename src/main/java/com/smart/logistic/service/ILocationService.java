package com.smart.logistic.service;

import org.springframework.data.domain.Page;

import com.smart.logistic.domain.Location;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.generic.IGenericService;

public interface ILocationService extends IGenericService<Location>{

	
	Page<Location> findingAllLocation(Integer pageNumber, Integer pageSize) throws WmsException;
	
}
