package com.smart.logistic.service;


import com.smart.logistic.domain.User;
import com.smart.logistic.dto.AuthDto;
import com.smart.logistic.dto.LoginDto;
import com.smart.logistic.exception.UserNotAuthenticatedException;
import com.smart.logistic.exception.WmsException;

public interface IAuthService {

	/**
	 * 
	 * @param loginDto
	 * @return
	 * @throws UserNotAuthenticatedException
	 */
	AuthDto login(LoginDto loginDto) throws UserNotAuthenticatedException;

	  /**
	   * 
	   * @param user
	   * @param token
	   * @throws WmsException
	   */
	  void logout(User user, String token) throws WmsException;
	
	
}
