package com.smart.logistic.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.core.io.FileSystemResource;

import com.smart.logistic.domain.DeviationReport;
import com.smart.logistic.domain.IndentReport;
import com.smart.logistic.domain.IndividualSalesOrderDeviationReport;
import com.smart.logistic.exception.WmsException;

public interface IReportService {

	public List<DeviationReport> getdeviationReportForLossLeader() throws WmsException;

	public List<IndividualSalesOrderDeviationReport> getDeviationReportForIndividualSalesOrder() throws WmsException;

	public List<IndentReport> getIndentReport();

	public List<IndentReport> saveIndentReport();

	public List<IndividualSalesOrderDeviationReport> saveDeviationReportForIndividualSalesOrder() throws WmsException;

	/**
	 * 
	 * @param response
	 * @param csvFileName
	 * @return csv file contain indent report
	 * @throws WmsException
	 * @throws IOException
	 */
	public FileSystemResource exportIndentReport(HttpServletResponse response, String csvFileName)
			throws WmsException, IOException;
}
