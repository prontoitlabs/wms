package com.smart.logistic.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.smart.logistic.domain.TransferOrderPackslip;
import com.smart.logistic.dto.TransferOrderPackslipDto;
import com.smart.logistic.dto.TransferOrderReceiptDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.generic.IGenericService;


public interface ITransferOrderPackslipService extends IGenericService<TransferOrderPackslip>{

	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @return Page<TransferOrderPackslip>
	 * @throws WmsException
	 */
	
	Page<TransferOrderPackslip> getAllTransferOrderPackingSlips(Integer pageNumber, Integer pageSize) throws WmsException;
	
	/**
	 * 
	 * @param transferOrderId
	 * @return List<TransferOrderPackslipDto>
	 */
	List<TransferOrderPackslipDto> getTransferOrderPackslipsByTransferOrderId(Long transferOrderId);

	/**
	 * 
	 * @param packslipId
	 * @return TransferOrderPackslip
	 * @throws WmsException
	 */
	TransferOrderPackslip validatePackslip(Long packslipId) throws WmsException;

	/**
	 * 
	 * @param transferOrderId
	 * @return List<TransferOrderPackslip>
	 * @throws WmsException
	 */
	List<TransferOrderPackslip> validateTransferOrderPackSlips(Long transferOrderId) throws WmsException;

	/**
	 * 
	 * @param transferOrderId
	 * @return List<TransferOrderPackslip>
	 * @throws WmsException 
	 */
	TransferOrderReceiptDto getPackslipsOfTransferOrderByTransferOrderId(Long transferOrderId) throws WmsException;

}
