package com.smart.logistic.service;

import java.util.List;

import com.smart.logistic.domain.SalesOrderDetails;
import com.smart.logistic.service.generic.IGenericService;

public interface ISalesOrderDetailsService extends IGenericService<SalesOrderDetails> {

	List<SalesOrderDetails> findBySalesOrderIdAndProductId(Long salesOrderId, Long productId);
	
	List<SalesOrderDetails> findByIdIn(List<Long> salesOrderIds);

}
