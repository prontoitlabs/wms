package com.smart.logistic.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.smart.logistic.domain.Product;
import com.smart.logistic.dto.ProductDto;
import com.smart.logistic.dto.ProductStatusOnDashBoardDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.generic.IGenericService;

public interface IProductService extends IGenericService<Product> {

	ProductDto saveProduct(ProductDto productDto) throws WmsException;

	/**
	 * 
	 * @param pageNumber,
	 *            pageSize
	 * @param pageSize
	 * @return Page<Product>
	 * @throws WmsException
	 */
	Page<Product> getAllProduct(Integer pageNumber, Integer pageSize) throws WmsException;

	/**
	 * 
	 * @param sku
	 * @return Product based on sku
	 */
	Product findBySkuIgnoreCase(String sku);

	List<ProductStatusOnDashBoardDto> findItemAndQuantityInHand();

	List<Product> importItemMaster() throws WmsException;

	List<Product> findByIdIn(List<Long> ids);
	
	
	Page<Product> findTopTenProduct() throws WmsException;

	List<Product> importAllItemsFromRamco() throws WmsException;

	void reconcileProductQuantityInHand();

}
