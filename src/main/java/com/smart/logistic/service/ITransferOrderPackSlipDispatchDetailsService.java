package com.smart.logistic.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;

import com.smart.logistic.domain.TransferOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.dto.TransferOrderPackSlipDispatchDetailsDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.generic.IGenericService;

public interface ITransferOrderPackSlipDispatchDetailsService
		extends IGenericService<TransferOrderPackSlipDispatchDetails> {


	List<TransferOrderPackSlipDispatchDetailsDto> confirmListOfPackSlipDispatchDetailAsDone(List<Long> ids,
			OrderStatus status) throws WmsException;

	List<TransferOrderPackSlipDispatchDetails> findAllPackslipDispatchDetailsByPackslipIdAndStatus(Long packslipId,
			OrderStatus status);

	/**
	 * 
	 * @param transferOrderId
	 * @param pageSize
	 * @param pageNumber
	 * @return List<TransferOrderPackSlipDispatchDetails> paginated
	 * @throws WmsException
	 */
	Page<TransferOrderPackSlipDispatchDetails> getTransferOrderPackslipDispatchDetailsById(Long transferOrderId,
			Integer pageNumber, Integer pageSize) throws WmsException;

	/**
	 * 
	 * @param transferOrderId
	 * @return List<TransferOrderPackSlipDispatchDetails> not paginated
	 */
	List<TransferOrderPackSlipDispatchDetails> getTransferOrderPackslipDispatchDetailsById(Long transferOrderId);

	/**
	 * 
	 * @param response
	 * @param transferOrderId
	 * @param csvFileName
	 * @return csv file
	 * @throws IOException
	 */
	FileSystemResource createCsvFileByTransferOrderId(HttpServletResponse response, Long transferOrderId,
			String csvFileName) throws IOException;

}
