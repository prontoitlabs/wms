package com.smart.logistic.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.smart.logistic.dto.*;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;

import com.smart.logistic.domain.SalesOrder;
import com.smart.logistic.domain.SalesOrderDetails;
import com.smart.logistic.domain.enums.OrderFulfillmentType;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.generic.IGenericService;

public interface ISalesOrderService extends IGenericService<SalesOrder> {

	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @return Page<SalesOrder>
	 * @throws WmsException
	 */
	Page<SalesOrder> getListOfSalesOrder(Integer pageNumber, Integer pageSize) throws WmsException;

	/**
	 * 
	 * @return List<String> DistinctProductDto contains distinct sku and product
	 *         name
	 * @throws WmsException
	 */
	List<DistinctProductDto> findDistinctSkuAndProductBySalesOrderId(Long salesOrderId) throws WmsException;

	List<SalesOrderDto> findSalesOrderDeliveryDetail(List<Long> ids) throws WmsException;

	Page<SalesOrder> getListOfDeliveryOrders(Integer pageNumber, Integer pageSize) throws WmsException;

	List<SalesOrder> getListOfDeliveryOrders() throws WmsException;

	SalesOrderDto addSalesOrderToDispatch(Long id, OrderFulfillmentType orderFulfillmentType) throws WmsException;

	SalesOrderDto removeSalesOrderFromDispatch(Long id) throws WmsException;

	List<SalesOrder> getAllDeliveryOrdersByCustomerCode(String customerCode) throws WmsException;

	SalesOrder findSalesOrderByNumber(String salesOrderNumber) throws WmsException;

	List<SalesOrder> importAllOpenSalesOrdersFromRamco() throws WmsException;

	String sendDeliveryOrdersForAllocation(DeliveryDetailsRequestDto deliveryDetailsRequestDto) throws WmsException;

	/**
	 *
	 * @param response
	 * @param salesOrderPackslipId
	 * @param csvFileName
	 * @return
	 * @throws IOException
	 * @throws WmsException
	 */
	FileSystemResource createCsvFileBySalesOrderPackSlipId(HttpServletResponse response, Long salesOrderPackslipId,
			String csvFileName) throws IOException, WmsException;

	Page<SalesOrder> findTopTenSalesOrder() throws WmsException;

	List<SalesOrder> findListOfSalesOrderWhichIsNotDispatched(OrderStatus status);

	List<SalesOrderDetails> findSalesOrderDetailByProductAndSalesOrder(Long productId, Long salesOrderId)
			throws WmsException;

	FileSystemResource exportSalesOrderDeviationReport(HttpServletResponse response, String csvFileName)
			throws WmsException, IOException;

	FileSystemResource exportSalesOrderIdDetails(HttpServletResponse response, String csvFileName, Long salesOrderId)
			throws WmsException, IOException;

	SalesOrderDto addSalesOrderForDispatch(List<SalesOrderDetailsDto> salesOrderDetailsDto, Long id,
			OrderFulfillmentType orderFulfillmentType) throws WmsException;

	SalesOrder releaseReservedProductFromSalesOrderDetails(Long salesOrderId) throws WmsException;

	SalesOrder updateSalesOrderStatus(UpdateStatusDto updateStatusDto) throws WmsException;

	Page<SalesOrder> getAllFulfilledSalesOrders(Integer pageNumber, Integer pageSize) throws WmsException;

	Page<SalesOrder> getAllPendingSalesOrders(Integer pageNumber, Integer pageSize) throws WmsException;

	Page<SalesOrder> getAllPartiallyFulfilledSalesOrders(Integer pageNumber, Integer pageSize) throws WmsException;
	
	List<SalesOrder> getListOfShippedOrders() throws WmsException;

	Long getCountOfPendingOrders () throws WmsException;

	List <OrderStatusDto> getOrderStatusToday () throws WmsException;

}
