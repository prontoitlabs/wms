package com.smart.logistic.repository;

import java.util.Date;
import java.util.List;

import com.smart.logistic.domain.SalesOrderPackslip;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.repository.generic.GenericRepository;

public interface IOrderPackslipRepository extends GenericRepository<SalesOrderPackslip> {

	SalesOrderPackslip findByPackslipNumber(String packslipNumber);

	List<SalesOrderPackslip> findByLastModifiedBetweenAndOrderStatus(Date startDate,Date endDate,OrderStatus orderStatus);
	
}
