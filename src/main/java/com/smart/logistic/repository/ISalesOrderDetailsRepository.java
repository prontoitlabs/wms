package com.smart.logistic.repository;

import java.util.List;

import com.smart.logistic.domain.SalesOrderDetails;
import com.smart.logistic.repository.generic.GenericRepository;

public interface ISalesOrderDetailsRepository extends GenericRepository<SalesOrderDetails> {
	List<SalesOrderDetails> findBySalesOrder_IdAndProduct_Id(Long salesOrderId, Long productId);
	
	List<SalesOrderDetails> findByIdIn(List<Long> salesOrderIds);
}
