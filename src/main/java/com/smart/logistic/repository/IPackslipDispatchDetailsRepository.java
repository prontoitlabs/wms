package com.smart.logistic.repository;

import java.util.List;

import com.smart.logistic.domain.SalesOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.repository.generic.GenericRepository;

public interface IPackslipDispatchDetailsRepository extends GenericRepository<SalesOrderPackSlipDispatchDetails> {

	List<SalesOrderPackSlipDispatchDetails> findAllByOrderPackslipItem_OrderPackslip_PackslipNumberAndProduct_Sku(
			String packslipNumber, String sku);

	List<SalesOrderPackSlipDispatchDetails> findAllByOrderPackslipItem_OrderPackslip_IdAndStatus(Long packslipId,
			OrderStatus status);

	List<SalesOrderPackSlipDispatchDetails> findAllByOrderPackslipItem_OrderPackslip_Id(Long id);

}
