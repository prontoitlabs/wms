package com.smart.logistic.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.smart.logistic.domain.Inventory;
import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.repository.generic.GenericRepository;

public interface IInventoryRepository extends GenericRepository<Inventory> {

	Inventory findByProduct_Id(Long productId);

	Inventory findByProduct_Sku(String sku);

	Page<Inventory> findAll(Pageable page);

	@Query(value = " Select order_status,Count(*) from inventory GROUP BY  order_status", nativeQuery = true)
	List<Object[]> findIvetoryByOrderStatus();

	@Query(value = "Select product_id, Count(*) from inventory where date_created > current_date - 1 GROUP BY product_id ", nativeQuery = true)
	List<Object[]> findInventoryByDateCreated();

	@Query(value = "Select Count(*) from inventory where date_created > current_date - 1", nativeQuery = true)
	Object countInventoryByDateCreated();

	Page<Inventory> findAllByOrderStatusAndProduct(OrderStatus orderStatus, Product product, Pageable page);

    @Modifying
    @Query(value = "update inventory set scan_barcode = ?2 where scan_barcode = ?1", nativeQuery = true)
    void updateBarCodeForInventory(String oldBarCode, String newBarCode);

	@Modifying
	@Query(value = "update inventory i set i.order_status = 'RESERVED' where i.id = ?1", nativeQuery = true)
	void updateInventoryOrderStatusAsReservedById(Long id);

	@Modifying
	@Query(value = "update inventory i set i.order_status = 'READY' where i.id = ?1", nativeQuery = true)
	void updateInventoryOrderStatusAsReadyById(Long id);

	Inventory findByScanBarcodeAndOrderStatus(String scanBarcode, OrderStatus available);

	@Query(value = "SELECT DISTINCT  a.pallet_barcode, a.inventory_date from inventory a where a.order_status = 'AVAILABLE' AND a.product_id = ?1  ORDER BY inventory_date ASC LIMIT ?2", nativeQuery = true)
	List<Object[]> findTOPNDistinctPalletBarCodesByStatus(Long productId, int numberOfPallets);

	List<Inventory> findInventoryByPalletBarcodeIn(List<String> palletBarCodeList);

	Long countByProduct_IdAndOrderStatus(Long id, OrderStatus orderStatus);

}
