package com.smart.logistic.repository.generic;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import com.smart.logistic.domain.AbstractEntity;

@NoRepositoryBean
public interface GenericRepository<T extends AbstractEntity> extends JpaRepository<T, Long>, QueryByExampleExecutor<T> {

	List<T> findAllByActiveTrue();

	Page<T> findAllByActiveTrue(Pageable pageable);

}
