package com.smart.logistic.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.smart.logistic.domain.Customer;
import com.smart.logistic.repository.generic.GenericRepository;

public interface ICustomerRepository extends GenericRepository<Customer>{
	
	Page<Customer> findAll(Pageable page);

	Customer findByCustomerCode(String cust_Code);
}
