package com.smart.logistic.repository;

import java.util.Date;
import java.util.List;

import com.smart.logistic.domain.IndividualSalesOrderDeviationReport;
import com.smart.logistic.repository.generic.GenericRepository;

public interface IIndividualSalesOrderDeviationReportRepository
		extends GenericRepository<IndividualSalesOrderDeviationReport> {

	List<IndividualSalesOrderDeviationReport> findByDateCreatedBetween(Date startDate, Date endDate);

	IndividualSalesOrderDeviationReport findTopByOrderByGeneratedDateTimeDesc();

	List<IndividualSalesOrderDeviationReport> findByGeneratedDateTime(Date generatedDateTime);
}