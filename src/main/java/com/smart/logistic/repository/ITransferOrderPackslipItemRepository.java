package com.smart.logistic.repository;

import com.smart.logistic.domain.TransferOrderPackslipItem;
import com.smart.logistic.repository.generic.GenericRepository;

public interface ITransferOrderPackslipItemRepository extends GenericRepository<TransferOrderPackslipItem> {
	
}
