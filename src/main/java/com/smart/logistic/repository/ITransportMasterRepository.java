package com.smart.logistic.repository;

import com.smart.logistic.domain.TransportMaster;
import com.smart.logistic.repository.generic.GenericRepository;

public interface ITransportMasterRepository extends GenericRepository<TransportMaster>{

}
