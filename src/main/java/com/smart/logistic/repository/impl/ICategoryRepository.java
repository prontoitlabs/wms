package com.smart.logistic.repository.impl;

import com.smart.logistic.domain.Category;
import com.smart.logistic.repository.generic.GenericRepository;

public interface ICategoryRepository extends GenericRepository<Category>{

}
