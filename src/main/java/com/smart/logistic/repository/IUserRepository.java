package com.smart.logistic.repository;

import com.smart.logistic.domain.User;
import com.smart.logistic.repository.generic.GenericRepository;

public interface IUserRepository extends GenericRepository<User> {

	User findByEmail(String email);

}
