package com.smart.logistic.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.smart.logistic.domain.Product;
import com.smart.logistic.repository.generic.GenericRepository;

public interface IProductRepository extends GenericRepository<Product> {

	Product findBySkuIgnoreCase(String sku);

	Page<Product> findAll(Pageable page);
	
	@Query(value = "Select distinct sku from  product where quantity_in_hand > 0", nativeQuery = true)
	List<String>  findDistinctBySku();
	
	List<Product> findByIdIn(List<Long> ids);

}
