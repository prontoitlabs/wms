package com.smart.logistic.repository;

import java.util.List;

import com.smart.logistic.domain.TransferOrder;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.repository.generic.GenericRepository;

public interface ITransferOrderRepository extends GenericRepository<TransferOrder> {
	
	TransferOrder findById(Long id);
	
	List<TransferOrder> findByStatus(OrderStatus status);

	TransferOrder findByTrasferOrderNumber(String transferOrderNumber);
}
