package com.smart.logistic.repository;

import java.util.List;

import com.smart.logistic.domain.TransferOrderPackslip;
import com.smart.logistic.repository.generic.GenericRepository;

public interface ITransferOrderPackslipRepository extends GenericRepository<TransferOrderPackslip> {

	List<TransferOrderPackslip> findByTransferOrder_Id(Long transferOrderId);

	TransferOrderPackslip findByPackslipNumber(String packslipNumber);
}
