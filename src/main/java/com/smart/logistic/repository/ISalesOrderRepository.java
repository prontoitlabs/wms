package com.smart.logistic.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.google.common.collect.ImmutableList;
import com.smart.logistic.domain.SalesOrder;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.RamcoOrderStatus;
import com.smart.logistic.repository.generic.GenericRepository;
import org.springframework.data.jpa.repository.Query;

public interface ISalesOrderRepository extends GenericRepository<SalesOrder> {

	SalesOrder findById(Long id);

	List<SalesOrder> findByStatus(OrderStatus status);

	Page<SalesOrder> findByStatusInAndActiveTrue(ImmutableList<OrderStatus> orderStatuses, Pageable pageable);
	
	List<SalesOrder> findByStatusInAndActiveTrue(ImmutableList<OrderStatus> orderStatuses);
	
	List<SalesOrder> findByStatusInAndCustomer_CustomerCode(ImmutableList<OrderStatus> orderStatuses, String customerCode);

	SalesOrder findByOrderNumber(String salesOrderNumber);
	
	List<SalesOrder> findByStatusNotAndRamcoOrderStatusNot(OrderStatus status, RamcoOrderStatus ramcoOrderStatus);
	
	Page<SalesOrder> findSalesOrderByStatus(OrderStatus status, Pageable pageable);

    Page<SalesOrder> findByStatusOrRamcoOrderStatus(OrderStatus status, RamcoOrderStatus ramcoOrderStatus, Pageable pageable);

	Page<SalesOrder> findSalesOrderByRamcoOrderStatus(RamcoOrderStatus status, Pageable pageable);

    Page<SalesOrder> findByRamcoOrderStatusInAndActiveTrue(ImmutableList<RamcoOrderStatus> statuses, Pageable pageable);
	
	Page<SalesOrder> findSalesOrderByStatusNot(OrderStatus status, Pageable pageable);

	Long countByRamcoOrderStatusInAndActiveTrue (RamcoOrderStatus statuses);

	@Query(
			value = "select count (id), status from sales_order where order_date > current_date - 1 group by status",
			nativeQuery = true)
	List<Object[]> orderReportForToday ();


}
