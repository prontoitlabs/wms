package com.smart.logistic.repository;

import java.util.Date;
import java.util.List;

import com.smart.logistic.domain.IndentReport;
import com.smart.logistic.repository.generic.GenericRepository;

public interface IIndentReportRepository extends GenericRepository<IndentReport>{

List<IndentReport>	findByDateCreatedBetween(Date startDate,Date endDate);

IndentReport findTopByOrderByFromDateDesc();

List<IndentReport> findByFromDate(Date fromDate);

IndentReport findTopByOrderByTillDateDesc();

}
