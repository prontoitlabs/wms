package com.smart.logistic.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.smart.logistic.domain.TransferOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.repository.generic.GenericRepository;

public interface ITransferOrderPackSlipDispatchDetailsRepository
		extends GenericRepository<TransferOrderPackSlipDispatchDetails> {

	Page<TransferOrderPackSlipDispatchDetails> findAllByTransferOrder_Id(Long transferOrderId, Pageable pageable);

	List<TransferOrderPackSlipDispatchDetails> findAllByTransferOrder_Id(Long transferOrderId);

	List<TransferOrderPackSlipDispatchDetails> findAllByTransferOrderPackslip_IdAndStatus(Long packslipId,
			OrderStatus status);

}
