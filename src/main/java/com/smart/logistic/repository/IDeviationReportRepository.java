package com.smart.logistic.repository;

import com.smart.logistic.domain.DeviationReport;
import com.smart.logistic.repository.generic.GenericRepository;

public interface IDeviationReportRepository extends GenericRepository<DeviationReport>{
	
}
