package com.smart.logistic.repository;

import com.smart.logistic.domain.SalesOrderPackslipItem;
import com.smart.logistic.repository.generic.GenericRepository;

public interface IOrderPackslipItemRepository extends GenericRepository<SalesOrderPackslipItem> {
	
}
