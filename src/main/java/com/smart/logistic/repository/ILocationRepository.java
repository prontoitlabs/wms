package com.smart.logistic.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.smart.logistic.domain.Location;
import com.smart.logistic.repository.generic.GenericRepository;

public interface ILocationRepository extends GenericRepository<Location>{

	Page<Location> findAll(Pageable page);
}
