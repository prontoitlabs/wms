package com.smart.logistic.transformer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.SalesOrder;
import com.smart.logistic.domain.SalesOrderPackslip;
import com.smart.logistic.dto.PageDto;
import com.smart.logistic.dto.SalesOrderDto;
import com.smart.logistic.dto.SalesOrderPackslipDto;
import com.smart.logistic.exception.WmsException;

@Service
public class SalesOrderPackslipTransformer extends GenericTransformer<SalesOrderPackslip, SalesOrderPackslipDto> {

	@Autowired
	private SalesOrderPackslipItemTransformer orderPackslipItemTransformer;
	
	@Autowired
	private SalesOrderTransformer salesOrderTransformer;

	public PageDto<SalesOrderPackslipDto> toPageDto(Page<SalesOrderPackslip> page) {
		PageDto<SalesOrderPackslipDto> pageDto = toDtoPage(page);
		pageDto.setContent(toDto(page.getContent()));
		return pageDto;
	}

	@Override
	public SalesOrderPackslipDto toDto(SalesOrderPackslip orderPackslip) throws WmsException {
		if (orderPackslip == null) {
			return null;
		}
		Set<SalesOrder> salesOrders = orderPackslip.getSalesOrders();
		List<SalesOrder> salesOrderList = new ArrayList<SalesOrder>();
		salesOrderList.addAll(salesOrders);
		SalesOrderPackslipDto orderPackslipDto = SalesOrderPackslipDto.builder().id(orderPackslip.getId())
				.packslipDate(orderPackslip.getPackslipDate())
				.customerName(salesOrders.iterator().next().getCustomer().getName())
				.packslipQuantity(orderPackslip.getPackslipQuantity())
				.verifiedCases(orderPackslip.getVerifiedCases())
				.vehicleNumber(orderPackslip.getVehicleNumber())
				.salesOrdersDto(new HashSet<SalesOrderDto> (salesOrderTransformer.toDto(salesOrderList)))
				.orderStatus(orderPackslip.getOrderStatus()).unitOfMeasure(orderPackslip.getUnitOfMeasure())
				.packslipNumber(orderPackslip.getPackslipNumber()).build();
		if(CollectionUtils.isNotEmpty(orderPackslip.getOrderPackslipItems())) {
			orderPackslipDto
			.setOrderPackSlipItemDto(orderPackslipItemTransformer.toDto(orderPackslip.getOrderPackslipItems()));
		}
		return orderPackslipDto;
	}

	@Override
	public SalesOrderPackslipDto toDtoEntity(SalesOrderPackslip orderPackslip) throws WmsException {
		SalesOrderPackslipDto orderPackslipDto = toDto(orderPackslip);
		if(CollectionUtils.isNotEmpty(orderPackslip.getOrderPackslipItems())) {
			orderPackslipDto
			.setOrderPackSlipItemDto(orderPackslipItemTransformer.toDto(orderPackslip.getOrderPackslipItems()));
		}
		return orderPackslipDto;
	}

}
