package com.smart.logistic.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.Location;
import com.smart.logistic.dto.LocationDto;
import com.smart.logistic.exception.WmsException;


@Service
public class LocationTransformer extends GenericTransformer<Location, LocationDto>  {

	@Override
	public LocationDto toDtoEntity(Location entity) throws WmsException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LocationDto toDto(Location location) throws WmsException {
		if (location == null) {
			return null;
		}
		LocationDto locationDto = new LocationDto();
		BeanUtils.copyProperties(location, locationDto);
		return locationDto;
	}

}
