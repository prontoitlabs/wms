package com.smart.logistic.transformer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.SalesOrder;
import com.smart.logistic.domain.SalesOrderDetails;
import com.smart.logistic.domain.SalesOrderPackslipItem;
import com.smart.logistic.dto.SalesOrderPackslipItemDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.IProductService;

@Service
public class SalesOrderPackslipItemTransformer
		extends GenericTransformer<SalesOrderPackslipItem, SalesOrderPackslipItemDto> {

	@Autowired
	private IProductService productService;

	@Override
	public SalesOrderPackslipItemDto toDtoEntity(SalesOrderPackslipItem orderPackslipItem) {
		return null;
	}

	@Override
	public SalesOrderPackslipItemDto toDto(SalesOrderPackslipItem orderPackslipItem) throws WmsException {
		if (orderPackslipItem == null) {
			return null;
		}
		Product product = productService.findOne(orderPackslipItem.getProduct().getId());
		SalesOrder salesOrder = orderPackslipItem.getSalesOrder();

		Integer initialDemandForItem = calculateInitialDemandForItem(salesOrder, product.getSku());
		
		SalesOrderPackslipItemDto orderPackslipItemDto = SalesOrderPackslipItemDto.builder().id(orderPackslipItem.getId()).sku(product.getSku())
				.productName(product.getName()).availableQuantity(product.getQuantityInHand())
				.verifiedCases(orderPackslipItem.getVerifiedCases())
				.salesOrderNumber(salesOrder.getOrderNumber())
				.packslipQuantity(orderPackslipItem.getPackslipQuantity())
				.unitOfMeasure(orderPackslipItem.getUnitOfMeasure())
				.initialDemand(initialDemandForItem)
				.build();

		return orderPackslipItemDto;
	}

	private Integer calculateInitialDemandForItem(SalesOrder salesOrder, String sku) {
		int initialDemand = 0;
		List<SalesOrderDetails> salesOrderDetails = salesOrder.getSalesOrderDetails();
		for (SalesOrderDetails salesOrderDetail : salesOrderDetails) {
			if(salesOrderDetail.getProduct().getSku().equals(sku)) {
				initialDemand = initialDemand  + (salesOrderDetail.getActualReservedQuantityInCB() != null ? salesOrderDetail.getActualReservedQuantityInCB() : 0);
			}
		}
		return initialDemand;
	}

}
