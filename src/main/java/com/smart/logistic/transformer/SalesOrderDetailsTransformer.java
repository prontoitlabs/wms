package com.smart.logistic.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.SalesOrderDetails;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.dto.SalesOrderDetailsDto;
import com.smart.logistic.service.IProductService;

@Service
public class SalesOrderDetailsTransformer extends GenericTransformer<SalesOrderDetails, SalesOrderDetailsDto> {

	@Autowired
	private ProductTransformer productTransformer;

	@Autowired
	private IProductService productService;

	@Override
	public SalesOrderDetailsDto toDtoEntity(SalesOrderDetails salesOrderDetails) {
		SalesOrderDetailsDto salesOrderDetailsDto = new SalesOrderDetailsDto();
		BeanUtils.copyProperties(salesOrderDetails, salesOrderDetailsDto);
		return salesOrderDetailsDto;
	}

	@Override
	public SalesOrderDetailsDto toDto(SalesOrderDetails salesOrderDetails) {
		SalesOrderDetailsDto salesOrderDetailsDto = new SalesOrderDetailsDto();
		BeanUtils.copyProperties(salesOrderDetails, salesOrderDetailsDto);
		if (salesOrderDetails.getProduct() != null) {
			Product product = productService.findBySkuIgnoreCase(salesOrderDetails.getProduct().getSku());
			salesOrderDetailsDto.setProductDto(productTransformer.toDto(product));
			salesOrderDetailsDto.setAvailableQuantity(product.getQuantityInHand());
			salesOrderDetailsDto.setReservedQuantity(calculateReservedQuantity(salesOrderDetails, product));
			
			if(salesOrderDetails.getSalesOrder().getStatus().equals(OrderStatus.WAITING)) {
				salesOrderDetailsDto.setActualReservedQuantityInCB(salesOrderDetailsDto.getReservedQuantity());
				salesOrderDetailsDto.setActualReservedQuantityInKG(salesOrderDetailsDto.getActualReservedQuantityInCB() * salesOrderDetailsDto.getConversionFactor());
				
				salesOrderDetailsDto.setActualShippedQuantityInCB(salesOrderDetails.getActualShippedQuantityInCB() != null
						? salesOrderDetails.getActualShippedQuantityInCB()
								: 0);
				salesOrderDetailsDto.setActualShippedQuantityInKG(salesOrderDetails.getActualShippedQuantityInKG() != null
						? salesOrderDetails.getActualShippedQuantityInKG()
								: 0D);
				
			}else {
				salesOrderDetailsDto.setActualReservedQuantityInCB(salesOrderDetails.getActualReservedQuantityInCB() != null
						? salesOrderDetails.getActualReservedQuantityInCB()
								: 0);
				salesOrderDetailsDto.setActualReservedQuantityInKG(salesOrderDetails.getActualReservedQuantityInKG() != null
						? salesOrderDetails.getActualReservedQuantityInKG()
								: 0D);
				salesOrderDetailsDto.setActualShippedQuantityInCB(salesOrderDetails.getActualShippedQuantityInCB() != null
						? salesOrderDetails.getActualShippedQuantityInCB()
								: 0);
				salesOrderDetailsDto.setActualShippedQuantityInKG(salesOrderDetails.getActualShippedQuantityInKG() != null
						? salesOrderDetails.getActualShippedQuantityInKG()
								: 0D);
			}
			
		}
		return salesOrderDetailsDto;
	}

	private Integer calculateReservedQuantity(SalesOrderDetails salesOrderDetails, Product product) {
		Integer reservedQuantity = 0;
		Integer pendingQuantityInCB = salesOrderDetails.getTotalOrderQuanityInCB() - (salesOrderDetails.getActualReservedQuantityInCB() + salesOrderDetails.getActualShippedQuantityInCB()) ;
		if (product.getQuantityInHand() > pendingQuantityInCB) {
			reservedQuantity = pendingQuantityInCB;
		}
		if (product.getQuantityInHand() <= pendingQuantityInCB) {
			reservedQuantity = product.getQuantityInHand();
		}
		return reservedQuantity;
	}

}
