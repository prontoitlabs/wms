package com.smart.logistic.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.smart.logistic.dto.DeliveryOrderDto;
import com.smart.logistic.dto.SalesOrderDto;
import com.smart.logistic.exception.WmsException;

@Service
public class DeliveryOrderTransformer extends GenericTransformer<SalesOrderDto, DeliveryOrderDto> {

	@Override
	public DeliveryOrderDto toDtoEntity(SalesOrderDto salesOrderDto) throws WmsException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeliveryOrderDto toDto(SalesOrderDto salesOrderDto) throws WmsException {
		if (salesOrderDto == null) {
			return null;
		}
		DeliveryOrderDto deliveryOrderDto = new DeliveryOrderDto();
		BeanUtils.copyProperties(salesOrderDto, deliveryOrderDto);
		deliveryOrderDto.setCustomerCode(salesOrderDto.getCustomer().getCustomerCode());
		deliveryOrderDto.setPendingQuantityInCB(salesOrderDto.getActualReservedQuantityInCB());
		deliveryOrderDto.setPendingQuantityInKG(salesOrderDto.getActualReservedQuantityInKG());
		deliveryOrderDto.setToLocation(salesOrderDto.getCustomer().getAddressline1() + " "
				+ salesOrderDto.getCustomer().getAddressline2() + " " + salesOrderDto.getCustomer().getAddressline3()
				+ " " + salesOrderDto.getCustomer().getCity() + " " + salesOrderDto.getCustomer().getCountry());
		deliveryOrderDto.setFromLocation("E-WHEELS and LOGISTICS, Reliance Road, Dasna,Ghaziabad,Uttar Pradesh, pin-201302");
		return deliveryOrderDto;
	}
}
