package com.smart.logistic.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.SalesOrderPackSlipDispatchDetails;
import com.smart.logistic.dto.SalesOrderPackSlipDispatchDetailsDto;
import com.smart.logistic.exception.WmsException;

@Service
public class SalesOrderPackslipDispatchDetailsTransformer
		extends GenericTransformer<SalesOrderPackSlipDispatchDetails, SalesOrderPackSlipDispatchDetailsDto> {

	@Autowired
	private SalesOrderPackslipItemTransformer orderPackslipItemTransformer;

	@Override
	public SalesOrderPackSlipDispatchDetailsDto toDtoEntity(SalesOrderPackSlipDispatchDetails entity) throws WmsException {
		return null;
	}

	@Override
	public SalesOrderPackSlipDispatchDetailsDto toDto(SalesOrderPackSlipDispatchDetails packslipDispatchDetails) throws WmsException {
		SalesOrderPackSlipDispatchDetailsDto packslipDispatchDetailsDto = new SalesOrderPackSlipDispatchDetailsDto();
		BeanUtils.copyProperties(packslipDispatchDetails, packslipDispatchDetailsDto);
		packslipDispatchDetailsDto.setOrderPackslipItemDto(
				orderPackslipItemTransformer.toDto(packslipDispatchDetails.getOrderPackslipItem()));
		return packslipDispatchDetailsDto;
	}

}
