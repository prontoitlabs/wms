package com.smart.logistic.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.Product;
import com.smart.logistic.dto.PageDto;
import com.smart.logistic.dto.ProductDto;

@Service
public class ProductTransformer extends GenericTransformer<Product, ProductDto> {

	@Override
	public ProductDto toDto(Product product) {
		if (product == null) {
			return null;
		}
		ProductDto productDto = new ProductDto();
		BeanUtils.copyProperties(product, productDto);
		return productDto;
	}

	public PageDto<ProductDto> toPageDto(Page<Product> page) {
		PageDto<ProductDto> pageDto = toDtoPage(page);
		pageDto.setContent(toDto(page.getContent()));
		return pageDto;
	}

	@Override
	public ProductDto toDtoEntity(Product entity) {
		return null;
	}

}
