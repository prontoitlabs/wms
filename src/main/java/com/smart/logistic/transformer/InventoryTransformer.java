package com.smart.logistic.transformer;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.Inventory;
import com.smart.logistic.dto.InventoryDto;
import com.smart.logistic.dto.PageDto;
import com.smart.logistic.exception.WmsException;

@Service
public class InventoryTransformer extends GenericTransformer<Inventory, InventoryDto> {

	@Override
	public InventoryDto toDtoEntity(Inventory inventory) throws WmsException {
		return null;
	}

	@Override
	public InventoryDto toDto(Inventory inventory) throws WmsException {
		if (inventory == null) {
			return null;
		}
		InventoryDto inventoryDto = InventoryDto.builder().batchNumber(inventory.getBatchNumber())
		.inventoryDate(inventory.getInventoryDate()).orderStatus(inventory.getOrderStatus()).palletBarcode(inventory.getPalletBarcode())
		.quantity(inventory.getQuantity()).scanBarcode(inventory.getScanBarcode())
		.unitOfMeasure(inventory.getUnitOfMeasure()).skuCode(inventory.getProduct().getSku()).productName(inventory.getProduct().getShortDescription())
		.productDescription(inventory.getProduct().getDescription())
		.netWeight(inventory.getNetWeight()).grossWeight(inventory.getGrossWeight())
		.build();
		return inventoryDto;
	}
	
	public PageDto<InventoryDto> toPageDto(Page<Inventory> page) {
		PageDto<InventoryDto> pageDto = toDtoPage(page);
		pageDto.setContent(toDto(page.getContent()));
		return pageDto;
	}
}
