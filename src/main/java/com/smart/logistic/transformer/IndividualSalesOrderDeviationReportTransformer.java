package com.smart.logistic.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.IndividualSalesOrderDeviationReport;
import com.smart.logistic.dto.IndividualSalesOrderDeviationReportDto;
import com.smart.logistic.exception.WmsException;

@Service
public class IndividualSalesOrderDeviationReportTransformer
		extends GenericTransformer<IndividualSalesOrderDeviationReport, IndividualSalesOrderDeviationReportDto> {

	@Autowired
	SalesOrderTransformer salesOrderTransformer;
	
	
	@Override
	public IndividualSalesOrderDeviationReportDto toDtoEntity(IndividualSalesOrderDeviationReport entity)
			throws WmsException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IndividualSalesOrderDeviationReportDto toDto(
			IndividualSalesOrderDeviationReport individualSalesOrderDeviationReport) throws WmsException {
		if (individualSalesOrderDeviationReport == null) {
			return null;
		}

		IndividualSalesOrderDeviationReportDto individualSalesOrderDeviationReportDto = new IndividualSalesOrderDeviationReportDto();
		BeanUtils.copyProperties(individualSalesOrderDeviationReport, individualSalesOrderDeviationReportDto);
		individualSalesOrderDeviationReportDto.setSalesOrder(salesOrderTransformer.toDto(individualSalesOrderDeviationReport.getSalesOrder()));
		
		
		
		return individualSalesOrderDeviationReportDto;
	}

}
