package com.smart.logistic.transformer;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.SalesOrder;
import com.smart.logistic.domain.SalesOrderDetails;
import com.smart.logistic.dto.PageDto;
import com.smart.logistic.dto.SalesOrderDetailsDto;
import com.smart.logistic.dto.SalesOrderDto;

@Service
public class SalesOrderTransformer extends GenericTransformer<SalesOrder, SalesOrderDto> {

	@Autowired
	private SalesOrderDetailsTransformer salesOrderDetailsTransformer;

	@Override
	public SalesOrderDto toDto(SalesOrder salesOrder) {

		SalesOrderDto salesOrderDto = calculateDerivedAttributeForSalesOrder(salesOrder);

		SalesOrderDto dto = SalesOrderDto.builder().id(salesOrder.getId()).orderDate(salesOrder.getOrderDate())
				.customer(salesOrder.getCustomer()).orderNumber(salesOrder.getOrderNumber())
				.grandTotal(salesOrder.getGrandTotal()).taxes(salesOrder.getTaxes())
				.untaxedAmount(salesOrder.getUntaxedAmount()).status(salesOrder.getStatus())
				.totalOrderQuanityInKG(salesOrderDto.getTotalOrderQuanityInKG())
				.totalOrderQuanityInCB(salesOrderDto.getTotalOrderQuanityInCB())
				.shippedQuantityInKG(salesOrderDto.getShippedQuantityInKG())
				.shippedQuantityInCB(salesOrderDto.getShippedQuantityInCB())
				.pendingQuantityInKG(salesOrderDto.getPendingQuantityInKG())
				.pendingQuantityInCB(salesOrderDto.getPendingQuantityInCB())
				.actualReservedQuantityInCB(salesOrderDto.getActualReservedQuantityInCB())
				.actualReservedQuantityInKG(salesOrderDto.getActualReservedQuantityInKG())
				.actualShippedQuantityInCB(salesOrderDto.getActualShippedQuantityInCB())
				.actualShippedQuantityInKG(salesOrderDto.getActualShippedQuantityInKG())
				.orderFulfillmentType(salesOrder.getOrderFulfillmentType())
				.ramcoOrderStatus(salesOrder.getRamcoOrderStatus()).build();
		return dto;
	}

	@Override
	public SalesOrderDto toDtoEntity(SalesOrder salesOrder) {
		SalesOrderDto salesOrderDto = new SalesOrderDto();
		BeanUtils.copyProperties(salesOrder, salesOrderDto);
		List<SalesOrderDetailsDto> salesOrderDetailsDto = salesOrderDetailsTransformer
				.toDto(salesOrder.getSalesOrderDetails());
		salesOrderDto.setSalesOrderDetails(salesOrderDetailsDto);
		calculateTotalQuantity(salesOrderDto, salesOrderDetailsDto);
		return salesOrderDto;
	}

	private void calculateTotalQuantity(SalesOrderDto salesOrderDto, List<SalesOrderDetailsDto> salesOrderDetailsDto) {
		Integer totalPendingQuantity = 0;
		Integer totalReservedQuantity = 0;
		for (SalesOrderDetailsDto salesOrderDetailDto : salesOrderDetailsDto) {
			totalReservedQuantity = totalReservedQuantity + salesOrderDetailDto.getReservedQuantity();
			totalPendingQuantity = totalPendingQuantity + salesOrderDetailDto.getPendingQuantityInCB();
		}
		salesOrderDto.setTotalReservedQuantity(totalReservedQuantity);
		salesOrderDto.setTotalPendingQuantity(totalPendingQuantity);
	}

	public PageDto<SalesOrderDto> toPageDto(Page<SalesOrder> page) {
		PageDto<SalesOrderDto> pageDto = toDtoPage(page);
		pageDto.setContent(toDto(page.getContent()));
		return pageDto;
	}

	private SalesOrderDto calculateDerivedAttributeForSalesOrder(SalesOrder salesOrder) {

		SalesOrderDto dto = new SalesOrderDto();

		Double totalOrderQuanityInKG = 0.0;

		Integer totalOrderQuanityInCB = 0;

		Double shippedQuantityInKG = 0.0;

		Integer shippedQuantityInCB = 0;

		Double pendingQuantityInKG = 0.0;

		Integer pendingQuantityInCB = 0;

		Integer totalActualReservedQuantityInCB = 0;
		
		Double totalActualReservedQuantityInKG = 0d;
		
		Integer totalActualShippedQuantityInCB = 0;
		
		Double totalActualShippedQuantityInKG = 0d;
		

		for (SalesOrderDetails salesOrderDetails : salesOrder.getSalesOrderDetails()) {

			totalOrderQuanityInKG = totalOrderQuanityInKG + salesOrderDetails.getTotalOrderQuanityInKG();
			totalOrderQuanityInCB = totalOrderQuanityInCB + salesOrderDetails.getTotalOrderQuanityInCB();
			shippedQuantityInKG = shippedQuantityInKG + salesOrderDetails.getShippedQuantityInKG();
			shippedQuantityInCB = shippedQuantityInCB + salesOrderDetails.getShippedQuantityInCB();
			pendingQuantityInKG = pendingQuantityInKG + salesOrderDetails.getPendingQuantityInKG();
			pendingQuantityInCB = pendingQuantityInCB + salesOrderDetails.getPendingQuantityInCB();
			totalActualReservedQuantityInCB = totalActualReservedQuantityInCB + (salesOrderDetails.getActualReservedQuantityInCB()!=null?salesOrderDetails.getActualReservedQuantityInCB():0);
			totalActualReservedQuantityInKG = totalActualReservedQuantityInKG + (salesOrderDetails.getActualReservedQuantityInKG()!=null?salesOrderDetails.getActualReservedQuantityInKG():0D);
			totalActualShippedQuantityInCB = totalActualShippedQuantityInCB + (salesOrderDetails.getActualShippedQuantityInCB()!=null?salesOrderDetails.getActualShippedQuantityInCB():0);
			totalActualShippedQuantityInKG = totalActualShippedQuantityInKG + (salesOrderDetails.getActualShippedQuantityInKG()!=null?salesOrderDetails.getActualShippedQuantityInKG():0D);
		}
		dto.setTotalOrderQuanityInKG(totalOrderQuanityInKG);
		dto.setTotalOrderQuanityInCB(totalOrderQuanityInCB);
		dto.setShippedQuantityInKG(shippedQuantityInKG);
		dto.setShippedQuantityInCB(shippedQuantityInCB);
		dto.setPendingQuantityInKG(pendingQuantityInKG);
		dto.setPendingQuantityInCB(pendingQuantityInCB);
		dto.setActualReservedQuantityInCB(totalActualReservedQuantityInCB);
		dto.setActualReservedQuantityInKG(totalActualReservedQuantityInKG);
		dto.setActualShippedQuantityInCB(totalActualShippedQuantityInCB);
		dto.setActualShippedQuantityInKG(totalActualShippedQuantityInKG);
		return dto;
	}
}
