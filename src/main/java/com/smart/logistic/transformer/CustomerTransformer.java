package com.smart.logistic.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.Customer;
import com.smart.logistic.dto.CustomerDto;
import com.smart.logistic.exception.WmsException;

@Service
public class CustomerTransformer extends GenericTransformer<Customer, CustomerDto> {

	@Override
	public CustomerDto toDtoEntity(Customer entity) throws WmsException {
		return null;
	}

	@Override
	public CustomerDto toDto(Customer customer) throws WmsException {
		if (customer == null) {
			return null;
		}
		CustomerDto customerDto = new CustomerDto();
		BeanUtils.copyProperties(customer, customerDto);
		return customerDto;
	}
	
	
	
}
