package com.smart.logistic.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.IndentReport;
import com.smart.logistic.dto.IndentReportDto;
import com.smart.logistic.exception.WmsException;

@Service
public class IndentReportTransformer extends GenericTransformer<IndentReport, IndentReportDto>{

	@Override
	public IndentReportDto toDtoEntity(IndentReport entity) throws WmsException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IndentReportDto toDto(IndentReport indentReport) throws WmsException {
		if (indentReport == null) {
			return null;
		}
		IndentReportDto indentReportDto = new IndentReportDto();
		BeanUtils.copyProperties(indentReport, indentReportDto);
		return indentReportDto;
	}

}
