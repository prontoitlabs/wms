package com.smart.logistic.transformer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;

import com.smart.logistic.dto.PageDto;
import com.smart.logistic.exception.WmsException;

public abstract class GenericTransformer<ENTITY, DTO> {

	public abstract DTO toDtoEntity(ENTITY entity) throws WmsException;

	public abstract DTO toDto(ENTITY entity) throws WmsException;

	public List<DTO> toDto(List<ENTITY> entities) {
		List<DTO> dtos = new ArrayList<>();
		entities.stream().forEach(entity -> {
			try {
				dtos.add(toDto(entity));
			} catch (WmsException e) {
				e.printStackTrace();
			}
		});
		return dtos;
	}

	public PageDto<DTO> toPageDto(Page<ENTITY> page) {
		PageDto<DTO> pageDto = toDtoPage(page);
		pageDto.setContent(toDto(page.getContent()));
		return pageDto;
	}

	public PageDto<DTO> toPageDtoMinimal(Page<DTO> page) {
		PageDto<DTO> pageDto = toDtoPage(page);
		pageDto.setContent(page.getContent());
		return pageDto;
	}

	protected PageDto<DTO> toDtoPage(Page<?> page) {
		PageDto<DTO> pageDto = new PageDto<>();
		pageDto.setCurrentPageSize(page.getNumberOfElements());
		pageDto.setCurrentPage(page.getNumber());
		pageDto.setTotalElements(page.getTotalElements());
		pageDto.setTotalPages(page.getTotalPages());
		return pageDto;
	}

}
