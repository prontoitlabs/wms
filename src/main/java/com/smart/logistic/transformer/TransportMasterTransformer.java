package com.smart.logistic.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.TransportMaster;
import com.smart.logistic.dto.TransportMasterDto;


@Service
public class TransportMasterTransformer extends GenericTransformer<TransportMaster, TransportMasterDto> {

	@Override
	public TransportMasterDto toDto(TransportMaster entity) {
		TransportMasterDto dto = new TransportMasterDto();
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

	@Override
	public TransportMasterDto toDtoEntity(TransportMaster entity) {
		return null;
	}

}
