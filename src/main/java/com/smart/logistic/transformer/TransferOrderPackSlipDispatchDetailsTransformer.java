package com.smart.logistic.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.TransferOrderPackSlipDispatchDetails;
import com.smart.logistic.dto.TransferOrderPackSlipDispatchDetailsDto;
import com.smart.logistic.exception.WmsException;

@Service
public class TransferOrderPackSlipDispatchDetailsTransformer
		extends GenericTransformer<TransferOrderPackSlipDispatchDetails, TransferOrderPackSlipDispatchDetailsDto> {

	@Override
	public TransferOrderPackSlipDispatchDetailsDto toDtoEntity(TransferOrderPackSlipDispatchDetails entity)
			throws WmsException {
		return null;
	}

	@Override
	public TransferOrderPackSlipDispatchDetailsDto toDto(
			TransferOrderPackSlipDispatchDetails transferOrderPackSlipDispatchDetails) throws WmsException {
		TransferOrderPackSlipDispatchDetailsDto transferOrderPackSlipDispatchDetailsDto = new TransferOrderPackSlipDispatchDetailsDto();
		BeanUtils.copyProperties(transferOrderPackSlipDispatchDetails, transferOrderPackSlipDispatchDetailsDto);
		transferOrderPackSlipDispatchDetailsDto.setProductId(transferOrderPackSlipDispatchDetails.getProduct().getId());
		transferOrderPackSlipDispatchDetailsDto.setTransferOrderNumber(
				transferOrderPackSlipDispatchDetails.getTransferOrder().getTrasferOrderNumber());
		transferOrderPackSlipDispatchDetailsDto
				.setTransferOrderId(transferOrderPackSlipDispatchDetails.getTransferOrder().getId());
		transferOrderPackSlipDispatchDetailsDto.setPackslipNumber(transferOrderPackSlipDispatchDetails
				.getTransferOrderPackslip().getPackslipNumber());
		transferOrderPackSlipDispatchDetailsDto.setNetWeight(transferOrderPackSlipDispatchDetails.getNetWeight());
		
		transferOrderPackSlipDispatchDetailsDto.setGrossWeight(transferOrderPackSlipDispatchDetails.getGrossWeight());
		transferOrderPackSlipDispatchDetailsDto.setProductName(transferOrderPackSlipDispatchDetails.getProduct().getDescription());
		transferOrderPackSlipDispatchDetailsDto.setSku(transferOrderPackSlipDispatchDetails.getProduct().getSku());
		transferOrderPackSlipDispatchDetailsDto.setScanDate(transferOrderPackSlipDispatchDetails.getScanDate());
		
		return transferOrderPackSlipDispatchDetailsDto;
	}

}
