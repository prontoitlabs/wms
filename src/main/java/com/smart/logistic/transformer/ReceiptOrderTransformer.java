package com.smart.logistic.transformer;

import org.springframework.stereotype.Service;

import com.smart.logistic.dto.ReceiptTransferOrderDto;
import com.smart.logistic.dto.TransferOrderDto;
import com.smart.logistic.exception.WmsException;

@Service
public class ReceiptOrderTransformer extends GenericTransformer<TransferOrderDto, ReceiptTransferOrderDto> {

	@Override
	public ReceiptTransferOrderDto toDtoEntity(TransferOrderDto transferOrderDto) throws WmsException {
		return null;
	}

	@Override
	public ReceiptTransferOrderDto toDto(TransferOrderDto transferOrderDto) throws WmsException {
		if (transferOrderDto == null) {
			return null;
		}
		ReceiptTransferOrderDto receiptTransferOrderDto = new ReceiptTransferOrderDto();
		receiptTransferOrderDto.setCustomerCode("E-Wheels and Logistics");
		receiptTransferOrderDto.setTransferQuantity(transferOrderDto.getTransferQuantity().intValue());
		receiptTransferOrderDto.setOrderNumber(transferOrderDto.getTransferOrderNumber());
		return receiptTransferOrderDto;
	}
}
