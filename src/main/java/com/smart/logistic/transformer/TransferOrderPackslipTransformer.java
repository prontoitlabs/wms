package com.smart.logistic.transformer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.TransferOrderPackslip;
import com.smart.logistic.dto.PageDto;
import com.smart.logistic.dto.TransferOrderDto;
import com.smart.logistic.dto.TransferOrderPackslipDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.ITransferOrderService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TransferOrderPackslipTransformer extends GenericTransformer<TransferOrderPackslip, TransferOrderPackslipDto> {

	@Autowired
	private TransferOrderPackslipItemTransformer transferOrderPackslipItemTransformer;
	
	@Autowired
	private TransferOrdersTransformer transferOrdersTransformer;
	
	@Autowired
	private ITransferOrderService transferOrderService;
	
	

	public PageDto<TransferOrderPackslipDto> toPageDto(Page<TransferOrderPackslip> page) {
		PageDto<TransferOrderPackslipDto> pageDto = toDtoPage(page);
		pageDto.setContent(toDto(page.getContent()));
		return pageDto;
	}

	@Override
	public TransferOrderPackslipDto toDto(TransferOrderPackslip transferOrderPackslip) throws WmsException {
		if (transferOrderPackslip == null) {
			return null;
		}
		TransferOrderPackslipDto transferOrderPackslipDto = TransferOrderPackslipDto.builder().id(transferOrderPackslip.getId())
				.transferOrderNumber(transferOrderPackslip.getTransferOrder().getTrasferOrderNumber())
				.packslipDate(transferOrderPackslip.getPackslipDate()).transferOrderId(transferOrderPackslip.getTransferOrder().getId())
				.orderStatus(transferOrderPackslip.getOrderStatus())
				.packslipNumber(transferOrderPackslip.getPackslipNumber())
				.packslipQuantity(transferOrderPackslip.getPackslipQuantity()).vehicleNumber(transferOrderPackslip.getVehicleNumber())
				.doneCases(transferOrderPackslip.getDoneCases())
				.build();
		transferOrderPackslipDto.setTransferOrderPackSlipItemDto(transferOrderPackslipItemTransformer.toDto(transferOrderPackslip.getTransferOrderPackslipItem()));
		return transferOrderPackslipDto;
	}

	@Override
	public TransferOrderPackslipDto toDtoEntity(TransferOrderPackslip transferOrderPackslip) throws WmsException {
		
		
		TransferOrderDto  tranferOrderDto =transferOrdersTransformer.toDto(transferOrderPackslip.getTransferOrder());
		
		transferOrderService.findOne(transferOrderPackslip.getTransferOrder().getId());
		
		TransferOrderPackslipDto transferOrderPackslipDto = toDto(transferOrderPackslip);
		transferOrderPackslipDto.setPackslipQuantity(transferOrderPackslip.getPackslipQuantity());
		transferOrderPackslipDto.setDoneCases(transferOrderPackslip.getDoneCases());
		transferOrderPackslipDto.setPendingCases(transferOrderPackslip.getPendingCases());
		transferOrderPackslipDto.setReservedCases(transferOrderPackslip.getReservedCases());
		transferOrderPackslipDto.setTransferOrderDate(tranferOrderDto.getTransferOrderDate());
		transferOrderPackslipDto.setTransferOrderPackSlipItemDto(transferOrderPackslipItemTransformer.toDto(transferOrderPackslip.getTransferOrderPackslipItem()));
		
		log.info("Getting Transfer Order Packslip by id: " + transferOrderPackslipDto);
		return transferOrderPackslipDto;
	}

}
