package com.smart.logistic.transformer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.TransferOrder;
import com.smart.logistic.domain.TransferOrderDetails;
import com.smart.logistic.dto.PageDto;
import com.smart.logistic.dto.TransferOrderDto;
import com.smart.logistic.exception.WmsException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TransferOrdersTransformer extends GenericTransformer<TransferOrder, TransferOrderDto> {

	@Autowired
	private TransferOrderDetailsTransformer transferOrderDetailsTransformer;

	@Override
	public TransferOrderDto toDto(TransferOrder transferOrder) throws WmsException {
		TransferOrderDto dto = TransferOrderDto.builder().id(transferOrder.getId())
				.transferOrderNumber(transferOrder.getTrasferOrderNumber())
				.transferOrderDate(transferOrder.getTransferOrderDate())
				.orderStatus(transferOrder.getStatus())
				.transferQuantity(calculateTransferQuantityForTransferOrder(transferOrder))
				.ramcoOrderNumber(transferOrder.getRamcoOrderNumber()).build();
		return dto;
	}

	@Override
	public TransferOrderDto toDtoEntity(TransferOrder transferOrder) throws WmsException {
		TransferOrderDto transferOrderDto = TransferOrderDto.builder().id(transferOrder.getId())
				.transferOrderNumber(transferOrder.getTrasferOrderNumber())
				.transferOrderDate(transferOrder.getTransferOrderDate())
				.orderStatus(transferOrder.getStatus())
				.transferQuantity(calculateTransferQuantityForTransferOrder(transferOrder))
				.ramcoOrderNumber(transferOrder.getRamcoOrderNumber()).build();
		transferOrderDto.setTransferOrderDetailsDto(
				transferOrderDetailsTransformer.toDto(transferOrder.getTransferOrderDetails()));
		return transferOrderDto;
	}

	public PageDto<TransferOrderDto> toPageDto(Page<TransferOrder> page) {
		PageDto<TransferOrderDto> pageDto = toDtoPage(page);
		pageDto.setContent(toDto(page.getContent()));
		log.info("getting list of paginated transfer orders: " + pageDto);
		return pageDto;
	}

	private Double calculateTransferQuantityForTransferOrder(TransferOrder transferOrder) {
		Double transferQuantity = 0.0;
		for (TransferOrderDetails transferOrderDetails : transferOrder.getTransferOrderDetails()) {
			transferQuantity = transferQuantity + transferOrderDetails.getTransferQuantity();
		}
		return transferQuantity;

	}
}
