package com.smart.logistic.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.TransferOrderDetails;
import com.smart.logistic.dto.TransferOrderDetailsDto;
import com.smart.logistic.service.IProductService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TransferOrderDetailsTransformer extends GenericTransformer<TransferOrderDetails, TransferOrderDetailsDto> {

	@Autowired
	private ProductTransformer productTransformer;

	@Autowired
	private IProductService productService;

	@Override
	public TransferOrderDetailsDto toDtoEntity(TransferOrderDetails transferOrderDetails) {
		TransferOrderDetailsDto transferOrderDetailsDto = new TransferOrderDetailsDto();
		BeanUtils.copyProperties(transferOrderDetails, transferOrderDetailsDto);
		return transferOrderDetailsDto;
	}

	@Override
	public TransferOrderDetailsDto toDto(TransferOrderDetails transferOrderDetails) {
		TransferOrderDetailsDto transferOrderDetailsDto = new TransferOrderDetailsDto();
		BeanUtils.copyProperties(transferOrderDetails, transferOrderDetailsDto);
		if (transferOrderDetails.getProduct() != null) {
			transferOrderDetailsDto.setProductDto(
					productTransformer.toDto(productService.findBySkuIgnoreCase(transferOrderDetails.getProduct().getSku())));
		}
		log.info("getting list of transfer order details: " + transferOrderDetailsDto);
		return transferOrderDetailsDto;

	}

}
