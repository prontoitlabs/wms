package com.smart.logistic.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.TransferOrderPackslipItem;
import com.smart.logistic.dto.TransferOrderPackslipItemDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.service.IProductService;

@Service
public class TransferOrderPackslipItemTransformer extends GenericTransformer<TransferOrderPackslipItem, TransferOrderPackslipItemDto> {
	

	@Autowired
	private IProductService productService;

	@Override
	public TransferOrderPackslipItemDto toDtoEntity(TransferOrderPackslipItem transferOrderPackslipItem) {
		return null;
	}

	@Override
	public TransferOrderPackslipItemDto toDto(TransferOrderPackslipItem transferOrderPackslipItem) throws WmsException {
		if (transferOrderPackslipItem == null) {
			return null;
		}
		TransferOrderPackslipItemDto transferOrderPackslipItemDto = new TransferOrderPackslipItemDto();
		BeanUtils.copyProperties(transferOrderPackslipItem, transferOrderPackslipItemDto);
		Product product = productService.findOne(transferOrderPackslipItem.getProduct().getId());
		transferOrderPackslipItemDto.setSku(product.getSku());
		transferOrderPackslipItemDto.setProduct(product.getName());
		transferOrderPackslipItemDto.setOrderQuantity(transferOrderPackslipItem.getPackslipQuantity());
		transferOrderPackslipItemDto.setDescription(product.getDescription());
		transferOrderPackslipItemDto.setCategory(product.getCategory().toString());
		return transferOrderPackslipItemDto;
	}

}
