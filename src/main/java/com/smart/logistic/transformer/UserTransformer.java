package com.smart.logistic.transformer;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.smart.logistic.domain.User;
import com.smart.logistic.dto.UserDto;

@Component
public class UserTransformer extends GenericTransformer<User, UserDto> {

	@Override
	public UserDto toDto(User entity) {
		UserDto dto = new UserDto();
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

	@Override
	public UserDto toDtoEntity(User entity) {
		return null;
	}

}
