package com.smart.logistic.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.smart.logistic.BaseControllerTest;
import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.TransferOrder;
import com.smart.logistic.domain.TransferOrderDetails;
import com.smart.logistic.domain.TransferOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.TransferOrderPackslip;
import com.smart.logistic.domain.TransferOrderPackslipItem;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;
import com.smart.logistic.dto.TransferOrderPackSlipDispatchDetailsDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.ITransferOrderPackslipRepository;
import com.smart.logistic.repository.ITransferOrderRepository;

public class TransferOrderPackSlipDispatchDetailsServiceTest extends BaseControllerTest {

	@Autowired
	private ITransferOrderRepository iTransferOrderRepository;

	@Autowired
	private ITransferOrderPackslipRepository iTransferOrderPackslipRepository;

	@Autowired
	private IProductService productService;

	@Autowired
	private ITransferOrderPackSlipDispatchDetailsService transferOrderPackSlipDispatchDetailsService;

	@SuppressWarnings("deprecation")
	@Test
	public void shouldBeAbleToConfirmListOfTransferOrderDispatchDetail() throws WmsException {

		// fetching all product
		List<Product> listOfProducts = productService.findAll();

		// createTransferOrder(listOfProducts);

		// creating packslip by providing list of transferorder and list of product
		List<TransferOrderPackslip> listOfTransferOrderPackslip = createTransferOrderPackslip(
				createTransferOrder(listOfProducts), listOfProducts);

		// fetching all the dispatchdetail related to noofpackslip
		List<TransferOrderPackSlipDispatchDetails> listOfTransferOrderPackSlipDispatchDetails = returnListOfAllDispatchDetailRelatedTotransferOrder(
				listOfTransferOrderPackslip);

		// fetching list of dispatchdetail ids
		List<Long> ids = returnListOfIdsOfAllDispatchDetailsOfTransferOrder(listOfTransferOrderPackSlipDispatchDetails);

		// calling confirm service
		List<TransferOrderPackSlipDispatchDetailsDto> listOfTransferOrderPackSlipDispatchDetailsDtoAfterChangingStatus = transferOrderPackSlipDispatchDetailsService
				.confirmListOfPackSlipDispatchDetailAsDone(ids, OrderStatus.DONE);

		// asserting that confirm service doesnot return null
		Assert.isTrue(listOfTransferOrderPackSlipDispatchDetailsDtoAfterChangingStatus != null);

		// asserting the count of dispatch details which been confirmed with provided
		// status
		Assert.isTrue(countStatusInListOfTransferOrderPackSlipDispatchDetails(
				listOfTransferOrderPackSlipDispatchDetailsDtoAfterChangingStatus, OrderStatus.DONE) == ids.size());
	}

	private long countStatusInListOfTransferOrderPackSlipDispatchDetails(
			List<TransferOrderPackSlipDispatchDetailsDto> listOfTransferOrderPackSlipDispatchDetailsDtoAfterChangingStatus,
			OrderStatus status) {

		long noOfStatus = 0;
		for (TransferOrderPackSlipDispatchDetailsDto transferOrderPackSlipDispatchDetailsDto : listOfTransferOrderPackSlipDispatchDetailsDtoAfterChangingStatus) {
			if (status.equals(transferOrderPackSlipDispatchDetailsDto.getStatus())) {
				noOfStatus = noOfStatus + 1;
			}
		}
		return noOfStatus;
	}

	private List<TransferOrderPackSlipDispatchDetails> returnListOfAllDispatchDetailRelatedTotransferOrder(
			List<TransferOrderPackslip> listOfTransferOrderPackslip) {

		List<TransferOrderPackSlipDispatchDetails> transferOrderPackSlipDispatchDetailsList = new ArrayList<TransferOrderPackSlipDispatchDetails>();

		for (TransferOrderPackslip transferOrderPackslip : listOfTransferOrderPackslip) {
			transferOrderPackSlipDispatchDetailsList.addAll(transferOrderPackslip.getTransferOrderPackSlipDispatchDetails());
		}
		return transferOrderPackSlipDispatchDetailsList;
	}

	private List<Long> returnListOfIdsOfAllDispatchDetailsOfTransferOrder(
			List<TransferOrderPackSlipDispatchDetails> ListOfTransferOrderPackSlipDispatchDetails) {

		List<Long> ids = new ArrayList<Long>();
		for (TransferOrderPackSlipDispatchDetails transferOrderPackSlipDispatchDetails : ListOfTransferOrderPackSlipDispatchDetails) {
			ids.add(transferOrderPackSlipDispatchDetails.getId());
		}

		return ids;

	}

	// Create Sample TransferOrder
	private List<TransferOrder> createTransferOrder(List<Product> products) {
		List<TransferOrder> listOfTransferOrder = new ArrayList<TransferOrder>();

		for (int i = 0; i < 2; i++) {
			TransferOrder transferOrder = new TransferOrder();
			transferOrder.setTrasferOrderNumber("TRAORD" + i);
			transferOrder.setTransferOrderDetails(createTransferOrderDetails(transferOrder, products.get(i)));
			transferOrder.setStatus(OrderStatus.WAITING);
			transferOrder.setTransferOrderDate(DateTime.now().toDate());
			transferOrder.setRamcoOrderNumber("SOTC/" + UUID.randomUUID() + "/17-18");
			listOfTransferOrder.add(transferOrder);
		}
		return iTransferOrderRepository.save(listOfTransferOrder);
	}

	// creating Sample Transfer Order Detail
	private List<TransferOrderDetails> createTransferOrderDetails(TransferOrder transferOrder, Product product) {
		List<TransferOrderDetails> listOfTransferOrderDetails = new ArrayList<TransferOrderDetails>();

		TransferOrderDetails transferOrderDetails = new TransferOrderDetails();
		transferOrderDetails.setTransferOrder(transferOrder);
		transferOrderDetails.setProduct(product);
		transferOrderDetails.setTransferQuantity(10.0);
		transferOrderDetails.setUnitsOfMeasure(UnitsOfMeasurement.CARTONS);
		listOfTransferOrderDetails.add(transferOrderDetails);
		return listOfTransferOrderDetails;
	}

	// create TransferOrder Packslip
	private List<TransferOrderPackslip> createTransferOrderPackslip(List<TransferOrder> transferOrders,
			List<Product> products) {
		List<TransferOrderPackslip> transferOrderPackslips = new ArrayList<TransferOrderPackslip>();

		TransferOrderPackslip transferOrderPackslip = new TransferOrderPackslip();
		transferOrderPackslip.setPackslipNumber("PKSCTest/009026/1718");
		transferOrderPackslip.setPackslipDate(DateTime.now().toDate());
		transferOrderPackslip.setTransferOrder(transferOrders.get(0));
		transferOrderPackslip.setPendingCases(10);
		transferOrderPackslip.setReservedCases(10);
		transferOrderPackslip.setDoneCases(0);
		transferOrderPackslip.setPackslipQuantity(10.0);
		transferOrderPackslip.setUnitOfMeasure(UnitsOfMeasurement.CARTONS);
		transferOrderPackslip.setOrderStatus(OrderStatus.WAITING);
		transferOrderPackslip.setTransferOrderPackslipItem(
				createTransferOrderPackslipItem(transferOrderPackslip, products.get(0), transferOrders.get(0)));
		transferOrderPackslip.setTransferOrderPackSlipDispatchDetails(createTransferOrderPackSlipDispatchDetails(transferOrderPackslip, products.get(0),transferOrders.get(0) ));
		transferOrderPackslips.add(transferOrderPackslip);

		return iTransferOrderPackslipRepository.save(transferOrderPackslips);
	}

	// create Transfer Order Pack slip Item
	private List<TransferOrderPackslipItem> createTransferOrderPackslipItem(TransferOrderPackslip transferOrderPackslip,
			Product product, TransferOrder transferOrder) {
		List<TransferOrderPackslipItem> transferOrderPackslipItems = new ArrayList<TransferOrderPackslipItem>();
		TransferOrderPackslipItem transferOrderPackslipItem = new TransferOrderPackslipItem();
		transferOrderPackslipItem.setTransferOrderPackslip(transferOrderPackslip);
		transferOrderPackslipItem.setPendingCases(10);
		transferOrderPackslipItem.setReservedCases(10);
		transferOrderPackslipItem.setDoneCases(0);
		transferOrderPackslipItem.setPackslipQuantity(10.0);
		transferOrderPackslipItem.setProduct(product);
		transferOrderPackslipItems.add(transferOrderPackslipItem);
		return transferOrderPackslipItems;
	}

	// create TransferOrder PackSlipDispatch Details
	private List<TransferOrderPackSlipDispatchDetails> createTransferOrderPackSlipDispatchDetails(
			TransferOrderPackslip transferOrderPackslip, Product product, TransferOrder transferOrder) {
		List<TransferOrderPackSlipDispatchDetails> listOfTransferOrderPackSlipDispatchDetails = new ArrayList<TransferOrderPackSlipDispatchDetails>();
		for (int i = 0; i < 10; i++) {
			TransferOrderPackSlipDispatchDetails transferOrderPackSlipDispatchDetails = new TransferOrderPackSlipDispatchDetails();
			transferOrderPackSlipDispatchDetails.setTransferOrder(transferOrder);
			transferOrderPackSlipDispatchDetails.setTransferOrderPackslip(transferOrderPackslip);
			transferOrderPackSlipDispatchDetails.setProduct(product);
			transferOrderPackSlipDispatchDetails.setScanBarcode("sc354789" + i + UUID.randomUUID());
			transferOrderPackSlipDispatchDetails.setBatchNumber("BATCH5678");
			transferOrderPackSlipDispatchDetails.setPalletBarcode("plt3566");
			transferOrderPackSlipDispatchDetails.setStatus(OrderStatus.RESERVED);
			listOfTransferOrderPackSlipDispatchDetails.add(transferOrderPackSlipDispatchDetails);
		}
		return listOfTransferOrderPackSlipDispatchDetails;
	}
}
