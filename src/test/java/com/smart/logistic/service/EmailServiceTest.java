package com.smart.logistic.service;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.smart.logistic.BaseControllerTest;
import com.smart.logistic.dto.EmailDto;
import com.smart.logistic.exception.WmsException;

public class EmailServiceTest extends BaseControllerTest {
	
	@Autowired
	private EmailService emailService;

	@Test
	@Ignore
	public void shouldBeAbleToSendEmail() throws WmsException {
		EmailDto emailDto = EmailDto.builder().to("kumod.sharma@prontoitlabs.com").from("kumod.kumar.sharma@gmail.com")
				.subject("WMS EMAIL").build();
		String response = emailService.send(emailDto);
		Assert.notNull(response, "Email sent Successfully");
	}
}
