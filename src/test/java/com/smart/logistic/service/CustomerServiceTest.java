package com.smart.logistic.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.util.Assert;

import com.smart.logistic.BaseControllerTest;
import com.smart.logistic.domain.Customer;
import com.smart.logistic.exception.WmsException;

public class CustomerServiceTest extends BaseControllerTest{
	
	@Autowired
	private ICustomerService customerService;
	

	@Test
	public void shouldBeAbleToFetchAllPaginatedCustomers() throws WmsException {
		Page<Customer> customers = customerService.findingAllCustomer(0,10);
		Assert.notEmpty(customers.getContent(), "customers can not empty");
	}
}
