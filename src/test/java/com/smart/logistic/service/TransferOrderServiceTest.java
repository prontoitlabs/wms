package com.smart.logistic.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.smart.logistic.BaseControllerTest;
import com.smart.logistic.domain.TransferOrder;
import com.smart.logistic.domain.TransferOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.TransferOrderPackslip;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.ITransferOrderPackslipRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TransferOrderServiceTest extends BaseControllerTest {

	@Autowired
	private ITransferOrderPackslipRepository transferOrderPackslipRepository;

	@Autowired
	private ITransferOrderService transferOrderService;

	@Autowired
	private ITransferOrderPackSlipDispatchDetailsService transferOrderPackSlipDispatchDetailsService;

	@Test
	@Transactional
	public void shouldBeAbleToGetTransferOrderDispatcahDetailByTransferOrderId() throws WmsException {
		List<TransferOrder> transferOrders = transferOrderService.findAll();
		Assert.notEmpty(transferOrders, "transferOrders can not be empty");
		TransferOrder transferOrder = transferOrders.get(0);
		List<TransferOrderPackslip> transferOrderPackslips = transferOrderPackslipRepository
				.findByTransferOrder_Id(new Long(transferOrder.getId()));
		Assert.notEmpty(transferOrderPackslips, "transferOrderPackslips can not be empty");
		int totalDispatchDetails = 0;
		for (TransferOrderPackslip transferOrderPackslip : transferOrderPackslips) {
			totalDispatchDetails = totalDispatchDetails
					+ transferOrderPackslip.getTransferOrderPackSlipDispatchDetails().size();
		}
		log.info("totalDispatchDetails " + totalDispatchDetails);
		Page<TransferOrderPackSlipDispatchDetails> transferOrderPackSlipDispatchDetails = transferOrderPackSlipDispatchDetailsService
				.getTransferOrderPackslipDispatchDetailsById(transferOrder.getId(), 0, 10);
		Assert.isTrue(transferOrderPackSlipDispatchDetails.getTotalElements() == totalDispatchDetails,
				"getting correct dispatch details of transfer order");
	}

	@Test
	@Transactional
	public void shouldBeAbleToImportTransferOrdersFromStockInReport() throws IOException, WmsException {
		File stockInFile = new File("src/test/resources/STNC-000409-1718-StockIN.csv");
		transferOrderService.importTransferOrdersFromCsv(stockInFile);
	}

}
