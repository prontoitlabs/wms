package com.smart.logistic.service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.google.gson.Gson;
import com.smart.logistic.BaseControllerTest;
import com.smart.logistic.domain.Customer;
import com.smart.logistic.domain.IndividualSalesOrderDeviationReport;
import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.SalesOrder;
import com.smart.logistic.domain.SalesOrderDetails;
import com.smart.logistic.domain.SalesOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.SalesOrderPackslip;
import com.smart.logistic.domain.SalesOrderPackslipItem;
import com.smart.logistic.domain.TransferOrder;
import com.smart.logistic.domain.enums.OrderFulfillmentType;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.domain.enums.UnitsOfMeasurement;
import com.smart.logistic.dto.CustomerDto;
import com.smart.logistic.dto.DeliveryDetailsRequestDto;
import com.smart.logistic.dto.SalesOrderDetailsDto;
import com.smart.logistic.dto.SalesOrderDto;
import com.smart.logistic.dto.SalesOrderPackslipDto;
import com.smart.logistic.dto.VehicleAllocationDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.transformer.SalesOrderPackslipItemTransformer;
import com.smart.logistic.transformer.SalesOrderTransformer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SalesOrderPackslipServiceTest extends BaseControllerTest {

	@Autowired
	private ISalesOrderService salesOrderService;

	@Autowired
	private IOrderPackslipService orderPackslipService;

	@Autowired
	private ICustomerService customerService;
	
	@Autowired
	private IProductService productService;

	@Autowired
	private SalesOrderTransformer salesOrderTransformer;
	
	@Autowired
	private ITransferOrderService transferOrderService;

	@Autowired
	private SalesOrderPackslipItemTransformer salesOrderPackslipItemTransformer;

	@Autowired
	private IPackslipDispatchDetailsService packslipDispatchDetailsService;
	
	@Autowired
	private IReportService reportService;

	@Test
	@Transactional
	public void shouldBeAbleToCreatePackslipsForSalesOrder() throws WmsException {
		Product product = productService.findBySkuIgnoreCase("H002522");
		product.setQuantityInHand(product.getQuantityInHand() + 100);
		product = productService.save(product);
		List<SalesOrder> salesOrders = salesOrderService.importAllOpenSalesOrdersFromRamco();
		Assert.notEmpty(salesOrders, "We should have some Sales Orders Imported from Ramco");
		SalesOrder salesOrderFromRamco = salesOrderService.findSalesOrderByNumber("SORH/000091/1819");
		Assert.notNull(salesOrderFromRamco, "We should have sales order by this order number");
		Assert.notEmpty(salesOrderFromRamco.getSalesOrderDetails(),
				"We should have one Sales Orders details for order ORH/000091/1819 in order to be DISPATCHED");
		SalesOrderDto salesOrderDto = salesOrderTransformer.toDtoEntity(salesOrderFromRamco);
		SalesOrderDetailsDto selectedSalesOrderDetailsDto = null;
		for (SalesOrderDetailsDto salesOrderDetailsDto : salesOrderDto.getSalesOrderDetails()) {
			if (salesOrderDetailsDto.getProductDto().getSku().equals("H002522")) {
				selectedSalesOrderDetailsDto = salesOrderDetailsDto;
				break;
			}
		}
		selectedSalesOrderDetailsDto.setActualReservedQuantityInCB(80);
		salesOrderDto = salesOrderService.addSalesOrderForDispatch(Arrays.asList(selectedSalesOrderDetailsDto),
				salesOrderFromRamco.getId(), OrderFulfillmentType.CARTON);
		

		List<SalesOrder> deliveryOrders = salesOrderService.getListOfDeliveryOrders();
		Assert.notEmpty(deliveryOrders, "Delivery Orders cannot be Empty after Added To Dispatch");
		Assert.isTrue(deliveryOrders.size() == 1, "There should be 1 Delivery Order created");

		SalesOrder deliveryOrder = deliveryOrders.get(0);
		DeliveryDetailsRequestDto deliveryDetailsRequestDto = DeliveryDetailsRequestDto.builder()
				.ids(Arrays.asList(deliveryOrder.getId())).build();
		String response = salesOrderService.sendDeliveryOrdersForAllocation(deliveryDetailsRequestDto);
		Assert.hasText(response, "Delivery Orders dispatch API call should return a Queue Id");
		Assert.isTrue(deliveryOrder.getStatus().equals(OrderStatus.QUEUED_FOR_VEHICLE_ALLOCATION),
				"The Delivery Order should be QUEUED for VEHICLE ALLOCATION with the Transporter");

		SalesOrderPackslipDto salesOrderPackslipDto = orderPackslipService.createPackSlip();
		salesOrderPackslipDto.setPackslipDate(DateTime.now().toDate());
		salesOrderPackslipDto.setVehicleNumber("DLRT4758");
		salesOrderPackslipDto.setPackslipQuantity(10);

		Map<String, CustomerDto> customersForPendingDeliveryOrders = customerService
				.getAllCustomersForPendingDeliveryOrders();
		Assert.notEmpty(customersForPendingDeliveryOrders, "Customers must exist for Pending Delivery Orders");

		Entry<String, CustomerDto> entry = customersForPendingDeliveryOrders.entrySet().iterator().next();
		String customerCode = entry.getKey();
		salesOrderPackslipDto.setCustomerName(entry.getValue().getName());
		List<SalesOrder> deliveryOrdersByCustomerCode = salesOrderService
				.getAllDeliveryOrdersByCustomerCode(customerCode);
		salesOrderPackslipDto.setSalesOrdersDto(
				new HashSet<SalesOrderDto>(salesOrderTransformer.toDto(deliveryOrdersByCustomerCode)));
		List<Long> deliveryDetailsRequest = new ArrayList<Long>();
		for (SalesOrder salesOrder : deliveryOrdersByCustomerCode) {
			deliveryDetailsRequest.add(salesOrder.getId());
		}

		List<SalesOrderPackslipItem> salesOrderPackslipItems = orderPackslipService
				.getAllPendingItemsFromSalesOrders(deliveryDetailsRequest);
		salesOrderPackslipDto.setOrderPackSlipItemDto(salesOrderPackslipItemTransformer.toDto(salesOrderPackslipItems));
		Gson gson = new Gson();
		String jsonInString = gson.toJson(salesOrderPackslipDto);
		log.info(jsonInString);
		salesOrderPackslipDto = orderPackslipService.savePackSlip(salesOrderPackslipDto);

		Assert.notNull(salesOrderPackslipDto, "Sales Order Packslip should have been created after Save API call");
		Set<SalesOrderPackslip> packslips = deliveryOrdersByCustomerCode.get(0).getPackslips();
		Assert.notEmpty(packslips, "Delivery Order should have a Packslip assigned against it");
		salesOrderPackslipDto.setVehicleNumber("DLRT2323");
		SalesOrderPackslipDto editedPackSlip = orderPackslipService.editPackSlip(salesOrderPackslipDto);
		Assert.isTrue(editedPackSlip.getVehicleNumber().equals("DLRT2323"),
				"Edited PAckslip should reflect all the changes Done");
	}

	@Test
	@Transactional
	public void shouldBeAbleToCreatePackslipUponVehicleAllocation() throws WmsException {
		Product product = productService.findBySkuIgnoreCase("H002522");
		product.setQuantityInHand(product.getQuantityInHand() + 100);
		product = productService.save(product);
		List<SalesOrder> salesOrders = salesOrderService.importAllOpenSalesOrdersFromRamco();
		Assert.notEmpty(salesOrders, "We should have some Sales Orders Imported from Ramco");
		SalesOrder salesOrderFromRamco = salesOrderService.findSalesOrderByNumber("SORH/000091/1819");
		Assert.notNull(salesOrderFromRamco, "We should have sales order by this order number");
		Assert.notEmpty(salesOrderFromRamco.getSalesOrderDetails(),
				"We should have one Sales Orders details for order ORH/000091/1819 in order to be DISPATCHED");
		SalesOrderDto salesOrderDto = salesOrderTransformer.toDtoEntity(salesOrderFromRamco);
		SalesOrderDetailsDto selectedSalesOrderDetailsDto = null;
		for (SalesOrderDetailsDto salesOrderDetailsDto : salesOrderDto.getSalesOrderDetails()) {
			if (salesOrderDetailsDto.getProductDto().getSku().equals("H002522")) {
				selectedSalesOrderDetailsDto = salesOrderDetailsDto;
				break;
			}
		}
		selectedSalesOrderDetailsDto.setActualReservedQuantityInCB(80);
		salesOrderDto = salesOrderService.addSalesOrderForDispatch(Arrays.asList(selectedSalesOrderDetailsDto),
				salesOrderFromRamco.getId(), OrderFulfillmentType.CARTON);
		

		List<SalesOrder> deliveryOrders = salesOrderService.getListOfDeliveryOrders();
		Assert.notEmpty(deliveryOrders, "Delivery Orders cannot be Empty after Added To Dispatch");
		Assert.isTrue(deliveryOrders.size() == 1, "There should be 1 Delivery Order created");

		SalesOrder deliveryOrder = deliveryOrders.get(0);
		DeliveryDetailsRequestDto deliveryDetailsRequestDto = DeliveryDetailsRequestDto.builder()
				.ids(Arrays.asList(deliveryOrder.getId())).build();
		String response = salesOrderService.sendDeliveryOrdersForAllocation(deliveryDetailsRequestDto);
		Assert.hasText(response, "Delivery Orders dispatch API call should return a Queue Id");
		Assert.isTrue(deliveryOrder.getStatus().equals(OrderStatus.QUEUED_FOR_VEHICLE_ALLOCATION),
				"The Delivery Order should be QUEUED for VEHICLE ALLOCATION with the Transporter");

		List<String> deliveryOrdersForAllocation = Arrays.asList(deliveryOrder.getOrderNumber());
		VehicleAllocationDto vehicleAllocationDto = VehicleAllocationDto.builder().totalPackslipQuantityInCartons(80)
				.vehicleNumber("DL5T789934").salesOrderNumbers(deliveryOrdersForAllocation).build();
		Gson gson = new Gson();
		String jsonInString = gson.toJson(vehicleAllocationDto);
		log.info(jsonInString);
		List<SalesOrderPackslip> salesOrderPackslipsCreated = orderPackslipService
				.allocateVehicle(Arrays.asList(vehicleAllocationDto));
		Assert.notEmpty(salesOrderPackslipsCreated,
				"Sales Order Packslip should have been created after Truck Allocation Call");
		Assert.notEmpty(salesOrderPackslipsCreated.get(0).getOrderPackslipItems(),
				"Sales Order Packslip should have Packslip Items after Truck Allocation Call");
	}
	
	@Test
	@Transactional
	public void shouldBeAbleToImportHSTDataForCartonizedSalesOrderPackslip() throws WmsException, IOException {
		File stockInFile = new File("src/test/resources/STNC-000409-1718-StockIN.csv");
		TransferOrder transferOrder = transferOrderService.importTransferOrdersFromCsv(stockInFile);
		Assert.notNull(transferOrder, "Transfer Order should be created after Stock In File Import");
		SalesOrder salesOrder = createSalesOrder();
		salesOrderService.addSalesOrderToDispatch(salesOrder.getId(), OrderFulfillmentType.CARTON);

		List<SalesOrder> deliveryOrders = salesOrderService.getListOfDeliveryOrders();
		Assert.notEmpty(deliveryOrders, "Delivery Orders cannot be Empty after Added To Dispatch");
		Assert.isTrue(deliveryOrders.size() == 1, "There should be 1 Delivery Order created");

		SalesOrder deliveryOrder = deliveryOrders.get(0);
		DeliveryDetailsRequestDto deliveryDetailsRequestDto = DeliveryDetailsRequestDto.builder()
				.ids(Arrays.asList(deliveryOrder.getId())).build();
		
		salesOrderService.sendDeliveryOrdersForAllocation(deliveryDetailsRequestDto);
		
		Assert.isTrue(deliveryOrder.getStatus().equals(OrderStatus.QUEUED_FOR_VEHICLE_ALLOCATION),
				"The Delivery Order should be QUEUED for VEHICLE ALLOCATION with the Transporter");

		List<String> deliveryOrdersForAllocation = Arrays.asList(deliveryOrder.getOrderNumber());
		VehicleAllocationDto vehicleAllocationDto = VehicleAllocationDto.builder().totalPackslipQuantityInCartons(30)
				.vehicleNumber("DL5T789934").salesOrderNumbers(deliveryOrdersForAllocation).build();
		List<SalesOrderPackslip> salesOrderPackslipsCreated = orderPackslipService
				.allocateVehicle(Arrays.asList(vehicleAllocationDto));
		Assert.notEmpty(salesOrderPackslipsCreated,
				"Sales Order Packslip should have been created after Truck Allocation Call");
		Assert.isTrue(salesOrderPackslipsCreated.size() == 1, "There should be exactly one Packslip Created for this Delivery Order");
		
		SalesOrderPackslip salesOrderPackslip = salesOrderPackslipsCreated.get(0);
		Assert.notEmpty(salesOrderPackslip.getOrderPackslipItems(),"Sales Order Packslip should have Packslip Items after Truck Allocation Call");
		Assert.isTrue(salesOrderPackslip.getOrderPackslipItems().size() == 3, "There should exactly be 3 Packslip Items created for this Cartonized Delivery Order");
		salesOrderPackslip.setPackslipNumber("SORC/010847/1718");
		salesOrderPackslip = orderPackslipService.save(salesOrderPackslip);
		
		salesOrderPackslip = orderPackslipService.importHSTDataForPackslip(salesOrderPackslip.getId());
		Assert.notEmpty(salesOrderPackslip.getOrderPackslipItems().get(0).getPackslipDispatchDetails(),
				"Sales Order Packslip Items should have individual Dispatch Details assigned from HST");
		
		List<SalesOrderPackSlipDispatchDetails> packslipDispatchDetails = packslipDispatchDetailsService.findAllPackslipDispatchDetailsByPackslipId(salesOrderPackslip.getId());
		Assert.notEmpty(packslipDispatchDetails, "Import HST Details should return All Packslip Dispatch Details for this Packslip Id");
		Assert.isTrue(packslipDispatchDetails.size() == 30, "There should be exactly 30 Cartons Dispatched from the system");
		salesOrder = salesOrderService.findOne(salesOrder.getId());
		
		reportService.saveDeviationReportForIndividualSalesOrder();
		List<IndividualSalesOrderDeviationReport> deviationReportBySalesOrder = reportService.getDeviationReportForIndividualSalesOrder();
		Assert.notEmpty(deviationReportBySalesOrder, "Deviation Report cannot be Empty as there is a single Partially Dispatched Order");
	}
	
	
	@Test
	@Transactional
	public void shouldBeAbleToProcessPalletizedSalesOrderPackslip() throws WmsException, IOException {
		File stockInFile = new File("src/test/resources/STNC-000409-1718-StockIN.csv");
		TransferOrder transferOrder = transferOrderService.importTransferOrdersFromCsv(stockInFile);
		
		Assert.notNull(transferOrder, "Transfer Order should be created after Stock In File Import");
		SalesOrder salesOrder = createSalesOrder();
		salesOrderService.addSalesOrderToDispatch(salesOrder.getId(), OrderFulfillmentType.PALLET);

		List<SalesOrder> deliveryOrders = salesOrderService.getListOfDeliveryOrders();
		Assert.notEmpty(deliveryOrders, "Delivery Orders cannot be Empty after Added To Dispatch");
		Assert.isTrue(deliveryOrders.size() == 1, "There should be 1 Delivery Order created");

		SalesOrder deliveryOrder = deliveryOrders.get(0);
		DeliveryDetailsRequestDto deliveryDetailsRequestDto = DeliveryDetailsRequestDto.builder()
				.ids(Arrays.asList(deliveryOrder.getId())).build();
		
		salesOrderService.sendDeliveryOrdersForAllocation(deliveryDetailsRequestDto);
		
		Assert.isTrue(deliveryOrder.getStatus().equals(OrderStatus.QUEUED_FOR_VEHICLE_ALLOCATION),
				"The Delivery Order should be QUEUED for VEHICLE ALLOCATION with the Transporter");

		List<String> deliveryOrdersForAllocation = Arrays.asList(deliveryOrder.getOrderNumber());
		VehicleAllocationDto vehicleAllocationDto = VehicleAllocationDto.builder().totalPackslipQuantityInCartons(30)
				.vehicleNumber("DL5T789934").salesOrderNumbers(deliveryOrdersForAllocation).build();
		List<SalesOrderPackslip> salesOrderPackslipsCreated = orderPackslipService
				.allocateVehicle(Arrays.asList(vehicleAllocationDto));
		Assert.notEmpty(salesOrderPackslipsCreated,
				"Sales Order Packslip should have been created after Truck Allocation Call");
		Assert.isTrue(salesOrderPackslipsCreated.size() == 1, "There should be exactly one Packslip Created for this Delivery Order");
		
		SalesOrderPackslip salesOrderPackslip = salesOrderPackslipsCreated.get(0);
		Assert.notEmpty(salesOrderPackslip.getOrderPackslipItems(),"Sales Order Packslip should have Packslip Items after Truck Allocation Call");
		Assert.isTrue(salesOrderPackslip.getOrderPackslipItems().size() == 1, "There should exactly be 3 Packslip Items created for this Cartonized Delivery Order");
		salesOrderPackslip.setPackslipNumber("SORC/010847/1718");
		salesOrderPackslip = orderPackslipService.save(salesOrderPackslip);
		
		salesOrderPackslip = orderPackslipService.allocateInventoryForOrderPackslip(salesOrderPackslip.getId());
		
		Assert.notNull(salesOrderPackslip, "Sales Order Packslip should be returned upon allocation call for Palletized Sales Order");
		Assert.notEmpty(salesOrderPackslip.getOrderPackslipItems().get(0).getPackslipDispatchDetails(),
				"Sales Order Packslip Items should have individual Dispatch Details reserved");
		
		salesOrder = salesOrderService.findOne(salesOrder.getId());
	}

	private SalesOrder createSalesOrder() {
		List<Customer> customers = customerService.findAll();
		
		SalesOrder salesOrder = SalesOrder.builder().customer(customers.get(0)).orderDate(DateTime.now().toDate())
				.orderNumber("SORH/000005/1718").status(OrderStatus.WAITING).untaxedAmount(new BigDecimal("1200.00"))
				.taxes(new BigDecimal("144.00")).grandTotal(new BigDecimal("1344.00"))
				.build();
		salesOrder.setSalesOrderDetails(createSalesOrderDetail(salesOrder));
		return salesOrderService.save(salesOrder);
	}
	
	private List<SalesOrderDetails> createSalesOrderDetail(SalesOrder salesOrder) {
		List<SalesOrderDetails> listOfSalesOrderDetails = new ArrayList<SalesOrderDetails>();
		List<Product> products = createProductList();
		
		for (Product product : products) {
			SalesOrderDetails testSalesOrderDetails = new SalesOrderDetails();
			testSalesOrderDetails.setSalesOrder(salesOrder);
			testSalesOrderDetails.setProduct(product);
			testSalesOrderDetails.setUnitsOfMeasure(UnitsOfMeasurement.KG);
			testSalesOrderDetails.setUnitPrice(new BigDecimal("300"));
			testSalesOrderDetails.setSubTotal(new BigDecimal("1200.00"));
			testSalesOrderDetails.setTaxes(new BigDecimal("12.00"));
			testSalesOrderDetails.setTotalOrderQuanityInKG(2520D);
			testSalesOrderDetails.setTotalOrderQuanityInCB(250);
			testSalesOrderDetails.setShippedQuantityInKG(1713.6D);
			testSalesOrderDetails.setShippedQuantityInCB(170);
			testSalesOrderDetails.setPendingQuantityInKG(806.4D);
			testSalesOrderDetails.setPendingQuantityInCB(80);
			testSalesOrderDetails.setConversionFactor(10.08D);
			listOfSalesOrderDetails.add(testSalesOrderDetails);
		}
		return listOfSalesOrderDetails;
	}

	
	private List<Product> createProductList() {
		List<Product> products = new ArrayList<Product>();
		List<String> productSkuList = Arrays.asList("H002793", "H005984", "H006112");
		for (String sku : productSkuList) {
			products.add(productService.findBySkuIgnoreCase(sku));
		}
		return products;
	}
}
