package com.smart.logistic.service;

import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import javax.transaction.Transactional;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.smart.logistic.domain.enums.RamcoOrderStatus;
import com.smart.logistic.dto.ImportExternalSalesOrderDto;
import com.smart.logistic.dto.ImportExternalSalesOrderJsonDto;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.smart.logistic.BaseControllerTest;
import com.smart.logistic.domain.Product;
import com.smart.logistic.domain.SalesOrder;
import com.smart.logistic.domain.enums.OrderFulfillmentType;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.dto.SalesOrderDetailsDto;
import com.smart.logistic.dto.SalesOrderDto;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.transformer.SalesOrderTransformer;

public class SalesOrderServiceTest extends BaseControllerTest {

	@Autowired
	private ISalesOrderService salesOrderService;

	@Autowired
	private SalesOrderTransformer salesOrderTransformer;

	@Autowired
	private IProductService productService;

	@Test
	public void shouldBeAbleToImportAllSalesOrdersFromRamco() throws WmsException {
		List<SalesOrder> salesOrders = salesOrderService.importAllOpenSalesOrdersFromRamco();
		Assert.notEmpty(salesOrders, "We should have some Sales Orders Imported from Ramco");
	}

	



	@Test
	@Transactional
	public void shouldBeAbleToDispatchSalesOrder() throws WmsException {
		Product product = productService.findBySkuIgnoreCase("H002522");
		product.setQuantityInHand(product.getQuantityInHand() + 100);
		product = productService.save(product);
		List<SalesOrder> salesOrders = salesOrderService.importAllOpenSalesOrdersFromRamco();
		Assert.notEmpty(salesOrders, "We should have some Sales Orders Imported from Ramco");
		SalesOrder salesOrderFromRamco = salesOrderService.findSalesOrderByNumber("SORH/000091/1819");
		Assert.notNull(salesOrderFromRamco, "We should have sales order by this order number");
		Assert.notEmpty(salesOrderFromRamco.getSalesOrderDetails(),
				"We should have one Sales Orders details for order ORH/000091/1819 in order to be DISPATCHED");
		SalesOrderDto salesOrderDto = salesOrderTransformer.toDtoEntity(salesOrderFromRamco);
		SalesOrderDetailsDto selectedSalesOrderDetailsDto = null;
		for (SalesOrderDetailsDto salesOrderDetailsDto : salesOrderDto.getSalesOrderDetails()) {
			if (salesOrderDetailsDto.getProductDto().getSku().equals("H002522")) {
				selectedSalesOrderDetailsDto = salesOrderDetailsDto;
				break;
			}
		}
		selectedSalesOrderDetailsDto.setActualReservedQuantityInCB(80);
		salesOrderDto = salesOrderService.addSalesOrderForDispatch(Arrays.asList(selectedSalesOrderDetailsDto),
				salesOrderFromRamco.getId(), OrderFulfillmentType.CARTON);
		Assert.notNull(salesOrderDto, "Response should not be null");
		Assert.isTrue(salesOrderDto.getStatus().equals(OrderStatus.ADDED_TO_DISPATCH),
				"Sales Order status should be ADDED_TO_DISPATCH after successfull Dispatched");
	}

	class Counter {
		int rem_qty = 0;
		int ship_qty = 0;
	}
}
