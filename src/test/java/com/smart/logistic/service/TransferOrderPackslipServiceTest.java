package com.smart.logistic.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.smart.logistic.BaseControllerTest;
import com.smart.logistic.domain.Inventory;
import com.smart.logistic.domain.TransferOrder;
import com.smart.logistic.domain.TransferOrderPackSlipDispatchDetails;
import com.smart.logistic.domain.TransferOrderPackslip;
import com.smart.logistic.domain.enums.OrderStatus;
import com.smart.logistic.exception.WmsException;
import com.smart.logistic.repository.ITransferOrderPackslipRepository;

public class TransferOrderPackslipServiceTest extends BaseControllerTest {

	@Autowired
	private ITransferOrderPackslipService transferOrderPackslipService;

	@Autowired
	private ITransferOrderPackslipRepository transferOrderPackslipRepository;

	@Autowired
	private ITransferOrderService transferOrderService;

	@Autowired
	private ITransferOrderPackSlipDispatchDetailsService transferOrderPackSlipDispatchDetailsService;

	@Autowired
	private IInventoryService inventoryService;

	@Test(expected = WmsException.class)
	public void shouldNotBeAbleToValidateTransferPackslipForAllReservedCases() throws WmsException {

		List<TransferOrderPackslip> transferOrderPackslips = transferOrderPackslipService.findAll();
		Assert.notEmpty(transferOrderPackslips, "transferOrderPackslips can not be empty");

		TransferOrderPackslip transferOrderPackslip = transferOrderPackslips.get(0);
		transferOrderPackslipService.validatePackslip(transferOrderPackslip.getId());

	}

	@Test
	@Transactional
	public void shouldBeAbleToValidateTransferPackslipForAllDoneCases() throws WmsException {

		List<TransferOrderPackslip> transferOrderPackslips = transferOrderPackslipService.findAll();
		Assert.notEmpty(transferOrderPackslips, "transferOrderPackslips can not be empty");

		TransferOrderPackslip transferOrderPackslip = transferOrderPackslips.get(0);
		processTransferOrderDispatchDetails(transferOrderPackslip);

		transferOrderPackslip = transferOrderPackslipService.validatePackslip(transferOrderPackslip.getId());
		Assert.isTrue(transferOrderPackslip.getOrderStatus().equals(OrderStatus.DONE),
				"Transfer Order Packslip status should be DONE after Validation");
		Assert.isTrue(transferOrderPackslip.getDoneCases() == 10,
				"Transfer Order Packslip Cartons should be all processed");
		List<Inventory> inventoryStock = inventoryService.findAll();
		Assert.isTrue(inventoryStock.size() == 10, "Inventory Stock should be updated for all the Cartons loaded");
	}

	private void processTransferOrderDispatchDetails(TransferOrderPackslip transferOrderPackslip) throws WmsException {
		List<Long> transferOrderPackSlipDispatchDetailsIds = new ArrayList<Long>();
		for (TransferOrderPackSlipDispatchDetails transferOrderPackSlipDispatchDetails : transferOrderPackslip
				.getTransferOrderPackSlipDispatchDetails()) {
			transferOrderPackSlipDispatchDetailsIds.add(transferOrderPackSlipDispatchDetails.getId());
		}

		transferOrderPackSlipDispatchDetailsService
				.confirmListOfPackSlipDispatchDetailAsDone(transferOrderPackSlipDispatchDetailsIds, OrderStatus.DONE);
	}

	private void partiallyProcessTransferOrderDispatchDetails(TransferOrderPackslip transferOrderPackslip)
			throws WmsException {
		List<Long> transferOrderPackSlipDispatchDetailsIds = new ArrayList<Long>();
		for (TransferOrderPackSlipDispatchDetails transferOrderPackSlipDispatchDetails : transferOrderPackslip
				.getTransferOrderPackSlipDispatchDetails()) {
			transferOrderPackSlipDispatchDetailsIds.add(transferOrderPackSlipDispatchDetails.getId());
			break;
		}
		transferOrderPackSlipDispatchDetailsService
				.confirmListOfPackSlipDispatchDetailAsDone(transferOrderPackSlipDispatchDetailsIds, OrderStatus.DONE);
	}

	@Test(expected = WmsException.class)
	@Transactional
	public void shouldNotBeAbleToValidateTransferPackslipForPartialDoneCases() throws WmsException {

		List<TransferOrderPackslip> transferOrderPackslips = transferOrderPackslipService.findAll();
		Assert.notEmpty(transferOrderPackslips, "transferOrderPackslips can not be empty");

		TransferOrderPackslip transferOrderPackslip = transferOrderPackslips.get(0);
		partiallyProcessTransferOrderDispatchDetails(transferOrderPackslip);

		transferOrderPackslipService.validatePackslip(transferOrderPackslip.getId());
		List<Inventory> inventoryStock = inventoryService.findAll();
		Assert.isTrue(inventoryStock.size() == 0,
				"Inventory Stock should not be updated for when only patially Cartons loaded");
	}

	@Test
	@Transactional
	public void shouldBeAbleToValidateTransferOrderPackslipForAllDoneCases() throws WmsException {

		List<TransferOrder> transferOrders = transferOrderService.findAll();
		Assert.notEmpty(transferOrders, "transferOrders can not be empty");
		TransferOrder transferOrder = transferOrders.get(0);
		List<TransferOrderPackslip> transferOrderPackslips = transferOrderPackslipRepository
				.findByTransferOrder_Id(new Long(transferOrder.getId()));
		Assert.notEmpty(transferOrderPackslips, "transferOrderPackslips can not be empty");
		for (TransferOrderPackslip transferOrderPackslip : transferOrderPackslips) {
			processTransferOrderDispatchDetails(transferOrderPackslip);
		}
		for (int i = 0; i < transferOrderPackslips.size(); i++) {
			List<TransferOrderPackslip> transferOrderPackslip = transferOrderPackslipService
					.validateTransferOrderPackSlips(transferOrder.getId());
			Assert.isTrue(transferOrderPackslip.get(i).getOrderStatus().equals(OrderStatus.DONE),
					"Transfer Order Packslip status should be DONE after Validation");
			Assert.isTrue(transferOrderPackslip.get(i).getDoneCases() == 10,
					"Transfer Order Packslip Cartons should be all processed");
			List<Inventory> inventoryStock = inventoryService.findAll();
			Assert.isTrue(inventoryStock.size() == 10, "Inventory Stock should be updated for all the Cartons loaded");
		}
	}

}
