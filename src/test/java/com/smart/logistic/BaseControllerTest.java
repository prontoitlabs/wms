package com.smart.logistic;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;
import com.smart.logistic.repository.ICustomerRepository;
import com.smart.logistic.repository.IDeviationReportRepository;
import com.smart.logistic.repository.IIndividualSalesOrderDeviationReportRepository;
import com.smart.logistic.repository.IOrderPackslipItemRepository;
import com.smart.logistic.repository.IOrderPackslipRepository;
import com.smart.logistic.repository.IPackslipDispatchDetailsRepository;
import com.smart.logistic.repository.IProductRepository;
import com.smart.logistic.repository.ISalesOrderDetailsRepository;
import com.smart.logistic.repository.ISalesOrderRepository;
import com.smart.logistic.repository.ITransferOrderPackslipRepository;
import com.smart.logistic.repository.ITransferOrderRepository;
import com.smart.logistic.service.ICustomerService;
import com.smart.logistic.service.IDataBuilderService;
import com.smart.logistic.service.IProductService;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = WmsApplication.class, webEnvironment = WebEnvironment.DEFINED_PORT)
public class BaseControllerTest {

	@Value("${local.server.port}")
	private int port;
	
	@Autowired
	private IDataBuilderService databuilderService;
	
	@Autowired
	private IProductService productService;
	
	@Autowired
	private ICustomerService customerService;

	@Autowired
	private IProductRepository productRepository;
	
	@Autowired
	private ICustomerRepository customerRepository;
	
	@Autowired
	private ISalesOrderRepository salesOrderRepository;
	
	@Autowired
	private ISalesOrderDetailsRepository salesOrderDetailsRepository;
	
	@Autowired
	private ITransferOrderRepository transferOrderRepository;
	
	@Autowired
	private ITransferOrderPackslipRepository transferOrderPackslipRepository;
	
	@Autowired
	private IOrderPackslipRepository orderPackslipRepository;
	
	@Autowired
	private IOrderPackslipItemRepository orderPackslipItemRepository;
	
	@Autowired
	private IPackslipDispatchDetailsRepository packslipDispatchDetailsRepository;
	
	@Autowired
	private IIndividualSalesOrderDeviationReportRepository individualSalesOrderDeviationReportRepository;
	
	@Autowired
	private IDeviationReportRepository deviationReportRepository;
	
	@Before
	public void setup() throws Exception {
		RestAssured.port = port;
		RestAssured.defaultParser = Parser.JSON;
//		productService.importItemMaster();
		productService.importAllItemsFromRamco();
//		customerService.importCustomerMaster();
		customerService.importAllCustomersFromRamco();
		databuilderService.createDummyData();
	}
	
	@After
	public void tearDown() {
		deviationReportRepository.deleteAllInBatch();
		individualSalesOrderDeviationReportRepository.deleteAllInBatch();
		
		packslipDispatchDetailsRepository.deleteAllInBatch();
		orderPackslipItemRepository.deleteAllInBatch();
		orderPackslipRepository.deleteAllInBatch();
		salesOrderDetailsRepository.deleteAllInBatch();
		salesOrderRepository.deleteAllInBatch();
		transferOrderPackslipRepository.deleteAll();
		transferOrderRepository.deleteAll();
		productRepository.deleteAll();
		customerRepository.deleteAll();
	}

	@Test
	public void contextLoads() {
	}

}
