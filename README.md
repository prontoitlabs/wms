# Wms Api

> Wms Backend Environment Setup.

### Requirements ###

* [Spring Tool Suite™](https://spring.io/tools/sts/all "Download STS").

*[my sql](version 5.5.58)

*[java](version 1.8)

*[maven](version 3.0.5)

### Before running application database should be created in mysql named WMS  ###

*Command for creating database

		1.create database WMS;

 
### How to run project ###

*execute following command one by one 


	1.mvn clean install //this will execute all the testcases
	2.mvn spring-boot:run //this will run web server

### Database configuration ### 
*path of properites file where we have added configuration for DB.

	-> /home/kumod/Documents/project/wms/src/main/resources

### Deployment process
git pull
mvn clean install
sudo service tomcat restart

insert into wms_user values(1,TRUE,'1999-04-26 13:00:00', '1999-04-26 13:00:00','nagendra@smart-logistics.in',NULL,'Nagendra',NULL, '098f6bcd4621d373cade4e832627b4f6', NULL,'ADMIN');
